#!/usr/bin/env fish
#

if test (count $argv) -lt 1
	echo "usage: date-diff <Target date> [Source date]"
	echo "If no source date is provided, the current date is used."
	exit 1
end

set Target (date --date $argv[1] +%s)
if test (count $argv) -ge 2
	set Source (date --date $argv[2] +%s)
else
	set Source (date +%s)
end

echo From: (date --date @"$Source")
echo To: (date --date @"$Target")
echo (math "($Target - $Source) / (60 * 60 * 24)") days
