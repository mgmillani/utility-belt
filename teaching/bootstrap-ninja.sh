#!/usr/bin/sh

mkdir -p "ohne Lösungen"
mkdir -p "mit Lösungen"

echo "include .ninja.rules" > build.ninja

Name='freiwillige$ Hausaufgabe'

for Fl in ha*.tex
do
	[[ -e $Fl ]] || break
	Num=$(echo $Fl | sed 's:ha\(.*\)\.tex:\1:')
	echo 'build ohne$ Lösungen/'$Name'$ '$Num'.pdf:                      no-solutions' $Fl
	echo 'build mit$ Lösungen/'$Name'$ '$Num'$ -$ Lösungsvorschläge.pdf: solutions' $Fl
done >> build.ninja

Name='Tutoriumsblatt'
for Fl in tut*.tex
do
	[[ -e $Fl ]] || break
	Num=$(echo $Fl | sed 's:tut\(.*\)\.tex:\1:')
	echo 'build ohne$ Lösungen/'$Name'$ '$Num'.pdf:                      no-solutions' $Fl
	echo 'build mit$ Lösungen/'$Name'$ '$Num'$ -$ Lösungsvorschläge.pdf: solutions' $Fl
done >> build.ninja

Name='schriftliche Teilleistung'
while read Fl
do
	[[ -e $Fl ]] || break
	Num=$(echo $Fl | sed 's:TL\(.*\)\.tex:\1:')
	echo 'build ohne$ Lösungen/'$Name'$ '$Num'.pdf:                      no-solutions' $Fl
	echo 'build mit$ Lösungen/'$Name'$ '$Num'$ -$ Lösungsvorschläge.pdf: solutions' $Fl
	echo 'build Schemata/'$Name'$ '$Num'$ -$ Schemata.pdf:               schemes' $Fl
done <<< `ls | grep '^TL.*\\.tex'` >> build.ninja

ninja
