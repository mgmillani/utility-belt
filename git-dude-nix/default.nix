{ lib, stdenv, fetchFromGitHub, bash, makeWrapper, git, libnotify }:

stdenv.mkDerivation rec {
    pname = "git-dude";
    version = "2018.01.24";

    src = fetchFromGitHub {
      owner = "sickill";
      repo = "git-dude";
      rev    = "2722400416bfc807d54a2d64a89bec18ad50c1e0";
      sha256 = "p2ZdnNm3Dd1dlUGZaNdgKgHa3rbNMOupMecTuLs3TkI=";
      fetchSubmodules = true;
    };

    nativeBuildInputs = [ makeWrapper git ];
    buildInputs = [
      bash
    ];

    preInstall = ''echo install: > Makefile'';

    installPhase = ''
      mkdir -p $out/lib/git-dude
      install -Dm755 git-dude "$out/lib/git-dude/git-dude"
      mkdir -p $out/bin
      makeWrapper $out/lib/git-dude/git-dude $out/bin/git-dude \
        --prefix PATH : ${lib.makeBinPath [git libnotify]} \
    '';
    }
