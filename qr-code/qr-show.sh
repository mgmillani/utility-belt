#!/usr/bin/env fish
#

set Info $argv
set Dir /tmp/$USER/qrencode
mkdir -p $Dir
set File (mktemp --tmpdir=$Dir XXXXXX.png)
echo -n $Info | qrencode --output $File
nomacs $File
rm $File
