{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Concurrent
import Control.Concurrent.MVar
import Control.Exception
import Data.Char
import qualified Data.Map as M
import Data.Either
import Data.List
import Data.Maybe
import Data.Time.Calendar
import Data.Time.Clock
import Data.Time.LocalTime
import System.Directory
import System.Environment
import System.FilePath
import System.IO
import System.Process
import UI.HSCurses.Curses hiding (isLeft, isRight)
import UI.HSCurses.CursesHelper
   ( black, blue, cyan, green, magenta, red, white, yellow )
import Text.Read (readMaybe)
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.State

import ExamScan.UI
import ExamScan.Recognition
import ExamScan.Exam

data Command = Interactive | Batch | Invalid String deriving (Eq)

main :: IO ()
main = do
  args <- getArgs
  let targetDir = case args of
                    _:d:_ -> d
                    _ -> "scan-data"
      command   = case args of
                    [] -> Interactive
                    "interactive":_ -> Interactive
                    "batch":_ -> Batch
                    _ -> Invalid $
                           "Invalid args\n" ++
                           show args

      scannerName = "fujitsu:ScanSnap iX1500:66066"
  createDirectoryIfMissing True (targetDir </> "scans-new")
  createDirectoryIfMissing True (targetDir </> "qr")
  createDirectoryIfMissing True (targetDir </> "scans-archive")
  eStudents <- parseStudents "losnummer-mtknr.csv"
  eExpectedPages <- parseExpectedPages "pages.csv"
  case (eStudents, eExpectedPages) of
    (Right students, Right expectedPages) -> do
      case command of
        Invalid err -> print err
        Batch -> do
          global <- loadExisting targetDir expectedPages students True
          batch global
        Interactive -> do
          global <- loadExisting targetDir expectedPages students False
          interactive scannerName global
    _ -> do
         when (isLeft eStudents) $ print eStudents
         when (isLeft eExpectedPages) $ print eExpectedPages

batch global = do
  forkIO $ let loop = do
                      f <- takeMVar $ gFail global
                      when f $
                        loop
    in loop
  forkIO $ let loop = do
                      f <- takeMVar $ gUpdate global
                      when (isJust f) $
                        loop
    in loop
  evalStateT recognizeExams global
  liftIO $ putMVar (gFail global) False
  liftIO $ putMVar (gUpdate global) Nothing
  status <- takeMVar $ gStatus global
  let finished = M.filter (\exam -> exFinished exam) (stExams status)
      unfinished = M.filter (\exam -> not $ exFinished exam) (stExams status)
  putStrLn "Finished:" 
  mapM_ (\(eID, exam) -> putStrLn $ "  " ++ eID ++ " -- " ++ exStudentID exam) $ M.assocs finished
  putStrLn "\nIncomplete:" 
  mapM_ (\(eID, exam) -> putStrLn $ "  " ++ eID ++ " -- " ++ exStudentID exam) $ M.assocs unfinished
  putStrLn "\nFailed:" 
  print (stFailed status)

interactive scannerName global = do
  forkIO $ evalStateT recognizeExams global >> return ()
  evalStateT (setupUI scannerName) global
  return ()

--batch targetDir scannerName expectedPages students = do
--  global <- loadExisting targetDir expectedPages students
--  nWorkers <- fmap (2*) getNumCapabilities
--  mFail <- newEmptyMVar
--  workerDone <- replicateM nWorkers $ newEmptyMVar
--  hError <- openFile "error" WriteMode
--  forM workerDone $ \mDone -> forkFinally (worker targetDir expectedPages students (gScan global) (gStatus global) (gUpdate global) mFail)
--    (\e -> do
--              putMVar mDone ()
--              if isLeft e then do
--                  hPutStrLn hError "worker error"
--                  hPutStrLn hError $ show (e :: Either SomeException ())
--              else return ())
--  forkIO $ do
--    mapM_ takeMVar workerDone
--    putMVar (gUpdate global) Nothing
--  forkFinally (let loop = takeMVar mFail >> loop in loop)
--              (\e -> do
--              if isLeft e then do
--                  hPutStrLn hError "fail error"
--                  hPutStrLn hError $ show (e :: Either SomeException ())
--              else putStrLn "fail done")
--  finishWorkerDone <- replicateM nWorkers $ newEmptyMVar
--  forM finishWorkerDone $ \mDone -> forkFinally (finishWorker targetDir (gStatus global) (gUpdate global))
--    (\e -> do
--           putMVar mDone ()
--           if isLeft e then do
--                hPutStrLn hError "finish worker error"
--                hPutStrLn hError $ show (e :: Either SomeException ())
--           else return ()
--           )
--  mapM_ takeMVar finishWorkerDone
--  status <- takeMVar $ gStatus global
--  putStrLn "Complete exams:"
--  forM (M.assocs $ stExams status) $ \(eID, exam) ->
--    when (exFinished exam) $ putStrLn $ eID ++ " -- " ++ exStudentID exam
--  putStrLn "Incomplete exams:"
--  forM (M.assocs $ stExams status) $ \(eID, exam) ->
--    when (not $ exFinished exam) $ do
--      putStrLn $ eID ++ " -- " ++ exStudentID exam
--      putStrLn $ "    missing: " ++ show (compactRange $ exPagesMissing exam)
--  hClose hError
--  return ()

--interactive targetDir scannerName expectedPages students = do
--  global <- loadExisting targetDir expectedPages students
--  window <- initScr
--  keypad window True
--  cBreak True
--  echo False
--  (h,w) <- scrSize
--  startColor
--  initPair (Pair 1) white black
--  initPair (Pair 2) black green
--  mTable <- newMVar $ ExamTable{etLines = M.empty, etFinished = M.empty, etUnfinished = M.empty}
--  mWindow <- newMVar window
--  mQuit <- newEmptyMVar
--  let mStatus = gStatus global
--      mUpdate = gUpdate global
--      mScan   = gScan global
--  --mStatus <- newMVar $ Status{stProcessing = 0, stFinished = 0, stFailed = 0, stExams = M.empty, stScanReady = True}
--  --mUpdate <- newEmptyMVar
--  --mScan <- newEmptyMVar
--  mFail <- newEmptyMVar
--  setupDisplay mWindow mUpdate mStatus mTable
--  forkIO $ let loop = do
--                         takeMVar mFail
--                         increaseFailure mWindow mStatus
--                         loop
--           in loop
--  setupScanning scannerName targetDir mWindow mQuit mScan mStatus mTable
--  nWorkers <- fmap (2*) getNumCapabilities
--  hError <- openFile "error" WriteMode
--  replicateM nWorkers $ forkFinally (worker targetDir expectedPages students mScan mStatus mUpdate mFail)
--    (\e -> if isLeft e then do
--                hPutStrLn hError "worker error"
--                hPutStrLn hError $ show (e :: Either SomeException ())
--           else return ())
--  takeMVar mQuit
--  hClose hError
--  endWin

--consumeWhile :: (Char -> Bool) -> State String String
--consumeWhile f = do
--  str <- get
--  let (x,r) = span f str
--  put r
--  return x
