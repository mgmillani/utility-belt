module ExamScan.UI where

import UI.HSCurses.Curses hiding (isLeft, isRight)
import UI.HSCurses.CursesHelper
   ( black, blue, cyan, green, magenta, red, white, yellow )

import ExamScan.Recognition
import ExamScan.Exam
import qualified Data.Map as M
import qualified Data.Set as S
import System.Process
import System.Directory
import System.Environment
import System.FilePath
import System.IO
import Control.Exception
import Control.Concurrent
import Control.Concurrent.MVar
import Data.List
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.State
import Data.Time.Calendar
import Data.Time.Clock
import Data.Time.LocalTime

data ExamTable a = ExamTable
  { etLines      :: M.Map Int    String
  , etFinished   :: M.Map String (Entry a)
  , etUnfinished :: M.Map String (Entry a)
  }

data Universe = Universe
  { uWindow :: MVar Window
  , uTableWindow :: MVar Window
  , uTableWindowSize :: MVar (Int, Int)
  , uTableTopVisibleRow :: MVar Int
  , uTable  :: MVar (ExamTable Exam)
  , uGlobal :: Global
  , uScannerName :: String
  , uQuit :: MVar ()
  }

setupUI scannerName = do
  global <- get
  liftIO $ do  
    writeFile "force-finish.log" ""
    window <- initScr
    keypad window True
    cBreak True
    echo False
    (h,w) <- scrSize
    startColor
    initPair (Pair 1) white black
    initPair (Pair 2) black green
    mWindow <- newMVar window
    mTable <- newMVar $ ExamTable{etLines = M.empty, etFinished = M.empty, etUnfinished = M.empty}
    mQuit <- newEmptyMVar
    tablePad <- newPad (2 * h) w
    mTablePad <- newMVar tablePad
    mTablePadSize <- newMVar (2 * h , w)
    mTableTopVisibleRow <- newMVar 1
    let universe = Universe{ uWindow = mWindow
                           , uTable = mTable
                           , uTableWindow = mTablePad
                           , uTableWindowSize = mTablePadSize
                           , uTableTopVisibleRow = mTableTopVisibleRow
                           , uGlobal = global
                           , uScannerName = scannerName
                           , uQuit = mQuit
                           }
    liftIO $ forkIO $ let loop = do
                                   f <- takeMVar $ gFail $ uGlobal universe
                                   evalStateT increaseFailure universe
                                   when f
                                     loop
             in loop
    evalStateT setupDisplay universe
    evalStateT setupScanning universe
    takeMVar mQuit
    endWin

setupDisplay = do
  universe <- get
  (h,s) <- liftIO $ scrSize
  writeHeader 
  updateStatus return
  -- readMVar mStatus >>= writeFooter window
  liftIO $ do
    forkIO $ evalStateT (redrawLoop h s) universe >> return ()
    forkIO $ evalStateT updateLoop universe >> return ()
  where
    updateLoop = do
      universe <- get
      let mTable = uTable universe
      (h,w) <- liftIO $ scrSize
      -- let maxLines = h - 5
      mUpd <- liftIO $ takeMVar $ gUpdate $ uGlobal universe
      case mUpd of
        Nothing -> return ()
        Just (eID, exam) -> do
          table0 <- liftIO $ readMVar $ uTable universe
          when (not $ M.member eID (etFinished table0) ) $ do
            table <- liftIO $ takeMVar $ uTable universe
            case M.lookup eID (etUnfinished table) of
              Just entry -> do
                let entry' = entry{enData = exam}
                table'' <- updateTablePositions table eID entry'
                liftIO $ putMVar mTable table''
                topVisible <- liftIO $ readMVar $ uTableTopVisibleRow universe
                let Just entry'' = (M.lookup eID $ etFinished   table'') `mplus`
                                   (M.lookup eID $ etUnfinished table'')
                when (enLine entry'' > (topVisible + h - 5) && 
                     (Just True == fmap (`M.member` (etFinished table'')) 
                                        (M.lookup (topVisible - 1) (etLines table''))))
                     $ do                 
                  liftIO $ modifyMVar_ (uTableTopVisibleRow universe) $ \r -> return (r + 1)

                if null $ exPagesMissing exam then
                  redrawTable
                else
                  writeEntry eID entry''
              Nothing -> do
                let entryLine = (M.size (etFinished table) + M.size (etUnfinished table) + 1)
                    entry = Entry{ enData = exam
                                 , enLine = entryLine
                                 }
                    table2 = table{ etLines = M.insert entryLine eID $ etLines table
                                  , etUnfinished = M.insert eID entry $ etUnfinished table
                                  }
                table3 <- updateTablePositions table2 eID entry
                liftIO $ putMVar mTable table3
                topVisible <- liftIO $ readMVar $ uTableTopVisibleRow universe
                when (entryLine > (topVisible + h - 5) && 
                     (Just True == fmap (`M.member` (etFinished table3)) 
                                        (M.lookup topVisible (etLines table3))))
                     $ do                 
                       liftIO $ modifyMVar_ (uTableTopVisibleRow universe) $ \r -> return (r + 1)
                if null $ exPagesMissing exam then
                  redrawTable
                else
                  writeEntry eID entry
      updateLoop

updateTablePositions table' eID entry = do
    let exam = enData entry
    if S.null $ exPagesMissing exam then do
      increaseFinished
      let freeLine = enLine entry
          lastFinishedLine = (M.size $ etFinished table') + 1
          shiftedEntries = M.filter (\e -> enLine e < freeLine) $ etUnfinished table'
      return $ table'{ etFinished = M.insert eID entry{enLine = lastFinishedLine} $ etFinished table'
                     , etUnfinished = M.delete eID $ 
                        foldr (\(eID', entry') m -> 
                                  M.alter (\_ -> Just entry'{enLine = enLine entry' + 1})
                                    eID'
                                    m
                              )
                              (etUnfinished table') $
                              M.assocs shiftedEntries
                     , etLines = M.alter (\_ -> Just eID) lastFinishedLine $
                        foldr (\(eID', entry') m -> M.alter (\_ -> Just eID') (enLine entry' + 1) m)
                              (etLines table') $ 
                              M.assocs shiftedEntries
                     }
    else 
      return table'{etUnfinished = M.insert eID entry $ etUnfinished table'}

redrawLoop h w = do
  universe <- get
  (h1, w1) <- liftIO $ scrSize
  when (h1 /= h || w1 /= w) $ do
    writeHeader
    updateStatus (return)
    refreshWindow
  liftIO $ threadDelay (100 * (10^6))
  redrawLoop h1 w1

refreshWindow = do
  withWindow $ \window ->
    liftIO $ wRefresh window

getScanDir = do
  universe <- get
  return $ (gWorkingDir $ uGlobal universe) </> "scans-new"

withWindow f = do
  universe <- get
  liftIO $ withMVar (uWindow universe) $ f

clearCursor :: StateT Universe IO ()
clearCursor = do
  withWindow $ \window -> do
    (h, _) <- scrSize
    forM_ [0..h - 1] $ \r -> do
      attrSet attr0 (Pair 1)
      wMove window r 0
      wAddStr window " "

scanPages filePattern = do
  universe <- get
  targetDir <- getScanDir
  let scannerName = uScannerName universe
  previousFiles <- liftIO $ fmap S.fromList $ listDirectory targetDir
  liftIO $ do
    (_,Just sout, Just serr,ph) <- createProcess (proc "scanimage"  
                    [ "-d", scannerName
                    , "--source=ADF Duplex"
                    , "--mode=Color"
                    , "--resolution=300"
                    , "--batch=" ++ targetDir </> filePattern
                    , "--format=jpeg"
                    , "--overscan=On"
                    , "--page-width=220mm"
                    , "--page-height=300mm"
                    ]
                  )
                  {std_out = CreatePipe, std_err = CreatePipe}
    forkIO $ do
      outStr <- hGetContents sout
      seq (length outStr) $ return ()
    forkIO $ do
      errStr <- hGetContents serr
      seq (length errStr) $ return ()
    waitForProcess ph
    newFiles <- fmap S.fromList $ listDirectory targetDir
    return $ map (targetDir </>) (S.toList $ S.difference newFiles previousFiles)


writeHeader = do
  universe <- get
  withWindow $ \window -> do
    (h,w) <- scrSize
    wMove window 0 0
    wAddStr window "  los     mtknr       finished             missing"
    wRefresh window

writeFooter status = do
  universe <- get
  withWindow $ \window -> do
    (h,w) <- scrSize
    wMove window (h - 1) 0
    wAddStr window "<q> Quit "
    if (stScanReady status) then do
      wAddStr window "<s> Scan        "
    else do
      wAddStr window "    Scanning... "
    wAddStr window "<f> Force finish   "
    wAddStr window ("Finished: " ++ take 5 ((show $ stFinished status) ++ "   "))
    wAddStr window ("In progress: " ++ take 5 ((show $ stProcessing status) ++ "   "))
    wAddStr window ("Failed: " ++ (show $ stFailed status))
    wRefresh window

increaseFailure :: StateT Universe IO ()
increaseFailure =
  updateStatus (\status -> do
    return $ status{stFailed = stFailed status + 1}
    )

increaseFinished :: StateT Universe IO ()
increaseFinished = do
  updateStatus (\status -> do
    return $ status{stFinished = stFinished status + 1}
    )

updateStatus f = do
  universe <- get
  liftIO $ modifyMVar_ 
    (gStatus $ uGlobal universe) 
    (\status ->
      do
      status' <- evalStateT (f status) universe
      evalStateT (writeFooter status') universe
      return status'
    )

updateTable f = do
  universe <- get
  liftIO $ modifyMVar_ (uTable universe) (\table ->
    evalStateT (f table) universe
    )

setupScanning = do
  universe <- get
  moveCursor 1 1
  scanDir <- getScanDir
  liftIO $ createDirectoryIfMissing True scanDir
  status <- liftIO $ readMVar $ gStatus $ uGlobal universe
  scanReady
  liftIO $ forkFinally (do
    evalStateT (keyboardLoop 1) universe
    )
    (\e -> do
      withFile "scanning.log" WriteMode $ \h -> do
        hPutStrLn h $ "scanning finshed"
        hPutStrLn h $ show (e :: Either SomeException ())
    )
  liftIO $ forkIO $ do
    targetDir <- evalStateT getScanDir universe
    currentFiles <- liftIO $ fmap S.fromList $ listDirectory targetDir
    evalStateT (checkNewFilesLoop currentFiles) universe
    
  where
    checkNewFilesLoop previous = do
      universe <- get
      targetDir <- getScanDir
      currentFiles <- liftIO $ fmap S.fromList $ listDirectory targetDir
      let newFiles = S.filter (\fl -> takeExtension fl == ".jpg") $ currentFiles `S.difference` previous
      liftIO $ forM (S.toList newFiles) $ \fl -> putMVar (gScan $ uGlobal universe) (Just $ targetDir </> fl)
      liftIO $ threadDelay (10^6)
      checkNewFilesLoop currentFiles
    keyboardLoop cursor = do
      scanReady
      universe <- get
      key <- liftIO $ getCh
      (h,w) <- liftIO $ scrSize
      table <- liftIO $ readMVar $ uTable universe
      -- let key = decodeKey keyC
      -- writeFile "key-press" $ show key ++ show keyC
      when (key == KeyChar 's' || key == KeyChar 'S') $ do
        scanBusy
        now <- liftIO $ fmap zonedTimeToLocalTime getZonedTime
        let filePattern = show (localDay now) ++ "-" ++ show (localTimeOfDay now) ++ "-%02d.jpg"
        newFiles <- scanPages filePattern
        -- liftIO $ mapM_ ((putMVar $ gScan $ uGlobal universe) . Just) newFiles
        scanReady
      when (key == KeyChar 'q' || key == KeyChar 'Q') $ liftIO $ putMVar (uQuit universe) ()
      when (key == KeyChar 'f' || key == KeyChar 'F') $ do
        case M.lookup cursor $ etLines table of
          Just eID -> do
            updateTable $ \table' -> do
              case M.lookup eID $ etUnfinished table' of
                Just entry -> do
                  let entry' = entry{enData = (enData entry){exPagesMissing = S.empty}}
                  done <- liftIO $ evalStateT (finish (eID, enData entry')) $ uGlobal universe
                  when done $ 
                    increaseFinished
                  -- writeEntry eID entry'
                  {-let table2 = table'{ etFinished   = M.insert eID entry' $ etFinished table'
                                     , etUnfinished = M.delete eID $ etUnfinished table'
                                     }
                  -}
                  updateTablePositions table' eID entry'
                Nothing -> return table
            refreshTable
          Nothing -> return ()
      let cursor' = case key of
                      KeyUp -> max (cursor - 1) 1
                      KeyDown -> min (cursor + 1) (M.size (etFinished table) + M.size (etUnfinished table))
                      _ -> cursor
      when (cursor /= cursor') $
        moveCursor cursor cursor'
      keyboardLoop cursor'

moveCursor old new =  do
  universe <- get
  let mWindow = uWindow universe
  topRow <- liftIO $ readMVar $ uTableTopVisibleRow universe
  (h,w) <- liftIO $ scrSize
  if new < topRow + 5 && topRow > 1 then do
    liftIO $ modifyMVar_ (uTableTopVisibleRow universe) $ \_ -> return $ topRow - 1
    refreshTable
  else if new > topRow + h - 5 then do
    liftIO $ modifyMVar_ (uTableTopVisibleRow universe) $ \_ -> return $ topRow + 1
    refreshTable
  else
    return ()
  topRow' <- liftIO $ readMVar $ uTableTopVisibleRow universe
  liftIO $ withMVar mWindow $ \window -> do
    attrSet attr0 (Pair 1)
    wMove window (old + 1 - topRow' + 1) 0
    wAddStr window " "
    wMove window (new + 1 - topRow' + 1) 0
    wAddStr window ">"

refreshTable = do
  universe <- get
  clearCursor
  liftIO $ do
    (ph0, pw0) <- readMVar (uTableWindowSize universe)
    topRow <- readMVar (uTableTopVisibleRow universe)
    pad <- readMVar (uTableWindow universe)
    (h,w) <- scrSize
    pRefresh pad topRow 2
                 2 2
                 (min (h - 2) (ph0 - topRow)) (w-1)

scanReady = do
  updateStatus (\status -> do
    return $ status{stScanReady = True}
    )

scanBusy = 
  updateStatus (\status -> do
    return status{stScanReady = False}
    )

writeEntry eID entry = do
  universe <- get
  topRow <- liftIO $ readMVar (uTableTopVisibleRow universe)
  liftIO $ withMVar (uWindow universe) $ \window -> do
    (ph0, pw0) <- readMVar (uTableWindowSize universe)
    when (enLine entry > ph0) $ do
      modifyMVar_ (uTableWindow universe) $ \_ -> do
        modifyMVar (uTableWindowSize universe) $ \(ph, pw) -> do
          pad' <- newPad (2*ph)  pw
          return ( (2 * ph, pw), pad')
    pad <- readMVar (uTableWindow universe)
    (ph, pw) <- readMVar (uTableWindowSize universe)
    (h,w) <- scrSize
    when (enLine entry < ph) $ do
      wMove pad (enLine entry) 2
      let exam = enData entry 
          scannedStr = compactRange $ exPagesScanned exam
          missingStr = compactRange $ exPagesMissing exam
          entryStr = concat
                [ take 6 eID
                , take (8 - length eID) $ repeat ' '
                , take 11 $ exStudentID exam
                , take (12 - (length $ exStudentID exam)) $ repeat ' '
                , take 20 scannedStr
                , take (21 - (length scannedStr)) $ repeat ' '
                , take 20 missingStr
                , take pw $ repeat ' '
                ]
      if S.null $ exPagesMissing exam then do
        wAttrSet pad (attr0, (Pair 2))
      else
        wAttrSet pad (attr0, (Pair 1))
      wAddStr pad $ take (pw - 3) entryStr 
      when (enLine entry - topRow + 3 < h && enLine entry - topRow + 1 > 0) $ do
        pRefresh pad (enLine entry) 0 (enLine entry - topRow + 2) 0 (enLine entry - topRow + 2) (w-1)
      wAttrSet pad (attr0, (Pair 1))
      wRefresh window

redrawTable = do
  universe <- get
  table <- liftIO $ readMVar $ uTable universe
  forM_ (M.assocs $ etFinished table) $ \(eID, entry) -> do
    writeEntry eID entry
  forM_ (M.assocs $ etUnfinished table) $ \(eID, entry) -> do
    writeEntry eID entry

compactRange s = intercalate ", " $ compactRange' $ S.toList s
  where
    compactRange' [] = []
    compactRange' (x:xs) = 
      let (inRange, rs) = takeInRange x xs
      in (if null inRange then show x else show x ++ "-" ++ show (last inRange)) : 
          compactRange' rs

takeInRange x [] = ([],[])
takeInRange x (y:ys)
  | y == x + 1 = 
    let (inRange', rs') = takeInRange y ys
    in (y : inRange', rs')
  | otherwise = ([], y:ys)
