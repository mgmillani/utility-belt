module ExamScan.Recognition 
       ( recognizeExams
       , Global(..)
       , Status(..)
       , finish
       , loadExisting
       )
where

import ExamScan.Exam
import qualified Data.Map as M
import qualified Data.Set as S
import qualified Data.Vector as V
import qualified Data.ByteString.Lazy as BS
import HSParallel.ProducerConsumer
import Control.Concurrent
import Control.Concurrent.MVar
import Control.Exception
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.State
import Data.Char
import Data.Either
import Data.List
import Data.Maybe
import System.Directory
import System.Environment
import System.FilePath
import System.IO
import System.Process
import Text.Read (readMaybe)

data Orientation = OUp | ORight | ODown | OLeft deriving (Show, Eq)

data QRCode a = QRCode a Orientation deriving (Show, Eq)

data Status = Status
  { stProcessing :: Int
  , stFinished :: Int
  , stFailed :: Int
  , stExams  :: M.Map String Exam
  , stScanReady :: Bool
  }
  deriving (Show)

data Global = Global
  { gStatus :: MVar Status
  , gScan   :: MVar (Maybe FilePath)
  -- , gTable  :: MVar (ExamTable Exam)
  , gUpdate :: MVar (Maybe (String, Exam))
  , gFail   :: MVar Bool
  , gStudents :: M.Map String String
  , gExpectedPages :: S.Set Int
  , gWorkingDir :: FilePath
  }

data Scan = Scan FilePath (QRCode String)

recognizeExams :: StateT Global IO [(String, Exam)]
recognizeExams = do
  global <- get
  output <- liftIO $ produceConsumeExhaustive
    (evalStateT worker (global :: Global))
    (\entry -> evalStateT (consumer entry) global)
  return output

loadExisting targetDir expectedPages students singleShot = do
  createDirectoryIfMissing True $ targetDir </> "scans-new"
  createDirectoryIfMissing True $ targetDir </> "exams"
  createDirectoryIfMissing True $ targetDir </> "failed"
  scans <- fmap (map ((targetDir </> "scans-new") </>)) $ listDirectory $ targetDir </> "scans-new"
  mScan <- newEmptyMVar
  forkFinally (mapM_ (\fl -> do
                                putMVar mScan $ Just fl)
                     scans)
              (\e -> case e of
                          Left err -> print (err :: SomeException)
                          Right _ -> when singleShot $ putMVar mScan Nothing)
  let status = Status
               { stProcessing = 0
               , stFinished   = 0
               , stFailed     = 0
               , stExams      = M.empty
               , stScanReady  = False
               }
  exams <- fmap (map ((targetDir </> "exams") </>)) $ listDirectory $ targetDir </> "exams"
  examsFound <- forM exams $ \student -> do
    pages <- fmap (map ((student </> "pages") </>)) $ do
                   exist <- doesDirectoryExist $ student </> "pages"
                   if exist then
                     listDirectory $ student </> "pages"
                   else
                     return []
    let eID = (takeFileName student)
    let foundPages = S.fromList 
                       [ fromJust num
                       | fl <- pages
                       , let num = readMaybe $ takeBaseName fl :: Maybe Int
                       , takeExtension fl == ".pdf" && isJust num
                       ]
        pageFiles = M.fromList [ (fromJust num, fl)
                               | fl <- pages
                               , let num = readMaybe $ takeBaseName fl :: Maybe Int
                               , takeExtension fl == ".pdf" && isJust num
                               ]
        missingPages = expectedPages `S.difference` foundPages
        exam = Exam 
               { exStudentID     = fromMaybe "???" $ M.lookup eID students
               , exPagesScanned  = foundPages
               , exPagesMissing  = missingPages
               , exFiles         = pageFiles
               , exFinished      = False -- S.null missingPages
               }
    return (eID, exam)
  let examsFound' = filter (\(eID, ex) -> exPagesMissing ex /= expectedPages 
                                       && (not $ S.null $ exPagesMissing ex)) 
                           examsFound
  mUpdate <- newEmptyMVar
  mStatus <- newMVar $ status{stExams = M.fromList examsFound'}
  mFail <- newEmptyMVar
  forkFinally (mapM_ (\(eID, exam) -> putMVar mUpdate $ Just (eID, exam)) examsFound')
              (\e -> case e of
                        Left err -> do
                          putStrLn "load exams error"
                          print (err :: SomeException)
                        Right _ -> return ())
  return Global{ gScan = mScan
               , gStatus = mStatus
               , gUpdate = mUpdate
               , gWorkingDir = targetDir
               , gFail = mFail
               , gStudents = students
               , gExpectedPages = expectedPages 
               }

worker = do
  global <- get
  qrDir <- getQRDir
  failedDir <- getFailedDir
  mScanFl <- nextScan
  case mScanFl of
    Nothing -> liftIO $ return Nothing
    Just scanFl -> do
      mQR <- liftIO $ readQRCode qrDir scanFl
      case mQR of
        Nothing -> liftIO $ do
          putMVar (gFail global) True
          renameFile scanFl (failedDir </> takeFileName scanFl)
          evalStateT worker global
        Just qr -> do
          return $ Just $ Scan scanFl qr

nextScan = do
  global <- get
  mScan <- liftIO $ takeMVar $ gScan global
  when (isNothing mScan) $ do
    liftIO $ putMVar (gScan global) Nothing
  return mScan

consumer (Scan scanFl qrCode) = do
  entry@(eID, exam) <- assignFile scanFl qrCode
  global <- get
  updateEntry entry
  when (null $ exPagesMissing exam) $ do
    finish entry
    return ()
  return entry

updateEntry entry = do
  global <- get
  liftIO $ putMVar (gUpdate global) $ Just entry

finish entry@(eID, exam) = do
  archiveDir <- getFinishedExamsDir
  global <- get
  joined <- liftIO $ modifyMVar (gStatus global) $ \status -> do
    let exam' = stExams status M.! eID
    return (status{stExams = M.insert eID exam{exFinished = True} $ stExams status}, exFinished exam')
  when (not joined) $ liftIO $ do
    createDirectoryIfMissing True $ archiveDir </> eID </> "pages"
    joinPdfs (M.elems $ exFiles exam) (archiveDir </> eID </> (exStudentID exam) <.> "pdf")
    mapM_ (\fl -> renameFile fl (archiveDir </> eID </> "pages" </> (takeFileName fl))) $ M.elems $ exFiles exam
  return (not joined)

getFinishedExamsDir = do
  global <- get
  return $ (gWorkingDir global) </> "exams-finished"

getFailedDir = do
  global <- get
  return $ (gWorkingDir global) </> "failed"

getQRDir = do
  global <- get
  return $ (gWorkingDir global) </> "qr"

getExamsDir = do
  global <- get
  return $ gWorkingDir global </> "exams"

getScanArchiveDir = do
  global <- get
  return $ gWorkingDir global </> "scans-archive"

assignFile fl (QRCode code orientation) = do
  targetDir <- getExamsDir
  finishedExamsDir <- getFinishedExamsDir
  scanArchiveDir <- getScanArchiveDir
  let (num, rs) = span (/='-') code
      page = tail rs
      dir = (targetDir </> num </> "pages")
      rotated = dir </> page <.> "jpg"
      pdfName = (dir </> page <.> "pdf")
  global <- get
  liftIO $ do
    createDirectoryIfMissing True dir
    createDirectoryIfMissing True finishedExamsDir
    createDirectoryIfMissing True scanArchiveDir
    rotate fl orientation pdfName
    renameFile fl (scanArchiveDir </> takeFileName fl)
    modifyMVar (gStatus global) (\status -> do
      let status' = status{ stExams = M.alter (\mex -> evalState (updateExam num page pdfName mex) global) num 
                                              $ stExams status}
      return (status', (num, stExams status' M.! num))
      )

updateExam num page fl Nothing = do
  global <- get
  return $ Just
    Exam{ exStudentID = searchStudentID num $ gStudents global
        , exPagesScanned = S.fromList [read page]
        , exPagesMissing = S.difference (gExpectedPages global) $ S.fromList [read page]
        , exFiles = M.singleton (read page) fl
        , exFinished = False
        }
updateExam num page fl (Just exam') = return $ Just
  exam'{ exPagesScanned = (exPagesScanned exam') `S.union` S.fromList [read page]
       , exPagesMissing = (exPagesMissing exam') `S.difference` S.fromList [read page]
       , exFiles = M.insert (read page) fl $ exFiles exam'
       }

searchStudentID num students = 
  case M.lookup num students of
    Just sID -> sID
    Nothing -> "???"

rotate fl orientation dest = do
  let degrees = case orientation of
                     OUp    -> 0
                     OLeft  -> 90
                     ODown  -> 180
                     ORight -> 270
  (_,Just sout, Just serr,ph) <- createProcess $
    (proc "convert" [fl, "-rotate", show degrees, "-quality", "70", "-geometry" , "1300x2300", dest])
      { std_out = CreatePipe
      , std_err = CreatePipe
      }
  forkIO $ do
    outStr <- hGetContents sout
    when (not $ null outStr) $ 
      withFile "error" AppendMode $ \h -> do
        hPutStrLn h "rotate:"
        hPutStrLn h fl
        hPutStrLn h dest
        hPutStrLn h outStr
    seq (length outStr) $ return ()
  forkIO $ do
    errStr <- hGetContents serr
    when (not $ null errStr) $ 
      withFile "error" AppendMode $ \h -> do
        hPutStrLn h "rotate:"
        hPutStrLn h fl
        hPutStrLn h dest
        hPutStrLn h errStr
    seq (length errStr) $ return ()
  waitForProcess ph
  return ()

toPdf fl target = do
  -- spawnProcess "convert" [fl, target] >>= waitForProcess
  (_,Just sout, Just serr,ph) <- createProcess $
    (proc "convert" [fl, "-quality" , "80", "-geometry", "1080x1920", target])
      { std_out = CreatePipe
      , std_err = CreatePipe
      }
  forkIO $ do
    outStr <- hGetContents sout
    when (not $ null outStr) $ 
      withFile "error" AppendMode $ \h -> do
        hPutStrLn h "toPdf: "
        hPutStrLn h fl
        hPutStrLn h target
        hPutStrLn h outStr
    seq (length outStr) $ return ()
  forkIO $ do
    errStr <- hGetContents serr
    when (not $ null errStr) $ 
      withFile "error" AppendMode $ \h -> do
        hPutStrLn h "toPdf: "
        hPutStrLn h fl
        hPutStrLn h target
        hPutStrLn h errStr
    seq (length errStr) $ return ()
  waitForProcess ph
  return ()

joinPdfs fls dest = do
  createDirectoryIfMissing True (takeDirectory dest)
  absFls <- mapM makeAbsolute fls
  (_,Just sout, Just serr,ph) <- createProcess $
    (proc "pdftk" (absFls ++ ["cat", "output", takeFileName dest]))
      { cwd = Just $ takeDirectory dest
      , std_out = CreatePipe
      , std_err = CreatePipe
      }
  forkIO $ do
    outStr <- hGetContents sout
    when (not $ null outStr) $ 
      withFile "error" AppendMode $ \h -> hPutStr h $ "joinPdfs:\n " ++ show fls ++ "\n" ++ outStr
    seq (length outStr) $ return ()
  forkIO $ do
    errStr <- hGetContents serr
    when (not $ null errStr) $ 
      withFile "error" AppendMode $ \h -> hPutStr h $ "joinPdfs:\n " ++ show fls ++ "\n" ++ errStr
    seq (length errStr) $ return ()
  waitForProcess ph
  return ()

readQRCode workingDir fl = do
  let target = workingDir </> (takeBaseName fl) <.> "jpg"
  (_,_,Just serr0,ph0) <- createProcess (proc "convert"
                  [ fl
                  , "-resize", "2048x2048>"
                  , "-colors", "2"
                  , "-colorspace", "gray"
                  , "-normalize"
                  , "-morphology", "Erode", "Ring:1"
                  , "-morphology", "Dilate", "Plus:1"
                  , "-morphology", "Dilate", "Plus:1"
                  , target
                  ]
                  )
                  {std_err = CreatePipe}
  waitForProcess ph0
  -- zbarimg 
  (_,Just sout1, Just serr1, ph1) <- createProcess (proc "zbarimg"
                  [ "-Sdisable"
                  , "-Sqrcode.enable"
                  , "--xml"
                  , target
                  ]
                  )
                  {std_out = CreatePipe, std_err = CreatePipe}
  mQRCode <- newEmptyMVar
  forkIO $ do
    outStr1 <- hGetContents sout1
    seq (length outStr1) $ return ()
    -- putStrLn $ fl ++ ": zbarimg output:" ++ outStr1 ++ "\n  " ++ show (parseQRCode outStr1)
    putMVar mQRCode $ parseQRCode outStr1
  waitForProcess ph1
  readMVar mQRCode

splitString c "" = []
splitString c (x:xs)
  | c == x = "" : splitString c xs
  | otherwise = 
    let splits = splitString c xs
    in case splits of
      (w:ws) -> (x:w) : ws
      [] -> [[x]]


parseQRCode str
  | null str = Nothing
  | "qr-code" `isPrefixOf` (map toLower str) = parse' $ drop 7 str
  | otherwise = parseQRCode $ tail str
    where
      parse' str
        | null str = Nothing
        | "orientation" `isPrefixOf` (map toLower str) = do
          (orientation, rs) <- getOrientation $ drop 12 str
          content <- getContent rs
          return $ QRCode content orientation
        | otherwise = parse' $ tail str

getContent str
  | null str = Nothing
  | "cdata" `isPrefixOf` (map toLower str) = Just $ takeWhile (/=']') $ dropWhile (=='[') $ drop 5 str
  | otherwise = getContent $ tail str
    
getOrientation str = 
  let (str', rs) = span (/='\'') $ dropWhile (=='\'') $ dropWhile (/='\'') str
  in case map toLower str' of
  "up" -> Just (OUp, rs)
  "left" -> Just (OLeft, rs)
  "right" -> Just (ORight, rs)
  "down" -> Just (ODown, rs)
  _ -> Nothing
