{-# LANGUAGE OverloadedStrings #-}

module ExamScan.Exam where

import Data.Csv
import qualified Data.Map as M
import qualified Data.Set as S
import qualified Data.ByteString.Lazy as BS
import qualified Data.Vector as V

data Student = Student
  { stID   :: String
  , stExam :: String
  } deriving (Eq, Show)

instance FromNamedRecord Student where
  parseNamedRecord m = Student <$> m .: "mtknr" <*> (fmap (\los -> reverse $ take 3 $ reverse $ "00" ++ los) $ m .: "los")
instance ToNamedRecord Student where
  toNamedRecord st = namedRecord [
        "mtknr" .= (stID st), "los" .= (stExam st)]
instance DefaultOrdered Student where
  headerOrder _ = header ["mtknr", "los"]

data Page = Page
  { pgNumber :: Int
  , pgName   :: String
  } deriving (Eq, Show)

instance FromNamedRecord Page where
  parseNamedRecord m = Page <$> m .: "number" <*> m .: "name"
instance ToNamedRecord Page where
  toNamedRecord pg = namedRecord [
        "number" .= (pgNumber pg), "name" .= (pgName pg)]
instance DefaultOrdered Page where
  headerOrder _ = header ["number", "name"]

data Exam = Exam
  { exStudentID :: String
  , exPagesScanned :: S.Set Int
  , exPagesMissing :: S.Set Int
  , exFiles :: M.Map Int FilePath
  , exFinished :: Bool
  } deriving (Eq, Show)

data Entry a = Entry
  { enLine :: Int
  , enData :: a
  }

instance (Show a) => Show (Entry a) where
  show e = (show $ enLine e) ++ ":" ++ show (enData e)

parseStudents fl = do
  csv <- fmap (decodeByName) $ BS.readFile fl
  return $ fmap (\table -> M.fromList [(stExam st, stID st) | st <- V.toList $ snd table]) csv

parseExpectedPages fl = do
  csv <- fmap (decodeByName) $ BS.readFile fl
  return $ fmap (\pages -> S.fromList $ map pgNumber $ V.toList $ snd pages) csv
