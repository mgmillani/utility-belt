{ pkgs, lib, config, ... }:

# Need to allow port 8448 for federation

let
  fqdn = "matrix.${config.networking.domain}";
  baseUrl = "https://${fqdn}";
  clientConfig."m.homeserver".base_url = baseUrl;
  serverConfig."m.server" = "${fqdn}:443";
  mkWellKnown = data: ''
    add_header Content-Type application/json;
    add_header Access-Control-Allow-Origin *;
    return 200 '${builtins.toJSON data}';
  '';
in {

  services.postgresql = { 
    enable = true;
  };
  # services.postgresql.initialScript = pkgs.writeText "synapse-init.sql" ''
  #   CREATE ROLE "matrix-synapse" WITH LOGIN PASSWORD 'synapse';
  #   CREATE DATABASE "matrix-synapse" WITH OWNER "matrix-synapse"
  #     TEMPLATE template0
  #     LC_COLLATE = "C"
  #     LC_CTYPE = "C";
  # '';

  services.nginx = {
    virtualHosts = {
      # If the A and AAAA DNS records on example.org do not point on the same host as the
      # records for myhostname.example.org, you can easily move the /.well-known
      # virtualHost section of the code to the host that is serving example.org, while
      # the rest stays on myhostname.example.org with no other changes required.
      # This pattern also allows to seamlessly move the homeserver from
      # myhostname.example.org to myotherhost.example.org by only changing the
      # /.well-known redirection target.
      "${config.networking.domain}" = {
        enableACME = true;
        # This section is not needed if the server_name of matrix-synapse is equal to
        # the domain (i.e. example.org from @foo:example.org) and the federation port
        # is 8448.
        # Further reference can be found in the docs about delegation under
        # https://matrix-org.github.io/synapse/latest/delegate.html
        locations."= /.well-known/matrix/server".extraConfig = mkWellKnown serverConfig;
        # This is usually needed for homeserver discovery (from e.g. other Matrix clients).
        # Further reference can be found in the upstream docs at
        # https://spec.matrix.org/latest/client-server-api/#getwell-knownmatrixclient
        locations."= /.well-known/matrix/client".extraConfig = mkWellKnown clientConfig;
      };
      "${fqdn}" = {
        enableACME = true;
        forceSSL = true;
        # It's also possible to do a redirect here or something else, this vhost is not
        # needed for Matrix. It's recommended though to *not put* element
        # here, see also the section about Element.
        locations."/".extraConfig = ''
          return 404;
        '';
        # Forward all Matrix API calls to the synapse Matrix homeserver. A trailing slash
        # *must not* be used here.
        locations."/_matrix".proxyPass = "https://[::1]:8448";
        # Forward requests for e.g. SSO and password-resets.
        locations."/_synapse/client".proxyPass = "https://[::1]:8448";
      };
      "element.${config.networking.domain}" = {
        enableACME = true;
        forceSSL = true;
        serverAliases = [
          "element.${config.networking.domain}"
        ];

        root = pkgs.element-web.override {
          conf = {
            default_server_config = clientConfig; 
          };
        };
      };
    };
  };

  services.matrix-synapse = {
    enable = true;
    settings.server_name = config.networking.domain;
    # The public base URL value must match the `base_url` value set in `clientConfig` above.
    # The default value here is based on `server_name`, so if your `server_name` is different
    # from the value of `fqdn` above, you will likely run into some mismatched domain names
    # in client applications.
    settings.tls_certificate_path = "/var/lib/acme/${fqdn}/fullchain.pem";
    settings.tls_private_key_path = "/var/lib/acme/${fqdn}/key.pem";
    settings.public_baseurl = baseUrl;
    settings.database = {
      name = "psycopg2";
      args = {
        database = "matrix-synapse";
      };
    };
    # Configured using extraConfigFiles
    # registration_shared_secret = ;
    settings.listeners = [
      { port = 8448;
        bind_addresses = [ "::1" ];
        type = "http";
        tls = true;
        x_forwarded = true;
        resources = [ {
          names = [ "client" "federation" ];
          compress = true;
        } ];
      }
    ];
    # plugins = with config.services.matrix-synapse.package.plugins; [] ;
    settings.enable_metrics = true;
    settings.enable_registration = false;
    extraConfigFiles = 
      [ config.age.secrets.matrix-registration-shared-secret.path
      ];
    settings.app_service_config_files = [
      "/var/lib/matrix-synapse/telegram-registration.yaml"
      ];
  };
  age.secrets = {
    "matrix-registration-shared-secret" = {
      file = ./secrets/matrix.registration-shared-secret.age;
      group = "matrix-synapse";
      owner = "matrix-synapse";
    };
  };
  users.users."matrix-synapse" = {extraGroups = ["nginx"];};
  # bridges
  # environment.systemPackages = 
  # [ pkgs.mautrix-discord
  #   pkgs.mautrix-signal
  #   pkgs.matrix-appeservice-slack
  #   pkgs.mautrix-telegram
  # ]
  services.mautrix-telegram = {
    enable = true;
    # file containing the appservice and telegram tokens
    environmentFile = "/etc/secrets/mautrix-telegram.env";

    # The appservice is pre-configured to use SQLite by default.
    # It's also possible to use PostgreSQL.
    settings = {
      homeserver = {
        address = "http://localhost:8448";
        domain = "${config.networking.domain}";
      };
      appservice = {
        provisioning.enabled = false;
        id = "telegram";
        public = {
          enabled = true;
          prefix = "/public";
          external = "https://${config.networking.domain}:8449/public";
        };

        # The service uses SQLite by default, but it's also possible to use
        # PostgreSQL instead:
        #database = "postgresql:///mautrix-telegram?host=/run/postgresql";
      };
      bridge = {
        relaybot.authless_portals = false;
        permissions = {
          "*" = "relaybot";
          # "@someadmin:${config.networking.domain}" = "admin";
        };

        # Animated stickers conversion requires additional packages in the
        # service's path.
        # If this isn't a fresh installation, clearing the bridge's uploaded
        # file cache might be necessary (make a database backup first!):
        # delete from telegram_file where \
        #   mime_type in ('application/gzip', 'application/octet-stream')
        # animated_sticker = {
        #   target = "gif";
        #   args = {
        #     width = 256;
        #     height = 256;
        #     fps = 30;               # only for webm
        #     background = "020202";  # only for gif, transparency not supported
        #   };
        # };
      };
    };
  };
  # systemd.services.mautrix-telegram.path = with pkgs; [
  #   lottieconverter  # for animated stickers conversion, unfree package
  #   ffmpeg           # if converting animated stickers to webm (very slow!)
  # ];
}
