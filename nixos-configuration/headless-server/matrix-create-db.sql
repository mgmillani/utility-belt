CREATE USER "matrix-synapse";

CREATE DATABASE "matrix-synapse"
    ENCODING 'UTF8'
    LC_COLLATE='C'
    LC_CTYPE='C'
    template=template0
    OWNER "matrix-synapse";
