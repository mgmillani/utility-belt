{ pkgs, lib, config, ... }:

let
  fqdn = "matrix.${config.networking.domain}";
  baseUrl = "https://${fqdn}";
  configDir = "/var/lib/mautrix-signal";
  mautrix-signal = pkgs.mautrix-signal.override (old :
                     { python3 = old.python3.withPackages (py: with py; [ aiosqlite ])});
in {
  systemd.services = {
    "mautrix-signal-bot" = {
      serviceConfig = {
        enable = true;
        Type = "exec";
        ExecStart = ${mautrix-signal}/bin/mautrix-signal;
        TimeoutStartSec = 30;
        Restart = "always";
        RestartSec = 10;
        WorkingDirectory = configDir;
        User="mautrix-signal";
        Group="matrix-synapse";
      };
      wantedBy = [ "multi-user.target" ];
      };
    };
  };
};
