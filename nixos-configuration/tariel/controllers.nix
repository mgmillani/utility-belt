{pkgs,...}:
{
  systemd.services.bigwon-xbox-controller = {
    # Required so that the unit is usable. This does not cause the unit to be started at boot.
    enable = true; 
    description = "Emulate xbox-controller";
    serviceConfig = {
      Type = "simple";
      ExecStart = "${pkgs.coreutils}/bin/sleep 1s ; ${pkgs.xboxdrv}/bin/xboxdrv --detach-kernel-driver --mimic-xpad --silent --deadzone 0% --evdev /dev/input/by-id/usb-Zikway_Pro_Controller_314838353800c9c4-event-joystick --evdev-absmap ABS_Y=Y1,ABS_X=X1,ABS_RY=Y2,ABS_RX=X2,ABS_HAT0X=DPAD_X,ABS_HAT0Y=DPAD_Y --axismap -Y1=Y1,-Y2=Y2,-DPAD_Y=DPAD_Y --evdev-keymap BTN_WEST=X,BTN_NORTH=Y,BTN_SOUTH=A,BTN_EAST=B,BTN_START=start,BTN_SELECT=back,BTN_THUMBL=TL,BTN_THUMBR=TR,BTN_TL2=LT,BTN_TR2=RT,BTN_TL=LB,BTN_TR=RB";
    };
  };

  systemd.services.rapoo-xbox-controller = {
    enable = true;
    description = "Emulate xbox-controller";
    serviceConfig = {
      Type = "simple";
      ExecStart = "${pkgs.coreutils}/bin/sleep 1s ; ${pkgs.xboxdrv}/bin/xboxdrv --detach-kernel-driver --evdev /dev/input/by-id/usb-U-COMM_PC_2.4G_Wireless_Controller-event-joystick --evdev-absmap 'ABS_Y=Y1,ABS_X=X1,ABS_HAT0Y=DPAD_Y,ABS_HAT0X=DPAD_X,ABS_Z=X2,ABS_RZ=Y2' --axismap '-Y1=y1,-y2=y2' --evdev-keymap 'BTN_SOUTH=y,BTN_EAST=b,BTN_C=a,BTN_NORTH=x,BTN_TR2=start,BTN_TL2=back,BTN_WEST=LB,BTN_TL=LT,BTN_Z=RB,BTN_TR=RT,BTN_SELECT=TL,BTN_START=TR' --mimic-xpad --silent --deadzone 5%";
    };
  };

  services.udev.extraRules = ''
SUBSYSTEM=="input",ACTION=="add",ATTRS{id/vendor}=="057e",ATTRS{id/product}=="2009",RUN+="${pkgs.systemd}/bin/systemctl start bigwon-xbox-controller.service"
SUBSYSTEM=="input",ACTION=="add",ATTRS{id/vendor}=="062a",ATTRS{id/product}=="8668",RUN+="${pkgs.systemd}/bin/systemctl start rapoo-xbox-controller.service"
'';
}
