{ mkDerivation, stdenv, ofono-phonesim, bash
}:
mkDerivation {
  pname = "phonesim-auto";
  version = "1.0";
  src = ./.;
	installPhase = ''
	mkdir -p $out/bin
	cat << EOF > $out/bin/phonesim-auto
#!/usr/bin/env bash
'' + ofono-phonesim.out + ''/bin/phonesim -p 12345 '' + ofono-phonesim.out + ''/share/phonesim/default.xml &
dbus-send --print-reply --system --dest=org.ofono /phonesim org.ofono.Modem.SetProperty string:"Powered" variant:boolean:true
dbus-send --print-reply --system --dest=org.ofono /phonesim org.ofono.Modem.SetProperty string:"Online" variant:boolean:true
EOF
chmod +x $out/bin/phonesim-auto
'';
}
