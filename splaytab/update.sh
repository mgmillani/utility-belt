#!/usr/bin/fish

set RefDir "$XDG_DATA_HOME/References/"
set Table $argv[1]

find $RefDir -regex '.*\.pdf' |	while read -l fl
	set Fname (dirname $fl | grep -o '[^/]*$')
	echo \"$Fname\" $fl
end > $Table
