#!/usr/bin/env bash

set -e
unset PATH
for p in $buildInputs; do
  export PATH=$p/bin${PATH:+:}$PATH
done

mkdir -p "$out/bin" "$out/share/applications"
cp $Desktop "$out/share/applications/splaytab-jgmenu.desktop"
echo "#! $(which fish)" > "$out/bin/splaytab-jgmenu"
tail -n+2 "$src" | sed "s:\([ \t]\)splaytab:\1\"$(which splaytab)\":g" >> "$out/bin/splaytab-jgmenu"
chmod +x "$out/bin/splaytab-jgmenu"
