module Main where

import qualified Splaytab.JGmenu as J

import Control.Monad.Trans.State
import Data.Char
import Data.List
import qualified Data.Map.Strict as M
import qualified Data.Map.Merge.Strict as Me
import System.Directory
import System.Environment
import System.Exit
import System.IO
import Control.Exception

help = do
  hPutStr stderr "usage: splaytab <FILE> <COMMAND>\n\
                 \ where COMMAND is one of the following:\n\
                 \   keys                 List all keys\n\
                 \   read <KEY>           Read value of given key\n\
                 \   write <KEY> <VALUE>  Update value of key\n\
                 \   update <NEW FILE>    Merge two tables\n\
                 \   jgmenu <FILE>        Output table in jgmenu-style CSV"

createIfNotExist fl = do
  b <- doesFileExist fl
  if b then
     return ()
  else do
    hFl <- openFile fl WriteMode
    hClose hFl

readIfExists fl = catch (readFile fl) ((\_ -> return "") :: IOError -> IO String)

main = do
  args <- getArgs
  case args of
    (fl:cmd:r) -> do
      execute fl (map toLower cmd) r
    _ -> help

execute fl "keys" _ = do
  keys <- readIfExists fl
  mapM_ putStrLn $ map (fst . asTuple) $ lines keys
execute fl "read" [key] = do
  keys <- readIfExists fl
  let table = map asTuple $ lines keys  
      value = lookup key table
  case value of
    Nothing -> do
      hPutStr stderr $ concat ["Key \"", key,"\" not found."]
      exitFailure
    Just v -> do
      putStrLn v
      update fl $ (key, v) : (table \\ [(key,v)])
execute fl "write" [key,value] = do
  keys <- readIfExists fl
  let table = map asTuple $ lines keys
      value' = lookup key table
  case value' of
    Nothing -> update fl ( (key, value) : table)
    Just v ->  update fl ( (key, value) : (table \\ [(key, v)]))
execute fl "update" [newFl] = do
  oldKeys <- readIfExists fl
  newKeys <- readIfExists newFl
  let oldTable = map asTuple $ lines oldKeys
      oldMap = M.fromList oldTable
      newMap = M.fromList $ map asTuple $ lines newKeys
      mergedMap = Me.merge Me.preserveMissing Me.dropMissing (Me.zipWithMatched (\_ x _ -> x)) newMap oldMap
      mergedTable =
        [(k,v) | (k,v) <- M.assocs mergedMap, not $ k `M.member` oldMap]
        ++ [(k,mergedMap M.! k) | (k, _) <- oldTable, k `M.member` mergedMap && k `M.member` oldMap]
  update fl mergedTable
execute fl "jgmenu" _ = do
  keys <- readIfExists fl
  case J.makeConfig $ map (fst . asTuple) $ lines keys of
    Right result -> putStrLn result
    Left err -> do
      hPutStrLn stderr err
      exitFailure
execute _ _ _ = help

update fl table = do
  tmp <- getTemporaryDirectory
  user <- getEnv "USER"
  let tmpDir = concat [tmp,"/", user, "/splaytab"]
  createDirectoryIfMissing True $ concat [tmp,"/", user]
  createDirectoryIfMissing True $ tmpDir
  existingFiles <- listDirectory tmpDir
  let n = length existingFiles
      tmpFl = concat [tmpDir, "/", "tmp-", show n]
  writeFile tmpFl $ concatMap (\(k,v) -> concat ["\"",k,"\" ", v,"\n"]) table
  copyFile tmpFl fl
  removeFile tmpFl

asTuple str = evalState readTuple str

consumeWhile :: (Char -> Bool) -> State String String
consumeWhile f = do
  str <- get
  let (x,r) = span f str
  put r
  return x

whenNotEmpty e x = do
  s <- get
  if s == "" then return e else x

readTuple :: State String (String, String)
readTuple = do
  consumeWhile isSpace
  quote <- consumeWhile (=='"')
  c1 <- case quote of
    "\"" -> readQuotedString
    "\"\"" -> return ""
    [] -> consumeWhile (not . isSpace)
  consumeWhile isSpace
  quote' <- consumeWhile (=='"')
  c2 <- case quote' of
    "\"" -> readQuotedString
    [] -> get
  return (c1,c2)

readQuotedString :: State String String
readQuotedString = do
  ss <- whenNotEmpty "" get
  case (ss) of
    '\\':x:rs -> do
      put rs
      s1 <- readQuotedString
      return $ x:s1
    '"':rs -> do
      put rs
      return ""
    x:rs -> do
      put rs
      s1 <- readQuotedString
      return $ x:s1
    "" -> do
      return ""
