module Splaytab.JGmenu where

import System.FilePath

import qualified Data.Map as M
import qualified Data.Set as S
import Data.List

data Entry = Entry
  { name :: String
  , path :: FilePath
  , icon :: Maybe FilePath
  } deriving (Show)

makeConfig es = do
  entries' <- mapM parseEntry es
  let topLevel = unique $ map (head . splitDirectories . path) entries'
      tagMap = foldl' addEntry M.empty entries'
      entries = M.mapWithKey makeEntry tagMap
  return $ concatMap (entries M.!) topLevel ++ M.foldrWithKey (makeTag entries) "" tagMap

addEntry :: M.Map FilePath (Int, [Entry]) -> Entry -> M.Map FilePath (Int, [Entry])
addEntry tagMap entry
  | (path entry) `M.member` tagMap = 
        M.insertWith (\(_, v) (n, es) -> (n, v ++ es)) (path entry) (-1, [entry]) $
        M.insertWith (\(_, v) (n, es) -> (n, v ++ es)) (name entry) (-1, [entry]) $
                     tagMap
  | otherwise = 
    let parts = splitDirectories $ path entry
        (present, missing) = span (\p -> p `M.member` tagMap) $ scanl1 (</>) parts
    in 
    M.insertWith (\(_, v) (n, es) -> (n, v ++ es)) (name entry) (-1, [entry]) $ 
      (case present of
        [] -> id
        _ -> 
            let root = last present
                child = head missing
            in M.insertWith (\(_, v) (n, es) -> (n, v ++ es)) (root)
                            (undefined, [Entry
                                         { name = child
                                         , path = foldl1' (</>) (present ++ [child])
                                         , icon = Nothing
                                         }])
      ) $ 
      foldr (\(key, i, children) -> M.insert key
                                             (i, children))
            tagMap $ zip3 missing [M.size tagMap + 1..] 
                          ([ [Entry{name = child, path = child, icon = Nothing}]
                           | child <- tail missing
                           ]
                          ++
                          [[entry]])

makeEntry :: FilePath -> (Int, [Entry]) -> String
makeEntry k (-1, [e]) = "\"\"\"" ++ takeBaseName k ++ "\"\"\",\"\"\"printf '" ++ escape (name e) ++ 
                      case icon e of
                        Just i -> "'\"\"\"," ++ i ++ "\n"
                        Nothing -> "'\"\"\"\n"
makeEntry k (n, _) = "\"\"\"" ++ takeBaseName k ++ "\"\"\"" ++ ",^checkout(" ++ show n ++ ")\n"

makeTag :: M.Map FilePath String -> a -> (Int, [Entry]) -> String -> String
makeTag _ _ (-1, _) str = str
makeTag entries _ (t, es) str = "\n^tag(" ++ (show t) ++ ")\n" ++ concatMap (entries M.!) (reverse $ map name es) ++ str

parseEntry e@('<' : str) =
  let (icon', str') = span (/='>') str
  in case str' of
    '>' : str'' -> 
      let dir = takeDirectory str''
      in Right $ Entry { name = str''
                       , path = dir
                       , icon = Just icon'
                       }
    _ -> Left $ "Missing closing '>' on entry: " ++ e
parseEntry str = 
      let dir = takeDirectory str
      in Right $ Entry { name = str
                       , path = dir
                       , icon = Nothing
                       }

escape [] = []
escape ('\'' : str) = '\\' : '\'' : escape str
escape (c : str) = c : escape str

unique xs = unique' xs S.empty
  where
    unique' [] s = []
    unique' (x:xs) s
      | x `S.member` s = unique' xs s
      | otherwise = x : (unique' xs $ S.insert x s)

