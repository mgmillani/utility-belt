{ mkDerivation, stdenv, splaytab, which, coreutils, fish, bash, gnused
}:
mkDerivation {
  name = "splaytab-wrapped";
  buildInputs = [ splaytab which coreutils fish gnused ]; 
	unpackPhase = "true";
	src = ./.;
	preInstall = ''echo install: > Makefile'';
	postInstall = ''
		mkdir -p $out/bin $out/share/applications
		cp '' + ./splaytab-jgmenu.desktop + '' $out/share/applications/splaytab-jgmenu.desktop
		echo "#! $(which fish)" > "$out/bin/splaytab-jgmenu"
		tail -n+2 '' + ./splaytab-jgmenu + '' | sed "s:\([ \t]\)splaytab:\1\"$(which splaytab)\":g" >> "$out/bin/splaytab-jgmenu"
		chmod +x "$out/bin/splaytab-jgmenu"
	'';
	system = builtins.currentSystem;
  license = "GNU General Public License v3.0 only";
}
