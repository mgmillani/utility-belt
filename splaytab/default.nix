{ stdenv, lib, gnused, coreutils, fish, haskellPackages }:

let splaytab = haskellPackages.callPackage ./splaytab.nix {};
in
stdenv.mkDerivation
{
	pname = "splaytab";
	version = "0.3";
	src = ./.;
	system = builtins.currentSystem;

	buildInputs = [ fish gnused coreutils splaytab ];

	preBuild = ''echo install: > Makefile'';
	postInstall = ''
		mkdir -p $out/bin $out/share/applications $out/share/icons/hicolor/scalable
		sed "s:Icon=.*:Icon=$out/share/icons/hicolor/scalable/splaytab-jgmenu.svg:" '' + ./splaytab-jgmenu.desktop + '' > $out/share/applications/splaytab-jgmenu.desktop
		cp '' + ./splaytab-jgmenu.png +     '' $out/share/icons/hicolor/scalable/splaytab-jgmenu.png
		cp '' + ./splaytab-jgmenu.svg +     '' $out/share/icons/hicolor/scalable/splaytab-jgmenu.svg
		echo "#!'' + fish + ''/bin/fish" > "$out/bin/splaytab-jgmenu"
		tail -n+2 '' + ./splaytab-jgmenu + '' | sed "s:\([ \t]\)splaytab:\1\"'' + splaytab + ''/bin/splaytab\":g" >> "$out/bin/splaytab-jgmenu"
		chmod +x "$out/bin/splaytab-jgmenu"
	'';
}

