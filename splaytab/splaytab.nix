{ mkDerivation, base, containers, directory, filepath, lib
, transformers
}:
mkDerivation {
  pname = "splaytab";
  version = "0.2.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base containers directory filepath transformers
  ];
  license = lib.licenses.gpl3Only;
}
