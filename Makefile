prefix=/usr/local

Tools := $(wildcard */Makefile)
Tools := $(addprefix install-,$(Tools:/Makefile=))

install: ${Tools}
install-%:
	cd $* ; make install

	
