{ wine
, which
, coreutils
, fish
, gnused
, mkDerivation
, lib
, pname ? "thumb-backup"
, cryptsetup
, ... } :

let thumb-backup-plugged = ''
  #!/usr/bin/env fish
  
  # usage
  # thumb
  
  set UUID \$argv[1]
  set Mountpoint \$argv[2]
  set Name (basename \$Mountpoint)
  
  ${cryptsetup}/bin/cryptsetup luksOpen /dev/disk/by-uuid/\$UUID \$Name --key-file=/root/key.\$UUID
  /run/wrappers/bin/mount /dev/mapper/\$Name \$Mountpoint
  chmod 777 \$Mountpoint
  
  for User in (users)
  	if test -e /home/\$User
  		/run/wrappers/bin/su --login \$User OUT_BIN/thumb-backup-plugged-user \$UUID \$Mountpoint
  	end
  end

  # check if some display is in use
  set Display (who | tail -n1 | sed 's:\( \|\t\)\+: :g' | cut -s -d' ' -f5 | sed 's:(\|)::g')
  if test \\"\$Display\\" != \\"\\"
    set Answer (env DISPLAY=\$Display notify-send --action=\\"Unmount backup device\\"=unmount --wait)
    if test \\"\$Answer\\" = unmount
      ${cryptsetup}/bin/cryptsetup luksClose /dev/mapper/\$Name
      umount \$Mountpoint
      udisksctl power-off -b \$UUID
      env DISPLAY=\$Display notify-send \\"\$UUID unmounted\\"
    end
  end
'';
    thumb-backup-plugged-user = ''
	#!/usr/bin/env fish

	set UUID \$argv[1]
	set Mountpoint \$argv[2]

	if test -e \$XDG_CONFIG_HOME/thumb-backup/plugged
		\$XDG_CONFIG_HOME/thumb-backup/plugged \$UUID \$Mountpoint
	else if test -e ~/.thumb-backup/plugged
		~/.thumb-backup/plugged \$UUID \$Mountpoint
	end
'';
in
mkDerivation
{
  pname = "thumb-backup";
  version = "1.0.0.0";
  buildInputs = [ which coreutils fish gnused ]; 
  license = "GNU General Public License v3.0 only";
	system = builtins.currentSystem;
	src = ./.;
	preInstall = ''mkdir -p $out/bin/

echo "'' + thumb-backup-plugged + ''" | sed s:OUT_BIN:$out/bin/:g > $out/bin/thumb-backup-plugged
chmod +x $out/bin/thumb-backup-plugged

echo "'' + thumb-backup-plugged-user + ''" > $out/bin/thumb-backup-plugged-user
chmod +x $out/bin/thumb-backup-plugged-user

echo "install:" > Makefile
	'';
}
