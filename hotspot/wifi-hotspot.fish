#!/usr/bin/env fish

echo $argv (count $argv)

if test (count $argv) != 1
	echo "usage:"
	echo "wifi-hotspot <start|stop>"
	exit 1
end

set Command $argv[1]
set Dir $XDG_CONFIG_HOME/wifi-hotspot

function getOptions
	if test -e $Dir/config
		set Options (cat $Dir/config)
		set SSID $Options[1]
		set Password $Options[2]
	else
		mkdir -p $Dir
		set SSID $hostname-hotspot
		set Password ""
	end
	echo $SSID
	echo $Password
end

if test $Command = "start"
	set Options (getOptions)
	set SSID $Options[1]  
	set Password $Options[2]
	if test $Password = ""
		echo "Hotspot password:"
		read Password
		echo $SSID > $Dir/config
		echo $Password >> $Dir/config
	end
	sudo nmcli device wifi hotspot con-name $SSID ssid $SSID band bg password $Password
	set Status $status
	if test $Status != 0
		echo "Could not setup hotspot."
		exit $Status
	else
		echo "Hotspot $SSID enabled."
	end
else if test $Command = "stop"
	set Options (getOptions)
	sudo nmcli connection delete $Options[1]
	set Status $status
	if test $Status != 0
		echo "Could not disable hotspot."
		exit $Status
	else
		echo "Hotspot $SSID disabled."
	end
end
