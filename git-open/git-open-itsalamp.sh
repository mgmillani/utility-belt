#!/bin/sh

function loop ()
{
	echo '#'$Color":\"$ICON\" git-open"
	oldIFS="$IFS"
	IFS="
"
	for t in ${Targets[@]}
	do
		Name=$(basename "$t")
		Target=$(realpath "$t")
		echo "+'$Name' $Target"
	done
	IFS="$oldIFS"
	
	while read line
	do		
		Dir=$(dirname "$line")
		Target=$(basename "$line")
		git-open "$Dir" "$Target" > /dev/null
	done < "$FIFO"
}

Color="F88755"
Dir="$1"
cd "$Dir"
oldIFS="$IFS"
Targets=$(ls)
IFS="
"
Targets=($Targets)
IFS="$oldIFS"
ICON="twin moons.svg"

PID=$$
FIFOD=/tmp/$USER/git-open-itsalamp/
FIFO="$FIFOD"/fifo.$PID
mkdir -p $FIFOD
mkfifo $FIFO

loop | itsalamp > $FIFO

rm $FIFO
