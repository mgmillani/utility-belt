#!/usr/bin/fish

set directory $argv[1]
set target $argv[2]
set name (basename $directory)
cd $directory

git pull
if test $status -ne 0
	notify-send "Pulling $name failed." -u critical -i weather-severe-alert
end
xdg-open $target
git add -A ; and git commit -m (date +'%Y.%m.%d %H:%M') ; and git push
if test $status -ne 0
	notify-send "Pushing $name failed." -u critical -i weather-severe-alert
end
