module Main where

import LittleScholar.Parse
import LittleScholar.Parse.PDF
import LittleScholar.Parse.Markdown
import LittleScholar.Fetch
import LittleScholar.Reference
import LittleScholar.Configuration
import qualified LittleScholar.Database as DB

import qualified Data.Map as M
import qualified Data.Set as S
import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.IO.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.State
import Data.List
import Data.Maybe
import Data.Either
import System.Environment
import System.Exit
import System.FilePath
import System.Directory
import System.Process

pdftotextPath = "pdftotext"

main = do
  args <- getArgs
  eAction <- runExceptT $ parseAction args
  case eAction of
    Left err -> print err
    Right (action, conf) -> do 
      eStatus <- runExceptT $ runAction action conf
      case eStatus of
        Left err -> putStrLn err
        Right _ -> return ()

runAction action conf =
  case action of
    FetchArticle{} -> mapM_ (fetchText conf) $ aTitles action
    FetchReferences{} -> do
      fetchReferences conf (aTitle action) (aRefIDs action)
    ListInfo{} -> do
      when (aInfoReferences action) $ do
        mapM_ (\t -> readReferences conf t >>= lift . (mapM_ print)) $ aTitles action
    FillMarkdown -> do
      md <- lift getContents
      let lns = map parseMarkdown $ lines md              
          existingTitles = S.fromList $ map (\(Title t _) -> t ) $ filter isTitle lns
      lift $ fillMarkdown conf existingTitles lns >>= putStrLn
    ImportArticle{} -> do
      let params = do
            authors' <- maybe (Left "No authors specified.") (Right) (aAuthors action)
            year'    <- maybe (Left "No year specified.")    (Right) (aYear action)
            venue'   <- maybe (Left "No venue specified.")   (Right) (aVenue action)
            return (authors', year', venue')
      case params of
        Left err -> throwE err
        Right (authors, year, venue) ->
          importArticle conf (aMTitle action)
                             (aMFile action) 
                             authors
                             year
                             venue
    ReadReferences{} -> do
      refs <- readReferencesInfo conf (aTitle action) (aRefIDs action)
      liftIO $ mapM_ print refs
    RenamePDF{}  -> mapM_ (renamePDF conf)     (aFiles action)
    GuessTitle{} -> liftIO $ mapM_ (guessPDFTitle (not $ null $ drop 1 $ aFiles action) conf) (aFiles action)
    AddToCollection{} -> return ()
    Search{} -> return ()
    CitationGraph{} -> return ()
    Help -> lift $ putStrLn $ helpText

helpText = intercalate "\n" $ 
  [ "usage:"
  , "little-scholar help"
  , "little-scholar fetch-article    [Options...] <Articles...>"
  , "little-scholar fetch-references [Options...] <Article> <Reference IDs...>"
  , "little-scholar read-references  [Options...] <Article> <Reference IDs...>"
  , "little-scholar list-authors     [Options...] <Articles...>"
  , "little-scholar list-references  [Options...] <Articles...>"
  , "little-scholar list-all         [Options...] <Articles...>"
  , "little-scholar rename-pdf       [Options...] <Articles...>"
  , "little-scholar guess-title      [Options...] <Articles...>"
  , "little-scholar import-article   [Options...] <PDF> --authors <Authors> --venue <Venue> --year <Year> [--title <Title>]"
  , "little-scholar add              [Options...] <Articles...> -- <Collections...>"
  , "little-scholar search           [Options...] <Keywords...>"
  , "little-scholar citation-graph   [Options...] <Articles...>"
  , ""
  , "Available options:"
  ]

importArticle :: Configuration
              -> (Maybe String)
              -> Maybe FilePath
              -> [String]
              -> Int
              -> String
              -> ExceptT String IO ()
importArticle conf title pdf authors year venue = do
  titleE <- liftIO $ do
    if isNothing title then do
       putStrLn "Title not specified, guessing from pdf..."
       case pdf of
         Just fl -> do
           guess <- guessTitle fl
           case guess of
             Right title'' -> putStrLn title''
             Left err -> return ()
           return guess
         Nothing -> return $ Left $ "Neither pdf nor a title were provided, aborting."
    else
      return $ Right $ fromJust title
  case titleE of
    Right title' -> 
      liftIO $ evalStateT 
        (DB.addArticle title' pdf authors year venue)
        (DB.Context{DB.databaseDirectory = dropFileName $ confPdfDir conf})
    Left err -> throwE err
  return ()

readReferencesInfo conf title refIDs = do
  mPath <- liftIO $ evalStateT
    (DB.textPath title)
    (DB.Context{DB.databaseDirectory = dropFileName $ confPdfDir conf})
  liftIO $ print $ dropFileName $ confPdfDir conf
  case mPath of
    Nothing -> fail $  "Article \"" ++ title ++ "\" not found in the database."
    Just fl -> do
      txt <- liftIO $ readFile fl
      let refs = filter (\r -> maybe False (`S.member` refIDs) $ refId r ) $ references txt
      if null refs then
        fail "No references found."
      else
        return refs

fetchReferences conf title refIDs = do
  txtDest <- fetchText conf title
  txt <- lift $ readFile txtDest
  let refs = filter (\r -> maybe False (`S.member` refIDs) $ refId r ) $ references txt
  lift $ mapM_ print refs

fillMarkdown conf existingTitles (title@(Title t str):ts) = do
  let (sec, ts') = span (not . isTitle) ts
  eTxtFl <- runExceptT $ fetchText conf t
  (existingTitles', out) <- case eTxtFl of
    Left err -> return $ (existingTitles, str ++ concatMap show sec)
    Right txtFl -> do
      txt <- readFile txtFl
      let refs = M.fromList [ (fromMaybe "" $ refId r, r) | r <- references txt]
      return (formatReferenceMarkdown refs existingTitles (title:sec))
  out' <- fillMarkdown conf existingTitles' ts'
  return $ out ++ out'
fillMarkdown conf existingTitles (Other str:ts) = 
  fmap (str++) $ fillMarkdown conf existingTitles ts
fillMarkdown _ _ [] = return []

formatReferenceMarkdown refs existingTitles (el@(ReferenceId refName):rs)
  | isJust mRef && (not $ refTitle ref `S.member` existingTitles) = 
    let refStr = intercalate "\n"
            [ "## " ++ refTitle ref
            , "Authors: " ++ (intercalate ", " $ refAuthors ref)
            , ""
            ]
        (existingTitles', str') = formatReferenceMarkdown refs (S.insert (refTitle ref) existingTitles) rs
    in (existingTitles', refStr ++ str')
  | otherwise = 
    let (existingTitles', str') = formatReferenceMarkdown refs existingTitles rs
    in (existingTitles', show el ++ str')
  where
    mRef = M.lookup refName refs
    Just ref = mRef
formatReferenceMarkdown refs existingTitles (el:rs) =
  let (existingTitles', str') = formatReferenceMarkdown refs existingTitles rs
  in (existingTitles', show el ++ str')
formatReferenceMarkdown _ existingTitles [] = (existingTitles, "")

readReferences conf title = do
  txtDest <- fetchText conf title
  fl <- lift $ readFile $ txtDest
  return $ references fl

textFile conf title = (confTextDir conf) </> title <.> "txt"

fetchText :: Configuration -> String -> ExceptT String IO FilePath
fetchText conf title = do
  ex <- lift $ doesFileExist txtDest
  if ex then do
    lift $ putStrLn $  "Article already downloaded and stored at " ++ pdfDest ++ "\n"
  else do
    fetch title pdfDest (confWorkingDir conf)
    convertPdfToText pdfDest txtDest
    return ()
  return $ txtDest
  where
    pdfDest = confPdfDir conf </> title <.> "pdf"
    txtDest = confTextDir conf </> title <.> "txt"

renamePDF :: Configuration -> FilePath -> ExceptT String IO ()
renamePDF conf pdf = do
  guess <- liftIO $ guessTitle pdf
  case guess of
    Right title -> do
      let title' = map (\c -> if c `elem` ['/', '.'] then ' ' else c) title
      liftIO $ renameFile pdf ((takeDirectory pdf) </> title' <.> "pdf")
    Left err -> throwE err

guessPDFTitle :: Bool -> Configuration -> FilePath -> IO ()
guessPDFTitle showFile conf pdf = do
  guess <- guessTitle pdf
  case guess of 
    Right title -> do
      if showFile then
        putStrLn $ pdf ++ ":\n  " ++ title
      else
        putStrLn $ title
    Left err -> do
      if showFile then
        putStrLn $ pdf ++ ":\n  " ++ err
      else
        putStrLn $ err

convertPdfToText :: FilePath -> FilePath -> ExceptT String IO Bool
convertPdfToText pdf txt = do
  c <- lift $ do
    print [pdf,txt]
    ph <- spawnProcess pdftotextPath [pdf,txt]
    waitForProcess ph
  case c of
    ExitSuccess -> return True
    ExitFailure _ -> throwE "Could not convert pdf to txt."

saveGraph fname graph = do
  writeFile fname $ intercalate "\n" $
    "digraph citation {" : 
    [ v ++ " -> { " ++ intercalate "," us ++ "};"
    | (v',su) <- M.assocs graph
    , let us = map enquote $ S.toList su
    , let v = enquote v'
    ] ++
    ["}"]

enquote s = '"' : enquote' s ++ "\""
  where
    enquote' ('"':ss) = "\\\"" ++ enquote' ss
    enquote' (s:ss) = s : enquote' ss
    enquote' [] = []

simpleRef title = Reference{refId = Just "", refTitle = title, refAuthors = [], refDate = Nothing, refVenue = Nothing}
