module LittleScholar.Utils where

import Data.List

splitStr token str = splitStr' [] str
  where
    n = length token
    splitStr' piece str
      | null str = if null piece then [] else [piece]
      | token `isPrefixOf` str = (reverse piece) : splitStr' [] (drop n str)
      | otherwise = splitStr' (head str : piece) $ tail str

splitChar _ [] = []
splitChar c str = 
  let (w, ss) = span (/=c) str
  in w : splitChar c (tail ss)
