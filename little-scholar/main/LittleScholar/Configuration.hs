module LittleScholar.Configuration
        ( loadConfig
        , Configuration(..)
        , Action(..)
        , parseAction
        )
where

import LittleScholar.Utils
import System.Directory
import System.FilePath
import qualified Data.Map as M
import qualified Data.Set as S
import qualified Language.Dot.Graph as D
import qualified Language.Dot.Parser as D
import qualified Language.Dot.Utils  as D
import Control.Monad.Trans.Except
import Control.Monad.IO.Class
import Control.Monad
import Text.Read
import Data.Char

data Configuration = 
  Configuration
  { confPdfDir     :: FilePath
  , confTextDir    :: FilePath
  , confGraphDir   :: FilePath
  , confWorkingDir :: FilePath
  , confDatabaseFile :: FilePath
  , confHelp :: Bool
  , confAllowFetch :: Bool
  , confRadius :: Maybe Int
  }
  deriving (Show, Eq)

data Action = 
    FetchArticle
    { aTitles :: [String]
    }
  | FetchReferences
    { aTitle  :: String
    , aRefIDs :: S.Set String
    }
  | ListInfo
    { aTitles         :: [String]
    , aInfoReferences :: Bool
    , aInfoAuthors    :: Bool
    }
  | FillMarkdown
    {
    }
  | AddToCollection
    { aTitles      :: [String]
    , aCollections :: [[String]]
    }
  | Search
    { aQuery :: S.Set String
    }
  | CitationGraph
    { aTitles :: [String]
    }
  | ImportArticle
    { aMTitle  :: Maybe String
    , aMFile   :: Maybe FilePath
    , aAuthors :: Maybe [String]
    , aYear    :: Maybe Int
    , aVenue   :: Maybe String
    }
  | ReadReferences
    { aTitle :: String
    , aRefIDs :: S.Set String
    }
  | RenamePDF
    { aFiles :: [FilePath]
    }
  | GuessTitle
    { aFiles :: [FilePath]
    }
  | Help

parseAction :: [String] -> ExceptT String IO (Action, Configuration)
parseAction [] = return (Help, defaultConfiguration)
parseAction (aStr:ps) = do
  (conf, ps') <- loadConfig ps
  let action = case map toLower $ aStr of
            "fetch-article" -> FetchArticle{aTitles = ps'}
            "fetch-references" ->
              if null ps' then
                Help
              else
                FetchReferences{aTitle = head ps', aRefIDs = S.fromList $ drop 1 ps'}
            "read-references" -> case ps' of
              (title : refIDs) -> 
                ReadReferences{aTitle = title, aRefIDs = S.fromList refIDs}
              [] ->
                Help
            "list-references" -> ListInfo{aTitles = ps', aInfoReferences = True, aInfoAuthors = False}
            "list-authors"    -> ListInfo{aTitles = ps', aInfoReferences = False, aInfoAuthors = True}
            "list-all"        -> ListInfo{aTitles = ps', aInfoReferences = True, aInfoAuthors = True}
            "fill-markdown"   -> FillMarkdown
            "add" ->
              let (titles, collections) = span (/="--") ps'
              in AddToCollection{aTitles = titles, aCollections = map (splitChar '/') $ tail collections}
            "search" -> Search{aQuery = S.fromList $ ps'}
            "citation-graph" -> CitationGraph{aTitles = ps'} 
            "import-article" -> parseImportArticle 
                                  ImportArticle
                                  { aMTitle  = Nothing
                                  , aMFile   = Nothing
                                  , aAuthors = Nothing 
                                  , aYear    = Nothing 
                                  , aVenue   = Nothing 
                                  }
                                  ps'
            "rename-pdf" -> case ps' of
              [] ->
                Help
              pdfs -> 
                RenamePDF{aFiles = pdfs}
            "guess-title" -> case ps' of
              [] ->
                Help
              pdfs -> 
                GuessTitle{aFiles = pdfs}
            "help" -> Help
            _ -> Help
  return (action, conf)

parseImportArticle action args =
  case args of
    ("--authors" : author : args') -> 
      parseAuthorList action{ aAuthors = (fmap (author:) $ aAuthors action)
                                         `mplus` (Just [author])}
                      args'
    ("--year" : year : args') ->
      parseImportArticle action{aYear = Just (read year)} args'
    ("--venue" : venue : args') ->
      parseImportArticle action{aVenue = Just venue} args'
    ("--title" : title : args') ->
      parseImportArticle action{aMTitle = Just title} args'
    (pdf : args') ->
      if aMFile action == Nothing then
        parseImportArticle action{aMFile = Just pdf} args'
      else
        Help
    [] -> action{aAuthors = fmap reverse $ aAuthors action}
  where
    parseAuthorList action args =
      case args of
        ("--authors" : _) -> parseImportArticle action args
        ("--venue" : _)   -> parseImportArticle action args
        ("--year" : _)    -> parseImportArticle action args
        ("--title" : _)   -> parseImportArticle action args
        ("--" : args')    -> parseImportArticle action args'
        [] -> action{aAuthors = fmap reverse $ aAuthors action}
        (author : args') -> parseAuthorList action{aAuthors = fmap (author : ) $ aAuthors action} args'
      

loadConfig args = do
  confDir <- liftIO $ getXdgDirectory XdgConfig "little-scholar"
  dataDir <- liftIO $ getXdgDirectory XdgData "little-scholar"
  let (conf, args') = parseArgs defaultConfiguration{ confPdfDir     = dataDir </> "pdfs"
                                                    , confTextDir    = dataDir </> "txt"
                                                    , confWorkingDir = dataDir </> "working"
                                                    , confGraphDir   = dataDir </> "graph"}
                               args
  liftIO $ do 
    createDirectoryIfMissing True (confTextDir conf)
    createDirectoryIfMissing True (confPdfDir conf)
    createDirectoryIfMissing True (confWorkingDir conf)
    createDirectoryIfMissing True (confGraphDir conf)
  return $ (conf, args')

--sortInputs conf = do
--  files <- mapM (\fl -> fmap ((,) fl) $ doesFileExist fl) $ confFetchTitles conf :: IO [(String, Bool)]
--  let pdfs = filter (\(fl, ex) -> ex && (takeExtension fl) == ".pdf") files :: [(String, Bool)]
--      txts = filter (\(fl, ex) -> ex && (takeExtension fl) == ".txt") files
--      titles = filter (\(fl, ex) -> not ex) files
--  return conf{confFetchTitles = map fst titles, confPdfs = map fst pdfs, confTextfiles = map fst txts}

parseArgs conf args = parseArgs' conf args []
parseArgs' conf args otherArgs = case args of
  "--pdf-directory":pdfDir:as     -> parseArgs' conf{confPdfDir = pdfDir} as otherArgs
  "--text-directory":textDir:as   -> parseArgs' conf{confTextDir = textDir} as otherArgs
  "--graph-directory":graphDir:as -> parseArgs' conf{confGraphDir = graphDir} as otherArgs
  "--radius":r:as                 -> parseArgs' conf{confRadius = readMaybe r} as otherArgs
  "-r":r:as                       -> parseArgs' conf{confRadius = readMaybe r} as otherArgs
  "--database":dbFile:as          -> parseArgs' conf{confDatabaseFile = dbFile} as otherArgs
  "--help":as                     -> parseArgs' conf{confHelp = True}  as otherArgs
  "-h":as                         -> parseArgs' conf{confHelp = True}  as otherArgs
  a:as                            -> parseArgs' conf as (a : otherArgs)
  []                              -> (conf, reverse otherArgs)

toRef ref = filter (not . (`elem` "[]")) ref

loadDB dbFile
  | takeExtension dbFile == ".gr" = do
    fl <- liftIO $ readFile dbFile
    case D.parse fl of
      Left msg -> throwE msg
      Right gr ->
        let (ns, es) = D.adjacency gr
        in return $ foldr (\(D.Edge u v _) m -> M.insertWith S.union u (S.singleton v) m)
                          (foldr (\(D.Node n _) m -> M.insert n S.empty m) M.empty $ ns)
                          es
  | otherwise = throwE $ "Unknown database extension: " ++ takeExtension dbFile

defaultConfiguration = 
  Configuration
  { confPdfDir   = ".little-scholar/pdfs"
  , confTextDir  = ".little-scholar/txt"
  , confGraphDir = ".little-scholar/graph"
  , confWorkingDir = ".little-scholar/working"
  , confDatabaseFile = ".little-scholar/db.gr"
  , confHelp = False
  , confRadius = Nothing
  , confAllowFetch = False
  }



