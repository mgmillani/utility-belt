module LittleScholar.Parse
       ( references
       )
where

import LittleScholar.Reference
import LittleScholar.Parse.Internal
import Text.Parsec hiding ((<|>), many, optional)
import Data.Either
import Data.List

references :: String -> [Reference]
references str = 
  [ r
  | er <- map (parse p_reference "") $ tails $ map (\c -> if c == '\n' then ' ' else c) str
  , let Right r = er
  , isRight er
  ]

