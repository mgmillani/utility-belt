module LittleScholar.Parse.Markdown
       ( MarkdownRef(..)
       , parseMarkdown
       , isTitle
       )
where

import Control.Monad (liftM, liftM2, liftM3, mplus, replicateM, forM)
import Control.Applicative
import Text.Parsec hiding ((<|>), many, optional)
import Text.Parsec.String
import Text.Parsec.Char hiding (crlf)
import Data.Char
import Data.Maybe
import Data.Either
import Data.List

data MarkdownRef =
    Title String String
  | ReferenceId String
  | Other String
  deriving (Eq)

isTitle (Title _ _) = True
isTitle _ = False

instance Show MarkdownRef where
  show (Title _ str) = str ++ "\n"
  show (ReferenceId str) = '[' : str ++ "]\n"
  show (Other str) = str ++ "\n"

parseMarkdown line = fromRight (Other "") $ parse (p_markdown line) "" line

p_markdown :: String -> Parser MarkdownRef
p_markdown line = 
  (p_title line) <|> p_ref <|> (Other <$> many anyChar)

p_title :: String -> Parser MarkdownRef
p_title line = 
  makeTitle <$> (char '#' *> (many $ char '#') *> spaces *> (many (satisfy (/='\n'))))
  where
    makeTitle t = Title (removeYear t) line
    removeYear str@(s:ss)
      | not $ null $ drop 6 str = case (s, removeYear ss) of
        (' ', "") -> ""
        (c,cs) -> c:cs
      | otherwise = case str of
        '(':a:b:c:d:")" -> ""
        _ -> trimEnd str

p_ref :: Parser MarkdownRef
p_ref = ReferenceId <$> (char '[' *> (many (satisfy (/=']'))) <* char ']')

trimEnd [] = []
trimEnd (' ':ss) = case trimEnd ss of
  "" -> ""
  str -> ' ':str
trimEnd (s:ss) = s : trimEnd ss
