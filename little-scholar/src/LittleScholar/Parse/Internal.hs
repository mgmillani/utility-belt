module LittleScholar.Parse.Internal where

import LittleScholar.Reference
import Control.Monad (liftM, liftM2, liftM3, mplus, replicateM, forM)
import Control.Applicative
import Text.Parsec hiding ((<|>), many, optional)
import Text.Parsec.String
import Text.Parsec.Char hiding (crlf)
import Data.Char
import Data.Maybe
import Data.Either
import Data.List

p_reference :: Parser Reference
p_reference = do
  iD <- p_refId
  spaces
  authors <- p_nameList
  spaces
  year <- optionMaybe p_year
  spaces
  title <- p_sentence
  spaces
  if null authors then
    unexpected "Empty author list."
  else
    return $ Reference{refId = Just iD, refAuthors = authors, refTitle = title, refDate = year, refVenue = Nothing}

p_refId :: Parser String
p_refId = char '[' *> many alphaNum <* char ']'

p_sentence :: Parser String
p_sentence = (:) <$> upper <*> many (satisfy (/= '.')) <* char '.'

p_nameList :: Parser [String]
p_nameList =
      (oneOf ".:" *> return [])
  <|> (lookAhead (char '(') *> return [])
  <|> ((:) <$> p_name <*> (spaces *> (((    string ", " <* optional (string "and"))
                                        <|> string "and")
                                      *> spaces *> p_nameList
                                     )
                                     <|> (oneOf ".:" *> return [])
                                     <|> (lookAhead (char '(') *> return [])
                          )
      )

p_name :: Parser String
p_name = do
  n0 <- p_namePart
  case n0 of
    [c]        -> (\n -> c:' ':intercalate " " n) <$> p_firstLastName
    (c:'.':[]) -> (\n -> c:' ':intercalate " " n) <$> p_firstLastName
    someName   ->     ((\n -> intercalate " " n ++ " " ++ someName) <$> p_lastFirstName)
                  <|> ((\n -> someName ++ ' ':intercalate " " n) <$> p_firstLastName)

p_firstLastName :: Parser [String]
p_firstLastName = spaces *> many (p_namePart <* spaces)

p_namePart :: Parser String
p_namePart = 
             (:) <$> upper
             <*> (     (char '.' *> return [])
                   <|> ((many (satisfy (\c -> isAlpha c || isMark c || c == '-' || c == '\''))) <* spaces))
                   <|> (char ' ' *> spaces *> return [])

p_lastFirstName :: Parser [String]
p_lastFirstName = char ',' *> spaces *> many (p_namePart <* spaces)

p_year :: Parser String
p_year = char '(' *> many digit <* char ')'
