module LittleScholar.Parse.PDF.Internal where

import Prelude hiding (Either(Left), Word)
import qualified Prelude as P (Either(..))
import Control.Exception
import Data.Char
import Data.List
import Data.Maybe
import Data.Either hiding (Either(..))
import LittleScholar.Utils
import qualified Data.Array.IArray as A
import qualified Data.Map as M
import System.IO.Temp
import System.Directory
import System.Process
import Text.Read (readEither)

data Field  = Level
            | Page
            | Par
            | Block
            | Line
            | Word
            | Left
            | Top
            | Width
            | Height
            | Conf
            | Text
            deriving (Eq, Ord, Show, Enum)

instance A.Ix Field where
  range (f0, f1) = [f0 .. f1]
  index (f0, f1) f2 = (fromEnum f2) - (fromEnum f0)
  inRange (f0, f1) f2 = 
    let i2 = fromEnum f2
    in i2 >= (fromEnum f0) && i2 <= (fromEnum f1)
  rangeSize (f0, f1) = (fromEnum f1) - (fromEnum f0) + 1

data BoundingBox = 
  BoundingBox
  { bbLevel  :: Int
  , bbPage   :: Int
  , bbPar    :: Int
  , bbBlock  :: Int
  , bbLine   :: Int
  , bbWord   :: Int
  , bbLeft   :: Float
  , bbTop    :: Float
  , bbWidth  :: Float
  , bbHeight :: Float
  , bbConf   :: Int
  , bbText   :: String
  }
  deriving (Eq, Show)

data Score = 
  Score
  { scAspectRatio :: Float
  , scHeight :: Float
  , scTop :: Float
  , scPage :: Int
  , scBadWords :: Int
  }
  deriving (Eq, Show)

instance Ord Score where
  compare s0 s1 =
    case compare (scBadWords s1) (scBadWords s0) of
      EQ -> 
        case compare (scHeight s0) (scHeight s1) of
          -- If equal, take the one which appears on the earliest page.
          -- We flip s1 and s0 here because we want the maximum score to be the best choice
          EQ -> case compare (scPage s1) (scPage s0) of
            EQ -> compare (scTop s0) (scTop s1)
            c -> c
          c -> c
      c -> c

guessTitle :: FilePath -> IO (P.Either String String)
guessTitle fl = do
  tsv <- generateBoundingBoxes fl
  case fmap lines tsv of
    Right (header : entries) ->
      case parseHeader header of
           P.Right combiners ->
             let
                 boxes = filter (\bb -> bbConf bb == 100) $ rights $ map (boundingBox combiners) $ entries
                 candidates = foldr (\bbx m -> M.insertWith (++) (bbLevel bbx, bbPage bbx, bbPar bbx, bbBlock bbx) [bbx] m) M.empty boxes
                 scores = map (\candidate -> (candidate, score candidate)) $ M.elems candidates
                 candidates' = filter (\(t, sco) -> let len = length t in
                                         scBadWords sco < 4
                                      && len > 1
                                      && len < 50
                                      && scAspectRatio sco > 1
                                      )
                                      scores
                 largest = maximum $ map (\(_, sco ) -> scHeight sco) candidates'
                 candidates'' = filter (\(t, sco) -> scHeight sco > largest * 0.7)
                                      candidates'
             in
             -- TODO:
             -- Consider other properties, not only text size.
             -- For example, words like 'Journal' probably will not appear in the title of a paper.
             if null candidates'' then
                return $ P.Left $ "No suitable title found for " ++ fl ++ "."
             else
                return $ P.Right $ toString $ fst $ maximumBy (\(_, s0) (_, s1) -> compare s0 s1) candidates''
           P.Left err -> return $ P.Left err
    P.Right _ -> return $ P.Left $ "TSV file for " ++ fl ++ " does not contain a header. Aborting."
    P.Left err -> return $ P.Left $ "Error guessing the title of " ++ fl ++ ":\n\t" ++ err

generateBoundingBoxes fl = do
  boxesFl <- emptySystemTempFile "little-scholar.txt"
  catch
    ( do
      callProcess "pdftotext" ["-f", "1", "-l", "2", "-tsv", fl, boxesFl]
      str <- readFile boxesFl
      removeFile boxesFl
      return $ P.Right str
    )
    (\e -> do
      removeFile boxesFl
      return $ P.Left (show (e :: IOException))
    )

score :: [BoundingBox] -> Score
score candidate = Score
  { scHeight = median $ map bbHeight candidate
  , scTop = minimum $ map bbTop candidate
  , scPage = minimum $ map bbPage candidate
  , scAspectRatio = sum (map (\bb -> bbWidth bb / bbHeight bb) candidate) / (fromIntegral $ length candidate)
  , scBadWords = length $ filter hasBadWord candidate
  }

badWords = 
  [ "downloaded"
  , "licensed"
  , "journal"
  , "copyright"
  , "unauthorized"
  , "proceedings"
  , "©"
  ]

hasBadWord bb = ("arXiv" `isPrefixOf` (bbText bb)) || ((map toLower (bbText bb)) `elem` badWords)

parseHeader header = 
  let allFields = map 
        (\field ->
            case field of
                 "level"     -> Just Level               
                 "page_num"  -> Just Page
                 "par_num"   -> Just Par
                 "block_num" -> Just Block
                 "line_num"  -> Just Line
                 "word_num"  -> Just Word
                 "left"      -> Just Left
                 "top"       -> Just Top
                 "width"     -> Just Width
                 "height"    -> Just Height
                 "conf"      -> Just Conf
                 "text"      -> Just Text
                 _ -> Nothing
        )
        $ splitBy ('\t'==) $ map toLower header
      knownFields = map fromJust $ filter isJust allFields
      existingFields = A.array (Level, Text) 
                               $ [(f, False) | f <- [Level .. Text]]
                               ++ zip knownFields (repeat True) :: A.Array Field Bool
      missingFields = map fst $ filter (not . snd) $ A.assocs existingFields
  in
  if null missingFields then
    P.Right $ 
      map (\field -> 
              case field of
                   Just Level  -> (\str bbx -> do
                                               bbx' <- bbx
                                               n <- readEither str
                                               return bbx'{bbLevel = n})
                   Just Page   -> (\str bbx -> do
                                               bbx' <- bbx
                                               n <- readEither str
                                               return bbx'{bbPage = n})
                   Just Par    -> (\str bbx -> do
                                               bbx' <- bbx
                                               n <- readEither str
                                               return bbx'{bbPar = n})
                   Just Block  -> (\str bbx -> do
                                               bbx' <- bbx
                                               n <- readEither str
                                               return bbx'{bbBlock = n})
                   Just Line   -> (\str bbx -> do
                                               bbx' <- bbx
                                               n <- readEither str
                                               return bbx'{bbLine = n})
                   Just Word   -> (\str bbx -> do
                                               bbx' <- bbx
                                               n <- readEither str
                                               return bbx'{bbWord = n})
                   Just Left   -> (\str bbx -> do
                                               bbx' <- bbx
                                               n <- readEither str
                                               return bbx'{bbLeft = n})
                   Just Top    -> (\str bbx -> do
                                               bbx' <- bbx
                                               n <- readEither str
                                               return bbx'{bbTop = n})
                   Just Width  -> (\str bbx -> do
                                               bbx' <- bbx
                                               n <- readEither str
                                               return bbx'{bbWidth = n})
                   Just Height -> (\str bbx -> do
                                               bbx' <- bbx
                                               n <- readEither str
                                               return bbx'{bbHeight = n})
                   Just Conf   -> (\str bbx -> do
                                               bbx' <- bbx
                                               n <- readEither str
                                               return bbx'{bbConf = n})
                   Just Text   -> (\str bbx -> do
                                               bbx' <- bbx
                                               return bbx'{bbText = str})
                   Nothing -> (\_ bbx -> bbx)
          )
          $ allFields
  else
    P.Left $ "The following fields are missing: " ++ (intercalate ", " $ map show missingFields)

boundingBox combiners line = 
  let fields = splitBy ('\t'==) line
  in
  foldr (\(c,f) bbx ->
              c f bbx
        )
        (P.Right BoundingBox{}) $
        zip combiners fields

toString :: [BoundingBox] -> String
toString boxes = intercalate " " $ map bbText $ 
  sortBy
    (\b0 b1 ->
      case compare (bbBlock b0) (bbBlock b1) of
        EQ -> case compare (bbLine b0) (bbLine b1) of
          EQ -> compare (bbWord b0) (bbWord b1)
          c -> c
        c -> c
    )
    boxes
