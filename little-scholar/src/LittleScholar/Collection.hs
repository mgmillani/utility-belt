module LittleScholar.Collection
        ( (C.!)
        , C.Collection
        , C.delete
        , C.elems
        , C.empty
        , C.encode
        , C.insert
        , C.keys
        , C.lookup
        , C.parse
        , C.assocs
        )
where

import qualified LittleScholar.Collection.Internal as C
