module LittleScholar.Utils
       ( splitBy
       , median
       )
where

import Data.List

-- | Split a list by the given predicate.
splitBy :: (a -> Bool) -> [a] -> [[a]]
splitBy _ [] = []
splitBy p xs =
  let (w, r) = break p (dropWhile p xs)
  in w : splitBy p r

-- | The median value in a list of numbers.
-- The list does not need to be sorted.
median :: (Ord a, Fractional a) => [a] -> a
median xs =
  let xs' = sort xs
      n = length xs
      ys = drop ((n - 1) `div` 2) xs'
  in
  if n `mod` 2 == 1 then
    head ys
  else
    (sum $ take 2 ys) / 2
