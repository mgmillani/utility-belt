module LittleScholar.Fetch.Utils where

import qualified Data.Map as M
import Network.URI (escapeURIString, isAllowedInURI, isUnreserved)
import Text.HTML.TagSoup.Entity
import System.Process
import System.IO
import Control.Monad
import Control.Concurrent

escapeURI = escapeURIString (\c -> isAllowedInURI c && isUnreserved c)

unescapeHTML ('&':xs) = 
  let (code, rs) = span (/=';') xs
  in case M.lookup (code ++ ";") htmlEntityMap of
    Just str -> str ++ unescapeHTML (tail rs)
    Nothing -> '&' : unescapeHTML xs
unescapeHTML [] = []
unescapeHTML (x:xs) = x : unescapeHTML xs

htmlEntityMap = M.fromList htmlEntities

wget url dest = do
  (_, Just sout, Just serr, ph) <- createProcess $
    (proc "wget" ["-U", "Mozilla", url, "--output-document", dest])
      { std_out = CreatePipe
      , std_err = CreatePipe
      }
  forkIO $ do
    outStr <- hGetContents sout
    seq (length outStr) $ return ()
  forkIO $ do
    errStr <- hGetContents serr
    when (not $ null errStr) $ 
      withFile "error" AppendMode $ \h -> do
        hPutStrLn h "wget:"
        hPutStrLn h url
        hPutStrLn h dest
        hPutStrLn h errStr
    seq (length errStr) $ return ()
  waitForProcess ph
