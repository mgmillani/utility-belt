module LittleScholar.Fetch.DBLP
where

publicationQuery query = "https://dblp.org/search/publ/api?q="
                         ++ (intercalate "+" $ words $ map toLower query)
                         ++ "&format=json"

authorQuery query = "https://dblp.org/search/author/api?q="
                  ++ (intercalate "+" $ words $ map toLower query)
                  ++ "&format=json"

venueQuery query = "https://dblp.org/search/venue/api?q="
                    ++ (intercalate "+" $ words $ map toLower query)
                    ++ "&format=json"
