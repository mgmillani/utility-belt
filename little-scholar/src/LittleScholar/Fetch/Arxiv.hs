{-# LANGUAGE OverloadedStrings #-}

module LittleScholar.Fetch.Arxiv
       ( fetch
       , pdfUrl
       , fetchPdfUrl
       , listEntries
       , rankEntries
       , Entry(..)
       )
where

import LittleScholar.Fetch.Utils

import Data.Time
import Control.Concurrent
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.State
import Data.List
import Data.Char
import Data.Maybe 
import Network.HTTP.Client
import Network.HTTP.Types.Status
import System.Directory
import System.Exit
import System.FilePath
import System.Process
import Text.HTML.TagSoup
import qualified Data.Map as M
import qualified Data.Set as S
import qualified Data.Text               as T
import qualified Data.Text.Encoding      as T
import qualified Data.Text.Lazy          as TL
import qualified Data.Text.Lazy.Encoding as TL

data Context = Context
  { cxLastRequest :: UTCTime
  , cxRequestInterval :: Int
  }

data Entry = Entry
  { enTitle           :: String
  , enPDFURL          :: String
  , enAuthors         :: [String]
  , enPublicationDate :: String
  }
  deriving (Eq, Show)

fetchQuery title = "http://export.arxiv.org/api/query?search_query=tl:" ++ escapeURI title ++ "&max_results=25"

fetch :: String -> FilePath -> ExceptT String IO FilePath
fetch title workingDir = do
  lift $ do
    createDirectoryIfMissing True (workingDir </> "arxiv")
    putStrLn $ "Looking up on arXiv..."
    putStrLn $ fetchQuery title
  fetchPdf (map (map toLower) $ words title) fname $ fetchQuery title
  return fname
  where
    fname = workingDir </> "arxiv" </> title <.> "pdf"

fetchPdf :: [String] -> FilePath -> String -> ExceptT String IO Bool
fetchPdf queryWords fname searchUrl = do
  lift $ threadDelay $ 5 * 10^6
  pdf' <- fetchPdfUrl queryWords searchUrl
  case pdf' of
    Nothing -> throwE $ "PDF for " ++ fname ++ " not found."
    Just urlPdf -> do
      c <- lift $ do
        wget urlPdf fname
      case c of
        ExitFailure _ -> throwE $ "Failed to fetch " ++ urlPdf
        ExitSuccess -> return True

fetchPdfUrl :: [String] -> String -> ExceptT String IO (Maybe String)
fetchPdfUrl queryWords url = do
  response <- lift $ do
    manager <- newManager defaultManagerSettings
    request <- parseRequest url
    httpLbs request manager
  if (responseStatus response) == ok200 then
    either (throwE . show)
           (\tStr -> do
              let str = TL.unpack tStr
                  result = pdfUrl queryWords str
              case result of
                Just entry -> lift $ do
                  putStrLn "\nResult on arXiv:\n"
                  putStrLn $ pretty entry
                  putStrLn ""
              return $ fmap enPDFURL result
           )
           (TL.decodeUtf8' $ responseBody response)
  else
    throwE $ either show T.unpack $ T.decodeUtf8' $ statusMessage $ responseStatus response

pdfUrl :: [String] -> String -> Maybe Entry
pdfUrl queryWords str = 
  case M.lookupMax scores of
    Just (s, entry) -> 
      if s > (-1) * (length queryWords) then
        Just $ head entry
      else
        Nothing
    Nothing -> Nothing
  where
    querySet = S.fromList queryWords
    entries = listEntries $ parseTags str
    scores = rankEntries entries querySet

rankEntries :: [Entry] -> S.Set String -> M.Map Int [Entry]
rankEntries entries querySet = 
  foldr (\(s,e) m -> M.insertWith (++) s [e] m) M.empty
                     [ (score, entry)
                     | entry <- entries
                     , let score = resultScore querySet $ enTitle entry
                     ]

listEntries :: [Tag String] -> [Entry]
listEntries tgs = 
  let mresult = runStateT nextEntry tgs
  in case mresult of
          Just (entry, tgs') -> entry : listEntries tgs'
          Nothing -> []

nextEntry = do
  findTag (isOpen "entry")
  let entry0 = Entry{enTitle = "", enAuthors = [], enPublicationDate = "", enPDFURL = ""}
  entry <- constructEntry entry0  
  findTag (isClose "entry")
  return entry

constructEntry entry = do
  tgs <- get
  case tgs of
    (TagOpen "title" _) : (TagText title) : rs -> do
      put rs
      findClose "title"
      constructEntry entry{enTitle = intercalate " " $ words title}
    (TagOpen "author" _) : rs -> do
      put rs
      mname <- getName
      findClose "author"
      constructEntry $ 
        if isNothing mname then
          entry 
        else
          entry{enAuthors = (fromJust mname) : enAuthors entry}
    (TagOpen "link" attrs) : rs -> do
      put rs
      if isJust $ lookup "title" attrs then
        let url = snd $ head $ filter ((=="href") . fst) attrs
        in constructEntry entry{enPDFURL = url}
      else
        constructEntry entry
    ((TagOpen "updated" _) : (TagText publicationDate) : rs) -> do
      put rs
      findClose "updated"
      constructEntry entry{enPublicationDate = publicationDate}
    (TagClose "entry") : rs -> return entry
    _ -> do
      put $ tail tgs
      constructEntry entry

getName = do
  tgs <- get
  case tgs of
    (TagClose "author") : rs -> do
      put rs
      return Nothing
    (TagOpen "name" _) : (TagText name) : rs  -> do
      put rs
      return $ Just name
    _ -> do
      put $ tail tgs
      getName

resultScore queryWords title = 
  let titleWords = map (map toLower) $ words title
      hits = filter (`S.member` queryWords) titleWords
      missing = queryWords `S.difference` S.fromList hits
  in (S.size missing) * (-3) + (length titleWords - length hits) * (-1)


findTag :: (Tag String -> Bool) -> StateT [Tag String] Maybe (Tag String)
findTag f = do
  tags <- get
  let rs = dropWhile (not . f) tags
  if null rs then
    fail []
  else do
    put $ tail rs
    return $ head rs

findClose :: String -> StateT [Tag String] Maybe (Tag String)
findClose str = findTag (isClose str)

{-findOpen :: String -> StateT [Tag String] Maybe (Tag String)
findOpen str = findTag (isOpen str)-}

isClose x (TagClose tag) = tag == x
isClose _ _ = False

isOpen x (TagOpen tag _) = tag == x
isOpen _ _ = False

pretty entry = intercalate "\n"
  [ "Title: " ++ enTitle entry
  , "Authors: " ++ intercalate "," (enAuthors entry)
  , "Date: " ++ enPublicationDate entry
  ]
