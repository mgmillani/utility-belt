{-# LANGUAGE OverloadedStrings #-}

module LittleScholar.Fetch.GoogleScholar
       ( fetch
       , pdfUrl
       )
where

import LittleScholar.Reference
import LittleScholar.Fetch.Utils

import Text.HTML.Scalpel
import Text.HTML.TagSoup
import Text.HTML.TagSoup.Entity
import Network.URI (escapeURIString, isAllowedInURI, isUnreserved)
import Data.Function
import Data.Maybe
import qualified Data.Map as M
import System.Process
import System.Exit
import System.Directory
import System.FilePath
import Control.Monad.Trans.Except
import Control.Monad.Trans.Class
import Control.Concurrent

fetchQuery q = "https://scholar.google.com/scholar?hl=en&as_sdt=0%2C5&q=" ++ escapeURI q ++ "&btnG="

fetch :: String -> FilePath -> ExceptT String IO FilePath
fetch title workingDir = do
  lift $ createDirectoryIfMissing True (workingDir </> "google-scholar")
  fetchPdf fname $ fetchQuery title
  return fname
  where
    fname = workingDir </> "google-scholar" </> title <.> "pdf"

-- fetchPdf fname [] = throwE $ "PDF for " ++ fname ++ " not found."
fetchPdf fname searchUrl = do
  lift $ threadDelay $ 20 * 10^6
  pdf' <- lift $ fmap (fmap unescapeHTML) $ scrapeURL searchUrl pdfUrl
  case pdf' of
    Nothing -> throwE $ "PDF for " ++ fname ++ " not found."
    Just urlPdf -> do
      c <- lift $ do
        ph <- spawnProcess "wget" ["-U", "Mozilla", urlPdf, "--output-document", fname]
        waitForProcess ph
      case c of
        ExitFailure _ -> throwE $ "Failed to fetch " ++ urlPdf
        ExitSuccess -> return True

pdfUrl :: Scraper String String
pdfUrl = chroot ("div" @: [hasClass "gs_ggsd"]) href

href :: Scraper String String
href = attr "href" $ "a" @: []
