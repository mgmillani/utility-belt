-- {-# LANGUAGE OverloadedStrings #-}

module LittleScholar.Database.Internal where

import Control.Monad
import Control.Monad.Trans.State
import Control.Monad.Trans.Class
import Control.Monad.IO.Class
import Control.Monad.Trans.Except
import qualified LittleScholar.Collection as C
import LittleScholar.Reference
import System.FilePath
import System.Directory
import System.Process
import System.IO
import System.Exit
import qualified Data.Text as T
import qualified Data.Text.Lazy as Tl
import qualified Data.Text.Lazy.IO as Tl
import qualified Data.FuzzySet as Fuzzy

data Context = Context
  { databaseDirectory :: FilePath
  }

addArticle title pdf authors year venue = do
  let reference = defaultReference
                  { refTitle = title
                  , refAuthors = authors
                  , refDate = Just $ show year
                  , refVenue = Just venue
                  }
  eCollection <- addToGlobalCollection reference
  case eCollection of
    Left msg -> return $ Left msg
    Right (_, artKey) -> do
      articleDir <- articleDirectory artKey
      let dst = articleDir </> "article" <.> ".pdf"
      exists <- liftIO $ doesFileExist dst
      if (not exists) then do
        liftIO $ do
          createDirectoryIfMissing True articleDir
          createDirectoryIfMissing True articleDir
          case pdf of
            Just file -> copyFile file dst
            Nothing -> return ()
          err <- liftIO $ generateText dst (articleDir </> "article" <.> ".txt")
          return err
      else
        return $ Right True

generateText fromPDF toTxt = runExceptT $ do
  c <- lift $ do
    ph <- spawnProcess "pdftotext" [fromPDF, toTxt]
    waitForProcess ph
  case c of
    ExitSuccess -> return True
    ExitFailure _ -> throwE $ "Could not convert file '" ++ fromPDF ++ "' to text."

articleDirectory :: (Monad m) => String -> StateT Context m FilePath
articleDirectory key = do
  cx <- get
  return $ databaseDirectory cx </> "articles" </> key

addToGlobalCollection :: Reference -> StateT Context IO (Either String (C.Collection, ArticleKey))
addToGlobalCollection reference = do
  cx <- get
  let indexFl = (databaseDirectory cx </> "index.yaml")
  globalCollection <- do
    ex <- liftIO $ doesFileExist indexFl
    if ex then do
        liftIO $ withFile indexFl ReadMode $ \h -> do
          str <- Tl.hGetContents h
          let col = C.parse str
          seq col $ return col
    else
      return $ Right C.empty
  case globalCollection of 
    Left err -> do
      return $ Left err
    Right col -> do
      let result@(collection', _) = C.insert col reference 
      liftIO $ withFile indexFl WriteMode $ \h -> do
        Tl.hPutStrLn h $ C.encode collection'
      return $ Right result

articlePath :: String -> StateT Context IO (Maybe FilePath)
articlePath title = do
  dir <- pdfDirectory
  let path = dir </> title <.> "pdf"
  exists <- liftIO $ doesFileExist dir
  if exists then
    return $ Just path
  else
    return Nothing

textPath articleKey = do
  dir <- textDirectory articleKey
  let path = dir </> "article.txt"
  exists <- liftIO $ doesFileExist path
  if exists then
    return $ Just path
  else
    return Nothing

textDirectory articleKey = do
  cx <- get
  return $ (databaseDirectory cx </> "articles" </> articleKey)

pdfDirectory = do
  cx <- get
  return $ (databaseDirectory cx </> "pdf")

searchArticle :: T.Text -> StateT Context IO (Maybe T.Text)
searchArticle query = do
  dir <- pdfDirectory
  articles <- liftIO $ fmap (map (T.pack . takeBaseName)) $ listDirectory dir
  let articleSet = foldr (flip Fuzzy.add) Fuzzy.defaultSet articles
  return $ Fuzzy.getOne articleSet $ query 


