{-# LANGUAGE OverloadedStrings #-}

module LittleScholar.Fetch
       ( fetch
       )
where

import qualified LittleScholar.Fetch.GoogleScholar as GoogleScholar
import qualified LittleScholar.Fetch.Arxiv         as Arxiv

import System.Directory
import Control.Monad.Trans.Except
import Control.Monad.Trans.Class
import Control.Concurrent

fetch :: String -> FilePath -> FilePath -> ExceptT String IO Bool
fetch title fname workingDir = do
  let workers = [ runExceptT $ GoogleScholar.fetch title workingDir
                -- , runExceptT $ Arxiv.fetch title workingDir
                ] :: [ IO (Either String FilePath)]
  lift $ do
    resultM <- newEmptyMVar
    countM  <- newMVar $ length workers
    tIds <- mapM (\f -> forkFinally f
                                    (\r -> case r of
                                      Left err -> print err >> workerFailed countM resultM
                                      Right ans -> workerSucceeded countM resultM ans
                                    )
                 )
                 workers
    pdf <- takeMVar resultM
    mapM_ killThread tIds
    case pdf of
      Nothing -> return False
      Just file -> do
        renameFile file fname
        return True
  where
    workerFailed countM resultM = do
      c <- takeMVar countM
      if c == 1 then do
        putMVar resultM Nothing
        putMVar countM 0
      else do
        putMVar countM (c - 1)
    workerSucceeded countM resultM (Left err) = print err >> workerFailed countM resultM
    workerSucceeded _ resultM (Right ans) = do
      putMVar resultM $ Just ans

