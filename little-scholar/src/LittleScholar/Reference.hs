module LittleScholar.Reference where

import Data.List
import Data.Maybe
-- import Data.Time

type ArticleKey = String

data Reference = 
  Reference
  { refId :: Maybe ArticleKey
  , refAuthors :: [String]
  , refTitle :: String
  , refDate :: Maybe String -- TODO: Use Data.Time instead
  , refVenue :: Maybe String
  }
  deriving (Eq, Ord)

instance Show Reference where
  show r = concat
    [ fromMaybe "" $ fmap (\i -> ('[' : i) ++ "]") $ refId r
    , intercalate ", " $ refAuthors r
    , ": "
    , refTitle r
    , "."
    , maybe "" (\v -> (' ' : v) ++ ".") $ refVenue r
    , maybe "" (\y -> (' ' : '(' : show y) ++ ")") $ refDate r
    ]

defaultReference = Reference
  { refId = Nothing
  , refAuthors = []
  , refTitle = "Default title"
  , refDate = Nothing
  , refVenue = Nothing
  }

generateKey :: Reference -> ArticleKey
generateKey ref = 
  let authorInitials = concatMap initial $ refAuthors ref
      titleInitials = concatMap initial $ words $ refTitle ref
  in authorInitials ++ (fromMaybe "??" $ refDate ref) ++ "-" ++ (take 5 titleInitials)

initial = (:[]) . head . last . words 
