{-# LANGUAGE OverloadedStrings #-}

module LittleScholar.Collection.Internal where

import qualified Data.Map as M
import qualified Data.Text as Ts
import qualified Data.Text.Lazy as T
import qualified Data.Text.Lazy.Encoding as T
import qualified Data.YAML as YAML
import qualified Data.FuzzySet as Fuzzy

import Data.Maybe
import LittleScholar.Reference

newtype Collection = Collection (M.Map ArticleKey Reference) deriving (Eq, Ord, Show)

empty :: Collection
empty = Collection M.empty

insert :: Collection -> Reference -> (Collection, ArticleKey)
insert (Collection c) article = 
  let key = fromMaybe (generateKey article) $ refId article
      altKeys = map (((key ++ "-") ++ ) . show) $ [1..]
      freeKey = head $ dropWhile (\k -> k `M.member` c) $ key : altKeys
  in (Collection $ M.insert freeKey article{refId = Just freeKey} c, freeKey)

insert' :: Collection -> Reference -> Collection
insert' col article  = fst $ insert col article

delete :: Collection -> ArticleKey -> Collection
delete (Collection c) key = Collection $ M.delete key c

search :: Collection -> String -> [(ArticleKey, Reference)]
search (Collection c) str = 
  filter (fuzzyMatch . snd) $ M.assocs c
  where
    fuzzyMatch ref = isJust $ Fuzzy.getOne fuzzySet (Ts.pack str)
      where
        fuzzySet = foldr (flip Fuzzy.add) 
                         Fuzzy.defaultSet $ (Ts.pack $ refTitle ref) : (map Ts.pack $ refAuthors ref)

(!) :: Collection -> ArticleKey -> Reference
(!) (Collection c) key = c M.! key

lookup :: Collection -> ArticleKey -> Maybe Reference
lookup (Collection c) key = key `M.lookup` c

elems :: Collection -> [Reference]
elems (Collection c) = M.elems c

keys :: Collection -> [ArticleKey]
keys (Collection c) = M.keys c

assocs :: Collection -> [(ArticleKey, Reference)]
assocs (Collection c) = M.assocs c

instance YAML.FromYAML Reference where
  parseYAML = YAML.withMap "Reference" $ \m -> Reference
      <$> (fmap (fmap Ts.unpack) $ m YAML..:! "key")
      <*> (fmap (maybe [] (map Ts.unpack)) $ m YAML..:! "authors")
      <*> (fmap (maybe "" Ts.unpack) $ m YAML..:! "title")
      <*> (fmap (fmap Ts.unpack) $ m YAML..:? "date")
      <*> (fmap (fmap Ts.unpack) $ m YAML..:? "venue")

instance YAML.ToYAML Reference where
  toYAML ref = YAML.mapping $
    [ "title" YAML..= (Ts.pack $ refTitle ref)
    , "authors" YAML..= (map Ts.pack $ refAuthors ref)
    ]
    ++
    (maybe [] (\key   -> ["key" YAML..= (Ts.pack key)]) $ refId ref)
    ++
    (maybe [] (\venue -> ["venue" YAML..= (Ts.pack venue)]) $ refVenue ref)
    ++
    (maybe [] (\date  -> ["date" YAML..= (Ts.pack date)]) $ refDate ref)

parse :: T.Text -> Either String Collection
parse txt = 
  let eArticles = YAML.decode $ T.encodeUtf8 txt
  in case eArticles of
    Right articles -> Right $ 
      foldr (flip insert') (Collection M.empty) articles
    Left (_, err) -> Left err

encode :: Collection -> T.Text
encode (Collection c) = T.decodeUtf8 $ YAML.encode $ M.elems c


