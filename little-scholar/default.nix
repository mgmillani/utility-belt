{ mkDerivation, base, bytestring, containers, directory, filepath
, fuzzyset_0_2_4, happy-dot, HsYAML, http-client, http-types, HUnit, lib
, mtl, network-uri, parsec, process, scalpel, tagsoup, text, time, temporary
, transformers
}:
mkDerivation {
  pname = "little-scholar";
  version = "1.0.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  libraryHaskellDepends = [
    base bytestring containers directory filepath fuzzyset_0_2_4 HsYAML
    http-client http-types mtl network-uri parsec process scalpel
    tagsoup text time transformers
  ];
  executableHaskellDepends = [
    base containers directory filepath happy-dot process transformers temporary
  ];
  testHaskellDepends = [
    base containers directory filepath http-client http-types HUnit
    network-uri parsec process tagsoup text time transformers
  ];
  doHaddock = false;
  description = "Extract and download references from scientific articles";
  license = lib.licenses.gpl3Only;
  mainProgram = "little-scholar";
}
