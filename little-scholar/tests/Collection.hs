module Main where

import Test.HUnit hiding (Node)
import System.Exit (exitFailure, exitSuccess)
import qualified Data.Set as S
import qualified Data.Text.Lazy as T
import Data.Maybe

import qualified LittleScholar.Collection.Internal as C
import LittleScholar.Reference

main = do 
  testCounts <- runTestTT tests
  if errors testCounts + failures testCounts > 0 then exitFailure else exitSuccess

tests = TestList                         
  [ TestLabel "Keys" $ TestCase
    ( do
      let ref0 = Reference
                  { refId = Just "P2000-Nt"
                  , refAuthors = ["A. Person"]
                  , refTitle = "No title"
                  , refDate = Just $ "2000"
                  , refVenue = Nothing
                  }
          ref1 = Reference
                  { refId = Just "P2001-St"
                  , refAuthors = ["A. Person"]
                  , refTitle = "Some title"
                  , refDate = Just "2001"
                  , refVenue = Nothing
                  }
          ref2 = Reference
                  { refId = Just "PAC2001-Awtlt"
                  , refAuthors = ["A. Person", "B. Another", "G. Co-Author"]
                  , refTitle = "A way too long title which will be shortened later"
                  , refDate = Just "2001"
                  , refVenue = Nothing
                  }
      assertEqual "Key 0" 
        (fromJust $ refId ref0)
        (generateKey ref0)
      assertEqual "Key 1" 
        (fromJust $ refId ref1)
        (generateKey ref1)
      assertEqual "Key 2" 
        (fromJust $ refId ref2)
        (generateKey ref2)
    )
  , TestLabel "Insert and remove 1" $ TestCase
    ( do
      let ref0 = Reference
                  { refId = Just "P2000"
                  , refAuthors = ["A. Person"]
                  , refTitle = "No title"
                  , refDate = Just $ "2000"
                  , refVenue = Nothing
                  }
          ref1 = Reference
                  { refId = Just "P2001"
                  , refAuthors = ["A. Person"]
                  , refTitle = "Some title"
                  , refDate = Just "2001"
                  , refVenue = Nothing
                  }
          col0 = C.empty
          (col1, key0) = C.insert col0 ref0
          (col2, key1) = C.insert col1 ref1
          col3 = C.delete col2 ("P2000")
      assertEqual "empty" 
        (S.empty)
        (S.fromList $ C.elems col0)
      assertEqual "Collection 1 - elements" 
        (S.fromList [ref0])
        (S.fromList $ C.elems col1)
      assertEqual "Collection 1 - keys" 
        (S.fromList $ map fromJust $ [refId ref0])
        (S.fromList $ C.keys col1)
      assertEqual "Collection 1 - new key" 
        (fromJust $ refId ref0)
        (key0)
      assertEqual "Collection 2 - elements" 
        (S.fromList [ref0, ref1])
        (S.fromList $ C.elems col2)
      assertEqual "Collection 2 - keys" 
        (S.fromList $ map fromJust $ [refId ref0, refId ref1])
        (S.fromList $ C.keys col2)
      assertEqual "Collection 2 - new key" 
        (fromJust $ refId ref1)
        (key1)
      assertEqual "Collection 3 - elements" 
        (S.fromList [ref1])
        (S.fromList $ C.elems col3)
    )
  , TestLabel "Insert 2" $ TestCase
    ( do
      let ref0 = Reference
                  { refId = Just "P2000-Nt"
                  , refAuthors = ["A. Person"]
                  , refTitle = "No title"
                  , refDate = Just $ "2000"
                  , refVenue = Nothing
                  }
          ref1 = Reference
                  { refId = Just "P2000-Nt-1"
                  , refAuthors = ["C. Person"]
                  , refTitle = "Nother title"
                  , refDate = Just "2000"
                  , refVenue = Nothing
                  }
          (col0, key0) = C.insert C.empty ref0{refId = Nothing}
          (col1, key1) = C.insert col0 ref1{refId = Nothing}
      assertEqual "Collection 0 - new key" 
        (fromJust $ refId ref0)
        (key0)
      assertEqual "Collection 0 - elements" 
        (S.fromList $ [ref0])
        (S.fromList $ C.elems col0)
      assertEqual "Collection 1 - elements" 
        (S.fromList [ref0, ref1])
        (S.fromList $ C.elems col1)
      assertEqual "Collection 1 - keys" 
        (S.fromList $ map fromJust $ [refId ref0, refId ref1])
        (S.fromList $ C.keys col1)
      assertEqual "Collection 1 - new key" 
        (fromJust $ refId ref1)
        (key1)
    )
  , TestLabel "Parse and encode" $ TestCase
    ( do
      let ref0 = Reference
                  { refId = Just "P2000-Nt"
                  , refAuthors = ["A. Person"]
                  , refTitle = "No title"
                  , refDate = Just $ "2000"
                  , refVenue = Nothing
                  }
          ref1 = Reference
                  { refId = Just "PM2000-Nt"
                  , refAuthors = ["C. Person", "T. Master"]
                  , refTitle = "Nother title"
                  , refDate = Just "2000"
                  , refVenue = Nothing
                  }
          col0 = foldr (flip C.insert') C.empty [ref0]
          col1 = foldr (flip C.insert') C.empty [ref0, ref1]
      assertEqual "Collection 0"
        (Right col0)
        (C.parse $ C.encode col0)
      assertEqual "Collection 1"
        (Right col1)
        (C.parse $ C.encode col1)
      assertEqual "Collection 1" 
        (T.pack $
          "authors:\n\
          \- A. Person\n\
          \date: \"2000\"\n\
          \key: \"P2000-Nt\"\n\
          \title: No title\n\
          \...\n\
          \authors:\n\
          \- C. Person\n\
          \- T. Master\n\
          \date: \"2000\"\n\
          \key: \"PM2000-Nt\"\n\
          \title: Nother title\n")
        (C.encode col1)
    )
  ]


