module Main where

import Test.HUnit hiding (Node)
import System.Exit (exitFailure, exitSuccess)

import LittleScholar.Parse.Internal
import LittleScholar.Reference
import Text.Parsec hiding ((<|>), many, optional)

main = do 
  testCounts <- runTestTT tests
  if errors testCounts + failures testCounts > 0 then exitFailure else exitSuccess

tests = TestList                         
  [ TestLabel "Reference 1" $ TestCase
    ( do
      let p = parse p_reference "" "[48] A. Person, B. Author and C. D. Name. Paper title here. Proc. 1st Symp. Science (SySci’94), 1994, 123–132."
      assertEqual ""
        (Right $
          Reference
          { refId = Just "48"
          , refAuthors = ["A Person", "B Author", "C D Name"]
          , refTitle = "Paper title here"
          , refDate = Nothing
          , refVenue = Nothing 
          }
        )
        p
    )
  , TestLabel "Reference 2" $ TestCase
    ( do
      let p = parse p_reference "" "[48] Person, A., Author, B. and Name, C. D. (1994) Paper title here. Proc. 1st Symp. Science (SySci’94), 123–132."
      assertEqual ""
        (Right $
          Reference
          { refId = Just "48"
          , refAuthors = ["A Person", "B Author", "C D Name"]
          , refTitle = "Paper title here"
          , refDate = Just "1994"
          , refVenue = Nothing 
          }
        )
        p
    )
  , TestLabel "Reference 3" $ TestCase
    ( do
      let p = parse p_reference "" "[48] Any Person, Be Author, and Call Door Name. Paper title here. Proc. 1st Symp. Science (SySci’94), 1994, 123–132."
      assertEqual ""
        (Right $
          Reference
          { refId = Just "48"
          , refAuthors = ["Any Person", "Be Author", "Call Door Name"]
          , refTitle = "Paper title here"
          , refDate = Nothing
          , refVenue = Nothing 
          }
        )
        p
    )
  , TestLabel "Reference 4" $ TestCase
    ( do
      let p = parse p_reference "" "[48] A. Person: Paper title here. Proc. 1st Symp. Science (SySci’94), 1994, 123–132."
      assertEqual ""
        (Right $
          Reference
          { refId = Just "48"
          , refAuthors = ["A Person"]
          , refTitle = "Paper title here"
          , refDate = Nothing
          , refVenue = Nothing 
          }
        )
        p
    )
  {-, TestLabel "Reference 5" $ TestCase
    ( do
      let p = parse p_reference "" "[48] A. Person: (Meta) Paper title here. Proc. 1st Symp. Science (SySci’94), 1994, 123–132."
      assertEqual ""
        (Right $
          Reference
          { refId = "48"
          , refAuthors = ["A Person"]
          , refTitle = "(Meta) Paper title here"
          }
        )
        p
    )-}
  {-, TestLabel "Reference 6" $ TestCase
    ( do
      let p = parse p_reference "" "[48] A. Person, B. van Author: Paper title here. Proc. 1st Symp. Science (SySci’94), 1994, 123–132."
      assertEqual ""
        (Right $
          Reference
          { refId = "48"
          , refAuthors = ["A Person", "B van Author"]
          , refTitle = "Paper title here"
          }
        )
        p
    )-}
  {-TestLabel "Reference 1a" $ TestCase -- Probably too complicated to support this
    ( do
      let p = parse p_reference "" "[48] A. Person, B. Author and C. D. Name, Paper title here, Proc. 1st Symp. Science (SySci’94), 1994, 123–132."
      assertEqual ""
        (Right $
          Reference
          { refId = "48"
          , refAuthors = ["A Person", "B Author", "C D Name"]
          , refTitle = "Paper title here"
          }
        )
        p
    )
  ,-}
  {-, TestLabel "Reference 1b" $ TestCase
    ( do
      let p = parse p_reference "" "[48] C.D. Name, Paper title here. Proc. 1st Symp. Science (SySci’94), 1994, 123–132."
      assertEqual ""
        (Right $
          Reference
          { refId = "48"
          , refAuthors = ["C D Name"]
          , refTitle = "Paper title here"
          }
        )
        p
    )-}
  ]


