module Main where

import Test.HUnit hiding (Node)
import System.Exit (exitFailure, exitSuccess)
import qualified Data.Map as M
import qualified Data.Set as S

import LittleScholar.Fetch.Arxiv
import Text.Parsec hiding ((<|>), many, optional)
import Text.HTML.TagSoup

main = do 
  testCounts <- runTestTT tests
  if errors testCounts + failures testCounts > 0 then exitFailure else exitSuccess

tests = TestList                         
  [ TestLabel "Arxiv 1" $ TestCase
    ( do
      html <- readFile "tests/arxiv-directed-grid.html"
      let entries = listEntries $ parseTags html
          expectedEntries = 
            [ Entry
              { enTitle = "Grid Movies"
              , enAuthors = ["Matthew Graham"]
              , enPublicationDate = "2014-07-18T16:10:04Z"
              , enPDFURL = "http://arxiv.org/pdf/1303.1831v2"
              }
            , Entry
              { enTitle = "On Piterbarg's max-discretisation theorem for homogeneous Gaussian random fields"
              , enAuthors = ["Kaiyong Wang", "Zhongquan Tan"]
              , enPublicationDate = "2015-02-04T20:55:17Z"
              , enPDFURL = "http://arxiv.org/pdf/1502.01333v1"
              }
            , Entry
              { enTitle = "The Poncelet grid and the billiard in an ellipse"
              , enAuthors = ["S. Tabachnikov", "M. Levi"]
              , enPublicationDate = "2005-11-01T02:47:31Z"
              , enPDFURL = "http://arxiv.org/pdf/math/0511009v1"
              }
            ]
      assertEqual "First entry"
        (take 1 expectedEntries)
        (take 1 entries)
      assertEqual "Second entry"
        (take 1 $ drop 1 expectedEntries)
        (take 1 $ drop 1 entries)
      assertEqual "Third entry"
        (take 1 $ drop 2 expectedEntries)
        (take 1 $ drop 2 entries)
      let gridTheoremEntry = 
              Entry
                { enTitle = "The Directed Grid Theorem"
                , enPDFURL = "http://arxiv.org/pdf/1411.5681v3"
                , enAuthors = ["Stephan Kreutzer", "Ken-ichi Kawarabayashi"]
                , enPublicationDate = "2022-05-06T11:32:00Z"
                }
      assertEqual "Ranks 1"
        (M.fromList
          [ (-1, [gridTheoremEntry]) ]
        )
        (rankEntries [gridTheoremEntry]
          $ S.fromList ["directed", "grid", "theorem"])
    )
  ]


