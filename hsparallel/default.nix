{ mkDerivation, base, lib }:
mkDerivation {
  pname = "hsparallel";
  version = "0.1.0.0";
  src = ./.;
  libraryHaskellDepends = [ base ];
  license = "unknown";
  hydraPlatforms = lib.platforms.none;
}
