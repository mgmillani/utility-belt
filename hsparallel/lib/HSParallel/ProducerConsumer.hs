module HSParallel.ProducerConsumer where

import Control.Concurrent
import Data.Maybe
import Data.Time.Clock
import Control.Monad
import Control.Concurrent.MVar
import Control.Exception
import Data.List

import qualified Debug.Trace as D (trace)

guessAndCheck guess check count mTries = do
  numForks <- getNumCapabilities
  mGuess <- newEmptyMVar
  mResult <- newEmptyMVar
  guessers <- replicateM numForks $ forkFinally
    (mapM_ id $ repeat $ do
      g <- guess
      modifyMVar_ mTries (\i -> return $ i + 1)
      putMVar mGuess g
    )
    (\e -> case e of
      Left err -> print err
      Right res -> return ()
    )
  checkers <- replicateM numForks $ forkFinally
    (mapM_ id $ repeat $ do
      g <- takeMVar mGuess
      when (check g) $ 
        putMVar mResult g
    )
    (\e -> case e of
      Left err -> print err
      Right res -> return ()
    )
  results <- replicateM count (takeMVar mResult)
  mapM_ killThread $ guessers ++ checkers
  return results

produceConsumeCount produce consume count mCount = do
  mProduced <- newMVar 0
  produceConsumeExhaustive
    (do
      c0 <- readMVar mProduced
      if c0 < count then do
        g <- produce
        c <- modifyMVar mProduced (\i -> return $ (i + 1, i + 1))
        if c <= count then do
          return $ Just g
        else
          return Nothing
      else
        return Nothing
    )
    (\g -> do
           j <- modifyMVar mCount (\i -> return $ (i + 1, i + 1))
           consume g j
    )
  return ()
  
produceConsumeExhaustive produce consume = do
  numForks <- getNumCapabilities
  mProduct <- newEmptyMVar
  workerDone <- replicateM numForks $ newEmptyMVar
  producers <- forM workerDone $ \doneM -> forkFinally
    ( let loop = do
              g <- produce
              when (isJust g) $ do
                putMVar mProduct g
                loop
      in loop
    )
    (\e -> do
      putMVar doneM ()
      case e of
        Left err -> (putStrLn "produce-consume error (producer):" >> print (err :: SomeException))
        Right res -> return ()
    )
  consumerDone <- replicateM numForks $ newEmptyMVar
  consumerOutput <- newEmptyMVar
  consumers <- forM consumerDone $ \doneM -> forkFinally
    ( let loop = do
                  g <- takeMVar mProduct
                  case g of
                    Just p -> do
                      out <- consume p
                      putMVar consumerOutput (Just out)
                      loop
                    Nothing -> putMVar mProduct Nothing
      in loop
    )
    (\e -> do
      putMVar doneM ()
      case e of
        Left err -> (putStrLn "produce-consume error (consumer):" >> print (err :: SomeException))
        Right res -> return ()
    )
  forkIO $ do
    mapM_ takeMVar workerDone
    putMVar mProduct Nothing
    mapM_ takeMVar consumerDone
    putMVar consumerOutput Nothing
  outputList consumerOutput
  where
    outputList consumerOutput = do
      out <- takeMVar consumerOutput
      case out of
        Just o -> do
          rs <- outputList consumerOutput
          return $ o : rs
        Nothing -> 
          return []

-- | Execute a producers and consumers in parallel.
-- Producers generate some product and the index of the generation to which the product belongs.
-- Producers are expected to give generations in order, that is, no product of generation less than g is produced after some product of generation g.
-- Producing Nothing signals that all products where generated and that the process should finish.
--
-- | The algorithm guarantees that products will be consumed by order of generation.
-- That is, after consume is called with some product of generation g, it will not be called with a product of generation less than g.
--
-- | The function generationComplete is called whenever switching between one generation to the next, and is called before any product of the newest generation is consumed.
produceConsumeByGeneration :: Int
                           -> (IO (Maybe (a, Int)))
                           -> (a -> Int -> IO ())
                           -> (Int -> IO ())
                           -> IO ()
produceConsumeByGeneration startGeneration produce consume generationComplete = do
  currentGenM <- newMVar startGeneration
  waitingForNextGenM <- newMVar []
  startNextGenM <- newEmptyMVar
  openM <- newMVar 0
  productM <- newEmptyMVar
  let finish mGen = do
        open <- takeMVar openM
        if open == 1 then do
          -- if all products have been processed, check if we can go to the next generation
          waitingList <- takeMVar waitingForNextGenM
          currentG <- modifyMVar currentGenM (\g -> return (g + 1, g))
          generationComplete currentG
          let currentList = filter (\(g', _ , _) -> (currentG + 1)== g') waitingList
          case currentList of
            [] -> 
              return ()
            (_, waiting, _) : _ -> do
              putMVar waiting ()
          putMVar waitingForNextGenM waitingList
          putMVar openM 0
        else
          putMVar openM $ open - 1
      waitForGen g = do
        (waitM, waitLengthM) <- modifyMVar waitingForNextGenM $ \ws -> do
          case filter (\(g', _, _) -> g == g') ws of
            [] -> do
              w <- newEmptyMVar
              wLength <- newMVar 1
              return ((g, w, wLength) : ws, (w, wLength))
            ((_, w, wLength) : _) -> do
              return (ws, (w, wLength))
        modifyMVar_ waitLengthM (\l -> return $ l + 1)
        finish (Just g)
        nextGen <- takeMVar waitM
        putMVar waitM nextGen
        modifyMVar_ waitLengthM $ \l -> do
          -- if this thread is the last in the waiting list, destroy the MVar
          when (l == 1) $ do
            modifyMVar_ waitingForNextGenM $ \ws ->
              return $ filter (\(g', _, _) -> g /= g') ws
          return $ l - 1
        modifyMVar_ openM (\o -> return $ o + 1)
      produce' = do
          modifyMVar_ openM (\o -> return $ o + 1)
          mp <- produce
          case mp of
            Nothing -> do
              finish Nothing
              return Nothing
            Just (_, g) -> do
              currentN <- readMVar currentGenM
              if currentN == g then
                return mp
              else do
                -- Wait until we can proceed to the next generation.
                waitForGen g
                return mp
      consume' (p, i) = do
          consume p i
          finish $ Just i
  produceConsumeExhaustive produce' consume'
  return ()

progressReport intervalSeconds mTries = do
  now <- getCurrentTime
  progressReport' now
  where
    progressReport' t0 = do
      threadDelay (intervalSeconds * (10^6))
      t1 <- getCurrentTime
      let dt = diffUTCTime t1 t0
          ds = nominalDiffTimeToSeconds dt
      tries <- fmap fromIntegral $ readMVar mTries
      let tStr = if ds < (60 * 10) then
                    show (round $ ds) ++ " seconds."
                 else if ds < (60 * 60 * 10) then
                    show (round $ ds / 60) ++ " minutes."
                 else
                    show (round $ ds / (60 * 60)) ++ " hours."
      let str0 = (show $ round tries) ++ " guesses in " ++ tStr
          str1 = if tries > 1000 * ds then
                   (show $ tries / (1000 * ds)) ++ " guesses per milisecond."
                 else if tries > ds then
                   (show $ tries / (ds)) ++ " guesses per second."
                 else if tries > (ds / 60) then
                   (show $ tries / (ds / 60)) ++ " guesses per minute."
                 else
                   (show $ tries / (ds / (60 * 60))) ++ " guesses per hour."
      putStrLn $ str0 ++ " (" ++ str1 ++ ")"
      progressReport' t0
  

