#!/usr/bin/fish

set pdf_dir $argv[1]
set txt_dir $argv[2]

set article_dir $txt_dir/Articles
mkdir -p $article_dir

set now (date +%Y-%m-%dT%H:%M:%S%:z)

echo "Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: $now

====== Articles ======
(File generated automatically)
" > $txt_dir/Articles.txt

for fl in $pdf_dir/*.pdf
	set name (basename $fl .pdf)
	set fname (echo $name | sed 's/[ :]/_/g')
	echo "[[:Result_Collection:Articles:$fname|$name]] - ()" >> $txt_dir/Articles.txt
	set fname $article_dir"/"$fname
	if test ! -e "$fname".txt
		echo "Content-Type: text/x-zim-wiki
		Wiki-Format: zim 0.4
		Creation-Date: $now

		====== $name ======
		" > "$fname".txt
		echo "====== $name ======" > "$fname".txt
		echo "" >> "$fname".txt
		echo "[[../../$fl|$name]] - ()" >> "$fname".txt
	end
end
