#!/usr/bin/env fish

set State "read monitor"
for Line in (xrandr | grep '^\(\([^ ].* connected\)\|\(.*+\)\)')
	if test $State = "read monitor"
		echo $Line | grep "primary" > /dev/null
		if test $status -eq 0
			set Display (string split ' ' -f 1,4 $Line)
			set Res (string split 'x' (echo $Display[2] | grep -o '[0-9]*x[0-9]*'))
			set State "read native resolution"
		end
	else if test $State = "read native resolution"
		set Native (string split ' ' -f 1 --no-empty $Line)
		set Native (string split 'x' $Native)
		set State "done"
	end
end

echo Current resolution : $Res
echo Native resolution : $Native

set NativeRatio (math "$Native[1] / $Native[2]")
set ResRatio (math "$Res[1] / $Res[2]")

if test $ResRatio -gt $NativeRatio
	set dx 0
	# nw / (c * nh) = rw / rh
	# (c * nh) / nw = rh / rw
	# c = rh * nw / (rw * nh)
	set Scale (math "($Native[1] * $Res[2]) / ($Native[2] * $Res[1])")
	set dy (math "abs($Native[2] * (1 - $Scale)) / 2")
else
	set dy 0
	# nw * c / nh = rw / rh
	# c = rw * nh / (rh * nw)
	set Scale (math "($Res[1] * $Native[2]) / ($Res[2] * $Native[1])")
	set dx (math "abs($Native[1] * (1 - $Scale)) / 2")
end


for Dev in (xsetwacom --list devices | cut -f1 | sed 's:[ ]*$::')
	xsetwacom set  $Dev ResetArea
	set Params (string split ' ' (xsetwacom get $Dev Area))

	set sx (math "$Params[3] / $Native[1]")
	set sy (math "$Params[4] / $Native[2]")
	set MinX (math "round($Params[1] + $dx * $sx)")
	set MinY (math "round($Params[2] + $dy * $sy)")
	set MaxX (math "round($Params[3] - $dx * $sx)")
	set MaxY (math "round($Params[4] - $dy * $sy)")
	echo $Dev: min x = $MinX , min y = $MinY , max x = $MaxX, max y = $MaxY
	xsetwacom set $Dev Area $MinX $MinY $MaxX $MaxY
end
