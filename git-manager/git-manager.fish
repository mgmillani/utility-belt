#!/usr/bin/fish

if test (count $argv) -eq 0
	set dir $XDG_CONFIG_HOME/git-manager
else
	set dir $argv[1]
end

cd $dir
set choice (ls | dmenu -i -l 40)
if test $status -eq 0
	cd $choice
	git pull
	gitk . &
	thunar &
end
