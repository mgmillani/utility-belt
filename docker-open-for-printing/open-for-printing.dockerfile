FROM archlinux:latest
RUN useradd --no-create-home --shell=/bin/false aur && usermod -L aur && echo "root:root" | chpasswd
RUN mkdir -p -m 0700 /root && mkdir -p -m 775 /pkgs && chown aur:aur /pkgs
RUN pacman -Syu --noconfirm firefox cups evince fish xorg-server xorg-xauth tar gzip fakeroot debugedit gcc-libs glibc libcups libxml2 gtk3 gzip jbigkit at-spi2-core cairo gdk-pixbuf2 ghostscript libjpeg6-turbo pango autoconf base-devel  
ADD https://aur.archlinux.org/cgit/aur.git/snapshot/cnrdrvcups-lb.tar.gz /pkgs/cnrdrvcups-lb.tar.gz
RUN chmod 664 /pkgs/cnrdrvcups-lb.tar.gz
USER aur
RUN cd /pkgs && tar -xaf cnrdrvcups-lb.tar.gz && ls && cd cnrdrvcups-lb && makepkg 
USER root
RUN cd /pkgs/cnrdrvcups-lb && ls . pkg && pacman -U --noconfirm cnrdrvcups-lb-*pkg.tar.zst
# RUN apt remove chromium-browser chromium-browser-l10n chromium-codecs-ffmpeg-extra
# RUN apt-get update && apt-get install -y dbus-user-session cups evince fish xauth software-properties-common chromium-browser
# RUN add-apt-repository ppa:thierry-f/fork-michael-gruz && apt-get update && apt-get install -y  scangearmp2 cnijfilter2 cnrdrvcups-lipslx cnrdrvcups-ufr2-uk cnrdrvcups-ufr2-us
# RUN add-apt-repository ppa:thierry-f/fork-michael-gruz && apt-get update && apt-get install -y dbus-user-session cups evince fish xauth scangearmp2 cnijfilter2 cnrdrvcups-lipslx cnrdrvcups-ufr2-uk cnrdrvcups-ufr2-us
COPY --chmod=755 ./open-file.fish /usr/bin/open-file
COPY --chmod=644 ./cupsd.conf /etc/cups/cupsd.conf
ENV InputDir=/mnt/input
ENV AuthToken=""
ENV DISPLAY=""
CMD ["/usr/bin/open-file"]
