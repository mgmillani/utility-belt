#!/usr/bin/env fish
eval "set Token $AuthToken"
echo $Token - $Token[2]
xauth add $Token
# avahi-daemon &
cupsd -l &
# lpinfo -v
avahi-browse --all --terminate
cd $InputDir
if test ! -e /cups-setup
	firefox "http://localhost:6631"
	touch /cups-setup
end
for Fl in (find . -maxdepth 1 -type f)
	set Ext (echo $Fl | sed 's:.*\\.\([^.]*\)$:\1:')
	if test x"$Ext" = xpdf
		evince $Fl
	end
end
	
