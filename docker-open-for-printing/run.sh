#!/usr/bin/env fish
set File $argv[1]
set InputDir /mnt/Input
set AuthToken (xauth list | head -n1)
if test -e $File
	docker run --net=host \
		--mount type=bind,source="$File",target=$InputDir/(basename $File),readonly \
		-e AuthToken="$AuthToken" \
		-e InputDir=$InputDir \
		-e DISPLAY=$DISPLAY \
		--volume /tmp/.X11-unix:/tmp/.X11-unix \
		--volume /var/run/dbus:/var/run/dbus \
		--volume /var/run/avahi-daemon/socket:/var/run/avahi-daemon/socket \
		-p 6631:631 \
		"open-for-printing:latest"
end
