#!/bin/bash

map()
{
	command="$1"
	while shift && [ -n "$1" ]; do
		eval "$command '$1'"
	done
}

foldl()
{
	command="$1"
	value="$2"
	while shift && [ -n "$2" ]; do
		value=$(eval "$command '$value' '$2'")
	done
	echo "$value"
}