"source $VIMRUNTIME/defaults.vim
set runtimepath=~/.config/nvim,~/.config/vim,$VIM/vimfiles,$VIMRUNTIME,$VIM/vimfiles/after,~/.config/vim/after,/usr/share/vim/vimfiles,/usr/share/vim

:command Mpdmode runtime Scripts/mpd.vim
:command Spreadsheetmode runtime Scripts/spreadsheet.vim

" Plugins
"call plug#begin('~/.config/vim/vim-plug')
"	Plug 'jceb/vim-orgmode'
"	Plug 'skywind3000/asyncrun.vim'
"	Plug 'sirver/UltiSnips'
"call plug#end()

set shiftwidth=2
set tabstop=2
set textwidth=0
set ts=2 "tab size"
set clipboard=unnamedplus
set linebreak
set autochdir
set autoindent
set wrap
set wildmode=longest,list,full
set nolist
set backspace=indent,eol,start  " more powerful backspacing
set nohlsearch
set mouse=a
set breakindent
set completeopt=longest,menu " auto complete menu. Take longest common match.
set complete=] " only use tags file for completion
syntax on
filetype plugin on

" move cursor over wrapped lines
noremap <UP> gk
noremap <DOWN> gj
inoremap <UP> <C-o>gk
inoremap <DOWN> <C-o>gj
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" Build commands
function SmartMake ()
	AsyncStop
	if filereadable(".vim.ninja")
		AsyncRun ninja -f ".vim.ninja"
	else
		AsyncRun make
	endif
endfunction
map <F9> :w<ENTER>:call SmartMake()<ENTER>
" Spell checking
map <F2> :set spell spelllang=en_us<enter>
map <F3> :set spell spelllang=de<enter>
map <F4> :set spell<Enter>:set spelllang=pt<Enter>
sunmap <F2>
sunmap <F3>
sunmap <F4>


" colours
highlight SpellBad cterm=underline ctermbg=DarkRed
highlight SpellCap cterm=underline ctermbg=DarkBlue
colo desert

" =======
" Haskell
" =======
" Use spaces
autocmd BufRead,BufNewFile *.hs set expandtab smartindent autoindent

" ===========
" LaTeX StUfF
" ===========
autocmd BufRead,BufNewFile *.tex,*.sty let g:tex_flavor = "latex" 
autocmd BufRead,BufNewFile *.tex,*.sty set iskeyword+=@,:,\
autocmd BufRead,BufNewFile *.tex set noexpandtab spell spelllang=en_us

runtime autocomplete.vim

" ultisnips
let g:UltiSnipsSnippetDirectories=["/home/marcelo/.config/vim/UltiSnips"]
let g:UltiSnipsExpandTrigger="<s-tab>"
let g:UltiSnipsJumpForwardTrigger="<s-tab>"
let g:UltiSnipsJumpBackwardTrigger="<c-tab>"
