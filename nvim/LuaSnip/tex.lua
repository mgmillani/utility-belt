local ls = require('luasnip')
local s = ls.snippet
local i = ls.insert_node
local t = ls.text_node
local d = ls.dynamic_node
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local rep = require("luasnip.extras").rep

local get_visual = function(args, parent)
  if (#parent.snippet.env.LS_SELECT_RAW > 0) then
    return sn(nil, i(1, parent.snippet.env.LS_SELECT_RAW))
  else  -- If LS_SELECT_RAW is empty, return a blank insert node
    return sn(nil, i(1))
  end
end

return {
	s({ trig = "env", dscr="Wraps selection around an environment."},
		fmta(
			[[
				\begin{<>}
						<>
						<>
				\end{<>}
			]],
			{
				i(1, "environment"),
				d(2, get_visual),
				i(0),
				rep(1),
			}
		)
	),
	s({ trig = "seq", dscr="Creates a sequence."},
		fmta("<>_{1}, <>_{2}, \\ldots, <>_{<>}",
			{
				i(1,"x"),
				rep(1),
				rep(1),
				i(2)
			}
		)
	),
	s({trig = "([^%a])mm", wordTrig = false, regTrig = true},
		fmta(
			"<>\\(<>\\)",
			{
				f( function(_, snip) return snip.captures[1] end ),
				d(1, get_visual),
			}
		)
	),
	s({trig = "([^%a])ml", wordTrig = false, regTrig = true},
		fmta(
			"<>\\mathcal{<>}",
			{
				f( function(_, snip) return snip.captures[1] end ),
				d(1, get_visual),
			}
		)
	),
	s({trig = "([^%a])scr", wordTrig = false, regTrig = true},
		fmta(
			"<>\\mathscr{<>}",
			{
				f( function(_, snip) return snip.captures[1] end ),
				d(1, get_visual),
			}
		)
	)
}
