let g:tex_flavor = "latex"
set iskeyword+=@,:,\
runtime autocomplete.vim
let g:vimtex_imaps_enabled = 0
let g:vimtex_view_method = 'general'
" Refocus on nvim on forward searches with synctex
if !exists("g:vim_window_id")
  let g:vim_window_id = system("xdotool getactivewindow")
endif
function! g:SynctexForwardSearch() abort
  " Give window manager time to recognize focus moved to Zathura;
  " tweak the 200m as needed for your hardware and window manager.
	call SVED_Sync()
  sleep 200m  

  " Refocus Vim and redraw the screen
  silent execute "!xdotool windowfocus " . expand(g:vim_window_id)
  redraw!
endfunction

" Forward search with synctex
nmap <F8> <CMD>call g:SynctexForwardSearch()<ENTER>
