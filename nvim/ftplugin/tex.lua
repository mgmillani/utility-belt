-- Insert tag
function rofi_tag ()
	if vim.fn.filereadable("tags") then
		local choice = vim.fn.system('grep \'\\\\\\\\label\' tags | cut -d"$(echo -en \'\\t\')" -f1 | sort -u | rofi -dmenu -method fuzzy')
		choice = string.gsub(choice, '\n', '')
		return choice
	else
		print("File \"tags\" not found.")
	end
end

local insert_tag = function ()
	local row, col = unpack(vim.api.nvim_win_get_cursor(0))
	local label = rofi_tag()
	vim.api.nvim_paste(label, true, -1)
end

vim.keymap.set({'i'}, '<C-l>', insert_tag)
