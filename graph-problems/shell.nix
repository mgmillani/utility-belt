with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "grapringe";
  buildInputs = [ cabal-install 
    (haskellPackages.ghcWithPackages (p:
    [ p.array
      p.base
      p.containers
      p.hgraph
      p.HUnit
      p.random
      p.MonadRandom
    ])
    )
  ];
}
