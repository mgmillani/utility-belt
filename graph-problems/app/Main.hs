module Main where

import System.Environment
import System.Exit
import qualified GraphProblems.Linkage.Generate   as Linkage
import qualified GraphProblems.Linkage.Reduce     as Linkage
import qualified GraphProblems.Linkage.Output.Dot as Linkage
import qualified GraphProblems.Linkage.Load.Dot as Linkage

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["linkage", "generate", maxVerticesStr, terminalsStr] -> do
      inst <- fmap Linkage.reduce $ Linkage.generate (read maxVerticesStr) (read terminalsStr)
      putStrLn $ Linkage.toString inst
    ["linkage", "reduce", instanceFile, partialSolutionFile] -> do
      inst <- fmap Linkage.load $ readFile instanceFile
      solution <- fmap Linkage.loadSolution $ readFile partialSolutionFile
      let inst' = Linkage.reduce $ Linkage.applySolution inst solution
      putStrLn $ Linkage.output inst'
    _ -> do
      hPutStr stderr "Invalid arguments.\n"
      hPutStr stderr "usage:\
      \ graps linkage generate <number of vertices> <number of terminal pairs>\
      \ graps linkage reduce <instance file> <partial solution file>"
      exitWith $ ExitCode 1
