module Main where

import System.Exit (exitFailure, exitSuccess)
import Test.HUnit hiding (Node)

import qualified HGraph.Directed as D
import qualified HGraph.Directed.Connectivity.IntegralLinkage as D
import qualified GraphProblems.Linkage.Generate.Internal as Gen
import qualified HGraph.Directed.AdjacencyMap as AM
import Data.Maybe

tests = TestList
  [ TestLabel "Two paths" $ TestCase
    ( do
      inst <- Gen.general (AM.emptyDigraph) 2 2 0
      assertEqual "Number of vertices" 6 (D.numVertices $ Gen.digraph inst)
      let sol = D.linkage (Gen.digraph inst) (map (\(_, s, t) -> (s,t)) $ Gen.terminalPairs inst)
      assertBool "Has solution" (isJust sol)
    )
  ]

main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
