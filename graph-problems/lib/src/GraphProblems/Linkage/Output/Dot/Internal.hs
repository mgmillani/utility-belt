module GraphProblems.Linkage.Output.Dot.Internal where

import qualified HGraph.Directed as D
import qualified HGraph.Directed.Output as Out
import GraphProblems.Linkage.Definitions

colors = 
  [ "#FF5a5a" -- red
  , "#1AC013" -- green
  , "#5A73FF" -- blue
  , "#D4B400" -- yellow
  , "#FC7114" -- orange
  , "#FF5AFF" -- magenta
  , "#03D899" -- mint
  , "#80CDFF" -- cyan
  ]
nColors = length colors
deletedColor = "#cccccc"

toString inst = 
  let d = digraph inst
      terminalsStyle = M.fromList
                          concatMap (\(i, s, t) -> 
                            [ (s, [ ("label", show s ++ ":s-" ++ show i)])
                            , (t, [ ("label", show t ++ ":t-" ++ show i)])
                            ]
                            )
                            (terminalPairs inst)
      nonTerminalsStyle = M.fromList
                            [ (v, [("label", show v)]
                            | v <- D.vertices d
                            , not (v `M.elem` terminalsStyle)
                            ]
  in
  Out.toDot d
            Out.defaultDotStyle
              { Out.everyNode = [("shape", "rectangle"), ("style", "filled")]
              , Out.nodeAttributes = 
                foldr M.unionWith (++) M.empty
                  [ M.fromList
                    [ (v, [
                            case (decisions inst) M.! v of
                              Deleted -> ("fillcolor", deletedColor)
                              Taken i -> ("fillcolor", colors !! (i `mod` nColors))
                          ]
                      )

                    , v `M.elem` (decisions inst)
                    ]
                  , M.fromList $
                  ]
                
              }
