module GraphProblems.Linkage.Generate
       ( general
       , Instance(..)
       , Decision(..)
       )
where

import GraphProblems.Linkage.Generate.Internal
