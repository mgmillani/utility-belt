module GraphProblems.Linkage.Definitions where

import qualified Data.Map as M

data Decision = 
  Taken Int
  | Deleted

data Instance t a = 
  Instance
  { digraph :: t a
  , terminalPairs :: [(Int, a, a)]
  , decisions :: M.Map a (Decision)
  }
