module GraphProblems.Linkage.Generate.Internal where

import qualified HGraph.Directed              as D
import qualified HGraph.Directed.Connectivity.Basic as D
import qualified Data.Map                     as M
import qualified Data.Set                     as S
import System.Random
import Control.Monad.Random
import qualified GraphProblems.Random as Random
import GraphProblems.Linkage.Definitions

general :: (D.DirectedGraph t, D.Mutable t, D.Adjacency t) => t Int -> Int -> Int -> Int -> IO (Instance t Int)
general d0 nVertices kTerminals mRandomPaths = do
  let tPairs = [(i, i, kTerminals + i) | i <- [0..kTerminals - 1]]
      d1 = foldr D.addVertex d0 [i | i <- [0..2*kTerminals - 1 + nVertices]]
  gen <- newStdGen
  let d2 = evalRand (generalYesInstance d1 nVertices kTerminals mRandomPaths) gen
  return $ Instance
           { digraph = d2
           , terminalPairs = tPairs :: [(Int, Int, Int)]
           , decisions = M.empty
           }

generalYesInstance :: (D.DirectedGraph t, D.Mutable t, D.Adjacency t, MonadRandom m) => t Int -> Int -> Int -> Int -> (m (t Int))
generalYesInstance d nVertices kTerminals mRandomPaths = do
  let nonTerminals = [2*kTerminals .. 2*kTerminals -1 + nVertices]
      sources      = [ 0 .. kTerminals - 1 ]
  -- there are disjoint paths connecting the terminals
  n0 <- Random.integer kTerminals nVertices
  vs0 <- Random.subset nonTerminals n0
  -- ensure that each terminal pair gets at least one non-terminal vertex in its path
  t0' <- Random.permutation sources
  t0 <- fmap (t0' ++) $ replicateM (n0 - kTerminals) (Random.integer 0 (kTerminals - 1))
  let mapping0 = foldr (\(t,v) m -> M.insertWith (++) t [v] m) M.empty $ zip t0 vs0
  paths0 <- forM (M.assocs mapping0) $ \(i, vsi) -> do
    path <- Random.permutation vsi
    return $ (i : path) ++ [i + kTerminals]
  -- no si-ti separator of size one
  paths1 <- fmap concat $ forM sources $ \i -> do
    -- number of vertices to be used in total by the two disjoint paths
    n1 <- Random.integer 2 nVertices
    -- which vertices will be used by the two disjoint paths
    vs1 <- Random.subset nonTerminals n1
    -- which vertices will be used by each path
    partition <- replicateM n1 (Random.integer 0 1)
    let (vsa, vsb) = foldr (\(v,p) (p0, p1) ->
                               if p == 1 then
                                  (p0, v : p1)
                               else
                                  (v : p0, p1)) ([],[]) $ zip vs1 partition
    -- in which order the vertices will be visited
    pathA <- Random.permutation vsa
    pathB <- Random.permutation vsb
    return [i : pathA ++ [i + kTerminals], i : pathB ++ [i + kTerminals]]
  -- each vertex is in some si-ti path for at least two terminal pairs
  paths2 <- forM nonTerminals $ \v -> do
    is <- Random.subset sources 2
    fmap concat $ forM is $ \i -> do
      n <- Random.integer 0 (nVertices - 1)
      vs <- Random.subset (filter (/=v) nonTerminals) n
      path' <- Random.permutation (v:vs)
      return $ i : path' ++ [i + kTerminals]
  -- each non-terminal vertex has indegree and outdegree at least two
  let d1 = foldr addPath d $ paths0 ++ paths1 ++ paths2
      zones = M.fromList $ 
                map (\si -> (si, (S.fromList $ D.reach d1 si)
                                 `S.intersection`
                                 (S.fromList $ D.reverseReach d1 (si + kTerminals))))
                    sources
  paths3 <- forM [ v
                 | v <- nonTerminals
                 , D.indegree d1 v <= (1 :: Int) || D.outdegree d1 v <= (1 :: Int)
                 ] $ \v -> do
                   let blocked = S.fromList $ v : (take 1 $ D.inneighbors d1 v)
                                              ++  (take 1 $ D.outneighbors d1 v)
                       candidates = [ zone `S.difference` (S.fromList [s, s + kTerminals])
                                    | (s, zone) <- M.assocs zones
                                    , S.size (zone
                                      `S.difference` blocked) >= 2
                                    ]
                   zone' <- fmap head $ Random.subset candidates 1
                   let zone'' = zone' `S.difference` blocked
                   neighbors <- Random.subset (S.toList zone'') 2 >>= Random.permutation
                   return ([head neighbors, v, last neighbors] :: [Int])
  -- add random si-ti paths
  paths4 <- replicateM mRandomPaths $ do
    i <- Random.integer 0 (kTerminals - 1)
    l <- Random.integer 1 nVertices
    vs <- Random.subset nonTerminals l
    path' <- Random.permutation vs
    return $ i : path' ++ [i + kTerminals]
  --
  return $ foldr addPath d1 $ paths3 ++ paths4

addPath :: (D.DirectedGraph t, D.Mutable t) => [a] -> t a -> t a
addPath vs d = foldr D.addArc d $ zip vs $ tail vs
