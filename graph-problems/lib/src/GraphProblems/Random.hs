module GraphProblems.Random
       ( integer
       , subset
       , permutation
       )
where
import GraphProblems.Random.Internal
