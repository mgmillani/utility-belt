module GraphProblems.Random.Internal where

import Control.Monad.Random
import Data.List
import qualified Data.Array as A

integer :: (MonadRandom m) => Int -> Int -> m Int
integer n0 n1 = getRandomR (n0, n1)

subset :: (MonadRandom m) => [a] -> Int -> m [a]
subset xs k = do
  let n = length xs
  if k > n then
    return []
  else do
    chosen <- fmap sort $ forM [0..k-1] $ \i -> do
                               integer 0 (n - i - 1)
    return $ takeChosen 0 xs chosen

takeChosen :: Int -> [a] -> [Int] -> [a]
takeChosen _ _ [] = []
takeChosen i (x:xs) (j:js)
  | j == i = x : takeChosen i xs js
  | otherwise = takeChosen (i + 1) xs js

permutation :: (MonadRandom m) => [a] -> m [a]
permutation xs = do
  let n = length xs
      arr = A.array (0, n - 1) $ zip [0..] xs
  order <- forM [0..n-1] $ \i -> do
                integer 0 (n - i - 1)
  return $ takeOrder (n - 1) arr order

takeOrder :: Int -> A.Array Int a -> [Int] -> [a]
takeOrder _ _ [] = []
takeOrder n arr (i:is) = 
  (arr A.! i) : takeOrder (n - 1) (arr A.// [(i, arr A.! n)]) is
