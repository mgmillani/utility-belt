{ mkDerivation, base, FontyFruity, lib, matrix, transformers, xml
}:
mkDerivation {
  pname = "pdfguesstitle";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base FontyFruity matrix transformers xml
  ];
  description = "Rename pdfs using heuristics";
  license = lib.licenses.gpl3Only;
}
