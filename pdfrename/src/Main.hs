module Main where

import qualified Text.XML.Light as X
import Data.Matrix hiding (trace)
import Control.Monad
import Control.Monad.Trans.State
import Data.Maybe
import Data.Char
import Data.List

import qualified Debug.Trace as T

import Graphics.Text.TrueType

import System.Environment

data Article =
  Article
  { title :: String
  }

dpi = (floor $ (mmFactor "in" / mmFactor "px"))

rotate a =
  fromList 3 3 [cos a, - sin a, 0
               ,sin a,   cos a, 0
               ,    0,       0, 1]
translate x y =
  fromList 3 3 [1,0,x
               ,0,1,y
               ,0,0,1]
skewx a =
  fromList 3 3 [1, tan a, 0
               ,0,     1, 0
               ,0,     0, 1]
skewy a =
  fromList 3 3 [1    , 0, 0
               ,tan a, 1, 0
               ,0    , 0, 1]
scale x y =
  fromList 3 3 [x,0,0
               ,0,y,0
               ,0,0,1]

isHorizontal m =
  let [ a,b,c
       ,d,e,f
       ,g,h,i] = toList m
  in a > 2*d

splitBy :: (Char -> Bool) -> String -> [String]
splitBy _ [] = []
splitBy p xs =
  let (w, r) = span (not . p) (dropWhile p xs)
  in w : (splitBy p r)

consumeWhile :: (Char -> Bool) -> State String String
consumeWhile f = do
  str <- get
  let (x,r) = span f str
  put $ r
  return x

whenNotEmpty e x = do
  s <- get
  if s == "" then return e else x

parseTransform transform = evalState parseTransform' transform
  where
    --  parseTransform' :: Floating a => State String (Matrix a)
    parseTransform' = do
      f <- function
      ps <- parameters
      let m = buildTransform  f ps
      rest <- whenNotEmpty (identity 3) $ parseTransform'
      return $ m * rest
    function :: State String String
    function = do
      consumeWhile (\x -> x `elem` [' ','\t'])
      f <- consumeWhile isAlpha
      return $ map toLower f
    --  parameters :: Floating a => State String [a]
    parameters = do
      consumeWhile  (\x -> x `elem` [' ', '(', '\t'])
      ps <- consumeWhile (/= ')')
      consumeWhile  (\x -> x `elem` [' ', ')', '\t'])
      let params = (splitBy (\x -> x `elem` ", ") ps)
      return $ map read params
    buildTransform :: Floating a => String -> [a] -> (Matrix a)
    buildTransform "scale" [sx]     = scale sx sx
    buildTransform "scale" [sx,sy]  = scale sx sy
    buildTransform "translate" [tx]     = translate tx 0
    buildTransform "translate" [tx, ty] = translate tx ty
    buildTransform "rotate" [a]        = rotate (pi * a / 180)
    buildTransform "rotate" [a,cx ,cy] = (translate cx cy) * (rotate (pi * a / 180)) * (translate (-cx) (-cy))
    buildTransform "matrix" [a,b,c,d,e,f] = fromList 3 3 [a,c,e,b,d,f,0,0,1]
    buildTransform "skewx" [a] = skewx a
    buildTransform "skewy" [a] = skewy a
    buildTransform _ _ = identity 3

parseElement t0 (X.Elem element) =
  let t1 = getTransform element
      t2 = t1 * t0
  in case (X.qName $ X.elName element) of
    "text" -> [parseText t2 element]
    _ -> concatMap (parseElement t2) $ X.elContent element
parseElement t0 _ = []

parseText t element =
  let fontSize = getFontSize element
      text = concatMap (getText fontSize) $ X.elContent element
  in (text, fontSize, t)

getText fontSize (X.Elem element)
  | (X.qName $ X.elName element) == "tspan" =
    let xs = getXs $ element
        dx = zipWith (-) (tail xs) xs
        e  = -fontSize / 2
        text = concatMap (getText fontSize) $ X.elContent element
    in if null text then [] else [(fst $ head text, dx)]
  | otherwise = concatMap (getText fontSize) $ X.elContent element
getText fontSize (X.Text t) = [(X.cdData t, [])]

insertSpaces font fontSize (string, dx) = -- TODO: Treat case where a line has no spaces
  let fontPt = fontSize / (mmFactor "pt")
      fontPx = fontSize / (mmFactor "px")
      pointSize = pixelSizeInPointAtDpi fontPx dpi
      ws = map (\c -> let bb = stringBoundingBox font dpi pointSize [c] in (_xMax bb) - (_xMin bb)) string
      mmWs = map (\x -> mmFactor "px" * x ) ws
      dx' = zipWith (-) dx mmWs
      ds = sort dx'
      dx'' = zipWith (\a b -> (a, a-b)) (tail ds) ds
      e = 0.95 * (fst $ maximumBy (\(a0, d0) (a1,d1) -> compare d0 d1) dx'')
      (c:ss) = string
  in c : insertSpaces' (zip ss dx') e

insertSpaces' [] e = []
insertSpaces' [(c,_)] e = [c]
insertSpaces' ((c,d):ss) e =
  let rs = c : (insertSpaces' ss e)
  in if d > e then  ' ' : rs else rs

mmFactor "in" = 25.4
mmFactor "cm" = 10
mmFactor "pt" = 2.834646
mmFactor "pc" = 0.2362205
mmFactor "mm" = 1
mmFactor "px" = 2.834646
mmFactor _    = 1

readLength len =
  let (n,u) = span (\x -> isNumber x || x == '.') len in (read n) * (mmFactor u)

getFontSize element =
  let style = fromMaybe [] $ X.findAttr (X.blank_name{X.qName = "style"}) element
      fontSizeAttr = filter (\(a,v) -> a == "font-size") $ parseStyle style
      fs = if null fontSizeAttr then "10pt" else snd $ head fontSizeAttr
  in readLength fs

getTransform element =
  let t = fromMaybe "identity" $ X.findAttr (X.blank_name{X.qName = "transform"}) element
  in parseTransform t

getXs element =
  let xs = fromMaybe [] $ X.findAttr (X.blank_name{X.qName = "x"}) element
  in  map (\w -> read w * mmFactor "px") $ words xs :: [Float]
  
getInfo font fl = do
  svg <- readFile fl
  let xml = X.parseXML svg
      text = filter (\(tx, fs, tm) -> isHorizontal tm) $ concatMap (parseElement (identity 3)) xml
      (title, fontSize, tM) = maximumBy (\(l0,s0, t0) (l1, s1, t1) -> compare s0 s1) text
      titles = concatMap (\(a,b,c) -> a) $ filter (\(t,fs, m) -> fs >= 0.95 * fontSize) text
      title' = map (insertSpaces font fontSize) titles
  putStr $ intercalate " " $ map fixcase title'

fixcase str = (fixcase' w) ++ (concatMap (\w -> ' ' : fixcase' w) ws)
  where
    (w:ws) = words str
    fixcase' (c:cs) = toUpper c : map toLower cs

parseStyle style = map (\f -> let (k,v) = span (/=':') f in (k,tail v) ) $ splitBy (==';') style

main = do
  (ttf:fls) <- getArgs
  fontE <- loadFontFile ttf
  case fontE of
    Right font -> mapM_ (getInfo font) fls
    Left msg -> putStrLn msg
