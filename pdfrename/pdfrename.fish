#!/usr/bin/env fish

set Font /usr/share/fonts/liberation/LiberationSerif-Regular.ttf

set Dir /tmp/"$USER/pdfguessname"
mkdir -p $Dir
set Tmp (mktemp -p $Dir tmp.XXXXXX)

set File $argv[1]
set OutDir (dirname $File)
# set Title0 (pdfinfo $File | grep Title | sed 's/[^:]*: *//')
inkscape --export-type=svg --export-filename=$Tmp.svg $File
set Title1 (pdfguesstitle $Font $Tmp.svg)
echo '"'$Title1'"'
mv $File $OutDir/$Title1".pdf"
rm $Tmp.svg
rm $Tmp
