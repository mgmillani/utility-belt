#!/usr/bin/env fish

set PKGS $argv
set PKGLIST (echo p.$PKGS)
nix-shell -E "let pkgs = import <nixpkgs> {}; in pkgs.haskellPackages.ghcWithPackages (p: ["$PKGLIST"])" --run ghci
