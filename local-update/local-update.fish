#!/usr/bin/fish

cd $XDG_CONFIG_HOME/local-update ; or exit 1
for Repo in *
	cd $Repo
	git pull
	make install prefix=~/.local
	cd ..
end
