#!/usr/bin/env fish

function canAskUser
	if test ! -z "$LastQuery"
		set Now (date +%s)
		set Diff (math "$Now - $LastQuery")
		if test $Diff -gt $QueryInterval
			echo 1
		else
			echo 0
		end
	else
		echo 1
	end
end


if not set -q Directory
	set Directory /var/lib/nixos-auto-upgrade/
end

if not set -q QueryInterval
	set QueryInterval (math "60 * 60")
end

set Dir $Directory/users/$USER
mkdir -p $Dir
if test $status != 0
	echo "Failed to create directory. Aborting"
	notify-send -u critical "nixos-auto-upgrade" "Failed to create $Dir. Aborting."
end
cd $Dir

if not set -q XDG_DATA_HOME
	set LastQueryFile $XDG_DATA_HOME/nixos-auto-upgrade/last-query
else
	set LastQueryFile $HOME/.data/nixos-auto-upgrade/last-query
end
mkdir -p (dirname $LastQueryFile)

if test -e $LastQueryFile
	set LastQuery (cat $LastQueryFile)
end

set Input input
set Output output
# rm -f $Input $Output
if test ! -e $Input
	mkfifo $Input
end
if test ! -e $Output
	mkfifo $Output
end

# echo "ready" > $Output

while true
	for Line in (cat $Input)
		echo "Got $Line"
		if test "$Line" = "update"
			set AskUser (canAskUser)
			if test $AskUser = 1
				set Now (date +%s)
				echo $Now > $LastQueryFile
				set Answer (notify-send "Do you want to update the system now?" --action=update=Update --action=later=Later --wait)
				echo User replied "$Answer"
				if test "$Answer" = "update"
					echo 'update' > $Output
				else
					echo 'later' > $Output
				end
			else
				echo later > $Output
			end
		else if test "$Line" = "retry-fail"
			set AskUser (canAskUser)
			if test $AskUser = 1
				set Answer (notify-send "Last update failed. Do you want to try again now?" --action=update=Update --action=later=Later --wait)
				echo User replied "$Answer"
				if test "$Answer" = "update"
					echo 'update' > $Output
				else
					echo 'later' > $Output
				end
			else
				echo 'later' > $Output
			end
		else if test "$Line" = "update-failed"
			notify-send -u critical "Last update failed."
		else if test "$Line" = "update-start"
			notify-send -u critical "Starting system update..."
		else if test "$Line" = "update-complete"
			notify-send -u critical "Update finished successfully."
		end
	end
end
