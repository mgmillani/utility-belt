#!/usr/bin/env fish

function help
	echo "Usage:"
	echo "nixos-auto-upgrade <user | system> <number of generations to keep> <minimum age of generation to delete> [switch | boot]"
	exit 1
end

function log
	echo (date +%Y-%m-%d-%H:%M): $argv[1]
end

function remove-generations
	set GenDir $argv[1]
	set GenPattern $argv[2]
	set KeepAmount $argv[3]
	set KeepAge $argv[4]
	set Generations (ls --sort=time $GenDir | grep '^'$GenPattern)
	set GenTime (ls -l --time-style=long-iso --sort=time  $GenDir | sed 's:\(.*\)->.*:\1:' | grep $GenPattern | sed 's: \+: :g' | cut -d' ' -f6,7)
	set Now (date +%s)
	for i in (seq (count $GenTime) -1 1)
		if test $i -gt $KeepAmount
			if test (math "($Now - "(date +%s --date="$GenTime[$i]")") / (60 * 60 * 24)") -gt $KeepAge
				rm $GenDir/$Generations[$i]
			end
		end
	end
end

function remove-home-manager-generations
  set KeepAmount $argv[1]
  set KeepAge $argv[2]
  set Generations (home-manager generations)
  set Kept 0
  set Now (date +%s)
  for Gen in $Generations
    set Parts (string split " " --no-empty -f 1,2,5 $Gen)
    if test $Kept = $KeepAmount
      set Age (date --date="$Parts[1] $Parts[2]" +%s)
      if test ! $status = 0
        echo Could not parse home-manager output.
        echo got \"$Gen\".
        exit 1
      end
      set Age (math "$Now - $Age")
      if test $Age -gt $KeepAge
        home-manager remove-generations $Parts[3]
      end
    else
      set Kept (math "$Kept + 1")
    end
  end
end

function update-sys
	set Dir /tmp/nixos-auto-upgrade
	mkdir -p $Dir 
	chmod +r $Dir
	log "Upgrading system"
	nixos-rebuild $When --upgrade 2> $Dir/update-sys.out ; or exit 1
	log "Removing old generations"
	remove-generations /nix/var/nix/profiles system- $KeepAmount $KeepAge
	for User in (ls /home/)
		log "Upgrading $User"
		sudo --login -u (basename $User) (realpath (status filename)) user $KeepAmount $KeepAge
	end
	log "Collecting garbage"
	nix-collect-garbage 2> $Dir/collect-garbage.out
	nix-store --optimise 2> $Dir/nix-store-optimise.out
end

function update-user
	# nix-env generations
	if test x"$XDG_DATA_HOME" = x
		set Dir $HOME/.data/nixos-auto-upgrade
	else
		set Dir $XDG_DATA_HOME/nixos-auto-upgrade
	end
	if test -e $HOME/.config/nixos-auto-upgrade/pre-update
		$HOME/.config/nixos-auto-upgrade/pre-update
	end
	mkdir -p $Dir
	log "Updating channels"
	nix-channel --update
	log "Upgrading nix-env"
	nix-env -u 2> $Dir/nix-env.out
	if test ! $status -eq 0
		exit 1
	end
	remove-generations /nix/var/nix/profiles/per-user/$USER 'profile-.*-link' $KeepAmount $KeepAge
	log "Upgrading home-manager"
	home-manager switch 2> $Dir/home-manager.out
	if test ! $status -eq 0
		exit 1
	end
	remove-home-manager-generations $KeepAmount $KeepAge
	log "Collecting garbage"
	nix-collect-garbage 2> $Dir/collect-garbage.out
	nix-store --optimise 2> $Dir/nix-store-optimise.out
end

if test (count $argv) -lt 3
	help
end

set Action $argv[1]
set KeepAmount $argv[2]
set KeepAge (math '1 * ('(echo $argv[3] | sed 's:\([^0-9]\)$:\10:' | sed 's:w: * 7 + :' | sed 's:m: * 30 + :')')')
set When boot
if test ! -z "$argv[4]"
	if test $argv[4] = boot
		set When boot
	else if test $argv[4] = switch
		set When switch
	else
		echo $argv[4] is not valid. Inform either '"'switch'"' or '"'boot'"'.
	end
end

if test $Action = "system"
	update-sys
else if test $Action = "user"
	update-user
else
	help
end
