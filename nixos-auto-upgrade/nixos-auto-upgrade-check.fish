#!/usr/bin/env fish

function notify
	echo $argv[1]
	for User in (ls users)
		echo $argv[1] > users/$User/input
	end
end

function ask-for-update
	for User in (ls users)
		set Dir users/$User/
		if test -e $Dir/input
			echo "Asking $User..."
			if test $Failed = 1
				timeout 5s echo 'retry-fail' > $Dir/input
			else
				timeout 5s echo 'update' > $Dir/input
			end
			set Answer (timeout 30s cat $Dir/output)
			if test "$Answer" = 'later'
				set UserPermission 0
				set GotAnswer 1
				break
			else if test "$Answer" = 'update'
				set UserPermission 1
				set GotAnswer 1
			end
		end
	end
end

# Update every 5 days
if not set -q UpdateInterval
	set UpdateInterval 432000
end
if not set -q NotificationInterval
	# Bother user at most once per day
	set NotificationInterval 86400
end
if not set -q KeepAmount
	set KeepAmount 3
end
if not set -q KeepAge
	set KeepAge 3w
end
if not set -q Directory
	set Directory /var/lib/nixos-auto-upgrade/
end

if test ! -e "$Directory"
	mkdir -p $Directory
	chmod +r $Directory
end

cd $Directory

set Update 0
set UserPermission 0
set Failed 0
set Now (date +%s)
if test ! -e last-update
	set Update 1
else
	if test -e last-notification
		set Notification (cat last-notification)
		set Diff (math "$Now - $Notification")
		if test $Diff -lt $NotificationInterval
			exit 0
		end
	end
	echo $Now > last-notification
	if test -e failed
		set FailedTime (stat --format=%Y failed)
		set LastUpdate (cat last-update)
		if test $FailedTime -gt $LastUpdate
			set Failed 1
			set Update 1
		else
			rm failed
		end
	else
		set LastUpdate (cat last-update)
		set Diff (math "$Now - $LastUpdate")
		if test $Diff -gt $UpdateInterval
			set Update 1
		end
	end
end

if test (count (ls users)) -gt 0
	echo "Some user is logged in, ask for permission before updating."
	set Tries 10
	set GotAnswer 0
	while test $Tries -gt 0
		ask-for-update
		if test $GotAnswer -eq 0
			set Tries (math "$Tries - 1")
			sleep 15s
		else
			if test "$UserPermission" = 0
				set Update 0
			end
			set Tries 0
		end
	end
end

echo "Finished askings users for permission."

if test $Update -eq 1
	if test $Failed -eq 1
		rm failed
	end

	echo "Starting update"
	notify 'update-start'
	nixos-auto-upgrade system $KeepAmount $KeepAge
	if test $status = 0
		notify 'update-complete'
		echo $Now > last-update
	else
		touch failed
		notify 'update-failed'
	end
end

