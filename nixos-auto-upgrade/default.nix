{ stdenv, runCommandLocal, lib, fish, gnused, coreutils, gnugrep, dmenu, libnotify, makeWrapper
}:
runCommandLocal "nixos-auto-upgrade" {
	script = ./nixos-auto-upgrade.fish;
  updateCheck = ./nixos-auto-upgrade-check.fish;
  updateCheckUser = ./nixos-auto-upgrade-check-user.fish;
	nativeBuildInputs = [ ];

} ''
mkdir -p $out/bin
echo '#!${fish}/bin/fish
set PATH $PATH ${gnused}/bin/ ${gnugrep}/bin ${coreutils}/bin
' > $out/bin/nixos-auto-upgrade
tail -n+2 $script >> $out/bin/nixos-auto-upgrade
chmod +x $out/bin/nixos-auto-upgrade

echo '#!${fish}/bin/fish
set PATH $PATH' $out/bin/ ' ${dmenu}/bin
' > $out/bin/nixos-auto-upgrade-check
tail -n+2 $updateCheck >> $out/bin/nixos-auto-upgrade-check
chmod +x $out/bin/nixos-auto-upgrade-check

echo '#!${fish}/bin/fish
set PATH $PATH ' $out/bin ' ${libnotify}/bin/
' > $out/bin/nixos-auto-upgrade-check-user
tail -n+2 $updateCheckUser >> $out/bin/nixos-auto-upgrade-check-user
chmod +x $out/bin/nixos-auto-upgrade-check-user
''
