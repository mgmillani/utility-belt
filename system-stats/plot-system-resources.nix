{pkgs,...}:

let user = "marcelo"
in
{
  systemd.services.plot-system-resources = {
    enable = true;
    description = "Plot system resource usage";
    serviceConfig = {
      Type = "simple";
      User = user;
      Group = "users";
      ExecStart = "${pkgs.plot-system-resources}/bin/plot-system-resources";
    };
  };
}
