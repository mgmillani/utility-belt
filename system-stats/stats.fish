#!/usr/bin/fish

cd $XDG_DATA_DIR
mkdir -p system-stats
cd system-stats

echo '
rule cpu-csv
  command = sadf -d -O oneday -P all -e 23:59:59 $in > $out
rule mem-csv
  command = sadf -d -O oneday -e 23:59:59 $in -- -r > $out
rule net-csv
  command = sadf -d -O oneday -e 23:59:59 $in --iface=ens3 -- -n DEV > $out
rule plots
  command = plot-system-resources-plot cpu.csv mem.csv net.csv > results/warnings
rule cat-csv
  command = head -n1 $Header > $out ; tail --quiet -n+2 $in >> $out' > stats.ninja

set Parts

for Sample in (string match -re "sa[^r/]*\$" /var/log/sa/sa*)
	set Name (basename $Sample)
	set Parts $Parts $Name
	echo "build $Name-cpu.csv : cpu-csv $Sample"
	echo "build $Name-mem.csv : mem-csv $Sample"
	echo "build $Name-net.csv : net-csv $Sample"
end >> stats.ninja

echo "build cpu.csv : cat-csv " $Parts-cpu.csv >> stats.ninja
echo "  Header = " $Parts[1]-cpu.csv >> stats.ninja
echo "build mem.csv : cat-csv " $Parts-mem.csv >> stats.ninja
echo "  Header = " $Parts[1]-mem.csv >> stats.ninja
echo "build net.csv : cat-csv " $Parts-net.csv >> stats.ninja
echo "  Header = " $Parts[1]-net.csv >> stats.ninja
echo "build results/warnings : plots cpu.csv mem.csv net.csv" >> stats.ninja

ninja -f stats.ninja

if test ! -e results/stats.html
  cp /usr/share/plot-system-resources/stats.html results/
  cp /usr/share/plot-system-resources/style.css results/
end
