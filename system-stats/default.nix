{ fish
, rWrapper
, rPackages
, r ? (rWrapper.override{packages = [ rPackages.ggplot2 ];})
, stdenv
, coreutils
, sysstat
, logrotate
, util-linux
, ninja
, makeWrapper
, which
, gnused
, lib
}:

stdenv.mkDerivation
{
  pname = "cosyre";
  version = "1.0.0.0";
  buildInputs = [ which coreutils fish gnused makeWrapper ]; 
  license = "GNU General Public License v3.0 only";
# lib.licenses.gpl3Only;
	system = builtins.currentSystem;
	src = ./.;
	preInstall = ''mkdir -p  $out/bin
  mkdir -p $out/share
  echo "#!${r}/bin/Rscript" > $out/bin/cosyre-plot
  tail -n+2 cosyre-plot >> $out/bin/cosyre-plot
  chmod +x $out/bin/cosyre-plot
  cp stats.html $out/share/stats.html
  cp style.css $out/share/style.css
  sed "s:/usr/share/cosyre:$out/share/:g" cosyre > $out/bin/cosyre
  chmod +x $out/bin/cosyre
  sed "s:/usr/share/cosyre:$out/share/:g" cosyre-cpu > $out/bin/cosyre-cpu
  chmod +x $out/bin/cosyre-cpu
  sed "s:/usr/share/cosyre:$out/share/:g" cosyre-mem > $out/bin/cosyre-mem
  chmod +x $out/bin/cosyre-mem
  sed "s:/usr/share/cosyre:$out/share/:g" cosyre-network > $out/bin/cosyre-network
  chmod +x $out/bin/cosyre-network
  cp logrotate.conf $out/share/logrotate.conf
  wrapProgram $out/bin/cosyre \
    --prefix PATH : ${lib.makeBinPath [fish util-linux logrotate sysstat]}
  wrapProgram $out/bin/cosyre-cpu \
    --prefix PATH : ${lib.makeBinPath [fish sysstat]}
  '';
}
