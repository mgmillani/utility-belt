#!/usr/bin/env fish

function get-github-plugin-info
  set Name $argv[2]
  set Owner $argv[1]
  echo "( pkgs.vimUtils.buildVimPlugin {
  name = \"$Name\";
  src = "
  nix-prefetch-github --nix $Owner $Name | tail -n+4
  echo ";})"
end


function generate-nix-file
echo "{pkgs,...}:
  ["

# get-github-plugin-info peterbjorgensen sved
# get-github-plugin-info skywind3000 asyncrun.vim
# get-github-plugin-info lervag vimtex
get-github-plugin-info gauteh vim-evince-synctex
get-github-plugin-info renerocksai telekasten.nvim
#get-github-plugin-info kyazdani42 nvim-web-devicons
#get-github-plugin-info folke tokyonight.nvim
#get-github-plugin-info nvim-lualine lualine.nvim
#get-github-plugin-info nvim-telescope telescope.nvim
# get-github-plugin-info L3MON4D3 LuaSnip
# get-github-plugin-info nvim-treesitter nvim-treesitter

echo "]"

end


generate-nix-file > plugins.nix
