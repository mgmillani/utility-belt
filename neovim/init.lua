vim.opt.clipboard="unnamedplus"
vim.opt.autochdir = true
vim.opt.wildmode="longest,list,full"
vim.opt.backspace="indent,eol,start"  -- more powerful backspacing
vim.opt.syntax="on"
vim.cmd('filetype plugin on')
-- Whether to show line number in the gutter
vim.opt.number = false
-- Enable mouse for every mode
vim.opt.mouse = 'a'
-- Ignore case when searching, except if query contains upper-case characters
vim.opt.ignorecase = true
vim.opt.smartcase = true
-- Do not highlight search
vim.opt.hlsearch = false
-- Wrap long lines, preserving indentation
vim.opt.wrap = true
vim.opt.breakindent = true
vim.opt.autoindent = true
vim.opt.linebreak = true
-- Size of a tab
-- vim.opt.ts=2 -- tab size
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = false
vim.opt.textwidth=0
-- How to display certain characters in list mode
vim.opt.list = false
vim.opt.listchars = {eol = '↲', tab = '▸ ', trail = '·'}
-- Font size
vim.opt.guifont = "Monospace:h12"
-- From where we take plugins
vim.opt.runtimepath="~/.config/nvim,$VIM/vimfiles,$VIMRUNTIME,$VIM/vimfiles/after,~/.config/vim/after,/usr/share/vim/vimfiles"
-- Auto-complete
-- Take longest common match
vim.opt.completeopt='longest'
-- Only use tags file for auto-complete
vim.opt.complete=']'
-- for VimWiki
vim.opt.compatible = false
vim.g.vimwiki_list = {{path = "~/Research/Notes/"}}
vim.g.vimwiki_global_ext = 0
-- reload files if they were uptaded on disk
vim.api.nvim_create_autocmd('FocusGained',
	{ pattern = '*'
	, command = ':checktime'
})

-- Setting keybindings
-- vim.keymap.set('n', '<space>w', '<cmd>write<cr>', {desc = 'Save'})
-- Leader is used in some commands
vim.g.mapleader = ' '
-- cp to copy to clipboard, cv to paste from clipboard
vim.keymap.set({'n', 'x'}, 'cy', '"+y')
vim.keymap.set({'n', 'x'}, 'cp', '"+p')

-- move cursor over wrapped lines
vim.keymap.set({'n','x'}, '<UP>', 'gk')
vim.keymap.set({'n','x'}, '<DOWN>', 'gj')
vim.keymap.set({'i', }, '<UP>', '<C-o>gk')
vim.keymap.set({'i',}, '<DOWN>', '<C-o>gj')

-- Build commands
function SmartMake ()
  vim.cmd("write")
	if vim.fn.filereadable(".vim.ninja") then
		vim.cmd('AsyncRun ninja -f .vim.ninja')
	else
		vim.cmd('AsyncRun make')
	end
end

vim.keymap.set({'n', 'x', 'o', 'i'}, '<F9>', SmartMake)
-- vim.keymap.set({'n', 'x', 'o', 'i'}, '<F9>', ':call SmartMake()<ENTER>')

-- Spell checking
vim.opt.spellsuggest="fast,15"
vim.keymap.set({'n', 'x', 'o', 'i'}, '<F2>', ':set spell spelllang=en_us<enter>:set spellfile=~/.config/nvim/spellfiles/spell.en_us.add<enter>')
vim.keymap.set({'n', 'x', 'o', 'i'}, '<F3>', ':set spell spelllang=de<enter>:set spellfile=~/.config/nvim/spellfiles/spell.de.add<enter>')
vim.keymap.set({'n', 'x', 'o', 'i'}, '<F4>', ':set spell spelllang=pt<Enter>:set spellfile=~/.config/nvim/spellfiles/spell.pt.add<enter>')
--vim.fn.highlight('SpellBad', 'cterm=underline', 'ctermbg=DarkRed')
--vim.fn.highlight('SpellCap', 'cterm=underline', 'ctermbg=DarkBlue')
vim.api.nvim_set_hl(0, 'SpellBad', {bg = 'DarkRed', underline=true})
vim.api.nvim_set_hl(0, 'SpellCap', {bg = 'DarkBlue', underline=true})

-- UltiSnips

--vim.g.UltiSnipsSnippetDirectories={"/home/marcelo/.config/nvim/UltiSnips"}
--vim.g.UltiSnipsExpandTrigger="<s-tab>"
--vim.g.UltiSnipsJumpForwardTrigger="<s-tab>"
--vim.g.UltiSnipsJumpBackwardTrigger="<c-tab>"
