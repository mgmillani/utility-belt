#!/usr/bin/env fish
if test (count $argv) = 0
	echo "usage: git-is-clean <Path to repository>"
	exit 1
end

cd $argv[1]
set Modified 0
set Untracked 0
set Unknown 0
for Line in (git status --porcelain=v1)
	set Type (string split " " "$Line" -f 1)
	if test $Type = "M"
		set Modified (math "$Modified + 1")
	else if test $Type = "??"
		set Untracked (math "$Untracked + 1")
	else
		set Unknown (math "$Unknown + 1")
	end
end
if test $status != 0
	echo "Error running git on $argv[1]." 1>&2
	exit 2
end
set Lines
if test $Modified -gt 0
	if test $Modified = 1
		set Files file
	else
		set Files files
	end
	set Lines $Lines "$Modified modified $files"
end
if test $Untracked -gt 0
	if test $Untracked = 1
		set Files file
	else
		set Files files
	end
	set Lines $Lines "$Untracked untracked $Files"
end
if test $Unknown -gt 0
	if test $Unknown = 1
		set Files file
	else
		set Files files
	end
	set Lines $Lines "$Unknown $Files of unknown status"
end
	
for Remote in (git remote show)
	for Line in (git remote show $Remote | grep 'pushes to.*([^)]*)$')
		set Fields (echo $Line | sed 's: *\([^ ]*\)[^)]*(\([^)]*\))$:\1 \2:' | string split ' ' --no-empty)
		set Branch $Fields[1]
		set Status $Fields[2..]
		if test "$Status" != "up to date"
			set Lines $Lines "$Remote/$Branch: $Status"
		end
	end
end

if test (count $Lines) -gt 0
	printf "%s\n" $Lines
end
