#!/usr/bin/env fish

if test (count $argv) -eq 0
	set -q XDG_CONFIG_HOME
	if test $status -eq 0
		set Directory $XDG_CONFIG_HOME/git-is-clean
	else
		set Directory $HOME/.config/git-is-clean
	end
	if test ! -e $Directory
		mkdir -p $Directory
		echo "No repositories founds in $Directory and none given as argument. Exiting." 1>&2
		exit 1
	end
	set Repos (find $Directory -mindepth 1 -maxdepth 1 -type d)
	if test (count $Repos) -eq 0
		echo "No repositories founds in $Directory and none given as argument. Exiting." 1>&2
		exit 1
	end
else
	set Repos $argv
end

for Repo in $Repos
	set Status (git-is-clean $Repo)
	if test (count $Status) -gt 0
		notify-send $Repo "$(printf "%s\n" $Status)"
	end
end


