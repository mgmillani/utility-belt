{ stdenv, runCommandLocal, lib, fish, gnused, coreutils, gnugrep, libnotify, makeWrapper
}:
runCommandLocal "nixos-auto-upgrade" {
	gitIsClean = ./git-is-clean.fish;
  gitIsCleanNotify = ./git-is-clean-notify.fish;
	nativeBuildInputs = [ makeWrapper ];

} ''
	makeWrapper $gitIsClean $out/bin/git-is-clean \
	--prefix PATH : ${lib.makeBinPath [ fish gnused coreutils gnugrep ]}
	makeWrapper $gitIsCleanNotify $out/bin/git-is-clean-notify \
	--prefix PATH : $out/bin:${lib.makeBinPath [ fish libnotify ]}
''
