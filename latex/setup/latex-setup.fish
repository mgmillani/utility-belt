#!/usr/bin/env fish
if not set --query LATEX_CONFIGURATION
	echo Please set LATEX_CONFIGURATION to the directory containing the configuration files.
	exit 1
end

function copy_file
	for File in $argv
		if test ! -e $File
			if test -e $LATEX_CONFIGURATION/$File
				cp $LATEX_CONFIGURATION/$File .
			else
				echo "File $LATEX_CONFIGURATION/$File is missing."
			end
		else
			echo "$File already exists; file will not be overwritten."
		end
	end
end

copy_file rofi-tag.sh latex.ctags rules.ninja .vim.ninja setup_commands.fish build.fish clean-submission.hs

if test ! -e ./setup_commands.fish
	echo "File ./setup_commands.fish not found. Aborting."
	exit 2
end
source ./setup_commands.fish

set MainFiles (grep -l '[^%]*\\\\documentclass' *.tex)
set AllFiles

if test (count $MainFiles) = 0
	echo "No main .tex file found."
else
	echo 'include rules.ninja' > build.ninja
	for Main in $MainFiles
		set Dependencies (tex-toch dependencies $Main)
		set AllFiles $AllFiles $Main $Dependencies
		set Base (basename $Main .tex)
		add_target $Main $Dependencies
	end
end

echo 'build tags : ctags ' (printf '%s\n' $AllDependencies | sed 's: :$ :g') >> build.ninja
