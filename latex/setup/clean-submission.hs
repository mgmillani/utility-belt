module Main where

import TeXTools.Evaluation
import TeXTools.Parser
import TeXTools.Utils
import Text.LaTeX.Base.Syntax
import Text.LaTeX.Base.Parser
import Text.LaTeX.Base.Render
import System.Directory
import System.FilePath
import System.Environment
import System.IO
import Control.Monad
import qualified Data.Text as T
import qualified Data.Text.IO as T

main = do
  args <- getArgs
  results <- forM args $ \fl -> withFile fl ReadMode $ \h -> do
    dir0 <- getCurrentDirectory
    setCurrentDirectory (takeDirectory fl)
    str <- T.hGetContents h
    eTex <- expandInput str
    setCurrentDirectory dir0
    case eTex of
      (Left err) -> do
        print err
        return $ T.pack ""
      (Right tex') -> do
        let texStr = render $ evaluateExpression mymacros $ evaluateIfs $ removeComments tex'
        seq (T.length $ texStr) $ return texStr
  mapM_ T.putStrLn results

mymacros = [qedproof, appendixproof, toappendix, task]

qedproof (TeXEnv "proof" args env) = Just $ TeXEnv "proof" args (env <> TeXCommS "qed")
qedproof tex@(TeXComm "end" [FixArg name])
  | name == TeXRaw (T.pack "proof") = Just $ (TeXCommS "qed" ) <> tex
  | otherwise = Nothing
qedproof _ = Nothing

appendixproof (TeXComm "appendixproof" [FixArg label, FixArg content]) = Just TeXEmpty
appendixproof _ = Nothing

toappendix (TeXComm "toappendix" [FixArg title, FixArg label, FixArg content]) = Just TeXEmpty
toappendix _ = Nothing

task (TeXComm "task" [FixArg _]) = Just TeXEmpty
task _ = Nothing
