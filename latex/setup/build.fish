#!/usr/bin/env fish

set Here (pwd)

if test -e Figures
	if test -e Figures/build.fish
		cd Figures
		./build.fish
		cd $Here
	end
end

ninja tags
ninja $argv

