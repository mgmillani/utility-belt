function add_target
	set Main $argv[1]
	set Dependencies $argv[2..]
	set Base (basename $Main .tex)
	set BuildDir .build.$Base
	echo "build $Base.pdf : latexrun $Main $Dependencies
    main = $Main
    build_dir = $BuildDir
" >> build.ninja
	echo "build clean-$Base.tex : clean $Main $Dependencies
    main = $Main
" >> build.ninja
	mkdir -p $BuildDir
	for Dep in $Dependencies
		if echo $Dep | grep '\\.bib$'
			if test ! -e $BuildDir/(basename $Dep)
				ln -s (realpath $Dep) $BuildDir/
			end
			break
		end
	end
	if test ! -e $Base.synctex.gz
		ln -s $BuildDir/$Base.synctex.gz .
	end
end
