#!/usr/bin/env runhaskell

module Main where

import Control.Applicative
import Control.Monad (liftM, liftM2, liftM3, mplus, replicateM, forM, when)
import Control.Monad.Trans.State as ST
import Data.Char
import Data.List
import Data.Maybe
import qualified Data.Map as M
import qualified Data.Set as S
import System.Environment
import System.FilePath
import System.Directory
import System.IO
import qualified HGraph.Directed.Output as D
import qualified Data.Text as T
import qualified Data.Text.IO as T
import           HGraph.Directed as D
import Text.LaTeX.Base.Syntax
import Text.LaTeX.Base.Render

import TeXTools.Parser
import TeXTools.Utils
import TeXTools.Parser.Math as X
import TeXTools.ReferenceDigraph
import TeXTools.Evaluation
import TeXTools.Dependencies

data Command =
  ReferenceDigraph
    { cmdInputFiles :: [FilePath]
    , cmdEnvironments :: [String]
    }
  | Dependencies
    { cmdInputFiles :: [FilePath]
    }
  | Math
    { cmdInputFiles :: [FilePath]
    }
  | Evaluate
    { cmdInputFiles :: [FilePath]
    }
  | FetchMissingCommands
    { cmdInputFile          :: FilePath
    , cmdCommandFile        :: FilePath
    , cmdGlobalCommandFiles :: [FilePath]
    }
  | Help

defaultEnvironments = ["theorem", "lemma", "corollary", "proposition", "observation"]

myTokens = 
  [ \t -> case t of
              TeXComm "bound" (label : name : args) -> 
                case tokenizeArgsBy myTokens args of
                  Left err -> Nothing
                  Right args' ->
                    Just $ [TokFunction "bound" $
                                     [ [TokTerm $ Variable $ concat $ fixedArgsToText [label]]
                                     , [TokTerm $ Variable $ concat $ fixedArgsToText [name]]]
                                     ++ args']
              _ -> Nothing
  ]

main = do
  args <- getArgs
  case commands args of
    cmd@(ReferenceDigraph{}) -> do
      ds <- fmap (map fromJust . filter isJust) $
        forM (cmdInputFiles cmd) $ \fl -> withFile fl ReadMode $ \h -> do
          dir0 <- getCurrentDirectory
          setCurrentDirectory (takeDirectory fl)
          str <- T.hGetContents h
          eTex <- expandInput str
          setCurrentDirectory dir0
          case eTex of
            (Left err) -> do
              print err
              return Nothing
            (Right tex') -> do
              let (d, atts) = referenceDigraph tex' (cmdEnvironments cmd)
              seq (length $ arcs d) $ return $ Just (d, atts)
      let ds'   = map fst ds
          atts' = map snd ds
          atts  = foldr (M.unionWith (++)) M.empty atts'
          d     = foldr D.union (head ds') (tail ds')
      putStrLn $ D.toDot d 
        D.defaultDotStyle
          { D.everyNode = [("shape","rectangle")]
          , D.nodeAttributes = atts
          }
    cmd@(Dependencies{}) -> do
      fls <- recursiveDependencies (cmdInputFiles cmd)
      mapM_ putStrLn $ S.toList $ fls
    cmd@(Math{}) -> do
      forM (cmdInputFiles cmd) $ \fl -> withFile fl ReadMode $ \h -> do
          dir0 <- getCurrentDirectory
          setCurrentDirectory (takeDirectory fl)
          str <- T.hGetContents h
          eTex <- expandInput str
          setCurrentDirectory dir0
          case eTex of
            (Left err) -> do
              print err
            (Right tex') -> do
              let math = map (X.parseBy myTokens) $ filterMath tex'
              mapM_ (\m -> print m >> putStrLn "") $ 
                    filter (\m -> fmap isEquation m == Right True) $
                           math
      return ()
    cmd@(Evaluate{}) -> do
      ts <- 
        forM (cmdInputFiles cmd) $ \fl -> withFile fl ReadMode $ \h -> do
          dir0 <- getCurrentDirectory
          setCurrentDirectory (takeDirectory fl)
          str <- T.hGetContents h
          eTex <- expandInput str
          setCurrentDirectory dir0
          case eTex of
            (Left err) -> do
              print err
              return $ T.pack ""
            (Right tex') -> do
              let texStr = render $ evaluateIfs $ removeComments tex'
              seq (T.length $ texStr) $ return texStr
      mapM_ (T.putStrLn) ts
    cmd@(FetchMissingCommands{}) -> do
      tex <- withFile (cmdInputFile cmd) ReadMode $ \h -> do
          dir0 <- getCurrentDirectory
          setCurrentDirectory (takeDirectory $ cmdInputFile cmd)
          str <- T.hGetContents h
          eTex <- expandInput str
          setCurrentDirectory dir0
          case eTex of
            (Left err) -> do
              print err
              return $ TeXEmpty
            (Right tex') -> 
              return tex'
      commands <- withFile (cmdInputFile cmd) ReadMode $ \h -> do
          dir0 <- getCurrentDirectory
          setCurrentDirectory (takeDirectory $ cmdCommandFile cmd)
          str <- T.hGetContents h
          eTex <- expandInput str
          setCurrentDirectory dir0
          case eTex of
            (Left err) -> do
              print err
              return $ []
            (Right tex') -> do
              case definedCommands [] tex' of
                Right cmds -> return cmds
                Left err -> do
                  print err
                  return []
      allCommands <- fmap concat $
        forM (cmdGlobalCommandFiles cmd) $ \fl -> withFile fl ReadMode $ \h -> do
          dir0 <- getCurrentDirectory
          setCurrentDirectory (takeDirectory fl)
          str <- T.hGetContents h
          eTex <- expandInput str
          setCurrentDirectory dir0
          case eTex of
            (Left err) -> do
              print err
              return $ []
            (Right tex') ->
              case definedCommands [] tex' of
                Right cmds -> return cmds
                Left err -> do
                  print err
                  return []
      let missing = missingCommands tex commands allCommands
      when (not $ null missing) $ do
        withFile (cmdCommandFile cmd) AppendMode $ \h -> do
          mapM_ (\cmd -> T.hPutStrLn h $ render cmd) missing
    cmd@(Help) -> do
      putStrLn "usage:"
      putStrLn "  tex-tools reference-digraph <Files...> -- <Environments...>"
      putStrLn "  tex-tools dependencies <Files...>"
      putStrLn "  tex-tools math <Files...>"
      putStrLn "  tex-tools evaluate <Files...>"
      putStrLn "  tex-tools fetch-missing-commands <main.tex> <current_definitions.sty> <other_definitions.sty...>"

isEquation (BinaryOperator "=" _ _) = True
isEquation _ = False

commands args = case args of
  ("reference-digraph" : args') -> 
    let (fls, envs) = span (/="--") args'
    in 
    ReferenceDigraph{ cmdInputFiles = fls
                    , cmdEnvironments = if null envs then defaultEnvironments else envs
                    }
  ("dependencies" : fls) -> Dependencies{cmdInputFiles = fls}
  ("math" : fls) -> Math{cmdInputFiles = fls}
  ("evaluate" : fls) -> Evaluate{cmdInputFiles = fls}
  ("fetch-missing-commands" : main : local : fls) -> 
    FetchMissingCommands
      { cmdInputFile = main
      , cmdCommandFile = local
      , cmdGlobalCommandFiles = fls
      }
  _ -> Help

recursiveDependencies fls = dependencies' S.empty fls
  where
    dependencies' found [] = return found
    dependencies' found fls = do
      deps <- forM fls $ \fl -> withFile fl ReadMode $ \h -> do
            str <- T.hGetContents h
            let deps = dependencies str
            deps' <- liftM (map fromJust . filter isJust) $ forM deps $ \d -> do
              exists <- doesFileExist d
              if exists then
                return $ Just d
              else
                return Nothing
            seq (length deps') $ return deps'
      let newDeps =  (S.fromList $ concat deps) `S.difference` found
      dependencies' (found `S.union` newDeps) (S.toList newDeps)
        

