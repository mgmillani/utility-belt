module Main where

import System.Environment
import System.FilePath
import System.Directory
import System.IO
import Data.Char
import Data.List
import Data.Maybe
import Control.Monad.Trans.State
import Control.Monad
import qualified TeXTools.Parser.Utils as TeX
import qualified TeXTools.Parser as TeX
import qualified TeXTools.TeX as TeX
import qualified Data.Set as S

environments = S.fromList [ "corollary"
                          , "definition"
                          , "lemma"
                          , "observation"
                          , "proof"
                          , "proposition"
                          , "theorem"
                          , "thm"
                          , "align"
                          , "align*"
                          , "defi"
                          ]
sections = S.fromList ["section", "subsection", "paragraph", "subsubsection"]

main = do
  fls <- getArgs
  forM fls $ \fl -> withFile fl ReadMode $ \h -> do
    str <- hGetContents h
    case TeX.parse str of
      Left err -> do
        hPutStr stderr $ "Error on \"" ++ fl ++ "\":\n" ++ (show err) ++ "\n"
        fail "1"
      Right tex -> do
        mapM_ (putStr . show) $ evalState filterTex tex

filterTex :: State [TeX.TeX] [TeX.TeX]
filterTex = do
  mMacro <- TeX.nextMacro
  case mMacro of
    Just m@(TeX.Macro macro) -> do
      if macro `S.member` sections then do
        sname <- TeX.nextElement
        case sname of
          Just name -> do
            fmap (\l -> m : name : l ) filterTex
          Nothing -> return [m]
      else if macro == "begin" then do
        ename <- TeX.nextElement
        case ename of
          Nothing -> return []
          Just g@(TeX.Group [TeX.Text name]) -> do
            if name `S.member` environments then do
              env <- TeX.findMatchingEnd
              fmap ((m : g : env) ++) filterTex
            else
              filterTex
      else
        filterTex
    Nothing -> return []
    _ -> filterTex
