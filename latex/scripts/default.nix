{ haskellPackages, lib, stdenv, ... }:

haskellPackages.callPackage ./tex-tools.nix 
  { lib = lib
  ; tex-tools-lib = haskellPackages.callPackage ./lib/tex-tools-lib.nix {}
  ; 
  }
