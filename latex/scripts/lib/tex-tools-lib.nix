{ mkDerivation, base, containers, directory, filepath, HaTeX
, hgraph, HUnit, lib, mtl, parsec, text, transformers
}:
mkDerivation {
  pname = "tex-tools-lib";
  version = "0.3.0.0";
  src = ./.;
  libraryHaskellDepends = [
    base containers directory filepath HaTeX hgraph mtl parsec text
    transformers
  ];
  testHaskellDepends = [
    base containers directory HaTeX hgraph HUnit mtl text transformers
  ];
  license = "unknown";
}
