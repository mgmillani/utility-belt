with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "tex-tools-lib";
  buildInputs =
    [ cabal-install 
      (haskellPackages.ghcWithPackages
        (p: [ p.containers
              p.filepath
              p.HaTeX
              p.hgraph
              p.mtl
              p.parsec
              p.text
              p.HUnit
              p.transformers
            ]
        ))
    ];
}
