{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified TeXTools.Parser as T
import TeXTools.Utils
import TeXTools.Parser.Math.Internal

import Data.Either
import qualified Data.Text as Tx
import qualified Data.Text.IO as Tx
import Control.Monad.Trans.State
import Text.LaTeX.Base.Syntax
import System.Exit (exitFailure, exitSuccess)
import System.IO
import Test.HUnit

tests = TestList                         
  [ TestLabel "Math 0" $ TestCase
    ( do
      let math0 = T.parse $ Tx.pack "$3a + 17bc$"
          math1 = T.parse $ Tx.pack "$3\\cdot a + 17 \\cdot b \\times c$"
          math2 = T.parse $ Tx.pack "$2\\Function{a,2a}$"
      assertBool "Parse math0" (isRight math0)
      assertBool "Parse math1" (isRight math1)
      assertBool "Parse math2" (isRight math2)
      let Right math0' = math0
          Right math1' = math1
          Right math2' = math2
      assertEqual "Tokens"
                  (Right
                    [ TokTerm (Integer 3)
                    , TokOperator "*"
                    , TokTerm (Variable "a")
                    , TokOperator "+"
                    , TokTerm (Integer 17)
                    , TokOperator "*"
                    , TokTerm (Variable "b")
                    , TokOperator "*"
                    , TokTerm (Variable "c")
                    ]
                  )
                  (fmap explicitProduct $ tokenizeBy [defaultTokens] math0')
      assertEqual "Tokens"
                  (Right
                    [ TokTerm (Integer 3)
                    , TokOperator "*"
                    , TokTerm (Variable "a")
                    , TokOperator "+"
                    , TokTerm (Integer 17)
                    , TokOperator "*"
                    , TokTerm (Variable "b")
                    , TokOperator "*"
                    , TokTerm (Variable "c")
                    ]
                  )
                  (fmap explicitProduct $ tokenizeBy [defaultTokens] math1')
      assertEqual "Tokens"
                  (Right
                    [ TokTerm (Integer 2)
                    , TokOperator "*"
                    , TokFunction "Function" [[ TokTerm (Variable "a")
                                             , TokOperator ","
                                             , TokTerm (Integer 2)
                                             , TokOperator "*"
                                             , TokTerm (Variable "a")]]
                    ]
                  )
                  (fmap explicitProduct $ tokenizeBy [defaultTokens] math2')
      let expr0 = (Right $ BinaryOperator "+"
                      (BinaryOperator "*"
                        (Value $ Integer 3)
                        (Value $ Variable "a"))
                      (BinaryOperator "*"
                        (BinaryOperator "*"
                          (Value $ Integer 17)
                          (Value $ Variable "b"))
                        (Value $ Variable "c"))
                  )
      assertEqual "Expression"
                  expr0
                  (parse math0')
      assertEqual "Expression"
                  expr0
                  (parse math1')
    )
  , TestLabel "Math Custom 1" $ TestCase
    ( do
      let tex' = T.parse $ "\\(\\bound{lemma:something++}{f}{a,b} = a \\cdot b^2\\)"
      assertBool "Parse tex" (isRight tex')
      let Right tex = tex'
      let myTokens = 
            [ \t -> case t of
                      TeXComm "bound" (label : name : args) -> 
                        case tokenizeArgsBy myTokens args of
                          Left err -> Nothing
                          Right args' ->
                            Just $ [TokFunction "bound" $
                                             [ [TokTerm $ Variable $ concat $ fixedArgsToText [label]]
                                             , [TokTerm $ Variable $ concat $ fixedArgsToText [name]]]
                                             ++ args']
                      _ -> Nothing
            ]
      let mathTex = filterMath tex
          math0   = map (tokenizeBy myTokens) mathTex
      assertEqual "Tokens 1"
                  [(Right
                    [ TokFunction "bound" 
                                  [ [TokTerm (Variable "lemma:something++")]
                                  , [TokTerm (Variable "f")]
                                  , [TokTerm (Variable "a"), TokOperator ",", TokTerm (Variable "b")]
                                  ]
                    , TokOperator "="
                    , TokTerm (Variable "a")
                    , TokFunction "cdot" []
                    , TokTerm (Variable "b")
                    , TokOperator "^"
                    , TokTerm (Integer 2)
                    ]
                  )]
                  (math0)
    )
  , TestLabel "Filter math 0" $ TestCase
    ( do
      tex0 <- withFile "tests/filter-math-0.tex" ReadMode $ \h -> do
        tex <- fmap T.parse $ Tx.hGetContents h
        assertBool "Parse tex" (isRight tex)
        let Right tex' = tex
        return tex'

      assertEqual "Math environments"
                  [ TeXSeq (TeXRaw "x ") (TeXSeq (TeXCommS "coloneqq") (TeXRaw " 1"))
                  , TeXRaw "y = 2x"
                  , TeXSeq (TeXRaw "z = 2^") (TeXBraces (TeXRaw "x + y^2"))
                  , TeXSeq (TeXRaw "\n    x + y = x + 2x") (TeXSeq (TeXLineBreak Nothing False) (TeXRaw "\n  "))
                  ]
                  (filterMath tex0)
      let math0 = map parse $ filterMath tex0
          math0Correct = 
            [ Right $ BinaryOperator "="
                (Value $ Variable "x")
                (Value $ Integer 1)
            , Right $ BinaryOperator "="
                (Value $ Variable "y")
                (BinaryOperator "*"
                  (Value $ Integer 2)
                  (Value $ Variable "x"))
            , Right $ BinaryOperator "="
                (Value $ Variable "z")
                (BinaryOperator "^"
                  (Value $ Integer 2)
                  (BinaryOperator "+"
                    (Value $ Variable "x")
                    (BinaryOperator "^"
                      (Value $ Variable "y")
                      (Value $ Integer 2)
                    )
                  )
                )
            , Right $ BinaryOperator "="
                (BinaryOperator "+"
                  (Value $ Variable "x")
                  (Value $ Variable "y")
                )
                (BinaryOperator "+"
                  (Value $ Variable "x")
                  (BinaryOperator "*"
                    (Value $ Integer 2)
                    (Value $ Variable "x")
                  )
                )
            ]
      assertEqual "Math 0"
                  math0Correct
                  math0
    )
  , TestLabel "Filter math 1" $ TestCase
    ( do
      let tex' = T.parse $ "\\begin{align}\n\
                           \ x & = 1,\\\\\n\
                           \ y & = x + x\\\\\n\
                           \   & = 2\n\
                           \ \\end{align}"
      assertBool "Parse tex" (isRight tex')
      let Right tex = tex'

      assertEqual "Math environments"
                  [ TeXSeq  (TeXRaw "\n x & = 1,")
                    (TeXSeq (TeXLineBreak Nothing False)
                    (TeXSeq (TeXRaw "\n y & = x + x")
                    (TeXSeq (TeXLineBreak Nothing False)
                            (TeXRaw "\n   & = 2\n "))))
                  ]
                  --[ TeXSeq (TeXRaw "\n x & = 1,") (TeXLineBreak Nothing False)
                  --, TeXSeq (TeXRaw "\n y & = x + x,") (TeXSeq (TeXLineBreak Nothing False) (TeXRaw "\n   & = 2\n "))
                  --]
                  (filterMath tex)
      let math0 = map parse $ filterMath tex
          math0Correct = 
            [ Right $ BinaryOperator ","
                (BinaryOperator "="
                  (Value $ Variable "x")
                  (Value $ Integer 1))
                (BinaryOperator "="
                  (BinaryOperator "="
                    (Value $ Variable "y")
                    (BinaryOperator "+"
                      (Value $ Variable "x")
                      (Value $ Variable "x")))
                  (Value $ Integer 2))
            ]
      assertEqual "Math 0"
                  math0Correct
                  math0
    )
  , TestLabel "Filter math 2" $ TestCase
    ( do
      let tex' = T.parse $ "\\begin{align}\n\
                           \ x & = 1,\\\\\n\
                           \ y & = x + x\\\\\n\
                           \   & = 2.\n\
                           \ \\end{align}"
          tex1' = T.parse $ "\\begin{align*}\n\
                             \ \\Fun & \\coloneqq\n\
                             \ 1, \\\\\n\
                             \ \\Fun & \\coloneqq\n\
                             \ 2.\n\
                             \\\end{align*}"
          tex2' = T.parse $ "\\begin{align*}\n\
                            \ \\Fun & = 1,\\\\\n\
                            \ \\Fun & = 2,\\\\\n\
                            \ \\Fun & = 3.\n\
                            \\\end{align*}"

      assertBool "Parse tex" (isRight tex')
      let Right tex = tex'
      assertBool "Parse tex" (isRight tex1')
      let Right tex1 = tex1'
      assertBool "Parse tex" (isRight tex2')
      let Right tex2 = tex2'

      assertEqual "Math environments"
                  [TeXSeq (TeXRaw "\n x & = 1,") 
                  (TeXSeq (TeXLineBreak Nothing False)
                  (TeXSeq (TeXRaw "\n y & = x + x")
                  (TeXSeq (TeXLineBreak Nothing False)
                          (TeXRaw "\n   & = 2.\n "))))
                  ]
                  (filterMath tex)
      assertEqual "Math environments"
                  [TeXSeq (TeXRaw "\n ") 
                  (TeXSeq (TeXCommS "Fun") 
                  (TeXSeq (TeXRaw " & ") 
                  (TeXSeq (TeXCommS "coloneqq")
                  (TeXSeq (TeXRaw "\n 1, ")
                  (TeXSeq (TeXLineBreak Nothing False)
                  (TeXSeq (TeXRaw "\n ")
                  (TeXSeq (TeXCommS "Fun")
                  (TeXSeq (TeXRaw " & ")
                  (TeXSeq (TeXCommS "coloneqq")
                          (TeXRaw "\n 2.\n"))))))))))]
                  (filterMath tex1)
      assertEqual "Math environments"
                  [TeXSeq (TeXRaw "\n ") 
                  (TeXSeq (TeXCommS "Fun") 
                  (TeXSeq (TeXRaw " & = 1,") 
                  (TeXSeq (TeXLineBreak Nothing False)
                  (TeXSeq (TeXRaw "\n ")
                  (TeXSeq (TeXCommS "Fun")
                  (TeXSeq (TeXRaw " & = 2,") 
                  (TeXSeq (TeXLineBreak Nothing False)
                  (TeXSeq (TeXRaw "\n ")
                  (TeXSeq (TeXCommS "Fun")
                          (TeXRaw " & = 3.\n") 
                          )))))))))]
                  (filterMath tex2)
      let math0 = map parse $ filterMath tex
          math0Correct = 
            [ Right $ BinaryOperator ","
                (BinaryOperator "="
                  (Value $ Variable "x")
                  (Value $ Integer 1))
                (BinaryOperator "="
                  (BinaryOperator "="
                    (Value $ Variable "y")
                    (BinaryOperator "+"
                      (Value $ Variable "x")
                      (Value $ Variable "x")))
                  (Value $ Integer 2))
            ]
      let math1 = map parse $ filterMath tex1
          math1Correct = 
            [ Right 
                (BinaryOperator "," 
                  (BinaryOperator "=" (Function "Fun" []) (Value (Integer 1)))
                  (BinaryOperator "=" (Function "Fun" []) (Value (Integer 2))))]
      let math2 = map parse $ filterMath tex2
          math2Correct = 
            [ Right 
                (BinaryOperator "," 
                  (BinaryOperator "," 
                    (BinaryOperator "=" (Function "Fun" []) (Value (Integer 1)))
                    (BinaryOperator "=" (Function "Fun" []) (Value (Integer 2))))
                  (BinaryOperator "=" (Function "Fun" []) (Value (Integer 3))))]
      assertEqual "Math 0"
                  math0Correct
                  math0
      assertEqual "Math 1"
                  math1Correct
                  math1
      assertEqual "Math 2"
                  math2Correct
                  math2
    )
  , TestLabel "Product vs. parameter" $ TestCase
    ( do
      let math0 = T.parse $ Tx.pack "$f(3)$"
          math1 = T.parse $ Tx.pack "$x_{1} y_{2}$"
          math2 = T.parse $ Tx.pack "$q' = x + x''$"
      assertBool "Parse math0" (isRight math0)
      assertBool "Parse math1" (isRight math1)
      assertBool "Parse math2" (isRight math2)
      let Right math0' = math0
          Right math1' = math1
          Right math2' = math2
      assertEqual "Tokens"
                  (Right
                    [ TokTerm (Variable "f")
                    , TokOperator ""
                    , TokOpen '('
                    , TokTerm (Integer 3)
                    , TokClose '('
                    ]
                  )
                  (fmap explicitProduct $ tokenizeBy [defaultTokens] math0')
      assertEqual "Tokens"
                  (Right
                    [ TokTerm (Variable "x")
                    , TokOperator "_"
                    , TokOpen '('
                    , TokTerm (Integer 1)
                    , TokClose '('
                    , TokOperator ""
                    , TokTerm (Variable "y")
                    , TokOperator "_"
                    , TokOpen '('
                    , TokTerm (Integer 2)
                    , TokClose '('
                    ]
                  )
                  (fmap explicitProduct $ tokenizeBy [defaultTokens] math1')
      assertEqual "Tokens"
                  (Right
                    [ TokTerm (Variable "q'")
                    , TokOperator "="
                    , TokTerm (Variable "x")
                    , TokOperator "+"
                    , TokTerm (Variable "x''")
                    ]
                  )
                  (fmap explicitProduct $ tokenizeBy [defaultTokens] math2')
      let expr0 = (Right $ BinaryOperator ""
                        (Value $ Variable "f")
                        (Value $ Integer 3))
          expr1 = (Right $ BinaryOperator ""
                        ( BinaryOperator "_"
                          (Value $ Variable "x")
                          (Value $ Integer 1)
                        )
                        ( BinaryOperator "_"
                          (Value $ Variable "y")
                          (Value $ Integer 2)
                        ))
      assertEqual "Expression"
                  expr0
                  (parse math0')
      assertEqual "Expression"
                  expr1
                 (parse math1')
    )
  ]

main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
