{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified TeXTools.Parser as T
import TeXTools.Utils
import TeXTools.Evaluation.Declaration.Internal

import Data.Either
import qualified Data.Text as Tx
import qualified Data.Text.IO as Tx
import Control.Monad.Trans.State
import Text.LaTeX.Base.Syntax
import Text.LaTeX.Base.Render
import System.Exit (exitFailure, exitSuccess)
import System.IO
import Test.HUnit

tests = TestList                         
  [ TestLabel "Macro definitions 0" $ TestCase
    ( do
      let et0 = T.parse $ Tx.pack "\\newcommand{\\MyMacro}{Macro}"
      assertBool "Parse ok" (isRight et0)
      let Right t0 = et0
      assertEqual "MyMacro"
        (Right
          [MacroDef
            { mdName = "\\MyMacro"
            , mdDef = (TeXComm "newcommand"
                [ FixArg $ TeXCommS $ "MyMacro"
                , FixArg $ TeXRaw $ Tx.pack "Macro"])
            }])
        (definedCommands [] t0)
    )
  , TestLabel "Macro definitions 1" $ TestCase
    ( do
      let et0 = T.parse $ Tx.pack "\\def\\MyMacro{Macro}"
      assertBool "Parse ok" (isRight et0)
      let Right t0 = et0
      assertEqual "MyMacro"
        (Right
          [MacroDef
            { mdName = "\\MyMacro"
            , mdDef = 
                TeXCommS "def"
                <> (TeXCommS $ "MyMacro")
                <> (TeXRaw $ Tx.pack "Macro")
            }])
        (definedCommands [] t0)
    )
  , TestLabel "Macro definitions 2" $ TestCase
    ( do
      let et0 = T.parse $ Tx.pack "\\def{\\MyMacro}{Macro}"
      assertBool "Parse ok" (isRight et0)
      let Right t0 = et0
      assertEqual "MyMacro"
        (Right
          [MacroDef
            { mdName = "\\MyMacro"
            , mdDef = 
                TeXCommS "def"
                <> (TeXCommS $ "MyMacro")
                <> (TeXBraces $ TeXRaw $ Tx.pack "Macro")
            }])
        (definedCommands [] t0)
    )
  , TestLabel "Environment definitions 0" $ TestCase
    ( do
      let et0 = T.parse $ Tx.pack "\\newenvironment{MyEnv}{Begin}{End}"
      assertBool "Parse ok" (isRight et0)
      let Right t0 = et0
      assertEqual "MyEnv"
        (Right
          [EnvironmentDef
            { mdName = "MyEnv"
            , mdDef = (TeXComm "newenvironment"
                [ FixArg $ TeXRaw $ Tx.pack "MyEnv"
                , FixArg $ TeXRaw $ Tx.pack "Begin"
                , FixArg $ TeXRaw $ Tx.pack "End"
                ])
            }])
        (definedCommands [] t0)
    )
  , TestLabel "Missing macros 0" $ TestCase
    ( do
      let et0 = T.parse $ Tx.pack "\\mymacro{2}\\othermacro"
      assertBool "Parse ok" (isRight et0)
      let Right t0 = et0
          myMacroDef = TeXComm "newcommand" [FixArg (TeXCommS "mymacro"), OptArg (TeXRaw $ Tx.pack "1"), FixArg $ TeXRaw $ Tx.pack "macro#1"]
          localDefs = 
            [ MacroDef
                { mdName = "\\othermacro"
                , mdDef = TeXComm "newcommand" [FixArg (TeXCommS "othermacro"), FixArg $ TeXRaw $ Tx.pack "macro"]
                }
            ]
          globalDefs = 
            [ MacroDef
                { mdName = "\\othermacro"
                , mdDef = TeXComm "newcommand" [FixArg (TeXCommS "othermacro"), FixArg $ TeXRaw $ Tx.pack "macro"]
                }
            , MacroDef
              { mdName = "\\mymacro"
              , mdDef = myMacroDef
              }
            ]
      assertEqual "refs"
        [TeXComm "mymacro" [FixArg (TeXRaw $ Tx.pack "2")], TeXCommS "othermacro"]
        (usedCommands t0)
      assertEqual "mymacro"
        [myMacroDef]
        (missingCommands t0 localDefs globalDefs)
    )
  , TestLabel "Missing macros 1" $ TestCase
    ( do
      let et0 = T.parse $ Tx.pack "\\mymacro{2}\\othermacro"
      assertBool "Parse ok" (isRight et0)
      let Right t0 = et0
          myMacroDef = TeXComm "newcommand" [ FixArg (TeXCommS "mymacro"), OptArg (TeXRaw $ Tx.pack "1")
                                            , FixArg $ 
                                                TeXCommS "thirdmacro"
                                                <> (TeXRaw $ Tx.pack "macro#1")
                                            ]
          thirdMacroDef = TeXComm "newcommand" [FixArg (TeXCommS "thirdmacro"), FixArg $ TeXCommS "othermacro"]
          localDefs = 
            [ MacroDef
                { mdName = "\\othermacro"
                , mdDef = TeXComm "newcommand" [FixArg (TeXCommS "othermacro"), FixArg $ TeXRaw $ Tx.pack "macro"]
                }
            ]
          globalDefs = 
            [ MacroDef
                { mdName = "\\othermacro"
                , mdDef = TeXComm "newcommand" [FixArg (TeXCommS "othermacro"), FixArg $ TeXRaw $ Tx.pack "macro"]
                }
            , MacroDef
              { mdName = "\\mymacro"
              , mdDef = myMacroDef
              }
            , MacroDef
              { mdName = "\\thirdmacro"
              , mdDef = thirdMacroDef
              }
            ]
      assertEqual "refs"
        [TeXComm "mymacro" [FixArg (TeXRaw $ Tx.pack "2")], TeXCommS "othermacro"]
        (usedCommands t0)
      assertEqual "mymacro"
        [myMacroDef, thirdMacroDef]
        (missingCommands t0 localDefs globalDefs)
    )
  ]

main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
