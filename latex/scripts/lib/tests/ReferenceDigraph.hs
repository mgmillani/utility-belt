module Main where

import qualified TeXTools.Parser as T
import TeXTools.ReferenceDigraph.Internal

import HGraph.Directed
import qualified Data.Text as Tx
import qualified Data.Text.IO as Tx
import Control.Monad.Trans.State
import Text.LaTeX.Base.Syntax
import System.Exit (exitFailure, exitSuccess)
import System.IO
import Test.HUnit

clearTex (ENode env es) = ENode env{envTeX = TeXEmpty} $ map clearTex es

tests = TestList                         
  [ TestLabel "References 0" $ TestCase
    ( do
      withFile  "tests/references-0.tex" ReadMode $ \h -> do
        Right tex <- fmap T.parse $ Tx.hGetContents h
        let envs = evalState (environments tex)
                              TeXState
                              { txEnvironment = defaultEnvironment
                              , txSectionNumber = 0 
                              }
            (d0, atts0) = referenceDigraph tex ["lemma","theorem","proof","align"]
        assertEqual "Environment list"
                    ([ ENode Environment
                             { envName = "lemma"
                             , envLabel = Just "lemma 1"
                             , envRefs = []
                             , envTeX = TeXEmpty
                             , envSection = 1
                             }
                       []
                     , ENode Environment
                             { envName = "theorem"
                             , envLabel = Just "theorem 2"
                             , envRefs = []
                             , envTeX = TeXEmpty
                             , envSection = 1
                             }
                             []
                     , ENode Environment
                       { envName = "proof"
                       , envLabel = Nothing
                       , envRefs = ["lemma 1"]
                       , envTeX = TeXEmpty
                       , envSection = 1
                       }
                       []
                     ])
                    (map clearTex envs)
        assertEqual "Number of arcs"
                     1
                     (length $ arcs d0)
    )
  , TestLabel "References 1" $ TestCase
    ( do
      withFile  "tests/references-1.tex" ReadMode $ \h -> do
        Right tex <- fmap T.parse $ Tx.hGetContents h
        let envs = evalState (environments tex)
                              TeXState
                              { txEnvironment = defaultEnvironment
                              , txSectionNumber = 0 
                              }
        assertEqual "Environment list"
                    ([ ENode
                       Environment
                       { envName = "lemma"
                       , envLabel = Just "lemma 1"
                       , envRefs = []
                       , envTeX = TeXEmpty
                       , envSection = 1
                       }
                       []
                     , ENode Environment
                       { envName = "theorem"
                       , envLabel = Just "theorem 2"
                       , envRefs = []
                       , envTeX = TeXEmpty
                       , envSection = 1
                       } 
                       [ ENode Environment
                               { envName = "align"
                               , envLabel = Just "align a"
                               , envRefs = []
                               , envTeX = TeXEmpty
                               , envSection = 1
                               }
                               []
                       ]
                     , ENode Environment
                       { envName = "proof"
                       , envLabel = Nothing
                       , envRefs = ["lemma 1"]
                       , envTeX = TeXEmpty
                       , envSection = 1
                       }
                       []
                     ])
                    (map clearTex envs)
    )
  ]

main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
