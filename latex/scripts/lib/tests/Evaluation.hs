{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified TeXTools.Parser as T
import TeXTools.Utils
import TeXTools.Evaluation.Internal

import Data.Either
import qualified Data.Text as Tx
import qualified Data.Text.IO as Tx
import Control.Monad.Trans.State
import Text.LaTeX.Base.Syntax
import Text.LaTeX.Base.Render
import System.Exit (exitFailure, exitSuccess)
import System.IO
import Test.HUnit

tests = TestList                         
  [ TestLabel "Remove comments" $ TestCase
    ( do
      let t0 = T.parse $ Tx.pack "Text\n%comment\nend."
      assertBool "Parse ok" (isRight t0)
      let Right t0r = t0
          t0' = removeComments t0r
      assertEqual "Text end" ("Text\nend.") (render t0')
      let t1 = T.parse $ Tx.pack "\\begin{env}\
      \body % comment\n\
      \\\end{env}"
      assertBool "Parse ok" (isRight t1)
      let Right t1r = t1
          t1' = removeComments t1r
      assertEqual "env" "\\begin{env}body \\end{env}" (render t1')
      let t2 = T.parse $ Tx.pack "\\macro{X % comment\n}"
      assertBool "Parse ok" (isRight t2)
      let Right t2r = t2
          t2' = removeComments t2r
      assertEqual "env" "\\macro{X }" (render t2')
    )
  , TestLabel "If 0" $ TestCase
    ( do
      let t0 = T.parse $ Tx.pack "\\newif\\ifA\n\
      \\\Atrue\n\
      \\\ifA\n\
      \A\n\
      \\\else\n\
      \B\n\
      \\\fi X"
      assertBool "Parse ok" (isRight t0)
      let Right t0r = t0
          t0' = evaluateIfs t0r
      assertEqual "A" "\n\nA\nX" (render t0')
      let t1 = T.parse $ Tx.pack "\\newif\\ifA\n\
      \\\Afalse\n\
      \\\ifA\n\
      \A\n\
      \\\else\n\
      \B\n\
      \\\fi"
      assertBool "Parse ok" (isRight t1)
      let Right t1r = t1
          t1' = evaluateIfs t1r
      assertEqual "B" "\n\nB\n" (render t1')
      let t2 = T.parse $ Tx.pack "\\newif\\ifA\\newif\\ifB\n\
      \\\Afalse\\Btrue\n\
      \\\ifA\n\
      \\\ifB\n\
      \AB\n\
      \\\else\n\
      \A\n\
      \\\fi\n\
      \\\else\n\
      \\\ifB\n\
      \B\n\
      \\\else\n\
      \-\n\
      \\\fi"
      assertBool "Parse ok" (isRight t2)
      let Right t2r = t2
          t2' = evaluateIfs t2r
      assertEqual "B" "\n\nB\n" (render t2')
      let t3 = T.parse $ Tx.pack "\\newif\\ifA\\newif\\ifB\n\
      \\\Atrue\\Btrue\n\
      \\\ifA\n\
      \\\ifB\n\
      \AB\n\
      \\\else\n\
      \A\n\
      \\\fi\n\
      \\\else\n\
      \\\ifB\n\
      \B\n\
      \\\else\n\
      \-\n\
      \\\fi"
      assertBool "Parse ok" (isRight t3)
      let Right t3r = t3
          t3' = evaluateIfs t3r
      assertEqual "AB" "\n\nAB\n" (render t3')
      let t4 = T.parse $ Tx.pack "\\newif\\ifA\\newif\\ifB\n\
      \\\Afalse\\Bfalse\n\
      \\\ifA\n\
      \\\ifB\n\
      \AB\n\
      \\\else\n\
      \A\n\
      \\\fi\n\
      \\\else\n\
      \\\ifB\n\
      \B\n\
      \\\else\n\
      \-\n\
      \\\fi"
      assertBool "Parse ok" (isRight t4)
      let Right t4r = t4
          t4' = evaluateIfs t4r
      assertEqual "-" "\n\n-\n" (render t4')
    )
  , TestLabel "If 1" $ TestCase
    ( do
      let t0 = T.parse $ Tx.pack "\\newif\\ifA\n\
      \\\Atrue\n\
      \\\ifA\n\
      \\\A\\B\n\
      \\\else\n\
      \\\C\\D\n\
      \\\fi \
      \body X"
      assertBool "Parse ok" (isRight t0)
      let Right t0r = t0
          t0' = evaluateIfs t0r
      assertEqual "A" "\n\n\\A\\B\nbody X" (render t0')
    )
  , TestLabel "Environment 0" $ TestCase
    ( do
      let t0 = T.parse $ Tx.pack "\\newif\\ifA\n\
      \\\Afalse\n\
      \\\ifA\n\
      \\\begin{env}[opt]\n\
      \\\else\n\
      \\\begin{env}\n\
      \\\fi \
      \body\n\
      \\\end{env}"
      assertBool "Parse ok" (isRight t0)
      let Right t0r = t0
          t0' = evaluateIfs t0r
      assertEqual "A" "\n\n\\begin{env}\nbody\n\\end{env}" (render t0')
    )
  ]

main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
