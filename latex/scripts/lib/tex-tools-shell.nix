{ nixpkgs ? import <nixpkgs> {}, compiler ? "default", doBenchmark ? false }:

let

  inherit (nixpkgs) pkgs;

  f = { mkDerivation, base, containers, filepath, HaTeX, hgraph
      , lib, mtl, parsec, text, transformers
      }:
      mkDerivation {
        pname = "tex-tools-lib";
        version = "0.2.0.0";
        src = ./.;
        libraryHaskellDepends = [
          base containers filepath HaTeX hgraph mtl parsec text transformers HUnit
        ];
        license = "unknown";
      };

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  drv = variant (haskellPackages.callPackage f {});

in

  if pkgs.lib.inNixShell then drv.env else drv
