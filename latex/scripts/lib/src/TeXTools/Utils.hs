module TeXTools.Utils where

import Text.LaTeX.Base.Syntax
import qualified Data.Text as T

mhead (x:xs) = Just x
mhead [] = Nothing

onlyText (TeXRaw txt) = T.unpack txt
onlyText (TeXBraces tex) = onlyText tex
onlyText (TeXSeq t0 t1) = (onlyText t0) ++ (onlyText t1)
onlyText _ = ""

fixedArgsToText [] = []
fixedArgsToText (FixArg a : as) = onlyText a : fixedArgsToText as
fixedArgsToText (_ : as) = fixedArgsToText as

argToTeX (FixArg tex) = tex
argToTeX (OptArg tex) = tex
argToTeX (ParArg tex) = tex
argToTeX (SymArg tex) = tex
argToTeX (MOptArg tex) = foldr (<>) TeXEmpty tex
argToTeX (MParArg tex) = foldr (<>) TeXEmpty tex
argToTeX (MSymArg tex) = foldr (<>) TeXEmpty tex

splitBy _ [] = []
splitBy p xs = 
  let (xs0, rs) = span (not . p) xs
  in xs0 : (splitBy p $ dropWhile p rs)
