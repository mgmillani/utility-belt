module TeXTools.TeX where

data TeX = 
    Macro String
  | Group [TeX]
  | Text String
  | Comment String
  deriving (Eq)

instance Show TeX where
  show (Macro str) = "\\" ++ str
  show (Group tex) = "{" ++ concatMap show tex ++ "}"
  show (Text str) = str
  show (Comment str) = "%" ++ str ++ "\n"

isGroup (Group _) = True
isGroup _ = False

isMacro (Macro _) = True
isMacro _ = False

isComment (Comment _) = True
isComment _ = False
