module TeXTools.Parser.Internal where

import TeXTools.TeX
import TeXTools.Utils
import Text.Parsec.Char hiding (crlf)
import Text.ParserCombinators.Parsec hiding ((<|>), many, optional, parse)
import qualified Text.ParserCombinators.Parsec as P (parse)
import Control.Applicative
import Control.Monad (liftM, liftM2, liftM3, mplus, replicateM, forM)
import Control.Monad.Trans.State as ST
import Data.Char
import Data.List
import Data.Maybe
import Text.LaTeX.Base
import Text.LaTeX.Base.Syntax
import Text.LaTeX.Base.Parser
import Control.Monad
import qualified Data.Map as M
import qualified Data.Set as S
import qualified Data.Text as T
import qualified Data.Text.IO as T
import System.Environment
import System.FilePath
import System.Directory
import System.IO

expandInput :: T.Text -> IO (Either ParseError LaTeX)
expandInput txt = do
  let tex = parse txt
  case tex of
    Left _ -> return tex
    Right txs -> expand txs
  where
    expand :: LaTeX -> IO (Either ParseError LaTeX)
    expand (TeXComm "input" [FixArg flTex]) = do
      let fl = onlyText flTex
      withFile (replaceExtension fl ".tex") ReadMode $ \h -> do
        str <- T.hGetContents h
        tx0 <- expandInput str
        return $ tx0
    expand (TeXSeq t0 t1) = do
      tx0 <- expand t0
      tx1 <- expand t1
      return $ liftM2 TeXSeq tx0 tx1
    expand (TeXBraces tex) = do
      tex' <- expand tex
      return $ liftM TeXBraces tex'
    expand env@(TeXEnv n a t0) = do
      tx0 <- expand t0
      return $ liftM (TeXEnv n a) tx0
    expand t@(_) = return $ Right t

parse :: T.Text -> Either ParseError LaTeX
parse = parseLaTeX

--expandInput :: T.Text -> IO (Either ParseError LaTeX)
--expandInput str = do
--  let tex = parse str
--  case tex of
--    Left _ -> return tex
--    Right txs -> expand txs
--  where
--    expand (Macro "input" : Group [Text fl] : ts) = do
--      withFile (replaceExtension fl ".tex") ReadMode $ \h -> do
--        str <- hGetContents h
--        tx0 <- expandInput str
--        tx1 <- expand ts
--        return $ liftM2 (++) tx0 tx1
--    expand (t : ts) = do
--      liftM (fmap (t :) ) (expand ts)
--    expand [] = return $ Right []
--
--parse :: String -> Either ParseError [TeX]
--parse = P.parse (p_tex <* eof) "(input)"
--
--p_tex :: CharParser st [TeX]
--p_tex = many (p_macro <|> p_group <|> p_text <|> p_comment)
--
--p_macro :: CharParser st TeX
--p_macro = Macro <$> ((string "\\") *> many (letter <|> oneOf "@/%"))
--
--p_group :: CharParser st TeX
--p_group = Group <$> (string "{" *> (p_tex <* string "}"))
--
--p_text :: CharParser st TeX
--p_text = Text <$> many1 (noneOf "{}%\\")
--
--p_comment :: CharParser st TeX
--p_comment = Comment <$> (string "%" *> many (noneOf "\n") <* (string "\n" <|> (eof *> return "\n")))
