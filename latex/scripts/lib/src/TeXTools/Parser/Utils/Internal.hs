module TeXTools.Parser.Utils.Internal where

import Control.Monad.Trans.State
import Data.Maybe
import TeXTools.Utils
import TeXTools.TeX

findMatchingEnd :: State [TeX] [TeX]
findMatchingEnd = do
  mElement <- nextElement
  case mElement of
    Just m@(Macro "end") -> do
      endName <- nextElement
      return $ m : (fromMaybe [] $ fmap (:[]) endName)
    Just t@(Macro "begin") -> do
      rest  <- findMatchingEnd
      rest' <- findMatchingEnd
      return $ t:(rest ++ rest')
    Just t@(_) -> do
      rest <- findMatchingEnd
      return $ t : rest
    Nothing -> return []

nextMacro :: State [TeX] (Maybe TeX)
nextMacro = do
  tex <- get
  let tex' = dropWhile (not . isMacro) tex
  case tex' of
    (macro : tex'') -> do
      put tex''
      return $ Just macro
    [] -> do
      put []
      return Nothing

nextGroup :: State [TeX] (Maybe TeX)
nextGroup = do
  tex <- get
  let tex' = dropWhile (not . isGroup) tex
  case tex' of
    (group : tex'') -> do
      put tex''
      return $ Just group
    [] -> do
      put []
      return Nothing

nextElement :: State [TeX] (Maybe TeX)
nextElement = do
  tex <- get
  put $ drop 1 tex
  return $ mhead $ tex

isTeXEnd :: State [TeX] Bool
isTeXEnd = do
  tex <- get
  return $ null tex
