module TeXTools.Parser.Math.Internal where

import qualified Text.Parsec.Expr  as P
import qualified Text.Parsec.Prim  as P
import qualified Text.Parsec       as P
import qualified Text.Parsec.Error as P
import qualified Data.Text as T
import Text.Read (readEither)
import Text.LaTeX.Base.Syntax
import Data.Char
import Data.Maybe
import Control.Monad
import Control.Monad.Trans.State

data Math o t = BinaryOperator o (Math o t) (Math o t)
              | UnaryOperator o (Math o t)
              | Function o [Math o t]
              | Value t
              deriving (Eq, Ord, Show)

data Term t = Float Double 
            | Integer Integer
            | Variable t
            deriving (Eq, Show)

data Token o t = TokOperator o
               | TokFunction o [[Token o t]]
               | TokTerm t
               | TokOpen Char
               | TokClose Char
               deriving (Show, Eq)

instance Ord t => Ord (Term t) where
  compare (Float f0) (Float f1)       = compare f0 f1
  compare (Integer i0) (Integer i1)   = compare i0 i1
  compare (Variable v0) (Variable v1) = compare v0 v1
  compare (Float _) (Variable _)      = LT
  compare (Integer _) (Variable _)    = LT
  compare (Variable _) (Float f0)     = GT
  compare (Variable _) (Integer f0)   = GT
  compare (Integer i0) (Float f0)     = compare (fromIntegral i0) f0
  compare (Float f0) (Integer i0)     = compare f0 (fromIntegral i0)

isTokFunction (TokFunction _ _) = True
isTokFunction _ = False

isTokFunctionName name (TokFunction name' _) = name' == name
isTokFunctionName _ _ = False

isTokTerm (TokTerm _) = True
isTokTerm _ = False

type StringToken = Token String (Term String)

--parse tex = Left "not implemented"

parseBy :: [LaTeX -> Maybe [StringToken]] -> LaTeX -> Either P.ParseError (Math String (Term String))
parseBy myTokens tex = case tokenizeBy myTokens tex of
  Left err -> P.parse (fail err) "" []
  Right tks -> parseTokenizedExpression $ explicitProduct $ trimTokens tks

parse :: LaTeX -> Either P.ParseError (Math String (Term String))
parse tex = parseBy [defaultTokens] tex

defaultTokens (TeXCommS "cdot") = Just $ [TokOperator "*"]
defaultTokens (TeXCommS "times") = Just $ [TokOperator "*"]
defaultTokens (TeXCommS "coloneqq") = Just $ [TokOperator "="]
defaultTokens (TeXCommS "geq") = Just $ [TokOperator ">="]
defaultTokens (TeXCommS "leq") = Just $ [TokOperator "<="]
defaultTokens (TeXCommS "choose") = Just $ [TokOperator "choose"]
defaultTokens (TeXCommS "subseteq") = Just $ [TokOperator "⊆"]
defaultTokens (TeXCommS "subset") = Just $ [TokOperator "⊂"]
defaultTokens (TeXCommS "supset") = Just $ [TokOperator "⊃"]
defaultTokens (TeXCommS "supseteq") = Just $ [TokOperator "⊇"]
defaultTokens (TeXCommS "in") = Just $ [TokOperator "∈"]
defaultTokens (TeXCommS "right") = Just []
defaultTokens (TeXCommS "left") = Just []
defaultTokens (TeXCommS "big") = Just []
defaultTokens _ = Nothing

--
parseTokenizedExpression :: [StringToken] -> Either P.ParseError (Math String (Term String))
parseTokenizedExpression tks = P.parse defaultParser "" tks
--parseTokenizedExpression tks = Left "Not implemented"

defaultParser :: Monad m => P.ParsecT [StringToken] u m (Math String (Term String))
defaultParser = P.buildExpressionParser defaultOperatorTable m_term

defaultOperatorTable :: (Monad m) => [[ P.Operator [StringToken] u m (Math String (Term String)) ]]
defaultOperatorTable = 
  [ [ binary (p_op "^") (BinaryOperator "^")
    , binary (p_op "_") (BinaryOperator "_")
    ]
  , [ prefix (p_op "-") (UnaryOperator "-")
    , postfix (p_op "!") (UnaryOperator "!")
    ]
  , [ binary (p_op "") (BinaryOperator "")
    ]
  , [ binary (p_op "*") (BinaryOperator "*")
    , binary (p_op "/") (BinaryOperator "/")
    ]
  , [ binary (p_op "+") (BinaryOperator "+")
    , binary (p_op "-") (BinaryOperator "-")
    ]
  , [ binary (p_op "choose") (\a b -> Function "binom" [a,b])]
  , [ binary (p_op "=")      (BinaryOperator "=")
    ]
  , [ binary (p_op "<")      (BinaryOperator "<")
    , binary (p_op ">")      (BinaryOperator ">")
    , binary (p_op ">=")     (BinaryOperator ">=")
    , binary (p_op "<=")     (BinaryOperator "<=")
    ]
  , [ binary (p_op ",") (BinaryOperator ",")]
  , [ binary (p_op "⊆") (BinaryOperator "⊆")
    , binary (p_op "∈") (BinaryOperator "∈")
    , binary (p_op "⊂") (BinaryOperator "⊂")
    , binary (p_op "⊃") (BinaryOperator "⊃")
    , binary (p_op "⊇") (BinaryOperator "⊇")
    ]
  ]

binary :: (P.ParsecT s u m b) -> (a -> a -> a) -> P.Operator s u m a
binary parser f = P.Infix   (parser >> return f) P.AssocLeft
prefix parser f = P.Prefix  (parser >> return f)
postfix parser f = P.Postfix (parser >> return f)

m_term :: (Monad m) => P.ParsecT [StringToken] u m (Math String (Term String))
m_term = 
  (p_term >>= (\(TokTerm t) -> return $ Value t))
  P.<|>
  (p_open_braces *> defaultParser <* p_close_braces)
  P.<|>
  (do 
   TokFunction f args <- p_function
   let args' = sequence $ map parseTokenizedExpression args
   case (f, args') of
     (_, Left err) -> fail (show err)
     ("frac", Right [num, den]) -> return $ BinaryOperator "/" num den
     (_, Right args'') -> return $ Function f args''
  )

p_op :: (Show o, Show t, Eq o, Eq t, Monad m)
     => o -> P.ParsecT [Token o t] u m (Token o t)
p_op o = P.tokenPrim show nextPos (tokTest ((TokOperator o)==))

p_term :: (Show o, Show t, Monad m) => P.ParsecT [Token o t] u m (Token o t) 
p_term = P.tokenPrim show nextPos (tokTest isTokTerm)

p_function :: (Show o, Show t, Monad m) => P.ParsecT [Token o t] u m (Token o t)
p_function = P.tokenPrim show nextPos (tokTest isTokFunction)

p_function_name :: (Show o, Show t, Eq o, Eq t, Monad m)
                => o -> P.ParsecT [Token o t] u m (Token o t)
p_function_name name = P.tokenPrim show nextPos (tokTest (isTokFunctionName name))

p_open_braces :: (Show o, Show t, Eq o, Eq t, Monad m)
              => P.ParsecT [Token o t] u m (Token o t)
p_open_braces  = P.tokenPrim show nextPos (tokTest ((TokOpen  '(')==))

p_close_braces :: (Show o, Show t, Eq o, Eq t, Monad m)
               => P.ParsecT [Token o t] u m (Token o t)
p_close_braces = P.tokenPrim show nextPos (tokTest ((TokClose '(')==))

nextPos pos t s = P.incSourceColumn pos 1

tokTest p x = if p x then Just x else Nothing

filterMath :: LaTeX -> [LaTeX]
filterMath tex = case tex of
    --TeXRaw Text 
    TeXRaw txt -> []
    --TeXComm String [TeXArg] 
    TeXComm name args -> []
    --TeXCommS String 
    TeXCommS name -> [ ]
    --TeXEnv String [TeXArg] LaTeX 
    TeXEnv name args body -> 
      if name `elem` ["align*", "align", "array", "array*", "equation", "equation*"] then
        -- filterAlign body
        [body]
      else
        filterMath body
    --TeXMath MathType LaTeX 
    TeXMath mtype tex -> [tex]
    --TeXLineBreak (Maybe Measure) Bool 
    TeXLineBreak mMeasure b -> []
    --TeXBraces LaTeX 
    TeXBraces tex -> filterMath tex
    --TeXComment Text 
    TeXComment txt -> []
    TeXSeq t0 t1 -> (filterMath t0) ++ (filterMath t1)
    TeXEmpty -> []

data AlignState = AlignState
  { asRest :: Maybe LaTeX
  , asNewLine :: Bool
  , asContinues :: Bool
  , asBeginning :: Bool
  }

filterAlign :: LaTeX -> [LaTeX]
filterAlign tex = 
  let (math, st) = runState (filterAlign' tex)
                            AlignState
                            { asRest = Nothing
                            , asNewLine = False
                            , asContinues = False
                            , asBeginning = True
                            }
  in case asRest st of 
        Nothing -> math
        Just t -> math ++ filterAlign tex

filterAlign' tex = case tex of
    --TeXRaw Text 
    t@(TeXRaw txt) -> do
      let (lhs, rhs) = T.span (/='&') txt
      if T.null rhs then
        return [TeXRaw lhs]
      else do
        st <- get
        if asNewLine st || asBeginning st then do
          put st{asNewLine = False, asBeginning = False}
          if T.null $ T.filter (not . isSpace) lhs then do
            when (asBeginning st)$ do
              st1 <- get
              put st1{asContinues = True}
            return [t]
          else do
            when (asBeginning st)$ do
              st1 <- get
              put st1{asContinues = False}
            st2 <- get
            put st2{asRest = Just $ t}
            return []
        else 
          return [t]
      
    --TeXComm String [TeXArg] 
    t@(TeXComm name args) -> return [t]
    --TeXCommS String 
    t@(TeXCommS name) -> return [t]
    --TeXEnv String [TeXArg] LaTeX 
    t@(TeXEnv name args body) -> return [t]
    --TeXMath MathType LaTeX 
    t@(TeXMath mtype tex) -> return [t]
    --TeXLineBreak (Maybe Measure) Bool 
    t@(TeXLineBreak mMeasure b) -> do
      st <- get
      put st{asNewLine = True}
      return [t]
    --TeXBraces LaTeX 
    t@(TeXBraces tex) -> return [t]
    --TeXComment Text 
    TeXComment txt -> return []
    TeXSeq t0 t1 -> do
      a0 <- filterAlign' t0
      st0 <- get
      case asRest st0 of
        Just t2 -> do
          put st0{asRest = Nothing}
          a1 <- filterAlign' $ TeXSeq t2 t1
          return $ a0 ++ a1
        Nothing -> do
          let continues0 = asContinues st0
          put st0{asContinues = True, asBeginning = True}
          a1 <- filterAlign' t1
          st1 <- get
          let continues1 = asContinues st1
          put st1{asContinues = continues0}
          case (a0,a1) of
            ([], _) -> return a1
            (_, []) -> return a0
            (_,_) -> let a0t = last a0
                         a1t = head a1
                     in if asContinues st1 then
                          return $ (init a0) ++ (TeXSeq a0t a1t : tail a1)
                        else
                          return $ a0 ++ a1
    TeXEmpty -> return []

tokenizeBy :: [LaTeX -> Maybe [StringToken]] -> LaTeX -> Either String [StringToken]
tokenizeBy myTokens tex = 
  case snd $ span isNothing $ map ($ tex) myTokens of
    (Just t:_) -> return t
    [] -> case tex of
              --TeXRaw Text 
              TeXRaw txt -> tokenizeText txt
              --TeXComm String [TeXArg] 
              TeXComm name args -> do
                args' <- tokenizeArgsBy myTokens args
                return [TokFunction name args']
              --TeXCommS String 
              TeXCommS name -> Right [TokFunction name [] ]
              --TeXEnv String [TeXArg] LaTeX 
              TeXEnv name args body -> tokenizeBy myTokens body
              --TeXMath MathType LaTeX 
              TeXMath mtype tex -> tokenizeBy myTokens tex
              --TeXLineBreak (Maybe Measure) Bool 
              TeXLineBreak mMeasure b -> Right []
              --TeXBraces LaTeX 
              TeXBraces tex -> do
                tks <- tokenizeBy myTokens tex
                return $ TokOpen '(' : (tks ++ [TokClose '('])
              --TeXComment Text 
              TeXComment txt -> Right []
              TeXSeq t0 t1 -> liftM2 (++) (tokenizeBy myTokens t0) (tokenizeBy myTokens t1)
              TeXEmpty -> Right []

tokenize :: LaTeX -> Either String [StringToken]
tokenize = tokenizeBy []
--tokenize mth = 
--  case mth of
--    --TeXRaw Text 
--    TeXRaw txt -> tokenizeText txt
--    --TeXComm String [TeXArg] 
--    TeXComm name args -> do
--      args' <- tokenizeArgs args
--      return [TokFunction name args']
--    --TeXCommS String 
--    TeXCommS name -> Right [TokFunction name [] ]
--    --TeXEnv String [TeXArg] LaTeX 
--    TeXEnv name args body -> tokenize body
--    --TeXMath MathType LaTeX 
--    TeXMath mtype tex -> tokenize tex
--    --TeXLineBreak (Maybe Measure) Bool 
--    TeXLineBreak mMeasure b -> Right []
--    --TeXBraces LaTeX 
--    TeXBraces tex -> do
--      tks <- tokenize tex
--      return $ TokOpen '(' : (tks ++ [TokClose '('])
--    --TeXComment Text 
--    TeXComment txt -> Right []
--    TeXSeq t0 t1 -> liftM2 (++) (tokenize t0) (tokenize t1)
--    TeXEmpty -> Right []


tokenizeArgs :: [TeXArg] -> Either String [[StringToken]]
tokenizeArgs = tokenizeArgsBy []
--tokenizeArgs args = sequence $ map tokenizeArg args
--  where
--    tokenizeArg (FixArg tex) = tokenize tex
--    tokenizeArg (OptArg tex) = tokenize tex
--    tokenizeArg (SymArg tex) = tokenize tex
--    tokenizeArg (ParArg tex) = tokenize tex
--    tokenizeArg (MOptArg tex) = fmap concat $ sequence $ map tokenize tex
--    tokenizeArg (MSymArg tex) = fmap concat $ sequence $ map tokenize tex
--    tokenizeArg (MParArg tex) = fmap concat $ sequence $ map tokenize tex

tokenizeArgsBy :: [LaTeX -> Maybe [StringToken]] -> [TeXArg] -> Either String [[StringToken]]
tokenizeArgsBy myTokens args = fmap (map explicitProduct) $ sequence $ map tokenizeArg args
  where
    tokenizeArg (FixArg tex) = tokenizeBy myTokens tex
    tokenizeArg (OptArg tex) = tokenizeBy myTokens tex
    tokenizeArg (SymArg tex) = tokenizeBy myTokens tex
    tokenizeArg (ParArg tex) = tokenizeBy myTokens tex
    tokenizeArg (MOptArg tex) = fmap concat $ sequence $ map (tokenizeBy myTokens) tex
    tokenizeArg (MSymArg tex) = fmap concat $ sequence $ map (tokenizeBy myTokens) tex
    tokenizeArg (MParArg tex) = fmap concat $ sequence $ map (tokenizeBy myTokens) tex

tokenizeText :: T.Text -> Either String [StringToken]
tokenizeText txt = fmap concat $ sequence $ map tokenizeWord $ T.words $ T.filter (/='&') txt

tokenizeWord :: T.Text -> Either String [StringToken]
tokenizeWord txt = 
  let terms = T.groupBy (\a b -> (isNumber a && isNumber b)
                              || (isNumber a && b =='.')
                              || (a == '.' && isNumber b)
                              || (isAlpha a && b == '\'')
                              || (isAlpha a && b == '*')
                              || (a == '\'' && b == '\'')
                              || (a == '\'' && b == '*')
                              || (a == '*' && b == '*')
                              || (a == '*' && b == '\'')
                        )
                        txt
      period = T.pack "."
  in sequence $ map tokenizeTerm $ filter (\t -> t /= period) terms

tokenizeTerm :: T.Text -> Either String StringToken
tokenizeTerm t = 
  case T.unpack t of
    "+" -> Right $ TokOperator "+"
    -- "*" -> Right $ TokOperator "*"
    "=" -> Right $ TokOperator "="
    "<" -> Right $ TokOperator "<"
    ">" -> Right $ TokOperator ">"
    "-" -> Right $ TokOperator "-"
    "/" -> Right $ TokOperator "/"
    "^" -> Right $ TokOperator "^"
    "_" -> Right $ TokOperator "_"
    "!" -> Right $ TokOperator "!"
    "," -> Right $ TokOperator ","
    "(" -> Right $ TokOpen '('
    ")" -> Right $ TokClose '('
    "[" -> Right $ TokOpen '['
    "]" -> Right $ TokClose '['
    _ -> if T.all (\c -> isNumber c || c == '.') t && T.any (\c -> isNumber c) t then
            parseNumber t >>= return . TokTerm
         else
            Right $ TokTerm $ Variable $ T.unpack t

parseNumber :: T.Text -> Either String (Term String)
parseNumber t = case T.groupBy (\a b -> isNumber a && isNumber b) t of
  [v] -> 
    case readEither $ T.unpack v of
      Right n -> return $ Integer n
      Left err -> Left $ "Failed to parse " ++ T.unpack v ++ ": " ++ err
  [v0,_,v2] -> 
    case readEither $ T.unpack t of
      Right n -> return $ Float n
      Left err -> Left $ "Failed to parse " ++ T.unpack t ++ ": " ++ err
  [v, period] -> 
    if period == T.pack "." then
      case readEither $ T.unpack v of
        Right n  -> return $ Integer n
        Left err -> Left $ "Failed to parse " ++ T.unpack t ++ ": " ++ err
    else
      Left $ "Invalid number: " ++ T.unpack t
  _ -> Left $ "Invalid number: " ++ T.unpack t


explicitProduct [] = []
explicitProduct [t] = [t]
explicitProduct (t0@(TokFunction _ _) : t1@(TokOperator _) : ts )    = t0 : t1 : explicitProduct (ts)
explicitProduct (t0@(TokFunction _ _) : t1@(TokClose _) : ts )       = t0 : explicitProduct (t1 : ts)
explicitProduct (t0@(TokFunction _ _) : t1 : ts )                    = t0 : TokOperator "" : explicitProduct (t1:ts)
explicitProduct (t0@(TokTerm _)       : t1@(TokTerm _) : ts )        = t0 : TokOperator "*" : explicitProduct (t1:ts)
explicitProduct (t0@(TokTerm _)       : t1@(TokFunction _ _) : ts )  = t0 : TokOperator "*" : explicitProduct (t1:ts)
explicitProduct (t0@(TokTerm (Variable _)) : t1@(TokOpen _) : ts )   = t0 : TokOperator "" : explicitProduct (t1:ts)
explicitProduct (t0@(TokTerm _)       : t1@(TokOpen _) : ts )        = t0 : TokOperator "*" : explicitProduct (t1:ts)
explicitProduct (t0@(TokClose _)      : t1@(TokOperator _) : ts )    = t0 : t1 : explicitProduct (ts)
explicitProduct (t0@(TokClose _)      : t1@(TokClose _) : ts )       = t0 : explicitProduct (t1 : ts)
explicitProduct (t0@(TokClose _)      : t1 : ts )                    = t0 : TokOperator "" : explicitProduct (t1:ts)
explicitProduct (t0:ts) = t0 : explicitProduct ts

trimTokens toks = dropWhile isTrimmable $ trimEnd toks

trimEnd (t : toks)
  | isTrimmable t =
    let toks' = trimEnd toks
    in if null toks' then
          []
       else
          t : toks'
  | otherwise = t : trimEnd toks
trimEnd [] = []

isTrimmable (TokOperator ",") = True
isTrimmable _ = False
