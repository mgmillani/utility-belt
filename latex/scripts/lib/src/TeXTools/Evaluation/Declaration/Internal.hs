module TeXTools.Evaluation.Declaration.Internal where

import Text.LaTeX.Base
import Text.LaTeX.Base.Syntax
import qualified Data.Map as M
import qualified Data.Set as S
import Control.Monad.Trans.State
import Control.Monad.Trans.Class
import Control.Monad
import qualified Data.Text as T
import Data.Char
import Data.Maybe
import TeXTools.Utils

data TeXState = 
  TeXState
  { stDef     :: Bool
  , stDefName :: Maybe String
  , stDefTex  :: LaTeX
  }

startState = TeXState
  { stDef = False
  , stDefName = Nothing
  , stDefTex = TeXEmpty
  }

data LatexDef = 
  MacroDef
  { mdName :: String
  , mdDef  :: LaTeX
  } 
  |
  EnvironmentDef
  { mdName :: String
  , mdDef  :: LaTeX
  }
  deriving (Eq, Show)

isMacroDef (MacroDef{}) = True
isMacroDef _ = False
isEnvDef (EnvironmentDef{}) = True
isEnvDef _ = False

definedCommands customDefiners tex = evalStateT (definedCommands' customDefiners tex) startState

definedCommands' customDefiners tex =
  fromMaybe
    ( case tex of
        TeXSeq t0 t1 -> do
          liftM2 (++) (definedCommands' customDefiners t0)
                      (definedCommands' customDefiners t1)
        TeXEnv name args env -> definedCommands' customDefiners env
        TeXComm "newcommand"    args -> newCommand tex args
        TeXComm "renewcommand"  args -> newCommand tex args
        TeXComm "newcommand*"   args -> newCommand tex args
        TeXComm "renewcommand*" args -> newCommand tex args
        TeXComm "newenvironment" args -> newEnvironment tex args
        TeXComm "def" args -> do
          st <- get
          put st{stDef = True, stDefTex = TeXCommS "def"}
          definedCommands' customDefiners 
            (foldr (\t a -> 
              if a == TeXEmpty then
                t
              else 
                t <> TeXBraces a)
            TeXEmpty $ map argToTeX args)
        TeXComm cmd args -> do
          st <- get
          case (stDef st, stDefName st) of
            (True, Just name) -> do
              put st{stDef = False, stDefName = Nothing, stDefTex = TeXEmpty}
              return [MacroDef{mdName = '\\' : name, mdDef = (stDefTex st) <> tex}]
            (True, Nothing) -> do
              put st{stDef = False, stDefName = Nothing, stDefTex = TeXEmpty}
              return [ MacroDef 
                        { mdName = '\\' : cmd
                        , mdDef = (stDefTex st)
                                  <> TeXCommS cmd
                                  <> (foldr (<>) TeXEmpty $ map argToTeX args)
                        }
                     ]
            (False, _) ->
              fmap concat $ mapM (definedCommandsArg customDefiners) args
        TeXBraces tex' -> do
          st <- get
          case (stDef st, stDefName st) of
            (True, Just name) -> do
              put st{stDef = False, stDefName = Nothing, stDefTex = TeXEmpty}
              return [MacroDef{mdName = '\\' : name, mdDef = (stDefTex st) <> tex}]
            (True, Nothing) -> 
              definedCommands' customDefiners tex'
            (False, _) -> 
              definedCommands' customDefiners tex'
        TeXMath mtype tex' -> definedCommands' customDefiners tex'
        TeXCommS "def" -> defCommand
        TeXCommS cmd -> do
          st <- get
          if stDef st then
            if isNothing (stDefName st) then do
              put st{stDefName = Just cmd, stDefTex = stDefTex st <> tex}
              return []
            else do
              put st{stDef = False, stDefName = Nothing, stDefTex = TeXEmpty}
              return [ MacroDef
                        { mdName = '\\' : fromJust (stDefName st)
                        , mdDef = stDefTex st <> tex
                        }
                     ]
          else
            return []
        TeXLineBreak _ _ -> return []
        TeXRaw txt -> do
          st <- get
          case (stDef st, stDefName st) of
            (True, Just name) -> do
              put st{stDefTex = (stDefTex st) <> tex}
              return []
            (True, Nothing) -> do
              put st{stDef = False, stDefName = Nothing, stDefTex = TeXEmpty}
              lift $ Left $ "Could not parse command defined by:\n" ++ (T.unpack $ render (stDefTex st))
            (False, _) ->
              return []
        TeXEmpty -> return []
        TeXComment _ -> return []
    )
    (fmap return $ foldr mplus Nothing (map ($ tex) customDefiners))

definedCommandsArg customDefiners (FixArg tex) = definedCommands' customDefiners tex
definedCommandsArg customDefiners (OptArg tex) = definedCommands' customDefiners tex
definedCommandsArg customDefiners (SymArg tex) = definedCommands' customDefiners tex
definedCommandsArg customDefiners (ParArg tex) = definedCommands' customDefiners tex
definedCommandsArg customDefiners (MOptArg tex) = fmap concat $ mapM (definedCommands' customDefiners) tex
definedCommandsArg customDefiners (MSymArg tex) = fmap concat $ mapM (definedCommands' customDefiners) tex
definedCommandsArg customDefiners (MParArg tex) = fmap concat $ mapM (definedCommands' customDefiners) tex

newCommand tex [FixArg cmd] =
  return [MacroDef{mdName = T.unpack $ render cmd, mdDef = tex}]
newCommand tex [FixArg cmd, FixArg _] =
  return [MacroDef{mdName = T.unpack $ render cmd, mdDef = tex}]
newCommand tex [FixArg cmd, OptArg _,  FixArg _] =
  return [MacroDef{mdName = T.unpack $ render cmd, mdDef = tex}]
newCommand tex [FixArg cmd, OptArg _, OptArg _, FixArg _] =
  return [MacroDef{mdName = T.unpack $ render cmd, mdDef = tex}]
newCommand tex _ = 
  lift $ Left ("Could not parse new command from:\n" ++ (T.unpack $ render tex))

newEnvironment tex [FixArg envName, FixArg beginDef, FixArg endDef] =
  return [EnvironmentDef{mdName = T.unpack $ render envName, mdDef = tex}]
newEnvironment tex [FixArg envName, OptArg numArgs, FixArg beginDef, FixArg endDef] =
  return [EnvironmentDef{mdName = T.unpack $ render envName, mdDef = tex}]
newEnvironment tex [FixArg envName, OptArg numArgs, OptArg optargDefault, FixArg beginDef, FixArg endDef] =
  return [EnvironmentDef{mdName = T.unpack $ render envName, mdDef = tex}]
newEnvironment tex _ =
  lift $ Left ("Could not parse new environment from:\n" ++ (T.unpack $ render tex))

defCommand = do
  st <- get
  put st{stDef = True, stDefTex = TeXCommS "def"}
  return []

missingCommands tex localDefs globalDefs = 
  let refs = usedCommands tex
      localMacros = M.fromList
        [ (mdName def, mdDef def)
        | def <- filter isMacroDef localDefs
        ]
      localEnvs = M.fromList
        [ (mdName def, mdDef def)
        | def <- filter isEnvDef localDefs
        ]
      globalMacros = M.fromList
        [ (mdName def, mdDef def)
        | def <- filter isMacroDef globalDefs
        ]
      globalEnvs = M.fromList
        [ (mdName def, mdDef def)
        | def <- filter isEnvDef globalDefs
        ]
      missing = missingTeX refs localMacros localEnvs globalMacros globalEnvs
  in missing

missingTeX [] _ _ _ _ = []
missingTeX (tex:refs) localMacros localEnvs globalMacros globalEnvs =
  case tex of
    TeXCommS cmd          -> missingMacro ('\\' : cmd) refs localMacros localEnvs globalMacros globalEnvs
    TeXComm cmd args      -> missingMacro ('\\' : cmd) refs localMacros localEnvs globalMacros globalEnvs
    TeXEnv name args body -> missingEnvironment name refs localMacros localEnvs globalMacros globalEnvs
    _ -> missingTeX refs localMacros localEnvs globalMacros globalEnvs
  where
    missingMacro cmd refs localMacros localEnvs globalMacros globalEnvs = 
      let localMacro  = M.lookup cmd localMacros
          globalMacro = M.lookup cmd globalMacros 
      in
      case (localMacro, globalMacro) of
        (Just _, Just _) ->
          missingTeX refs (M.delete cmd localMacros) localEnvs (M.delete cmd globalMacros) globalEnvs
        (Nothing, Just def) ->
          def : missingTeX ((usedCommands def) ++ refs) localMacros localEnvs (M.delete cmd globalMacros) globalEnvs
        _ -> missingTeX refs localMacros localEnvs globalMacros globalEnvs
    missingEnvironment name refs localMacros localEnvs globalMacros globalEnvs = 
      let localEnv  = M.lookup name localEnvs 
          globalEnv = M.lookup name globalEnvs
      in
      case (localEnv, globalEnv) of
        (Just _, Just _) ->
          missingTeX refs localMacros (M.delete name localEnvs) globalMacros (M.delete name globalEnvs)
        (Nothing, Just def) ->
          def : missingTeX ((usedCommands def) ++ refs) localMacros localEnvs globalMacros (M.delete name globalEnvs)
        _ -> missingTeX refs localMacros localEnvs globalMacros globalEnvs
      
usedCommands tex =
  case tex of
    TeXSeq t0 t1 -> 
      (usedCommands t0) ++ (usedCommands t1)
    TeXEnv name args env -> 
      tex : (concatMap (usedCommands . argToTeX) args ++ usedCommands env)
    TeXComm cmd args ->
      tex : (concatMap (usedCommands . argToTeX) args)
    TeXBraces tex' ->
      usedCommands tex'
    TeXMath mtype tex' ->
      usedCommands tex'
    TeXCommS cmd ->
      [tex]
    _ -> []
