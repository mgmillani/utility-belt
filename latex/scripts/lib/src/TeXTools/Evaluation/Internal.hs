module TeXTools.Evaluation.Internal where

import Text.LaTeX.Base
import Text.LaTeX.Base.Syntax
import qualified Data.Map as M
import qualified Data.Set as S
import Control.Monad.Trans.State
import Control.Monad
import qualified Data.Text as T
import Data.Char
import Data.Maybe

evaluateExpression evals tex = 
  fromMaybe
    ( case tex of
        TeXSeq t0 t1 -> TeXSeq (evaluateExpression evals t0) (evaluateExpression evals t1)
        TeXEnv name args env -> TeXEnv name (map evaluateArgs args) $ evaluateExpression evals env
        TeXComm cmd args -> TeXComm cmd $ map evaluateArgs args
        TeXBraces tex' -> TeXBraces $ evaluateExpression evals tex'
        TeXMath mtype tex' -> TeXMath mtype $ evaluateExpression evals tex'
        TeXCommS cmd -> tex
        TeXLineBreak _ _ -> tex
        TeXRaw _ -> tex
        TeXEmpty -> tex
        TeXComment _ -> tex
    )
    (foldr mplus Nothing (map ($ tex) evals))
  where
    evaluateArgs arg = case arg of
      (FixArg tex)   ->  FixArg  $ evaluateExpression evals tex
      (OptArg tex)   ->  OptArg  $ evaluateExpression evals tex
      (MOptArg texs) ->  MOptArg $ map (evaluateExpression evals) texs
      (SymArg tex)   ->  SymArg  $ evaluateExpression evals tex
      (MSymArg texs) ->  MSymArg $ map (evaluateExpression evals) texs
      (ParArg tex)   ->  ParArg  $ evaluateExpression evals tex
      (MParArg texs) ->  MParArg $ map (evaluateExpression evals) texs
      
removeComments tex = do
  case tex of
    TeXComment _          -> TeXEmpty
    (TeXSeq t0 t1)        -> TeXSeq (removeComments t0) (removeComments t1)
    TeXMath mathType math -> TeXMath mathType $ removeComments math
    TeXEnv name args body -> TeXEnv name (map removeCommentsArg args) $ removeComments body
    TeXBraces tex'        -> TeXBraces $ removeComments tex'
    TeXComm cmd args      -> TeXComm cmd (map removeCommentsArg args)
    _                     -> tex

removeCommentsArg arg = case arg of
  (FixArg tex)   ->  FixArg  $ removeComments tex
  (OptArg tex)   ->  OptArg  $ removeComments tex
  (MOptArg texs) ->  MOptArg $ map removeComments texs
  (SymArg tex)   ->  SymArg  $ removeComments tex
  (MSymArg texs) ->  MSymArg $ map removeComments texs
  (ParArg tex)   ->  ParArg  $ removeComments tex
  (MParArg texs) ->  MParArg $ map removeComments texs

data TeXState = 
  TeXState
  { stIfs     :: M.Map String Bool
  , stNewIf   :: Bool
  , stIfStack :: [(Bool, Bool, Bool)]
  , stConsumeLineBreak :: Bool
  }

startState = TeXState
  { stIfs = M.empty
  , stNewIf = False
  , stIfStack = []
  , stConsumeLineBreak = False
  }

evaluateIfs tex = evalState (evaluateIfs' tex) startState

evaluateIfs' tex = 
  case tex of
    TeXSeq t0 t1 -> do
      t0e <- evaluateIfs' t0
      t1e <- evaluateIfs' t1
      return $ t0e <> t1e
    TeXEnv name args env -> do
      evaluateIfs'
        ( (TeXComm "begin" ([FixArg $ TeXRaw $ T.pack name] ++ args))
        <> env
        <> (TeXComm "end" [FixArg $ TeXRaw $ T.pack name])
        )
    TeXComm cmd args -> do
      args' <- mapM evaluateIfsArgs args
      consume $ TeXComm cmd args'
    TeXCommS cmd -> 
      let cmdLen = length cmd
          ((radical5, suffix5), (radical4,suffix4)) =
            if cmdLen >= 5 then
              (splitAt (cmdLen - 5) cmd, splitAt (cmdLen - 4) cmd)
            else
              (("",""), ("", ""))
      in
      case cmd of 
        "newif" -> do
          setNewIf True
          return TeXEmpty
        ('i':'f':ifname) -> do
          st <- get
          if stNewIf st then do
            newIf ifname
            return TeXEmpty
          else do
            mvalue <- ifValue ifname
            case mvalue of 
              Just v -> do
                ifStackPush v
                return TeXEmpty
              Nothing -> consume tex
        "else" -> do
          ifStackElse
        "fi" -> do
          ifStackPop
        _ -> do
          if suffix5 == "false" then do
            ifExists <- doesIfExist radical5
            if ifExists then do
              setIf radical5 False
              return TeXEmpty
            else
              consume tex
          else if suffix4 == "true" then do
            ifExists <- doesIfExist radical4
            if ifExists then do
              setIf radical4 True
              return TeXEmpty
            else
              consume tex
          else
            consume tex
    TeXBraces tex' -> do
      tex'' <- evaluateIfs' tex'
      case tex'' of
        TeXEmpty -> 
          if tex' == TeXEmpty then
            consume tex
          else
            return TeXEmpty
        _ -> 
          return $ TeXBraces tex''
    TeXMath mtype tex' -> do
      tex'' <- evaluateIfs' tex'
      case tex'' of
        TeXEmpty -> return TeXEmpty
        _ -> return $ TeXMath mtype tex''
    TeXLineBreak _ _ -> do
      st <- get
      if stConsumeLineBreak st then do
        put st{stConsumeLineBreak = False}
        return TeXEmpty
      else do
        consume tex
    TeXRaw txt -> do
      st <- get
      if stConsumeLineBreak st then do
        put st{stConsumeLineBreak = False}
        let next = T.take 1 txt
        consume $ TeXRaw ((T.filter (not . isSpace) next) <> (T.drop 1 txt))
      else
        consume tex
    _ -> consume tex

evaluateIfsArgs arg = case arg of
  (FixArg tex)   -> expandWrap  FixArg tex
  (OptArg tex)   -> expandWrap  OptArg tex
  (MOptArg texs) -> expandWrapM MOptArg texs
  (SymArg tex)   -> expandWrap  SymArg tex
  (MSymArg texs) -> expandWrapM MSymArg texs
  (ParArg tex)   -> expandWrap  ParArg tex
  (MParArg texs) -> expandWrapM MParArg texs
  where
    expandWrap c t = do
      st <- get
      t' <- evaluateIfs' t
      put st
      return $ c t'
    expandWrapM c ts = do
      st <- get
      ts' <- mapM evaluateIfs' ts
      put st
      return $ c ts'

consume tex = do
  st <- get
  case stIfStack st of
    ((ifTaken, val, elseClause) : stack') -> do
      if ifTaken then
        if val /= elseClause then
          return tex
        else
          return TeXEmpty
      else
        return TeXEmpty
    [] -> return tex

newIf ifname = do
  st <- get
  put st{stIfs = M.insert ifname False $ stIfs st, stNewIf = False}

setNewIf value = do
  st <- get
  put st{stNewIf = value}

ifValue ifname = do
  st <- get
  return $ M.lookup ifname (stIfs st) 

doesIfExist ifname = do
  st <- get
  return $ ifname `M.member` (stIfs st)

setIf ifname value = do
  st <- get
  put st{stIfs = M.adjust (\_ -> value) ifname (stIfs st)}

ifStackPush value = do
  st <- get
  let stack' = case stIfStack st of
        stack@((ifTaken, val, elseClause) : _) -> (ifTaken && val /= elseClause, value, False) : stack
        [] -> [(True, value, False)]
  put st{stIfStack = stack', stConsumeLineBreak = True}

ifStackPop = do
  st <- get
  case stIfStack st of
    [] -> return $ TeXCommS "fi"
    ((_, _ , _) : stack') -> do
      put st{stIfStack = stack', stConsumeLineBreak = True}
      return TeXEmpty

ifStackElse = do
  st <- get
  case stIfStack st of
    [] -> return $ TeXCommS "else"
    ((ifTaken, val, _) : stack') -> do
      put st{stIfStack = (ifTaken, val, True) : stack', stConsumeLineBreak = True}
      return TeXEmpty
