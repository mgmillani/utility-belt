module TeXTools.Dependencies.Internal where

import Control.Monad
import Control.Monad.Trans.State
import TeXTools.Parser
import TeXTools.TeX
import TeXTools.Utils
import System.FilePath
import Text.LaTeX.Base
import Text.LaTeX.Base.Syntax

dependencies :: Text -> [FilePath]
dependencies str = 
  case parse str of
    Right tex -> texDependencies tex
    Left err -> []

texDependencies :: LaTeX -> [FilePath]
texDependencies tex = case tex of
  TeXComm macro args ->
    let fl = case args of
               [FixArg a] -> onlyText a
               _ -> ""
    in case macro of
        "input"          -> [fl, fl <.> "tex"]
        "include"        -> [fl, fl <.> "tex"]
        "addbibresource" -> [fl, fl <.> "bib"]
        "bibliography"   -> [fl, fl <.> "bib"]
        "usepackage"     -> 
          let pkg = concatMap (splitBy (==',')) $ fixedArgumentsToText args
          in pkg ++ (map (<.> "sty") pkg)
        "RequirePackage" ->
          let pkg = fixedArgumentsToText args
          in pkg ++ (map (<.> "sty") pkg)
        _ -> []
  TeXEnv _ _ tex' -> texDependencies tex'
  TeXBraces tex' -> texDependencies tex'
  TeXSeq tex0 tex1 -> (texDependencies tex0) ++ (texDependencies tex1)
  _ -> []

fixedArgumentsToText [] = []
fixedArgumentsToText (FixArg a : r) = onlyText a : fixedArgumentsToText r
fixedArgumentsToText (_ : r) = fixedArgumentsToText r

--texDependencies :: State LaTeX [FilePath]
--texDependencies = do
--  m <- nextMacro
--  case m of
--    (Just (Macro "input")) -> do
--      e <- nextElement
--      case e of
--        (Just (Group [Text fl])) -> liftM ([fl, fl <.> "tex"] ++ ) texDependencies
--        (Just (Text fl)) -> liftM ([fl, fl <.> "tex"] ++ ) texDependencies
--        _ -> return []
--    (Just (Macro "addbibresource")) -> do
--      e <- nextElement
--      case e of
--        (Just (Group [Text fl])) -> liftM ([fl, fl <.> "bib"] ++ ) texDependencies
--        (Just (Text fl)) -> liftM ([fl, fl <.> "bib"] ++ ) texDependencies
--        _ -> return []
--    (Just (Macro "bibliography")) -> do
--      e <- nextElement
--      case e of
--        (Just (Group [Text fl])) -> liftM ([fl, fl <.> "bib"] ++ ) texDependencies
--        (Just (Text fl)) -> liftM ([fl, fl <.> "bib"] ++ ) texDependencies
--        _ -> return []
--    (Just (Macro "usepackage")) -> do
--      g <- nextGroup
--      case g of
--        (Just (Group [Text pkg])) -> 
--          liftM ((concatMap (\fl -> [fl, fl <.> "sty"]) $ splitBy (==',') pkg) ++ ) texDependencies
--        _ -> return []
--    (Just (Macro "include")) -> do
--      e <- nextElement
--      case e of
--        Just (Group [Text fl]) -> liftM ([fl, fl <.> "tex"] ++) texDependencies
--        Just (Text fl) -> liftM ([fl, fl <.> "tex"] ++) texDependencies
--        _ -> return []
--    (Just (Macro "RequirePackage")) -> do
--      g <- nextGroup
--      case g of
--        Just (Group [Text pkg]) -> 
--          liftM ((concatMap (\fl -> [fl, fl <.> "sty"]) $ splitBy (==',') pkg) ++ ) texDependencies
--        _ -> return []
--    Nothing -> return []
--    _ -> texDependencies


--nextElement = do
--  s <- get
--  case s of 
--    [] -> return Nothing
--    (t :ts) -> do
--      put ts
--      return $ Just t
--
--nextGroup = do
--  s <- liftM (dropWhile (not . isGroup)) get
--  case s of 
--    [] -> return Nothing
--    (t : ts) -> do
--      put ts
--      return $ Just t
--
--nextMacro = do
--  s <- liftM (dropWhile (not . isMacro)) get
--  case s of 
--    [] -> return Nothing
--    (t : ts) -> do
--      put ts
--      return $ Just t
