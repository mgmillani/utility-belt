module TeXTools.ReferenceDigraph.Internal where

import TeXTools.TeX
import TeXTools.Utils
import Text.LaTeX.Base
import Text.LaTeX.Base.Syntax
import Data.Maybe
import Control.Monad.Trans.State as ST
import qualified HGraph.Directed.AdjacencyMap as AM
import           HGraph.Directed as D
import qualified HGraph.Directed.Output as D
import qualified Data.Map as M
import qualified Data.Set as S

data TeXState = 
  TeXState
  { txEnvironment   :: Environment
  , txSectionNumber :: Int
  }

data EnvTree = ENode Environment [EnvTree] deriving (Eq, Show)

data Environment = 
  Environment
  { envName  :: String
  , envLabel :: Maybe String
  , envRefs  :: [String]
  , envTeX   :: LaTeX
  , envSection :: Int
  }
  deriving (Eq, Show)

defaultEnvironment = Environment
  { envName = ""
  , envLabel = Nothing
  , envRefs = []
  , envTeX = TeXEmpty
  , envSection = 0
  }

referenceDigraph :: LaTeX -> [String] -> (AM.Digraph String, M.Map String [(String, String)])
referenceDigraph t envTypes = 
  let es = evalState (environments t) TeXState{txEnvironment = defaultEnvironment, txSectionNumber = 0}
      toNodes sec i (ENode e0 e0s : n1@(ENode e1 e1s) : evs)
        | (isNothing $ envLabel e0) ||
          (not $ envName e0 `S.member` envTypesSet) = 
            toNodes sec i e0s ++ toNodes sec i (n1:evs)
        | (isNothing $ envLabel e1) &&
          (not $ null $ envRefs e1) =
            let (s', i', n) = makeNode sec i e0 (Just e1)
            in
            n : (toNodes s' i' e0s ++ toNodes s' i' e1s ++ toNodes s' i' evs)
        | otherwise = 
            let (s', i', n) = makeNode sec i e0 Nothing
            in
            n : (toNodes s' i' e0s ++ toNodes s' i' e1s ++ toNodes s' i' (n1:evs))
      toNodes sec i [ENode e0 e0s]
        | (isJust $ envLabel e0) && (envName e0 `S.member` envTypesSet) = 
            let (s', i', n) = makeNode sec i e0 Nothing
            in n : toNodes s' i' e0s
        | otherwise = toNodes sec i e0s
      toNodes _ _ [] = []
      envTypesSet = S.fromList envTypes
      makeNode s i e0 mE1 = 
        let i' = if (envSection e0) == s then i + 1 else 1 :: Int
        in
        ( envSection e0
        , i'
        , ( fromJust $ envLabel e0
          , [ ("label"
              , envName e0 ++ " " ++ (show $ envSection e0)
                ++ "." ++ (show i') ++ "\\n"
                ++ (fromJust $ envLabel e0)
              )
            ]
          , envRefs e0 ++ case mE1 of
                            Nothing -> []
                            Just e1 -> (filter (/= (fromJust $ envLabel e0)) $ envRefs e1)
         )
        )
      nodes' = toNodes (0 :: Int) (0 :: Int) es
      attrs  = M.fromList $ map (\(v, atts, _) -> (v,atts) ) nodes'
      nodes = map (\(v, _, us) -> (v, us)) nodes' :: [(String, [String])]
      dg = foldr (\(v, ns) d -> 
                 foldr (\u h -> addArc (v,u) $ addVertex u h) (addVertex v d) $ ns )
           AM.emptyDigraph $ nodes
  in (dg, attrs)

environments :: LaTeX -> ST.State TeXState [EnvTree]
environments tex = do
  case tex of
    TeXSeq t0 t1 -> do
      env0 <- environments t0
      env1 <- environments t1
      return $ env0 ++ env1
    TeXEnv name args env -> do
      st <- get
      let env0 = txEnvironment st
      put $ st{txEnvironment = defaultEnvironment{envName = name, envSection = txSectionNumber st}}
      envs <- environments env
      st1 <- get
      put $ st1{txEnvironment = env0}
      return $ [ENode (txEnvironment st1) envs]
    TeXComm "section"  _ -> nextSection >> return []
    TeXComm "section*" _ -> nextSection >> return []
    TeXComm "label" label' -> do
      let label = fixedArgsToText label'
      case label of
        (l:_) -> setLabel l
        _ -> return ()
      return []
    TeXComm "ref" refs' -> do
      case fixedArgsToText refs' of
        (ref : _ ) -> addReferences $ splitBy (==',') ref
        _ -> return ()
      return []
    TeXComm "cref" refs' -> do
      case fixedArgsToText refs' of
        (ref : _ ) -> addReferences $ splitBy (==',') ref
        _ -> return ()
      return []
    TeXBraces tex -> environments tex
    TeXMath _ tex -> environments tex
    _ -> return []

    --TeXRaw Text	
    --TeXCommS String	
    --TeXLineBreak (Maybe Measure) Bool	
    --TeXComment Text	
    --TeXEmpty

setLabel label = do
  st <- get
  put st{txEnvironment = (txEnvironment st){envLabel = Just label}}

addReferences :: [String] -> ST.State TeXState ()
addReferences refs = do
  st <- get
  let env = txEnvironment st
  put st{txEnvironment = env
          { envRefs = refs ++ envRefs env
          }
        }

nextSection :: ST.State TeXState ()
nextSection = do
  st <- get
  put $ st{txSectionNumber = txSectionNumber st + 1}

