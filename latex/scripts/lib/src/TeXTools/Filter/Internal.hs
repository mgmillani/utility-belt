module TeXTools.Filter.Internal where

import qualified TeXTools.ReferenceDigraph.Internal as I
import TeXTools.TeX
import TeXTools.Utils
import TeXTools.Parser
import Text.LaTeX.Base
import Text.LaTeX.Base.Syntax
import Data.Maybe
import Control.Monad.Trans.State as ST
import qualified Data.Map as M
import qualified Data.Set as S
import System.Directory
import System.FilePath
import System.IO
import qualified Data.Text as T
import qualified Data.Text.IO as T

allEnvironments fl = do
  withFile fl ReadMode $ \h -> do
    dir0 <- getCurrentDirectory
    setCurrentDirectory (takeDirectory fl)
    str <- T.hGetContents h
    eTex <- expandInput str
    setCurrentDirectory dir0
    case eTex of
      (Left err) -> do
        print err
        return Nothing
      (Right tex') -> do
        return $ Just $ evalState (I.environments tex') (I.TeXState{I.txEnvironment = I.defaultEnvironment, I.txSectionNumber = 0})
