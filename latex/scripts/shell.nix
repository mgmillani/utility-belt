with import <nixpkgs> {};
let tex-tools-lib = (haskellPackages.callPackage ./lib/tex-tools-lib.nix {});
in
stdenv.mkDerivation {
  name = "tex-tools";
  buildInputs = [ 
		cabal-install
    (haskellPackages.ghcWithPackages (p:
    [ p.containers
      p.hgraph
      p.parsec
      p.transformers
      p.HaTeX
      p.HUnit
      tex-tools-lib
    ]))
    (haskellPackages.callPackage ./tex-tools.nix {lib = lib; tex-tools-lib = tex-tools-lib; })
  ];
}
