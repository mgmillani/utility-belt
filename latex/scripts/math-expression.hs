module Main where

import qualified Text.Parsec       as P
import qualified Text.Parsec.Expr  as P
import qualified Text.Parsec.Token as P

main = do
  print $ P.parse expression "" "1 + 2 * 3 * 4 + 15"

expression = P.buildExpressionParser opTable number

opPlus = (P.char '+') *> P.spaces *> return (+)
opPlus' = P.Infix opPlus P.AssocLeft

opMult = (P.char '*') *> P.spaces *>  return (*)
opMult' = P.Infix opMult P.AssocLeft
opTable = [[opMult'], [opPlus']]

number = do
  digits <- P.many1 P.digit
  P.spaces
  return $ read digits
