{ mkDerivation, base, containers, hgraph, lib, mtl, parsec, tex-tools-lib }:
mkDerivation {
  pname = "tex-tools";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [ base containers hgraph mtl parsec tex-tools-lib ];
  license = "unknown";
  hydraPlatforms = lib.platforms.none;
}
