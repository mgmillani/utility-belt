#!/usr/bin/env runhaskell

module Main where

import System.Environment
import System.IO
import Data.List
import qualified Data.Map as M
import qualified Data.Set as S
import Control.Monad (liftM, liftM2, liftM3, mplus, replicateM, forM)
import Control.Applicative
import Text.ParserCombinators.Parsec hiding ((<|>), many, optional)
import Text.Parsec.Char hiding (crlf)
import Data.Char
import Control.Monad.Trans.State


main = do
  args <- getArgs
  print $ parse (p_macro <* eof) "(test 1)" "\\macro"
  print $ parse (p_text <* eof) "(test 2)" "macro"
  print $ parse (p_text <* eof) "(test 3)" "macro}"
  print $ parse (p_comment <* eof) "(test 4)" "% macro"
  print $ parse (p_group <* eof) "(test 6)" "{\\macro}"
  print $ parseTeX "\\begin{theorem}\n\t\\label{lemma:something something} % comment \nnext text\n\\end{theorem}"

data TeX = 
    Macro String
  | Group [TeX]
  | Text String
  | Comment String
  deriving (Eq)

instance Show TeX where
  show (Macro str) = "\\" ++ str
  show (Group tex) = "{" ++ concatMap show tex ++ "}"
  show (Text str) = str
  show (Comment str) = "%" ++ str ++ "\n"

parseTeX :: String -> Either ParseError [TeX]
parseTeX = parse (p_tex <* eof) "(input)"

p_tex :: CharParser st [TeX]
p_tex = many (p_macro <|> p_group <|> p_text <|> p_comment)

p_macro :: CharParser st TeX
p_macro = Macro <$> ((string "\\") *> many (letter <|> oneOf "@/"))

p_group :: CharParser st TeX
p_group = Group <$> (string "{" *> (p_tex <* string "}"))

p_text :: CharParser st TeX
p_text = Text <$> many1 (noneOf "{}%\\")

p_comment :: CharParser st TeX
p_comment = Comment <$> (string "%" *> many (noneOf "\n") <* (string "\n" <|> (eof *> return "\n")))

data TeXState = 
  TeXState
  { envs :: S.Set String
  , tex :: [TeX]
  }

environment :: State TeXState (String, [TeX])
environment = do
  name <- nextElement
  content <- untilEndEnvironment
  return (name, content)

untilEndEnvironment :: State TeXState [TeX]
untilEndEnvironment = do
  element <- nextElement
  case element of
    Macro "end" -> return []
    t@(Macro "begin") -> do
      rest <- untilEndEnvironment
      rest' <- untilEndEnvironment
      return $ t:(rest ++ (Macro "end" : rest'))
    t@(_) -> do
      rest <- untilEndEnvironment
      return $ t : rest

environmentRef = do
  (name, env) <- environment
  b <- isTargetEnvironment name
  if b then do
    let body = takeWhile (/= (Macro "begin")) env
        label' = dropWhile (/= (Macro "label")) body
        refs = evalState takeReferences TeXState{envs = S.empty, tex = env}
    return $ case label' of
      (_:Group l:rs) -> M.singleton (concatMap show l) refs
      _ -> M.empty
  else do
    envs <- environments
    st <- get
    return $ foldr M.union M.empty $ map (\e -> evalState environmentRef st{tex = e}) envs
