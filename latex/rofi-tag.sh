#!/usr/bin/env fish

grep '\\\\label' tags | cut -d(echo -en '\t') -f1 | sort -u | rofi -dmenu -method fuzzy
