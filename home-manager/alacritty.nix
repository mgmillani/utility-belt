{
  programs.alacritty =
  { enable = true
  ; settings = 
    { # Theme: Alabaster (dark), by tonksy
      colors =
      { primary = 
        { background = "#12121F"
        ; foreground = "#CECECE";
        }
      ; cursor = 
        { text = "#0E1415"
        ; cursor = "#CECECE";
        }
      ; normal = 
        { black = "#0E1415"
        ; red = "#e25d56"
        ; green = "#73ca50"
        ; yellow = "#e9bf57"
        ; blue = "#4a88e4"
        ; magenta = "#915caf"
        ; cyan = "#23acdd"
        ; white = "#f0f0f0";
        }
      ; bright = 
        { black = "#777777"
        ; red = "#f36868"
        ; green = "#88db3f"
        ; yellow = "#f0bf7a"
        ; blue = "#6f8fdb"
        ; magenta = "#e987e9"
        ; cyan = "#4ac9e2"
        ; white = "#FFFFFF";
        }
      ;
      }
    ; window = 
      { opacity = 0.9
      ;
      }
    ; font = 
      { normal = 
        { family = "inconsolata"
        ;
        }
      ; size = 13
      ;
      }
    ; cursor = 
      { style = 
        { shape = "Beam"
        ;
        }
      ;
      }
    ;
    };
  };
}
