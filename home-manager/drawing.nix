{pkgs,...}:
{
  home.packages = [
      pkgs.inkscape
      pkgs.krita
      pkgs.haskellPackages.pictikz
  ];
}
