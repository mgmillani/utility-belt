{pkgs,...}:
{
  home.packages = [
      pkgs.onboard
      pkgs.zauboard
      pkgs.xournal
  ];
}
