{pkgs,...}:
{
  home.packages = [
      pkgs.qsyncthingtray
      pkgs.nextcloud-client
      pkgs.borgbackup
  ];
}
