{ pkgs, ... }:

let customVimPlugins = {
          "asyncrun" = pkgs.vimUtils.buildVimPlugin {
            name = "asyncrun";
            src = pkgs.fetchFromGitHub {
              owner = "skywind3000";
              repo = "asyncrun.vim";
              rev = "aa8a99e87e64276d52556f4d9d4f4a19afd37556";
              sha256 = "sha256-QAxAWnfHNNVFuGXhVULM9JTZHziq8s2buiwT6gyO6WQ=";
            };
          };
					"vim-evince-synctex" = pkgs.vimUtils.buildVimPlugin {
            name = "vim-evince-synctex";
            src = pkgs.fetchFromGitHub {
              owner = "gauteh";
              repo = "vim-evince-synctex";
              rev = "ca7a1381bd0badbd22c243f3f1540bda5e6b68ad";
              sha256 = "sha256-remaxLd59Pdg7KvbeA6Aq0lDdsSQmeNWHbD5J5hgHgY=";
            };
          };
					"vimtex" = pkgs.vimUtils.buildVimPlugin {
            name = "vimtex";
            src = pkgs.fetchFromGitHub {
              owner = "lervag";
              repo = "vimtex";
              rev = "e56daa62a38160fbba45aca9f1943c14cefe4876";
              sha256 = "sha256-YdU4lfz4Sdh4sE8l8PD27s9iqtKerd7/rr4gsHbxr0c=";
            };
          };
					"sved" = pkgs.vimUtils.buildVimPlugin {
            name = "sved";
            src = pkgs.fetchFromGitHub {
              owner = "peterbjorgensen";
              repo = "sved";
              rev = "7da91cb0eacdaae5e1a41722e95800c98d4ca675";
              sha256 = "sha256-PK6rw1GsLS0IzCqlLOxqWesUTQKQvndtoHah6um85WY=";
            };
          };
        };
	myNvim = pkgs.neovim.override {
		extraPython3Packages = p: [ p.dbus-python ];
		configure = {
			customRC = builtins.readFile /home/marcelo/src/utility-belt/nvim/init.vim;
			#vam = {
			#	knownPlugins = pkgs.vimPlugins // customVimPlugins;
			#	pluginDictionaries = [
			#		{ name = "asyncrun" ; }
			#		{ name = "sved" ; }
			#		{ name = "vimtex" ; }
			#		{ name = "vim-evince-synctex" ; }
			#	];
			#};
			packages.myVimPackage = with pkgs.vimPlugins; {
				start = [ UltiSnips
				          customVimPlugins.asyncrun
									customVimPlugins.sved
									customVimPlugins.vimtex
									customVimPlugins.vim-evince-synctex
									unicode-vim ] ; 
			};
		};
	};
in
{
	vim = (pkgs.vimHugeX.override {}).customize {
		vimrcConfig = {
			packages.myVimPackage = with pkgs.vimPlugins ; 
			{
				start = [ UltiSnips vim-addon-async ] ;
			};
		};
		vimrcConfig.customRC = ''
			source ~/.vimrc
			'';
		name = "vim-with-plugins";
	};
	nvim = myNvim;
	nvim-qt = (pkgs.neovim-qt.override { neovim = myNvim ; });
}
