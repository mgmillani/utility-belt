{pkgs,...}:
{
  home.packages = [
      pkgs.video-chat-test
			pkgs.zoom-us
      pkgs.obs-studio
  ];
}
