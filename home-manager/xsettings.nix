{
  services.xsettingsd =
  { enable = true
  ; settings = 
    { "Xft/dpi" = 144
    ; "Xft/autohint" = false
    ; "Xft/lcdfilter" = "lcddefault"
    ; "Xft/hintstyle" =  "hintfull"
    ; "Xft/hinting" = true
    ; "Xft/antialias" = true
    ; "Xft/rgba" = "rgb"
    ; 
    }
  ;
  };

  xresources.extraConfig = ''
    Xft.dpi: 144
    Xft.autohint: 0
    Xft.lcdfilter: lcddefault
    Xft.hintstyle: hintfull
    Xft.hinting: 1
    Xft.antialias: 1
    Xft.rgba: rgb
  '';

  home.pointerCursor = 
  { package = pkgs.bibata-cursors
  ; name = "Bibata-Modern-Ice"
  ; size = 12
  ; gtk.enable = true
  ; x11.enable = true
  ;
  };

  gtk = 
  { enable = true
  ; theme = 
    { name = "Matcha-dark-azul"
    ; package = pkgs.matcha-gtk-theme
    ;
    }
  ; font =
    { name = "DejaVu Sans Book"
    ; size = 14;
    }
  ; iconTheme = 
    { name = "elementary"
    # ; package = pkgs.
    ;
    }
  # ; cursorTheme =
  #   { name = "whiteglass"
  #   ; package = pkgs.xorg.xcursorthemes
  #   ;
  #   }
  ; 
  };
}
