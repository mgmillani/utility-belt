{ pkgs, ... }:

with builtins;
with pkgs.lib;

let vims = import packages/vim.nix {pkgs = pkgs;};
    # myPurpleSlack = pkgs.callPackage
    #  /home/marcelo/src/nixpkgs/pkgs/applications/networking/instant-messengers/pidgin-plugins/purple-slack {};
    #myPidgin = pkgs.pidgin.override { plugins = 
    #  [ myPurpleSlack ] ;
      # [ purple-slack telegram-purple] ;
    # };
    # myNextcloud-client = with qt5; pkgs.callPackage
    #   (/home/marcelo/src/nixpkgs/pkgs/applications/networking/nextcloud-client)
    #   {mkDerivation = mkDerivation; qtkeychain = libsForQt5.qtkeychain;};
    # myOwncloud-client = pkgs.owncloud-client.override { extra-cmake-modules = pkgs.extra-cmake-modules; qtbase = pkgs.qt515.qtbase; };
    myNixosAutoUpgrade = pkgs.callPackage
      (/home/marcelo/src/utility-belt/nixos-auto-upgrade)
      {};
    #wrappedSplaytab = with pkgs ; callPackage
    #  "/home/marcelo/src/utility-belt/splaytab/wrappedSplaytab.nix"
    #  { mkDerivation = stdenv.mkDerivation ;  splaytab = pkgs.haskellPackages.splaytab; };
    # myPhonesim = with pkgs; callPackage
    #   /home/marcelo/src/utility-belt/auto-phonesim
    #   {mkDerivation = stdenv.mkDerivation;};
    ghc-nolas = pkgs.ghc.overrideAttrs(old:
      { configureFlags = old.configureFlags ++ ["--disable-large-address-space"] ; }
      );
    alacritty = import ./alacritty.nix;
    kitty = import ./kitty.nix;
    xsettings = import ./xsettings.nix;
#     nix-script = pkgs.fetchFromGitHub
#       { owner = "BrianHicks"
#       ; repo = "nix-script"
#       ; rev = "3fb63b03f6242c8c518729af154e702424040055"
#       ; sha256 = "1xv532n8pyq4pmrj98wjsy7qcy27cc4llqhqwjqlhqd92j1m5ngx" ;
#       };
in
xsettings //
{
  imports = [
    ./mail.nix
    ];
  home.packages = [
      myNixosAutoUpgrade
      myPidgin
      pkgs._7zz
      pkgs.alacritty
      pkgs.any-nix-shell
      pkgs.arandr
      pkgs.babelfish
      pkgs.bakoma_ttf
      pkgs.bar
      pkgs.blender
      pkgs.blueberry
      pkgs.borgbackup
      pkgs.cabal2nix
      pkgs.cabal-install
      pkgs.cliphist
      pkgs.compton
      pkgs.cowsay
      pkgs.decrypt-your-fortune
      pkgs.element-desktop
      pkgs.evince
      pkgs.file
      pkgs.fortune
      pkgs.ghc
      pkgs.gimp
      pkgs.git
      pkgs.git-cola
      pkgs.git-dude
      pkgs.gmpc
      pkgs.gnome.gnome-bluetooth
      pkgs.gnome.gnome-keyring
      pkgs.gnome.libgnome-keyring
      pkgs.gnome.seahorse
      pkgs.graphviz
      pkgs.hack-font
      pkgs.haskellPackages.boomange
      pkgs.haskellPackages.hasched
      pkgs.haskellPackages.paphragen
      pkgs.haskellPackages.pdfguesstitle
      pkgs.haskellPackages.pictikz
      pkgs.htop
      pkgs.hyprland
      pkgs.imagemagick
      pkgs.inconsolata
      pkgs.inkscape
      pkgs.inotify-tools
      pkgs.ipafont
      pkgs.itsalamp
      pkgs.krita
      pkgs.la-capitaine-icon-theme
      pkgs.latexrun
      pkgs.liberation_ttf
      pkgs.libnotify
      pkgs.libreoffice
      pkgs.librsvg
      pkgs.libsForQt5.kiconthemes
      pkgs.liferea
      pkgs.little-scholar
      pkgs.lsof
      pkgs.lxappearance
      pkgs.mako
      pkgs.matcha-gtk-theme
      pkgs.moreutils
      pkgs.msmtp
      pkgs.mutt
      pkgs.myPass
      pkgs.myPython3
      pkgs.nerdfonts
      pkgs.nextcloud-client
      pkgs.ninja
      pkgs.nitrogen
      pkgs.nix-script
      pkgs.notify-desktop
      pkgs.numlockx
      pkgs.obs-studio
      pkgs.okular
      pkgs.pandoc
      pkgs.pavucontrol
      pkgs.pdftk
      pkgs.picom
      pkgs.poppler_utils
      pkgs.qrencode
      pkgs.qtpass
      pkgs.quoteme
      pkgs.rofi
      pkgs.scrot
      pkgs.sfwbar
      # pkgs.signal-desktop
      pkgs.signal-desktop
      pkgs.snowblind
      pkgs.spaceFM
      pkgs.splaytab
      pkgs.sshfs
      pkgs.sshpass
      pkgs.stilo-themes
      pkgs.swaybg
      pkgs.sweet
      pkgs.tango-icon-theme
      pkgs.tdesktop
      pkgs.terminator
      pkgs.texlive.combined.scheme-full
      # pkgs.tex-tools
      pkgs.thunderbird
      pkgs.tint2
      pkgs.tokyo-night-gtk
      pkgs.universal-ctags
      pkgs.unzip
      pkgs.video-chat-test
      pkgs.vimix-gtk-themes
      pkgs.vimix-icon-theme
      pkgs.virtmanager
      pkgs.vlc
      pkgs.wl-clipboard
      pkgs.wofi
      pkgs.xclip
      pkgs.xdotool
      pkgs.xfce.thunar
      pkgs.xfce.xfce4-notifyd
      pkgs.xlayoutdisplay
      pkgs.xorg.xcursorthemes
      pkgs.xournal
      pkgs.xscreensaver
      pkgs.xss-lock
      pkgs.youtube-dl
      pkgs.zip
      vims.nvim
      vims.nvim-qt
      vims.vim
    ];

  services.gpg-agent = {
    enable = true;
    defaultCacheTtl = 1800;
    enableSshSupport = true;
  };
  services.gnome-keyring.enable = true;
  programs.home-manager.enable = true;
  manual.man-pages.enable = false;

  xsession.numlock.enable = true;
  home.stateVersion = "18.09";
  home.file = 
    { ".local/share/applications/splaytab-jgmenu.desktop" = 
        { source = (pkgs.splaytab.out + "/share/applications/splaytab-jgmenu.desktop")
        ;
        }
    ; ".local/share/fonts/truetype/LiterationMonoNerdFont-Regular.ttf" = 
        { source = pkgs.nerdfonts.out + "/share/fonts/truetype/NerdFonts/LiterationMonoNerdFont-Regular.ttf"
        ;
        }
    ; ".local/share/fonts/truetype/FiraCodeNerdFontMono-Regular.ttf" = 
        { source = pkgs.nerdfonts.out + "/share/fonts/truetype/NerdFonts/FiraCodeNerdFontMono-Regular.ttf"
        ;
        }
    ; ".local/share/fonts/opentype/HasklugNerdFontMono-Regular.otf" = 
        { source = pkgs.nerdfonts.out + "/share/fonts/opentype/NerdFonts/HasklugNerdFontMono-Regular.otf"
        ;
        }
    ;
    };
}
