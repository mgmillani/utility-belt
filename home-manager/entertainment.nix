{pkgs,...}:
{
  home.packages = [
      pkgs.decrypt-your-fortune
      pkgs.youtube-dl
      pkgs.yt-dlp
  ];
}
