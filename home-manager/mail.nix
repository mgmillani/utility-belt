{pkgs, ...}:
[ pkgs.neomutt
  pkgs.notmuch
  pkgs.notmuch-mailmover
]
