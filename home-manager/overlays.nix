[ (self: super:
  {
    haskellPackages = super.haskellPackages.override {
      overrides = hself: hsuper: {
        splaytab  = hself.callPackage "/home/marcelo/src/utility-belt/splaytab" {lib=self.lib;};
        hasched   = hself.callPackage "/home/marcelo/src/hasched" {lib=self.lib;};
        happy-dot = hself.callPackage "/home/marcelo/src/happy-dot" {lib=self.lib;};
        pictikz   = hself.callPackage "/home/marcelo/src/pictikz" {lib=self.lib;};
        paphragen = hself.callPackage "/home/marcelo/src/paphragen" {lib=self.lib;};
        pdfguesstitle = hself.callPackage "/home/marcelo/src/utility-belt/pdfrename" {lib=self.lib;};
        hgraph = hself.callPackage ./packages/hgraph {lib=self.lib;};
      };
    };

  little-scholar = with self; with self.lib; with self.haskellPackages;
		callPackage ../../src/utility-belt/little-scholar/default.nix {lib=self.lib;}; 
  quoteme = with self; with self.lib; with self.haskellPackages; callPackage ../../src/utility-belt/quote-collector/default.nix { mkDerivation = self.lib.mkDerivation;};

  nextcloud-3-2-1 = super.nextcloud-client.overrideAttrs(old : 
    { src = super.fetchFromGitHub {
        owner = "nextcloud";
        repo = "desktop";
        rev = "v3.2.1";
        sha256 = "0gm3skh275iwfmar1269d74a4imbm6j3r61yjn318rw3s7pp0z93";
      };
      version = "3.2.1";
    });
	myPass = super.pass.withExtensions (pkgs: [ pkgs.pass-checkup ]);
  })
]
