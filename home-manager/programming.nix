{pkgs,...}:
{
  home.packages = [
      pkgs.cabal2nix
      pkgs.cabal-install
      pkgs.ghc
      pkgs.git
      pkgs.git-cola
      pkgs.git-dude
      pkgs.graphviz
      pkgs.inotify-tools
      pkgs.latexrun
      pkgs.libnotify
      pkgs.moreutils
      pkgs.msmtp
      pkgs.mutt
      pkgs.myVims.nvim
      pkgs.myVims.nvim-qt
      pkgs.myVims.vim
      pkgs.ninja
      pkgs.nix-script
      pkgs.pandoc
      pkgs.pdftk
      pkgs.poppler_utils
      pkgs.splaytab
      pkgs.universal-ctags
      pkgs.texlive.combined.scheme-full
      pkgs.myPython3
      pkgs.any-nix-shell
  ];
  home.file = {
    ".local/share/fonts/truetype/LiterationMonoNerdFont-Regular.ttf" = {
      source = pkgs.nerdfonts.out + "/share/fonts/truetype/NerdFonts/LiterationMonoNerdFont-Regular.ttf";
    };
    ".local/share/fonts/truetype/devicons.ttf" = {
      source = pkgs.nerdfonts.out + "/share/fonts/truetype/NerdFonts/devicons.ttf";
    };
    ".local/share/fonts/opentype/HasklugNerdFontMono-Regular.otf" = {
      source = pkgs.nerdfonts.out + "/share/fonts/opentype/NerdFonts/HasklugNerdFontMono-Regular.otf";
    };
  };
}
