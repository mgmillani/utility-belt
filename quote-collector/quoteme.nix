{ mkDerivation, base, containers, directory, filepath, lib, process
}:
mkDerivation {
  pname = "quoteme";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base containers directory filepath process
  ];
  license = "unknown";
  hydraPlatforms = lib.platforms.none;
}
