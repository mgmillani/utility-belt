#!/usr/bin/env runghc

module Main where

import System.Environment
import System.Directory
import System.FilePath
import Data.List
import Data.Char
import System.Process
import qualified Data.Set as S

data Quote = Quote String String

makeKey str = (map toLower $ filter (\c -> not $ isSpace c) str)
makeQuote str = Quote str $ makeKey str

instance Show Quote where
  show (Quote str _) = str

instance Eq Quote where
  (Quote _ key0) == (Quote _ key1) = key0 == key1
  (Quote _ key0) /= (Quote _ key1) = key0 /= key1

instance Ord Quote where
  compare (Quote _ key0) (Quote _ key1) = compare key0 key1

main = do
  args <- getArgs
  quoteDir <- getQuoteDir
  case args of
    "remove":fragment:[] -> removeQuote quoteDir fragment
    "add":file:quote:[] -> addQuote (quoteDir </> file) quote Nothing
    "add":file:quote:author:[] -> addQuote (quoteDir </> file) quote (Just author)
    _ -> help

removeQuote quoteDir fragment = do
  fls <- listDirectory quoteDir
  let fls' = filter (not . (== ".dat") . takeExtension) fls
  quotes <- mapM readQuotes (map (quoteDir </>) fls')
  let fragment' = makeKey fragment
  let matches = map (filter ((fragment' `isInfixOf`) . makeKey)) quotes
      matchSet = S.fromList $ map makeQuote $ concat $ filter (not . null) matches
      numMatches = S.size matchSet
  if numMatches == 1 then do
    let qs = filter (\(m,_,_) -> not $ null $ m) $ zip3 matches quotes fls'
    mapM_ (\(_, q, fl) -> do
      writeQuotes (quoteDir </> fl) $ filter (not . ((fragment' `isInfixOf`) . makeKey)) q
      putStrLn $ "Removed unique match from " ++ (fl) ++ "."
      ) qs
    else do
      putStrLn $ "Match is not unique. Found " ++ show numMatches
      mapM_ (mapM_ putStrLn) matches

addQuote file quote mAuthor = do
  quotes <- readQuotes file
  let quote' = case mAuthor of
        Nothing -> quote
        Just author -> quote ++ "\n -- " ++ author
  writeQuotes file (quotes ++ [quote'])

writeQuotes fl quotes = do
  writeFile (fl <.> "temp") $ intercalate "\n%\n" quotes
  renameFile (fl <.> "temp") fl
  callProcess "strfile" [fl]

readQuotes fl = do
  exist <- doesFileExist fl
  str <- if exist then readFile fl else return []
  return $ parseQuotes str

parseQuotes [] = []
parseQuotes str = parse' $ lines str
  where
    parse' [] = []
    parse' lns = 
      let (quote, rs) = span (/= "%") lns
      in (intercalate "\n" quote) : parse' (drop 1 rs)

getQuoteDir = do
  mdir <- lookupEnv "QUOTE_DIR"
  case mdir of
    Just dir -> return dir
    Nothing -> do
      dir <- getXdgDirectory XdgConfig "quotes"
      exist <- doesDirectoryExist dir
      if exist then
        return dir
        else do
          createDirectory dir
          return dir

help = do
  putStr $ intercalate "\n"
    ["usage:"
    ,"  quote-man remove FRAGMENT          Remove quote containing FRAGMENT if such a quote is unique."
    ,"  quote-man add FILE QUOTE [AUTHOR]  Add quote to FILE."
    ]
