#!/usr/bin/fish

if test (count $argv) -eq 0
	echo "Please provide URL for extraction."
	exit 1
end

set URL $argv[1]
set Domain (echo $URL | sed 's|[^/]*//\([^.]*\)\..*|\1|')

set Temp (mktemp)
set EscapeAmpersand (mktemp)
echo 'module Main where

replaceAmpersand (\'&\':\'a\':\'m\':\'p\':\';\':rs) = \'&\' : replaceAmpersand rs
replaceAmpersand (\'&\':\'#\':rs) = 
  let (num, rs\') = span (/=\';\') rs
  in (toEnum (read num :: Int)):replaceAmpersand (tail rs\')
replaceAmpersand (c:rs) = c:replaceAmpersand rs
replaceAmpersand [] = []

main = do
  str <- getContents
  putStr $ replaceAmpersand str
' > $EscapeAmpersand

if test $Domain = "wisdomquotes"
	set Name (echo $URL | sed 's|.*\.com\/\([^/]*\).*|\1|')
	wget $URL -O $Temp
	grep '<blockquote>' $Temp |\
		sed  's:<a[^<]*>\([^<]*\)</a>:\1:g' |\
		sed 's:<[^>]*>::g' | sed 's:$:\n%:' |\
		sed 's:\(Click to tweet\|(Submitted by the Wisdom Quotes Community)\|(This is one of my favorite[^)]*)\)::' |\
		sed 's:\([^A-Z][.?!]\)\(\([^!.?]\|[A-Z][!.?]\)*\)$:\1\n --\2:' |\
		sed 's:^ -- *$::' | runhaskell $EscapeAmpersand > $Name
end
strfile $Name

rm $Temp
rm $EscapeAmpersand
