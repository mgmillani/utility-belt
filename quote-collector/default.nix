{ mkDerivation, callPackage, base, containers, directory, filepath, lib, process, fortune, makeWrapper
}:
let wrapperPath = with lib ; makeBinPath [
      fortune
			];
		quoteme = callPackage ./quoteme.nix {};
in quoteme.overrideAttrs (old :
	{ postFixup = 
		''wrapProgram $out/bin/quoteme \
			--prefix PATH : "${wrapperPath}"
		'';
		buildInputs = old.buildInputs ++ [ makeWrapper ];
	})
