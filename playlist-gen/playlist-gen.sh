#!/usr/bin/fish

set MUSIC_DIR (grep "music_directory" $HOME/.config/mpd/mpd.conf | cut -d' ' -f2 | sed 's:"::g' | sed 's:^~:'"$HOME"':')
set PLAYLIST_DIR (grep "playlist_directory" $HOME/.config/mpd/mpd.conf | cut -d' ' -f2 | sed 's:"::g' | sed 's:^~:'"$HOME"':')

set LISTS (ls $MUSIC_DIR)

for PLAYLIST in $LISTS
	mpc listall $PLAYLIST > $PLAYLIST_DIR/$PLAYLIST.m3u
end
