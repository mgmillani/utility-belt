#!/usr/bin/env fish

set ExcludedMountpoints / /boot /nix/store
set ExcludedDevices

set DeviceNames
set DeviceMountpoints

function mount-device
	set Msg (gio mount --device /dev/$argv[1] 2>&1)
	if test $status = 0
		set Mountpoint (lsblk -o MOUNTPOINT /dev/$argv[1] | tail -n1)
		notify-send "External storage" "/dev/$argv[1] mounted successfully on $Mountpoint."
	else
		notify-send --urgency critical "External storage" "Failed to mount /dev/$argv[1].

$Msg
"

	end
end

function unmount-device
	set Msg (umount $argv[2])
	if test $status = 0
		notify-send "External storage" "$argv[2]  unmounted successfully.
Do not unplug device before powering it off."
	else
		notify-send --urgency critical "External storage" "Failed to unmount $argv[2].

$Msg
"
	end

end

function power-off-device
	set UnmountOk 0
	if test "$argv[2]" != ""
		set Msg (umount $argv[2] 2>&1)
		if test $status = 0
			set UnmountOk 1
		else
			set UnmountOk 0
			notify-send --urgency critical "External storage" "Failed to unmount $argv[2].

$Msg
"
		end
	else
		set UnmountOk 1
	end
	if test $UnmountOk -eq 1
		set Msg (udisksctl power-off -b /dev/$argv[1])
		if test $status = 0
			notify-send "External storage" "/dev/$argv[1] powered off successfully.
You can safely remove the device now."
		else
			notify-send --urgency critical "External storage" "Failed to power off /dev/$argv[1].

$Msg
"
		end
	end
end

for Line in (lsblk --list)
	set Fields (string replace -ra "  *" " " $Line | string split " ")
	set DeviceType $Fields[6]
	set Mountpoint $Fields[7]
	if test "$DeviceType" = "part"
		set Valid 1
		for M in $ExcludedMountpoints
			if test "$Mountpoint" = "$M"
				set Valid 0
				break
			end
		end
		if test $Valid -eq 1
			set DeviceNames $DeviceNames $Fields[1]
			set DeviceMountpoints $DeviceMountpoints $Mountpoint
			set DeviceLabel $DeviceLabel ""
		end
	end
end

if test -e /dev/disk/by-label
	for Label in /dev/disk/by-label/*
		set Path (realpath $Label)
		set i 1
		echo $Path
		echo $Label
		for Name in $DeviceNames
			if test "/dev/$Name" = "$Path"
				set DeviceLabel[$i] (basename $Label)
			end
			set i (math "$i + 1")
		end
	end
end

set NumDevices (count $DeviceNames)
set Entries
for i in (seq 1 $NumDevices)
	if test "$DeviceLabel[$i]" != ""
		set Label "$DeviceLabel[$i] (device: /dev/$DeviceNames[$i])"
	else
		set Label "/dev/$DeviceNames[$i]"
	end
	if test "$DeviceMountpoints[$i]" != ""
		set Mountpoint "mounted on $DeviceMountpoints[$i]"
	else
		set Mountpoint ""
	end
	set Entries $Entries "$Label -> $Mountpoint"
end


if test $NumDevices -ge 1
	set i (printf "%s\n" $Entries | rofi -dmenu -no-custom -format i -p "Device")
	set Status $status
	echo $i
	set i (math "$i + 1")
	if test $Status -eq 0
		set Answer $DeviceNames[$i]
		if test "$DeviceMountpoints[$i]" = ""
			set Answer (echo "Mount
Power off" | rofi -dmenu -p "Action")
			if test "$Answer" = "Mount"
				mount-device $DeviceNames[$i]
			else if test "$Answer" = "Power off"
				power-off-device $DeviceNames[$i]
			end
		else
			notify-send "External storage" "Device $DeviceNames[$i] is mounted on $DeviceMountpoints[$i]"

			set Answer (echo "Unmount
Power off" | rofi -dmenu -p "Action")
			if test "$Answer" = "Unmount"
				unmount-device $DeviceNames[$i] $DeviceMountpoints[$i]
			else if test "$Answer" = "Power off"
				power-off-device $DeviceNames[$i] $DeviceMountpoints[$i]
			end
		end
	end
else
	notify-send "External storage" "No external devices found."
end
