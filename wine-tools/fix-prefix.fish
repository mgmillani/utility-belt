#!/usr/bin/env fish

if test -z "$WINEPREFIX" -o -z "$WINEARCH"
	echo "Please set WINEPREFIX path and WINEARCH."
	exit 1
end

if test -z "$XDG_DATA_HOME"
	set WorkingDir ~/.data/wine-tools
else
	set WorkingDir $XDG_DATA_HOME/wine-tools
end
mkdir -p $WorkingDir

cd $WorkingDir
set WineDir (wine --version)-$WINEARCH
if test -e $WineDir
	echo $WineDir already exists, something is wrong. Please delete (pwd)/$WineDir.
	exit 2
end
mkdir $WineDir
cd $WineDir
set OLDPREFIX $WINEPREFIX
set WINEPREFIX (pwd)
wine boot
for Fl in (find .)
	if test -f $Fl
		if test ! -e $OLDPREFIX/$Fl
			echo "File $Fl not found. Copying..."
			mkdir -p $OLDPREFIX/(dirname $Fl)
			cp $Fl $OLDPREFIX/$Fl
		else if test (sha512sum $Fl) != (sha512sum $OLDPREFIX/$Fl)
			echo "File $Fl changed, will not be overwritten."
		end
	end
end
cd ..
echo removing (pwd)/$WineDir
# rm -fr $WineDir
