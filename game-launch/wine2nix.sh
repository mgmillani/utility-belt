#!/usr/bin/env fish

set Dir $argv[1]
set Name (basename $Dir)
set Pname (echo $Name | sed s:"[^a-zA-Z0-9]":-:g)
set AppPath (realpath $Dir)
set ExePattern "\\.lnk\$"

cd $Dir/drive_c
set Candidates (ls | grep -vi "users\|windows")
set Path (find $Candidates | grep $ExePattern | head -n1)
if test x"$Path" = x
	echo "No executable found" 1>&2
	exit 1
end

set GamePath (dirname $Path)
set Icon (find (realpath $GamePath) -maxdepth 1 | grep -i "ico\$" | head -n1)
if test x"$WINEARCH" = x
	set -x WINEARCH win32
end

mkdir -p ~/.config/nixpkgs/games/enabled
cd ~/.config/nixpkgs/games/enabled
mkdir -p $Pname
cd $Pname

echo "{pkgs, ...} : " > default.nix
echo "with pkgs.lib;
with builtins;

let launcher = 
        with pkgs ; wine-launcher.override (old : 
            { wine = pkgs.wineWowPackages.base;
					  });" >> default.nix


if test x"$Icon" != x
	convert $Icon -thumbnail 16x16 -alpha on -background none -flatten Icon.png
	echo "
    game-icon = with pkgs ; with pkgs.stdenv ;
        mkDerivation
        {
          pname = \"game-icon\";
          version = \"1.0.0.0\";
          buildInputs = [ which coreutils fish gnused ]; 
          license = \"GNU General Public License v3.0 only\";
        	system = builtins.currentSystem;
        	src = ./.;
        	preInstall = ''mkdir -p  \$out/share/icons/
        cp Icon.png \$out/share/icons/"$Pname-icon.png"
        
        echo \"install:\" > Makefile
        	'';
        };" >> default.nix
end

echo "					
    desktop = 
        with pkgs ; desktop-entry.override (old : 
            { appName = \""$Name"\"
            ; pname = \""$Pname"\"
            ; exePath = launcher + \"/bin/wine-launcher\"" >> default.nix
if test x"$Icon" != x
	echo "
            ; appIcon = game-icon + ''/share/icons/"$Pname-"icon.png''" >> default.nix
end
echo "
            ; exeArgs = [
                \"Games/"$Name"\"
                \"drive_c/"$Path"\"
								\""$WINEARCH"\"
            ];
					  });
in
{ pkg = desktop
; desktop =
        { \${\".local/share/applications/"$Name".desktop\"} =
                { source = desktop + 
                           \"/share/applications/"$Name".desktop\";
                };
        }
        ;
}" >> default.nix


