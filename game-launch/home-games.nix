{ pkgs , ... }:

with builtins;
with pkgs.lib;

let games = with pkgs ; with pkgs.stdenv ; 
               builtins.map (p: callPackage ((./games/enabled) + ("/" + p)) {}) (builtins.attrNames (builtins.readDir ./games/enabled));
    splaytab = with pkgs ; with pkgs.stdenv ; callPackage ./packages/splaytab {};
in
{
  home.packages = [
    pkgs.compton
    pkgs.dosbox
    pkgs.wineWowPackages.base
    pkgs.winetricks
    pkgs.dunst
    pkgs.dex
    pkgs.steam
    pkgs.steam-run-native
    pkgs.tango-icon-theme
    pkgs.unclutter
    pkgs.xscreensaver
    splaytab
  ] ++
  (builtins.map (g: g.pkg) games);

  home.file = 
    builtins.foldl' (p: q: p // q) {}
      ( map (g: g.desktop) games);
}
