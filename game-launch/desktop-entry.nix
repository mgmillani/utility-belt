{ which
, coreutils
, gnused
, mkDerivation
, lib
, icon-library
, appName
, appIcon ? icon-library.out + "/share/icon-library/icon-dev-kit/gamepad-symbolic.svg"
, exePath
, exeArgs ? []
, pname ? appName
, ... } :

let iconFile = builtins.baseNameOf appIcon;
    argv = builtins.concatStringsSep " " (builtins.map (x: ''\"'' + x + ''\"'') exeArgs);
    desktopContent = ''
[Desktop Entry]
Name='' + appName + ''

Comment='' + appName + '' 

GenericName='' + appName + '' 

Keywords=game;
Terminal=false
StartupNotify=false
Type=Application
Categories=Game;'';
in
mkDerivation
{
  pname = pname;
  version = "1.0.0.0";
  buildInputs = [ which coreutils gnused ]; 
  license = "GNU General Public License v3.0 only";
# lib.licenses.gpl3Only;
	system = builtins.currentSystem;
	src = ./.;
	preInstall = ''mkdir -p $out/share/applications/ $out/bin/ $out/share/icons

cp "'' + appIcon + ''" $out/share/icons/'' + iconFile + ''

echo "'' + desktopContent + ''

Icon=$out/share/icons/'' + iconFile + ''

Exec='' + exePath + '' "" '' + argv + ''" > $out/share/applications/"'' + appName + ''".desktop
echo "install:" > Makefile
	'';
}
