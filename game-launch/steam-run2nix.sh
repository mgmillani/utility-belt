#!/usr/bin/env fish

if test (count $argv) -lt 2
  echo "usage: steam-run2nix <Name> <Executable>"
  exit 1
end

set Exe $argv[2]
set Dir (dirname $Exe)
set Name $argv[1]
set Pname (echo $Name | sed s:"[^a-zA-Z0-9]":-:g)
set AppPath (realpath $Dir)

set Icon (find $AppPath -maxdepth 1 | grep -i "ico\$" | head -n1)

mkdir -p ~/.config/nixpkgs/games/enabled
cd ~/.config/nixpkgs/games/enabled
mkdir -p $Pname
cd $Pname

echo "{pkgs, ...} : " > default.nix
echo "with pkgs.lib;
with builtins;

let pname = \"$Pname\";
    appname = \"$Name\";
    executable-path = \"$Exe\";
    my-steam-run = (pkgs.steam.override (old : 
                         { extraPkgs = pkgs : 
                           [
                           ];
                         })).run;
    pkg = 
        with pkgs ; with pkgs.stdenv ; steam-run-launcher.override (old :
            { mkDerivation = mkDerivation
            -- ; preLaunchPhase = '' '';
            ; launchPhase = my-steam-run + ''/bin/steam-run \$Target''
            ; pname = pname
            ;
            });" >> default.nix


if test x"$Icon" != x
  convert $Icon -thumbnail 16x16 -alpha on -background none -flatten Icon.png
  echo "
    game-icon = with pkgs ; with pkgs.stdenv ;
        mkDerivation
        {
          pname = \"game-icon\";
          version = \"1.0.0.0\";
          buildInputs = [ which coreutils fish gnused ]; 
          license = \"GNU General Public License v3.0 only\";
          system = builtins.currentSystem;
          src = ./.;
          preInstall = ''mkdir -p  \$out/share/icons/
        cp Icon.png \$out/share/icons/"'' + pname + ''-icon.png"
        
        echo \"install:\" > Makefile
        '';
        };" >> default.nix
end

echo "
    desktop = 
        with pkgs ; with pkgs.stdenv ; desktop-entry.override (old :
            { mkDerivation = mkDerivation
            ; appName = appname
            ; pname = pname
            ; exePath = pkg + \"/bin/steam-run-launcher\"" >> default.nix
if test x"$Icon" != x
  echo "
            ; appIcon = game-icon + ''/share/icons/'' + pname + ''-icon.png''" >> default.nix
end
echo "
            ; exeArgs = [
                (\"Games/\" + appname)
                executable-path
            ];
					  });
in
{ pkg = desktop
; desktop =
        { \${\".local/share/applications/\" + appname + \".desktop\"} =
                { source = desktop + 
                           \"/share/applications/\" + appname + \".desktop\";
                };
        }
        ;
}" >> default.nix


