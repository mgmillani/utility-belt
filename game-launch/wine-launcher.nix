{ wine
, which
, coreutils
, fish
, gnused
, mkDerivation
, libnotify
, lib
, pname ? "wine-launcher"
, wineSetup ? ""
, winePreLaunch ? ""
, winePostLaunch ? ""
, ... } :

let launchContent = ''
#!/usr/bin/env fish
cd ~/\$argv[1]
set -x WINEPREFIX (pwd)
if test (count \$argv) -ge 3
	set -x WINEARCH \$argv[3]
else
	set -x WINEARCH win32
end
if test ! -e .setup_complete
	'' + wineSetup + ''

	touch .setup_complete
end
'' + winePreLaunch + ''

cd (dirname \$argv[2])
if test ".lnk" = (echo \$argv[2] | grep -o '\.[^.]*')
	'' + wine.outPath + ''/bin/wine start (basename \$argv[2]) \$argv[(seq 4 (count \$argv))]
else
	'' + wine.outPath + ''/bin/wine (basename \$argv[2]) \$argv[(seq 4 (count \$argv))]
end
set EXIT_STATUS $status
if test $EXIT_STATUS != 0
  ${libnotify}/bin/notify-send "\$argv[2]" "Application finished with an error."
end
'' + winePostLaunch;
in
mkDerivation
{
  pname = "wine-launcher";
  version = "1.0.0.0";
  buildInputs = [ which coreutils fish gnused ]; 
  license = "GNU General Public License v3.0 only";
	system = builtins.currentSystem;
	src = ./.;
  preInstall = ''mkdir -p $out/bin/

echo '#!${fish}/bin/fish' > $out/bin/wine-launcher
echo . $out/bin/setup.fish >> $out/bin/wine-launcher
cat ./wine-launcher.fish >> $out/bin/wine-launcher
chmod +x $out/bin/wine-launcher

echo 'function wineSetup
  ${builtins.replaceStrings ["\n"] ["\n  "] wineSetup}
  touch .setup_complete
end' >> $out/bin/setup.fish

echo 'function winePreLaunch
  ${builtins.replaceStrings ["\n"] ["\n  "] winePreLaunch}
end' >> $out/bin/setup.fish

echo 'function winePostLaunch
  ${builtins.replaceStrings ["\n"] ["\n  "] winePostLaunch}
end' >> $out/bin/setup.fish

echo 'function custom_wine
  ${wine}/bin/wine $argv
end' >> $out/bin/setup.fish

echo 'function custom_notify
  ${libnotify}/bin/notify-send $argv
end' >> $out/bin/setup.fish

echo "install:" > Makefile
'';
# 	preInstall = ''mkdir -p $out/bin/
# 
# echo '
# '' + launchContent + ''' > $out/bin/wine-launcher
}
