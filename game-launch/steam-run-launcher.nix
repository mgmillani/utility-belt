{ steam-run-native
, which
, coreutils
, fish
, gnused
, mkDerivation
, lib
, pname ? "steam-run-launcher"
, setupPhase ? ""
, preLaunchPhase ? ""
, postLaunchPhase ? ""
, ... } :

let launcher-name = "steam-run-launcher";
    launchContent = ''
#!/usr/bin/env fish
cd ~/\$argv[1]

if test ! -e .setup_complete
	'' + setupPhase + ''

	touch .setup_complete
end
'' + preLaunchPhase + ''

cd (dirname \$argv[2])
'' + steam-run-native.outPath + ''/bin/steam-run ./(basename \$argv[2])

'' + postLaunchPhase;
in
mkDerivation
{
  pname = pname;
  version = "1.0.0.0";
  buildInputs = [ which coreutils fish gnused ]; 
  license = "GNU General Public License v3.0 only";
# lib.licenses.gpl3Only;
	system = builtins.currentSystem;
	src = ./.;
	preInstall = ''mkdir -p  $out/bin
echo "'' + launchContent + ''" > $out/bin/"'' + launcher-name +''"

chmod +x $out/bin/"'' + launcher-name + ''"

echo "install:" > Makefile
	'';
}
