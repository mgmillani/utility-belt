{ which
, coreutils
, fish
, gnused
, mkDerivation
, lib
, pname ? "generic-launcher"
, setupPhase ? ""
, preLaunchPhase ? ""
, launchPhase ? ""
, postLaunchPhase ? ""
, ... } :

let launcher-name = "generic-launcher";
    launchContent = ''
#!/usr/bin/env fish
cd ~/\$argv[1]

if test ! -e .setup_complete
	'' + setupPhase + ''

	touch .setup_complete
end
'' + preLaunchPhase + ''

cd (dirname \$argv[2])
set Target \$argv[2]
'' + launchPhase + ''

'' + postLaunchPhase;
in
mkDerivation
{
  pname = pname;
  version = "1.0.0.0";
  buildInputs = [ which coreutils fish gnused ]; 
  license = "GNU General Public License v3.0 only";
# lib.licenses.gpl3Only;
	system = builtins.currentSystem;
	src = ./.;
	preInstall = ''mkdir -p  $out/bin
echo "'' + launchContent + ''" > $out/bin/"'' + launcher-name +''"

chmod +x $out/bin/"'' + launcher-name + ''"

echo "install:" > Makefile
	'';
}
