#!/usr/bin/env fish
cd ~/$argv[1]
set -x WINEPREFIX (pwd)
if test (count $argv) -ge 3
	set -x WINEARCH $argv[3]
else
	set -x WINEARCH win32
end
if test ! -e .setup_complete
  wineSetup
end

winePreLaunch

cd (dirname $argv[2])
if test ".lnk" = (echo $argv[2] | grep -o '\.[^.]*')
	custom_wine start (basename $argv[2]) $argv[(seq 4 (count $argv))]
else
	custom_wine (basename $argv[2]) $argv[(seq 4 (count $argv))]
end
set EXIT_STATUS $status
if test $EXIT_STATUS != 0
  custom_notify "$argv[2]" "Application finished with an error."
end

winePostLaunch
