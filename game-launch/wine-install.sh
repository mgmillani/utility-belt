#!/usr/bin/env fish

if test (count $argv) -lt 2
	echo "usage: wine-install NAME SETUP.exe"
	exit 1
end

if test ! -e $argv[2]
	echo "Setup file $argv[2] does not exist."
	exit 1
end

if test (count $argv) -ge 3
	set -xg WINEARCH $argv[3]
else
	set -xg WINEARCH win32
end

if test (count $argv) -ge 4
	set WinePkg "wineWowPackages."$argv[4]
else
	set WinePkg "wineWowPackages.base"
end

set HDir Games/$argv[1]
set Dir $HOME/$HDir
set AppName $argv[1]
set Pname (echo $AppName | sed 's:[ "'"'"']\+:-:g')

mkdir -p $Dir
cd $Dir
set -xg WINEPREFIX $(pwd)
wine $argv[2]

echo Finished installation

wine2nix $Dir
echo Package written to ~/.config/nixpkgs/games/enabled/$Pname
sed -i 's:wineWowPackages.base:$WinePkg:g' ~/.config/nixpkgs/games/enabled/$Pname
home-manager switch
splaytab-jgmenu update
