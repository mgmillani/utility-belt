#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

// times in microseconds
#define CHECK_INTERVAL (50*1000) 
#define WAIT_TIME_BEFORE_CMD (100*1000)

int get_files_cmd(const char ***filesp, char **cmdp, int argc, char **argv){
	const char **files = malloc(argc*sizeof(*files));
	char *cmd;
	int numFiles, len;
	int i;
	for(numFiles = 1; numFiles < argc
	&& ('-'!=argv[numFiles][0]||'\0'!=argv[numFiles][1]) ; ++numFiles){
		files[numFiles-1] = argv[numFiles];
	}
	--numFiles;
	// no files to watch or no cmds
	if(!numFiles || numFiles >= argc-2){
		free(files);
		return 0;
	}
	*filesp = realloc(files, numFiles*sizeof(*files));

	len = 0;
	for(i=numFiles+2; i < argc; ++i)
		len += strlen(argv[i])+1;
	*cmdp = cmd = malloc(len);
	len = 0;
	for(i=numFiles+2; i < argc; ++i){
		len = strlen(argv[i]);
		memcpy(cmd,argv[i],len);
		cmd[len] = ' ';
		cmd += len + 1;
	}
	cmd[-1] = '\0';

	return numFiles;
}

time_t get_mtime(const char *path){
	struct stat st;
	if(lstat(path, &st))
		return 0; // not exactly correct as 0 is a valid value for time_t
	return st.st_mtime;
}

int main(int argc, char **argv){
	char timeBuf[26];
	time_t *previous;
	const char **files;
	char *cmd;
	time_t mtime;
	int numFiles;
	int i;
	char changed = 0;
	numFiles = get_files_cmd(&files, &cmd, argc, argv);
	if(!numFiles){
		printf("usage: %s FILES_TO_WATCH - COMMAND_TO_EXECUTE\n", argv[0]);
		return 1;
	}
	previous = malloc(numFiles*sizeof(*previous));
	for(i=0;i<numFiles;++i)
		previous[i] = get_mtime(files[i]);
	while(1){
		for(i=0; i<numFiles; ++i){
			mtime = get_mtime(files[i]);
			if(previous[i] != mtime){
				previous[i] = mtime;
				changed = 1;
			}
		}
		if(changed){
			changed = 0;
			usleep(WAIT_TIME_BEFORE_CMD);
			time_t t;
			time(&t);
			ctime_r(&t,timeBuf);
			printf("------------------------\n%s------------------------\n",timeBuf);
			system(cmd);
		}else{
			usleep(CHECK_INTERVAL);
		}
	}
}
