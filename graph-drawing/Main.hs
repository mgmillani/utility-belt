module Main where

import qualified Language.Dot.Parser as D
import qualified Language.Dot.Pretty as P
import qualified Data.Map as M
import qualified Data.Set as S

import Data.Maybe

import System.Environment

step = 100

adjacencyWithPorts (_,_,_, stmts) = adjacency' [] [] stmts
  where
    adjacency' _ _ [] = ([],[])
    adjacency' nodeAttr edgeAttr (stmt:ss) = 
      case stmt of
        (D.EdgeStatement subgraphs attributes) -> 
          let sgs = (map (subgraphAdjacency nodeAttr edgeAttr) subgraphs)
              edges = concatMap snd sgs
              nodes = map fst sgs
              pathEdges = makePath ((reverse attributes) ++ edgeAttr) nodes
              (ns, es) = adjacency' nodeAttr edgeAttr ss
          in ((concat nodes) ++ ns, (edges ++ pathEdges) ++ es)
        (D.NodeStatement name port attributes) -> 
          let (ns, es) = adjacency' nodeAttr edgeAttr ss
          in ((D.NodeStatement name port (nodeAttr ++ attributes)) : ns, es)
        (D.SubgraphStatement subgraph)     -> case subgraph of
          D.NodeRef name port -> 
            let (ns, es) = adjacency' nodeAttr edgeAttr ss
            in ((D.NodeStatement name port nodeAttr) : ns, es)
          D.Subgraph name stmts -> adjacency' nodeAttr edgeAttr stmts
        (D.AttributeStatement attribute)   -> adjacency' nodeAttr edgeAttr ss
        (D.EdgeAttribute attributes)  -> adjacency' nodeAttr ((reverse attributes) ++ edgeAttr) ss
        (D.NodeAttribute attributes)  -> adjacency' ((reverse attributes) ++ nodeAttr) edgeAttr ss
        (D.GraphAttribute attributes) -> adjacency' nodeAttr edgeAttr ss
    subgraphAdjacency nodeAttr edgeAttr (D.NodeRef name port) = ([D.NodeStatement name port []], [])
    subgraphAdjacency nodeAttr edgeAttr (D.Subgraph name stmts) = adjacency' nodeAttr edgeAttr stmts
    makePath edgeAttr [n1] = []
    makePath edgeAttr (n0:n1:ns) = [D.EdgeStatement [(D.NodeRef v0 port0), (D.NodeRef v1 port1)] (reverse edgeAttr) | (D.NodeStatement v0 port0 _) <- n0, (D.NodeStatement v1 port1 _) <- n1] ++ makePath edgeAttr (n1:ns)

pointStr (x,y) = show x ++ "," ++ show y

getCompass Nothing = Nothing
getCompass (Just (D.Port _ compass)) = compass

readPort (Nothing) = (1,1)
readPort (Just D.North) = (0,1)
readPort (Just D.NorthEast) = (1,1)
readPort (Just D.East) = (1,0)
readPort (Just D.SouthEast) = (1,-1)
readPort (Just D.South) = (0,-1)
readPort (Just D.SouthWest) = (-1,-1)
readPort (Just D.West) = (-1,0)
readPort (Just D.NorthWest) = (-1,1)

fixPos (x,y) portFrom portTo = 
  let (ux1, uy1) = readPort $ getCompass portFrom
      (ux2, uy2) = readPort $ getCompass portTo
      dx = step * (ux1 - ux2)
      dy = step * (uy1 - uy2)
  in (x + dx, y + dy)

toNodeStatement (name, (x,y)) = D.NodeStatement (D.StringID name) Nothing [(D.StringID "pos", D.StringID $ pointStr (x,y))]

makeGraph nodes edges =
  let gr = M.fromList [(show name, []) | (D.NodeStatement name _ _) <- nodes]
      gr' = foldl (\m (D.EdgeStatement (v0r:v1r:[]) []) ->
            let (D.NodeRef v0 mport0) = v0r
                (D.NodeRef v1 mport1) = v1r
            in M.insertWith (++) (show v0) [(show v1, mport0, mport1)] $ M.insertWith (++) (show v1) [(show v0, mport1, mport0)] m) gr edges
  in gr'

placeNodes gr = 
  let v0 = fst $ head $ M.assocs gr
  in placeNodes' (S.singleton v0) S.empty gr (M.fromList [(v0, (0,0))])
placeNodes' vs visited gr positions
 | M.null gr = positions
 | S.null vs = 
    let v0 = fst $ head $ M.assocs gr
    in placeNodes' (S.singleton v0) visited gr (M.insert v0 (0,0) positions)
 | otherwise = 
    let v = head $ S.toList vs
        (x,y) = positions M.! v
        positions' = positions `M.union`
          (M.fromList
            [ (u, uPos)
            | (u, vPort, uPort) <- gr M.! v
            , let uPos = fixPos (x,y) vPort uPort
            , u `M.member` gr])
        gr' = M.delete v gr
        us = (S.fromList $ map (\(u,_,_) -> u) $ gr M.! v) `S.difference` visited
        visited' = S.insert v visited
        vs' = (S.delete v vs) `S.union` us
    in placeNodes' vs' visited' gr' positions'

draw (d, t, name, stmt) =
  let (nodes, edges) = adjacencyWithPorts (d, t, name, stmt)
      gr = makeGraph nodes edges
      positions = placeNodes gr
  in (False, D.Graph, name, (map toNodeStatement $ M.assocs $ positions) ++ stmt)

main = do
  args <- getArgs
  fl <- readFile $ args!!0
  let Right gr = D.parse fl
  let drawing = draw gr
  putStr $ P.render drawing


