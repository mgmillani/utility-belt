if status --is-login
	set -x PATH $HOME/.local/bin $HOME/.cabal/bin $PATH
	set -x XDG_CONFIG_HOME $HOME/.config
	set -x XDG_DATA_HOME $HOME/.data
	set -x XDG_DATA_DIRS $HOME/.local/share/:$XDG_DATA_DIRS
	set -x TERMINAL urxvt
	set -x TEXMFHOME $HOME/.local/share/texmf/
	set -x _JAVA_AWT_WM_NONREPARENTING 1
	rm ~/Downloads/*
	# startup programs
	# rehask &
end
