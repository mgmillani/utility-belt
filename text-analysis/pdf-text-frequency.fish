#!/usr/bin/env fish

pdfgrep -hoir $argv | sort | tr '[:upper:]' '[:lower:]' | uniq --count
