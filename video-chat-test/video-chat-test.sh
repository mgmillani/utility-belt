#!/usr/bin/env fish

function Notify
	notify-send "Video/Mic test" $argv
end

set Mic (pactl get-default-source)
Notify "Default microphone is $Mic."

set Notifications

echo $Mic | grep 'echo-cancel' > /dev/null
if test $status != 0
	# pactl list sources | grep "Name: $Mic.echo-cancel" > /dev/null
	pactl list sources | grep "Name: echo-cancel-source" > /dev/null
	if test $status = 0
		set Mic echo-cancel-source
		set Notifications $Notifications "Set microphone to $Mic."
		pactl set-default-source $Mic
	else
		pactl list modules | grep module-echo-cancel > /dev/null
		if test $status = 1
			pactl load-module module-echo-cancel
			sleep 1s
			pactl list sources | grep "Name: echo-cancel-source" > /dev/null
			if test $status = 0
				set Mic echo-cancel-source
			end
			set Notifications $Notifications "Set microphone to $Mic."
			pactl set-default-source $Mic
		else
			pactl unload-module module-echo-cancel
			pactl load-module module-echo-cancel
			set Waiting 10
			set Notifications $Notifications "Loading echo-cancel module..."
			while test $Waiting != 0
				pactl list sources | grep "Name: echo-cancel-source" > /dev/null
				if test $status = 0
					set Mic echo-cancel-source
					set Waiting 0
				else
					set Waiting (math "$Waiting - 1")
					sleep 1s
				end
			end
			set Notifications $Notifications "Set microphone to $Mic."
			pactl set-default-source $Mic
		end
	end
end

set Dir $HOME/.config/video-chat-test
mkdir -p $Dir

if test -e $Dir/$Mic
	set Volume (cat $Dir/$Mic)
else
	echo '100%' > $Dir/$Mic
	set Volume '100%'
end

set Notifications $Notifications "Set volume to $Volume"
echo Setting volume of $Mic to $Volume
pactl set-source-volume $Mic $Volume
Notify (printf "%s\n" $Notifications)
# notify-send "Microphone" $Mic
fish -c "sleep 5s ; pactl set-source-volume $Mic $Volume" &
arecord -f dat | aplay -f dat &
set PID (jobs -lp | tail -n1)
cheese
kill $PID
