{ stdenv
, lib
, fish
, makeWrapper
, cheese
, alsa-utils
, pulseaudio
}:
  stdenv.mkDerivation {
    pname = "video-chat-test";
    name = "video-chat-test";
    buildInputs = [ fish cheese pulseaudio ];
    nativeBuildInputs = [ makeWrapper ];
    src = ./.;
    installPhase = ''
      mkdir -p $out/bin
      cp video-chat-test.sh $out/bin/video-chat-test
      wrapProgram $out/bin/video-chat-test \
        --prefix PATH : ${lib.makeBinPath [ fish cheese alsa-utils pulseaudio ]}
    '';
  }
