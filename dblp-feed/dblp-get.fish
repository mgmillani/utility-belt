#!/usr/bin/env fish

if test (count $argv) -lt 1
	echo "Please provide RSS-feed URL."
	exit 1
end

set FEED $argv[1]
set DIR $XDG_DATA_HOME/dblp-feed
set MAX_DELTA (math "24*60*60")

mkdir -p $DIR
set FILE "$DIR"/(echo $FEED | grep -o '[^/]*$')
set OLD 1
if test -e $FILE
	set FILE_AGE (stat -c '%Y' $FILE)
	set NOW (date +'%s')
	set DELTA (math "$NOW - $FILE_AGE")
	if test $DELTA -le $MAX_DELTA
		set OLD 0
	end
end

if test 1 -eq $OLD
	wget $FEED -O $FILE
	sed s'-<pubDate>\([0-9]*\)</pubDate>-<pubDate>1 Jan \1 00:00:00 GMT</pubDate>-' < $FILE | sponge $FILE
end

cat $FILE
