{ stdenv
, abook
, coreutils
, fish
, gnugrep
, isync
, lib
, libnotify
, procmail
, runCommandLocal
}:
let pathWrap = src: dest: pkgs: 
''

echo '#!${fish}/bin/fish
set PATH $PATH ${builtins.toString (builtins.map (pkg: "${pkg}/bin" ) pkgs)}
' > $out/${dest}
tail -n+2 ${src} >> $out/${dest}
chmod +x $out/${dest}
'';
in
runCommandLocal "mbsync-notify" {
	script = ./mbsync-notify.fish;
	nativeBuildInputs = [ ];
}
(''
mkdir -p $out/bin
mkdir -p $out/lib
''
+ (pathWrap ./mbsync-notify.fish "bin/mbsync-notify" [gnugrep libnotify isync])
+ (pathWrap ./notify-delayed.fish "bin/notify-delayed" [ coreutils libnotify ])
+ (pathWrap ./notify-contacts.fish "lib/notify-contacts" [ coreutils gnugrep abook procmail libnotify ])
+ (pathWrap ./notify-ignore.fish "lib/notify-ignore" [ coreutils gnugrep procmail ])
+ (pathWrap ./notify-batched.fish "lib/notify-batched" [ coreutils gnugrep libnotify ]))

