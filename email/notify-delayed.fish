#!/usr/bin/env fish
set Target $argv[1]
set Count $argv[2] 
if test $Count -gt 1
	set S s
else
	set S ""
end
set Done 0
while test $Done = 0
	set Now (date +%s)
	if test (math "$Now - $Target") -ge 0
		notify-send "New mail" "You have $argv[2] new message$S."
		set Done 1
	else
		# We sleep only 1m because the computer might be suspended at some point.
		sleep 1m
	end
end
