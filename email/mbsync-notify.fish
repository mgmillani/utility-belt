#!/usr/bin/env fish
if set -q MBSYNC_MAILDIR
	set MailDir $MBSYNC_MAILDIR
else if set -q XDG_DATA_HOME
	set MailDir $XDG_DATA_HOME/mail
else
	set MailDir $HOME/.data/mail
end

if set -q XDG_CONFIG_HOME
	set ConfigDir $XDG_CONFIG_HOME/mbsync
else
	set ConfigDir $HOME/.config/mbsync
end

if test (count $argv) = 0
	set AccOpt "--all"
else
	set AccOpt $argv
end

echo Syncing...
mbsync --config ~/.config/mbsync/mbsyncrc $AccOpt --quiet
if test $status != 0
	notify-send -u critical "mbsync" "mbsync failed"
	exit 1
end

function defaultNotification
	set Count (count $argv)
	if test $Count = 1
		notify-send "New mail" "If have 1 new message."
	else if test $Count -gt 1
		notify-send "New mail" "If have $Count new messages."
	end
end

set NewMail (find -L $MailDir -type f | grep '/new/' )
set RemainingMail $NewMail
if test -e $ConfigDir/notify
	set Filters (find -L $ConfigDir/notify -type f | sort -n)
	if test (count $Filters) -gt 0
		for Filter in $Filters
      echo "Calling filter $Filter..." 1>&2
			set RemainingMail (printf "%s\n" $RemainingMail | $Filter)
		end
		if test (count $RemainingMail) -gt 0
			defaultNotification $RemainingMail 
		end
	else
    echo "No filters found in $ConfigDir/notify." 1>&2
		defaultNotification $RemainingMail 
	end
else
  echo "No filters found in $ConfigDir/notify." 1>&2
	defaultNotification $RemainingMail 
end
