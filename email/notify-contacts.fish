#!/usr/bin/env fish

set Senders
while read Email
	set From (formail -x From < $Email | grep -o '[^<> @]\+@[^<> ]\+')
	set Found 0
	for Address in $From
		set Contact (abook --mutt-query $Address | grep '@' | string split --no-empty ' ')
		if test -n "$Contact"
			set Found 1
			break
		end
	end
	if test $Found = 0
		# Not in address book
		echo $Email
	else
		# Notify email about contacts in address book
		set Name $Contact[2..]
		if test -z "$Name"
			set Senders $Senders $Contact[1]
		else
			set Senders $Senders $Name
		end
	end
end

# printf "%s\n" $Senders 1>&2

if test (count $Senders) -gt 0
	set Senders (printf "%s\n" $Senders | sort --unique)
	set Count (count $Senders)
	if test $Count -gt 0
		if test $Count = 1
			set S ""
		else
			set S s
		end
		notify-send "New mail from contacts" "You have $(count $Senders) new message$S from:\n"(printf " - %s\n" $Senders)
	end
end
