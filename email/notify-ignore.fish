#!/usr/bin/env fish
while read Email
	if test -e $Email
		set To (formail -x To < $Email)
		echo $To | grep "dmanet@zpr.uni-koeln.de"
		set DMANET $status

		if test $DMANET != 0
			# E-mails not in the blacklist may be notified.
			echo $Email
		end
	end
end
