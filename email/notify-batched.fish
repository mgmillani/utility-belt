#!/usr/bin/env fish
set MinDelay (math "60 * 60")
set WorkingDir $XDG_DATA_HOME/mail-notify
mkdir -p $WorkingDir

set Count 0
while read Email
	set $Count (math "$Count + 1")
end
if test $Count -gt 1
	set S s
else
	set S ""
end

if test $Count -gt 0

	set Now (date +%s)

	if test -e $WorkingDir/last-notification
		set Last (cat $WorkingDir/last-notification)
		set Diff (math "$Now - $Last")
		if test $Diff -ge $MinDelay
			notify-send "New mail" "You have $Count new message$S."
			echo $Now > $WorkingDir/last-notification
		else
			set Target (math "$Now + $MinDelay - $Diff")
			notify-delayed $Target $Count &
			disown
			echo $Target > $WorkingDir/last-notification
		end
	else
		notify-send "New mail" "You have $Count new message$S."
		echo $Now > $WorkingDir/last-notification
	end
end
