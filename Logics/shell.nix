with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "logics";
  buildInputs = [ cabal-install
    (haskellPackages.ghcWithPackages (p:
    [ p.array
      p.base
      p.containers
      p.transformers
      p.random
    ])
    )
  ];
}
