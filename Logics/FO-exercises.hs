module Main where

import FO
import EFGames
import Data.Tree
import Data.List
import qualified Data.Set as S
import qualified Data.Map as M
import System.Random
import Control.Monad
import Control.Monad.Trans.State

main = do
  writeFile "sequents.tex" $ concatMap sequentCalculusExercise $ concat
    [ take 5 $ generateSequent baseFormulas proofTree
    | baseFormulas <- [baseFormulas0, baseFormulas1]
    , proofTree <- [tree1, tree2, tree3, tree4, tree5]
    ]
  print $ isSpoilerStrategy 3 strategy0 a0 b0
  gen <- newStdGen
  p1 <- evalStateT (generateStructurePair 10000 (game 3 strategy0) 8 a1 b1) gen
  p2 <- evalStateT (generateStructurePair 10000 (game 3 strategy0) 10 a1 b1) gen
  p3 <- evalStateT (generateStructurePair 10000 (game 3 strategy0) 12 a1 b1) gen
  p4 <- evalStateT (generateStructurePair 10000 (game 3 strategy0) 14 a1 b1) gen
  writeFile "efgames.tex" $ intercalate "\n\\newpage\n" $ map (efgameTikz tikzA1 tikzB1) [p1,p2,p3,p4]
  --print $ duplicatorWins 2 a0 b0
  --print $ not $ spoilerHasBlindStrategy 3 a0 b0

game m spoilerStrategy a b =
  (duplicatorWins (m - 1) a b) &&
  (isSpoilerStrategy m spoilerStrategy a b) &&
  (not $ spoilerHasBlindStrategy m a b)
  

sequentCalculusExercise s@(Node{rootLabel = ((l, r), _)}) = intercalate "\n" $ 
    [ "\\["
    , sequentToLatex (l,r)
    , "\\]\n\\vspace{1\\baselineskip}\n"
    , "\\begin{center}\n\\begin{prooftree}[template=$\\inserttext$]"
    , sequentCalculusToLatex s
    , "\\end{prooftree}\\end{center}\n\\newpage\n"
    ]

efgameTikz _ _ (Left err) = "\n"
efgameTikz tikzA1 tikzB1 (Right (a1,b1)) = intercalate "\n" $ 
  [ "\\begin{tikzpicture}"
  , prettyTikz tikzA1
  , intercalate "\n" $ 
    [ "\\path[-latex, draw = black] (V" ++ vid ++ ") to (V" ++ uid ++ ");"
    | [v,u] <- S.toList $ (stRelations a1) M.! "E"
    , let vid = aIds M.! v
    , let uid = aIds M.! u
    ]
  , "\\end{tikzpicture}\\quad"
  ] ++ 
  [ "\\begin{tikzpicture}"
  , prettyTikz tikzB1
  , intercalate "\n" $ 
    [ "\\path[-latex, draw = black] (V" ++ vid ++ ") to (V" ++ uid ++ ");"
    | [v,u] <- S.toList $ (stRelations b1) M.! "E"
    , let vid = bIds M.! v
    , let uid = bIds M.! u
    ]
  , "\\end{tikzpicture}\n\\newpage\n"
  ]
  where
    aIds = M.fromList $ zip (stUniverse a1) $ map show [1..]
    bIds = M.fromList $ zip (stUniverse b1) $ map show [1..]

baseFormulas0 = [Predicate "P" [Var "a"], Predicate "Q" [Var "a"]]

baseFormulas1 = [Predicate "R" [Var "x", Var "y"], Predicate "R" [Var "y", Var "x"]]

tree1 = Node{ 
             rootLabel = OrRight (Predicate "P" [])
           , subForest = [ Node{ 
             rootLabel = NotRight $ Predicate "P" []
           , subForest = [ Node{
             rootLabel = ForallRight (Predicate "P" []) (Var "a") 
           , subForest = [ Node{
             rootLabel = ForallLeft (Predicate "P" []) (Var "a")
           , subForest = [ Node{
             rootLabel = ForallLeft (Predicate "P" []) (Var "a")
           , subForest = [ Node{
             rootLabel = ImplicationLeft (Predicate "P" [])
           , subForest = 
              [ Node{rootLabel = Axiom, subForest = []}
              , Node{rootLabel = Axiom, subForest = []}]}]}]}]}]}]}
tree2 = Node{ 
             rootLabel = AndLeft (Predicate "P" [])
           , subForest = [ Node{ 
             rootLabel = NotLeft $ Predicate "P" []
           , subForest = [ Node{
             rootLabel = ExistsLeft (Predicate "P" []) (Var "a") 
           , subForest = [ Node{
             rootLabel = ForallLeft (Predicate "P" []) (Var "a")
           , subForest = [ Node{
             rootLabel = ExistsRight (Predicate "P" []) (Var "a")
           , subForest = [ Node{
             rootLabel = AndRight (Predicate "P" [])
           , subForest = 
              [ Node{rootLabel = Axiom, subForest = []}
              , Node{rootLabel = Axiom, subForest = []}]}]}]}]}]}]}

tree3 = Node{ 
             rootLabel = ImplicationRight (Predicate "P" [])
           , subForest = [ Node{ 
             rootLabel = NotLeft $ Predicate "P" []
           , subForest = [ Node{
             rootLabel = ForallRight (Predicate "P" []) (Var "a") 
           , subForest = [ Node{
             rootLabel = ForallLeft (Predicate "P" []) (Var "a")
           , subForest = [ Node{
             rootLabel = ExistsRight (Predicate "P" []) (Var "a")
           , subForest = [ Node{
             rootLabel = AndRight (Predicate "P" [])
           , subForest = 
              [ Node{rootLabel = Axiom, subForest = []}
              , Node{rootLabel = Axiom, subForest = []}]}]}]}]}]}]}

tree4 = Node{ 
             rootLabel = ExistsLeft (Predicate "P" []) (Var "a")
           , subForest = [ Node{ 
             rootLabel = NotRight (Predicate "P" [])
           , subForest = [ Node{
             rootLabel = ForallLeft (Predicate "P" []) (Var "a")
           , subForest = [ Node{
             rootLabel = ImplicationLeft $ Predicate "P" []
           , subForest = [ Node{
                rootLabel = Axiom
              , subForest = []}, Node{
                rootLabel = ExistsRight (Predicate "P" []) (Var "a")
              , subForest = [ Node{
                rootLabel = OrRight (Predicate "P" [])
              , subForest = 
                 [ Node{rootLabel = Axiom, subForest = []}]}]}]}]}]}]}
tree5 = Node{ 
             rootLabel = ExistsLeft (Predicate "P" []) (Var "a")
           , subForest = [ Node{ 
             rootLabel = NotLeft (Predicate "P" [])
           , subForest = [ Node{
             rootLabel = ImplicationLeft $ Predicate "P" []
           , subForest = [ Node{
                rootLabel = ForallLeft (Predicate "P" []) (Var "a")
              , subForest = [ Node{
                rootLabel = ExistsRight (Predicate "P" []) (Var "a")
              , subForest = [ Node{
                rootLabel = OrRight (Predicate "P" [])
              , subForest = 
                 [ Node{rootLabel = Axiom, subForest = []}]}]}]}, Node{
                rootLabel = Axiom
              , subForest = []}]}]}]}








-- EF-Games
--

a0 = Structure{ stUniverse = ["$x_1$","$x_2$","$x_3$","$x_4$","$x_5$"]
              , stRelationArity = M.fromList [("E", 2)]
              , stRelations = M.fromList [("E", S.fromList [["$x_1$","$x_2$"], ["$x_1$","$x_4$"], ["$x_1$","$x_5$"], ["$x_2$","$x_4$"], ["$x_2$","$x_3$"]])]
              }

b0 = Structure{ stUniverse = ["$w_1$","$w_2$","$w_3$","$w_4$","$w_5$","$w_6$"]
              , stRelationArity = M.fromList [("E", 2)]
              , stRelations = M.fromList [("E", S.fromList [["$w_1$","$w_2$"], ["$w_2$","$w_6$"], ["$w_3$","$w_4$"], ["$w_3$","$w_5$"], ["$w_4$","$w_5$"]])]
              }

a1 = Structure{ stUniverse = ["$x_1$","$x_2$","$x_3$","$x_4$","$x_5$"]
              , stRelationArity = M.fromList [("E", 2)]
              , stRelations = M.fromList [("E", S.fromList [["$x_1$","$x_2$"], ["$x_2$","$x_1$"], ["$x_4$","$x_1$"], ["$x_1$","$x_4$"]])]
              }

tikzA1 = Tikz
  { tzPosition = M.fromList [ ("$x_1$", "(1,0)")
                            , ("$x_2$", "(0,1)")
                            , ("$x_3$", "(2,1)")
                            , ("$x_4$", "(0.5,2)")
                            , ("$x_5$", "(1.5,2)")
                            ]
  , tzLabel = M.fromList [ ("$x_1$", "below")
                         , ("$x_2$", "left")
                         , ("$x_3$", "right")
                         , ("$x_4$", "left")
                         , ("$x_5$", "right")
                         ]
  , tzProperties = M.fromList [(x, ["draw", "circle", "fill"]) | x <- stUniverse a1]
  }

b1 = Structure{ stUniverse = ["$w_1$","$w_2$","$w_3$","$w_4$","$w_5$","$w_6$"]
              , stRelationArity = M.fromList [("E", 2)]
              , stRelations = M.fromList [("E", S.fromList [["$w_1$","$w_2$"], ["$w_2$","$w_1$"], ["$w_6$","$w_2$"], ["$w_2$","$w_6$"]])]
              }


tikzB1 = Tikz
  { tzPosition = M.fromList $ zip (stUniverse b1)
                               [ "(1,0)"
                               , "(0,1)"
                               , "(2,1)"
                               , "(0,2)"
                               , "(2,2)"
                               , "(1,3)"
                               ]
  , tzLabel = M.fromList $ zip (stUniverse b1)
                               [ "below"
                               , "left"
                               , "right"
                               , "left"
                               , "right"
                               , "above"
                               ]
  , tzProperties = M.fromList [(x, ["draw", "circle", "fill"]) | x <- stUniverse b1]
  }

strategy0 = 
  Node
  { rootLabel = (S.singleton "$x_1$", False)
  , subForest = 
    [ Node
      { rootLabel = (S.fromList ["$w_3$","$w_4$","$w_5$"], True)
      , subForest = 
        [ Node
          { rootLabel = (S.singleton "$x_5$", False)
          , subForest =
            [ Node
              { rootLabel = (S.fromList ["$w_1$","$w_2$","$w_3$","$w_4$","$w_5$","$w_6$"], True)
              , subForest = 
                [ Node
                  { rootLabel = (S.singleton "$x_4$", False)
                  , subForest = [Node{rootLabel = (S.fromList ["$w_1$","$w_2$","$w_3$","$w_4$","$w_5$","$w_6$"], True), subForest = []}]
                  }
                ]
              }
            ]
          }
        ]
      }
    , Node
      { rootLabel = (S.fromList ["$w_1$","$w_2$","$w_6$"], True)
      , subForest = 
        [ Node
          { rootLabel = (S.singleton "$x_4$", False)
          , subForest = 
            [ Node
              { rootLabel = (S.fromList ["$w_1$","$w_2$","$w_3$","$w_4$","$w_5$","$w_6$"], True)
              , subForest = 
                [ Node
                  { rootLabel = (S.singleton "$x_2$", False)
                  , subForest = [Node{rootLabel = (S.fromList ["$w_1$","$w_2$","$w_3$","$w_4$","$w_5$","$w_6$"], True), subForest = []}]
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
