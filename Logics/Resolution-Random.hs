module Main where

import Logics
import System.Directory
import System.FilePath
import System.Process
import System.Random
import System.IO
import Data.Time
import Data.Time.Format.ISO8601
import Control.Concurrent
import Control.Monad
import Control.Monad.Trans.State
import qualified Data.Map as M

n = length digraphs
steps = 7
vars = map (map Var)
  [ ["A", "B", "C", "D", "E"]
  , ["X", "Y", "W", "Z"]
  , ["U", "V", "W", "X"]
  , ["P", "Q", "R", "S", "T"]
  ]

d0 = 
  [ (0, (1,2))
  , (1, (3,4)) 
  , (3, (5,6))
  , (2, (7,8))
  , (7, (9,10))
  , (8, (11,12))
  , (11, (13,14))
  , (12, (15,16))
  ]
e0 = 
  [ (0, (3,0))
  , (1, (1,-1))
  , (2, (4,-1))
  , (3, (2,-2))
  , (4, (0,-4))
  , (5, (1,-4))
  , (6, (2,-4))
  , (7, (4,-2))
  , (8, (6,-2))
  , (9, (3,-4))
  , (10, (5,-4))
  , (11, (6,-3))
  , (12, (8,-3))
  , (13, (6,-4))
  , (14, (7,-4))
  , (15, (8,-4))
  , (16, (9,-4))
  ]
d1 = 
  [ (0, (1,2))
  , (1, (3,4)) 
  , (3, (5,6))
  , (2, (15,16))
  , (7, (13,14))
  , (5, (8,9))
  , (6, (10,11))
  , (4, (12,7))
  ]
e1 = 
  [ (0, (5,0))
  , (1, (3,-1))
  , (2, (7.5,-1))
  , (3, (1.5,-2))
  , (4, (4.5,-2))
  , (5, (0.5,-3))
  , (6, (2.5,-3))
  , (7, (5.5,-3))
  , (8, (0,-4))
  , (9, (1,-4))
  , (10, (2,-4))
  , (11, (3,-4))
  , (12, (4,-4))
  , (13, (5,-4))
  , (14, (6,-4))
  , (15, (7,-4))
  , (16, (8,-4))
  ]

digraphs = map M.fromList
  [ d0
  , d1
  ]

embeddings = map M.fromList
  [ e0
  , e1
  ]

main = do
  now <- getCurrentTime
  let dir = "resolution" </> (iso8601Show now)
  createDirectoryIfMissing True dir
  tids <- forM [ (i,j) | i <- [1..length digraphs] , j <- [1..length vars]] $ \(i,j) -> forkIOMVar $ do
    let d = digraphs !! (i - 1)
        vars' = vars !! (j - 1)
    gen <- newStdGen
    res <- evalStateT (randomResolutionFromDigraph d 0 vars') gen
    let derivationTree = M.fromList $ map (\(v,(l,r)) -> (res M.! v, (res M.! l, res M.! r))) $
                                          M.assocs d
        embedding = (M.mapKeys (\v -> res M.! v) $ embeddings !! (i - 1))
        tikz = resolutionToTikzEmbedded derivationTree embedding
        tikzAnswer = resolutionToTikzEmbeddedAnswer derivationTree embedding
    writeFile (dir </> (show i) ++ "-" ++ show j <.> "tex") $ tikzpicture tikz
    writeFile (dir </> (show i) ++ "-" ++ show j ++ "-answer" <.> "tex") $ tikzpicture tikzAnswer
  writeFile (dir </> "resolution.tex") $
    header
    ++ concat (["\\input{" ++ show i ++ "-" ++ show j ++ ".tex}\\newpage\n\n\\input{" ++ show i ++ "-" ++ show j ++ "-answer.tex}\\newpage\n\n" | i <- [1..length digraphs], j <- [1..length vars]])
    ++ footer
  writeFile (dir </> "build.ninja") $ 
    "rule pdf\n\
    \  command = latexmk -pdf $in\n\
    \rule svg\n\
    \  command = inkscape --export-plain-svg=$out -D --pdf-poppler --pdf-page=$page $in\n\
    \build resolution.pdf : pdf resolution.tex\n" ++ concat 
    [ "build resolution-" ++ show i ++ "-" ++ show j ++ ".svg: svg resolution.pdf\n\
      \  page=" ++ (show $ 2*((i - 1) * (length vars) + j)) ++ "\n"
    | i <- [1 .. length digraphs]
    , j <- [1 .. length vars]
    ]
  mapM_ takeMVar tids
  (_, _, _, ninja) <- createProcess (proc "ninja" []){cwd = Just dir}
  waitForProcess ninja

tikzpicture tex =
  "\\begin{tikzpicture}[>=stealth', auto, node distance=5em, semithick,xscale=2.5,yscale=1.3]\n"
  ++ tex
  ++ "\n\\end{tikzpicture}"

header = 
  "\\documentclass[tikz,border=0.5cm]{standalone}\n\
  \\\usepackage{hyperref}\n\
  \\\usetikzlibrary{calc,through,arrows,automata,decorations.pathmorphing,decorations.pathreplacing,shapes,backgrounds,fit,positioning,shapes.geometric,patterns}\n\
  \\\hypersetup{colorlinks=true,linkcolor=black}\n\
  \\\DeclareUnicodeCharacter{2228}{\\vee}\n\
  \\\DeclareUnicodeCharacter{2227}{\\wedge}\n\
  \\\DeclareUnicodeCharacter{00AC}{\\ensuremath{\\neg}}\n\
  \\\begin{document}\n\
  \\\tikzset{\n\
  \answer/.style={\n\
  \rectangle,\n\
  \draw,\n\
  \minimum size=8pt\n\
  \}\n\
  \}\n"

footer = "\n\\end{document}"

forkIOMVar :: IO () -> IO (MVar ())
forkIOMVar io = do
  mvar <- newEmptyMVar
  forkFinally io (\_ -> putMVar mvar ())
  return mvar

