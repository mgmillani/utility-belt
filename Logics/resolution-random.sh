#!/usr/bin/env nix-shell
#!nix-shell -i fish

ghc -threaded Resolution-Random.hs ; and ./Resolution-Random +RTS -N
