module EFGames where

import Control.Applicative
import Control.Monad
import Control.Monad (liftM, liftM2, liftM3, mplus, replicateM, forM)
import Control.Monad.Trans.State
import Data.Char
import Data.Either
import Data.List
import Data.Maybe
import Data.Tree
import qualified Data.Map as M
import qualified Data.Set as S
import System.Random

data Structure a = Structure
  { stUniverse      :: [a]
  , stRelations     :: M.Map String (S.Set [a])
  , stRelationArity :: M.Map String Int
  }
  deriving (Show)

data Tikz a = Tikz
  { tzPosition   :: M.Map a String
  , tzLabel      :: M.Map a String
  , tzProperties :: M.Map a [String]
  }

duplicatorWins m a b = duplicatorWins' m M.empty M.empty
  where
    duplicatorWins' 0 f fi = isPartialIso f fi a b
    duplicatorWins' m f fi
      | isPartialIso f fi a b = 
        (all (\x -> if x `M.member` f then
                       True
                    else 
                      any (\y -> if y `M.member` fi then
                                    False
                                 else
                                    duplicatorWins' (m-1) (M.insert x y f) (M.insert y x fi)) $ stUniverse b) $ stUniverse a)
        &&
        (all (\x -> if x `M.member` fi then
                             True
                          else 
                            any (\y -> if y `M.member` f then
                                          False
                                       else
                                          duplicatorWins' (m-1) (M.insert y x f) (M.insert x y fi)) $ stUniverse a) $ stUniverse b)
      | otherwise = False

isSpoilerStrategy m strategy a b = isSpoilerStrategy' M.empty M.empty m strategy
  where
    isSpoilerStrategy' f fi 0 _ = not $ isPartialIso f fi a b
    isSpoilerStrategy' f fi m strategy = 
      case rootLabel strategy of
        (xs, False) -> all (\x -> all (\n@(Node{rootLabel = (ys, _)}) ->
                                           all (\y ->
                                            isSpoilerStrategy' (M.insert x y f) (M.insert y x fi) (m - 1) $ head $ subForest n) $ filter (\y -> not $ y `S.member` ys) $ S.toList ys
                                      )
                                      $ subForest strategy )
                           $ S.toList xs
        (xs, True) -> all (\x -> all (\n@(Node{rootLabel = (ys, _)}) ->
                                           all (\y ->
                                             isSpoilerStrategy' (M.insert y x f) (M.insert x y fi) (m - 1) $ head $ subForest n) $ filter (\y -> not $ y `S.member` ys) $ S.toList ys
                                      )
                                      $ subForest strategy )
                           $ S.toList xs

spoilerHasBlindStrategy m a b = 
  (any (\a' -> not $ hasTotalPartialIso a' b
      ) $ substructures m a)
  ||
  (any (\b' -> not $ hasTotalPartialIso b' a
      ) $ substructures m b)

substructures k a = map (inducedSubstructure a) $ kSubsets k $ stUniverse a

isPartialIso f fi a b = null $ 
  [ t
  | (r,ts) <- M.assocs $ stRelations a
  , t <- filter (\l -> all (\x -> M.member x f) l) $ S.toList ts
  , not $ S.member (map (\x -> f M.! x) t) $ (stRelations b) M.! r
  ] ++
  [ t
  | (r,ts) <- M.assocs $ stRelations b
  , t <- filter (\l -> all (\x -> M.member x fi) l) $ S.toList ts
  , not $ S.member (map (\x -> fi M.! x) t) $ (stRelations a) M.! r
  ]

hasTotalPartialIso a b = hasTPIso M.empty M.empty (stUniverse a)
  where
    hasTPIso f fi [] = isPartialIso f fi a b
    hasTPIso f fi (x:xs)
      | not $ isPartialIso f fi a b = False
      | otherwise = any (\y -> hasTPIso (M.insert x y f) (M.insert y x fi) xs) $ filter (\y -> not $ y `M.member` f) $ stUniverse b

inducedSubstructure a xs = a{stUniverse = xs, stRelations = M.map (S.filter (all (`elem` xs))) $ stRelations a }

generateStructurePair 0 _ _ _ _ = return $ Left "Failed :("
generateStructurePair tries p m a0 b0 = do
  a' <- randomRelations a0 m
  b' <- randomRelations b0 m
  if p a' b' then
    return $ Right (a', b')
  else
    generateStructurePair (tries - 1) p m a0 b0

randomRelations st 0 = return $ st
randomRelations st m = do
  r <- fmap head $ choose (M.keys $ stRelations st) 1
  let arity = (stRelationArity st) M.! r
  t <- choose (stUniverse st) arity
  randomRelations (st{stRelations = M.insertWith (S.union) r (S.singleton t) $ stRelations st}) (m - 1)

prettyTikz tikz = intercalate "\n" $ 
  [ "\\node (V" ++ show i ++ ") at " ++ pos ++ "[" ++ (intercalate ", " $ "draw" : properties) ++ ", label=" ++ label ++ "] {};"
  | ((x, pos), i) <- zip (M.assocs $ tzPosition tikz) [1..]
  , let label = ((tzLabel tikz) M.! x) ++ ":{" ++ x ++ "}"
  , let properties = (tzProperties tikz) M.! x
  ]
  

kSubsets k xs = kSubsets' k (length xs) xs
  where
    kSubsets' 0 _ _ = [[]]
    kSubsets' _ _ [] = [[]]
    kSubsets' k n (x:xs)
      | k == n = [x:xs]
      | k < n = 
        (map (x:) (kSubsets' (k - 1) (n-1) xs)) ++
        (kSubsets' k (n-1) xs)
      | otherwise = [[]]

rollRange mn mx = do
  gen <- get
  let (x, gen') = randomR (mn,mx) gen
  put gen'
  return x

choose n 0 = return []
choose n k = do
  i <- rollRange 0 (length n - 1)
  let x = n !! i
  liftM (x : ) (choose (delete x n) (k-1))
