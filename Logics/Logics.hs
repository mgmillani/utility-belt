module Logics where

import Control.Monad (liftM, liftM2, liftM3, mplus, replicateM, forM)
import Control.Applicative
import Text.ParserCombinators.Parsec hiding ((<|>), many, optional)
import Text.Parsec.Char hiding (crlf)
import Data.Char
import Data.Maybe
import Data.List
import qualified Data.Map as M
import qualified Data.Set as S
import System.Random
import Control.Monad.Trans.State

data PLFormula =
    Var String
  | Implication PLFormula PLFormula
  | Biimplication PLFormula PLFormula
  | And PLFormula PLFormula
  | Or PLFormula PLFormula
  | Not PLFormula
  | Bottom
  | Top
  deriving (Eq, Ord)

instance Show PLFormula where
  show (Var str) = str
  show (Implication phi psi)   = "(" ++ (show phi) ++ " → " ++ (show psi) ++ ")"
  show (Biimplication phi psi) = "(" ++ (show phi) ++ " ↔ " ++ (show psi) ++ ")"
  show (And phi psi) = "(" ++ (show phi) ++ " ∧ " ++ (show psi) ++ ")"
  show (Or phi psi)  = "(" ++ (show phi) ++ " ∨ " ++ (show psi) ++ ")"
  show (Not phi)     = "¬" ++ show phi
  show Top     = "⊤"
  show Bottom  = "⊥"

parseFormula :: String -> Either ParseError PLFormula
parseFormula = parse (p_formula <* eof) "(input)" . filter (not . isSpace)

parseLatexFormula str = parseFormula $ replaceLatex str
  where
    replaceLatex ('\\':s) = 
      case span (/=' ') s of
        ("land", rs) -> '&' : replaceLatex rs
        ("lor", rs) -> '∨' : replaceLatex rs
        ("rightarrow", rs) -> '→' : replaceLatex rs
        ("leftrightarrow", rs) -> '↔' : replaceLatex rs
        ("neg", rs) -> '¬' : replaceLatex rs
        ("top", rs) -> '⊤' : replaceLatex rs
        ("bot", rs) -> '⊥' : replaceLatex rs
        (x, rs) -> ('\\' : x) ++ replaceLatex rs
    replaceLatex (c:cs) = c : replaceLatex cs
    replaceLatex [] = []

randomUnsatisfiableCNF vars m = do
  phi <- randomCNF vars m
  if isJust $ satisfiable phi then randomUnsatisfiableCNF vars m else return phi

--randomResolutionTikz n m = do
--  gen <- newStdGen
--  return $ resolutionToTikz $ simplifyResolution $ fromJust $ unsatisfiable $ cnfToClauseSet $
--           evalState (randomUnsatisfiableCNF
--                         [ Var $ "X_{" ++ show x ++ "}" | x <- [1..n]] m)
--                     gen

randomResolutionTikz :: Int -> Int -> IO String
randomResolutionTikz steps vars = do
  gen <- newStdGen
  return $ resolutionToTikz $ evalState (randomUnsatisfiableResolution steps vars) gen

randomResolutionTikzPlanar :: Int -> [PLFormula] -> IO String
randomResolutionTikzPlanar steps vars = do
  gen <- newStdGen
  return $ uncurry resolutionToTikzEmbedded $ evalState (randomResolutionPlanar steps vars 5) gen

eval :: M.Map String Bool -> PLFormula -> Maybe Bool
eval m (Var v) = M.lookup v m
eval m (Not phi) = fmap not $ eval m phi
eval m (And phi psi)
  | (phiV == Just False) || (psiV == Just False) = Just False
  | otherwise = liftM2 (&&) phiV psiV
  where
    phiV = eval m phi
    psiV = eval m psi
eval m (Or phi psi)
  | (phiV == Just True) || (psiV == Just True) = Just True
  | otherwise = liftM2 (||) phiV psiV
  where
    phiV = eval m phi
    psiV = eval m psi
eval m (Implication phi psi)
  | (phiV == Just False) || (psiV == Just True) = Just True
  | otherwise = liftM2 (\x y -> not x || y) phiV psiV
  where
    phiV = eval m phi
    psiV = eval m psi
eval m (Biimplication phi psi) = liftM2 (==) (eval m phi) (eval m psi)
eval m Bottom = Just False
eval m Top = Just True

satisfiable phi = satisfiable' (S.toList $ variables phi) phi M.empty
  where
    satisfiable' vars phi assignment
      | phiEval == Just True = Just assignment
      | phiEval == Just False = Nothing
      | otherwise = satisfiable' (tail vars) phi assignmentT `mplus` satisfiable' (tail vars) phi assignmentF
      where
        phiEval = eval assignment phi
        x = head vars
        assignmentT = M.insert x True assignment
        assignmentF = M.insert x False assignment

equivalent psi phi = isNothing $ satisfiable (Not (phi `Biimplication` psi))

variables (Var x) = S.singleton x
variables (Implication phi psi) = S.union (variables phi) (variables psi)
variables (Biimplication phi psi) = S.union (variables phi) (variables psi)
variables (And phi psi) = S.union (variables phi) (variables psi)
variables (Or phi psi) = S.union (variables phi) (variables psi)
variables (Not phi) = variables phi
variables _ = S.empty

unsatisfiable cnf = unsatisfiable' (S.fromList cnf) (S.fromList cnf) M.empty
  where
    unsatisfiable' clauses new parents
      | null newClauses = Nothing
      | S.empty `S.member` new' = Just parents'
      | otherwise = unsatisfiable' (clauses `S.union` new) new' parents'
      where
        newClauses = [ (c', c, d)
                     | c <- S.toList new
                     , d <- S.toList $ S.union new clauses
                     , c' <- resolve c d
                     , not $ c' `S.member` clauses
                     ]
        new' = S.fromList $ map (\(c,_,_) -> c) newClauses
        parents' = foldr (\(c',c, d) m-> M.insertWith (\_ o -> o) c' (c,d) m) parents newClauses

resolve c1 c2 = [ (S.delete l c1) `S.union` (S.delete l' c2) | l <- S.toList c1, let l' =  complement l, l' `S.member` c2 ]
  where
    complement (Var x) = (Not (Var x))
    complement (Not phi) = phi

simplifyResolution deriv = M.restrictKeys deriv (S.fromList $ simplify' [S.empty])
  where
    simplify' active
      | null active = []
      | otherwise = active ++ simplify' active'
      where
        active' = concat [ [f1, f2] | phi <- active, let (f1, f2) = deriv M.! phi, phi `M.member` deriv]

clauseSetToFormula phi
  | null phi = Top
  | otherwise = foldr1 And $ map clauseToFormula phi

clauseToFormula phi
  | null phi = Bottom
  | otherwise = foldr1 Or phi

resolutionToTikz deriv = intercalate "\n" $
  [ "\\node (v" ++ name ++ ") at (" ++ x ++ ", " ++ y ++ ") {" ++ showClause phi ++ "};"
  | (h,phis) <- M.assocs layers
  , (phi,i) <- zip phis [1..]
  , let x = show $ (i - (length phis `div` 2))*2
  , let y = show (-h)
  , let name = nid M.! phi
  ] ++ 
  [ "\\draw (v" ++ n1 ++ ") edge (v" ++ n2 ++ ");" ++
    "\\draw (v" ++ n1 ++ ") edge (v" ++ n3 ++ ");"
  | (phi, (c,d)) <- M.assocs deriv
  , let n1 = nid M.! phi
  , let n2 = nid M.! c
  , let n3 = nid M.! d
  ]
  where
    children = foldr (\(k,v) m -> M.insertWith (++) k v m) M.empty $ concat
      [ [(c1, [c]), (c2,[c])]
      | (c, (c1,c2)) <- M.assocs deriv
      ]
    nid = M.fromList $ zip (M.keys depths) $ map show [0..]
    depths = foldr getDepth M.empty $ M.keys children
    getDepth phi m
      | phi `M.member` m = m
      | otherwise = setDepth 0 phi m
    setDepth d phi m
      | fmap (< d) (M.lookup phi m) /= Just False = M.insert phi d $
          if phi `M.member` children then
            foldr (setDepth (d+1)) m $ children M.! phi
          else
            m
      | otherwise = m
    layers = foldr (\(phi, h) m -> M.insertWith (++) h [phi] m) M.empty $ M.assocs depths

resolutionToTikzEmbedded deriv embedding = intercalate "\n" $
  [ "\\node (v" ++ name ++ ") at (" ++ show x ++ ", " ++ show (-y) ++ ") {" ++ showClause phi ++ "};"
  | (phi, (x,y)) <- M.assocs embedding
  , let name = nid M.! phi
  ] ++ 
  [ "\\draw (v" ++ n1 ++ ") edge (v" ++ n2 ++ ");" ++
    "\\draw (v" ++ n1 ++ ") edge (v" ++ n3 ++ ");"
  | (phi, (c,d)) <- M.assocs deriv
  , let n1 = nid M.! phi
  , let n2 = nid M.! c
  , let n3 = nid M.! d
  ]
  where
    nid = M.fromList $ zip (M.keys embedding) $ map show [0..]

resolutionToTikzEmbeddedAnswer deriv embedding = intercalate "\n" $
  [ "\\node (v" ++ name ++ ") at (" ++ show x ++ ", " ++ show (-y) ++ ") {" ++ showClause phi ++ "};"
  | (phi, (x,y)) <- M.assocs embedding
  , let name = nid M.! phi
  , not $ phi `M.member` deriv
  ] ++ 
  [ "\\node[answer] (v" ++ name ++ ") at (" ++ show x ++ ", " ++ show (-y) ++ ") {}; % {" ++ showClause phi ++ "}"
  | (phi, (x,y)) <- M.assocs embedding
  , let name = nid M.! phi
  , phi `M.member` deriv
  ] ++ 
  [ "\\draw (v" ++ n1 ++ ") edge (v" ++ n2 ++ ");" ++
    "\\draw (v" ++ n1 ++ ") edge (v" ++ n3 ++ ");"
  | (phi, (c,d)) <- M.assocs deriv
  , let n1 = nid M.! phi
  , let n2 = nid M.! c
  , let n3 = nid M.! d
  ]
  where
    nid = M.fromList $ zip (M.keys embedding) $ map show [0..]

randomUnsatisfiableResolution 0 _ = return M.empty
randomUnsatisfiableResolution steps maxVar = do
  left <- rollRange 0 (min (steps - 1) maxVar)
  let right = steps - left - 1
      varL = Var $ "X_" ++ show maxVar
      varR = Not varL
  resL <- randomUnsatisfiableResolution left (maxVar - 1) >>= addVar varL S.empty
  resR <- randomUnsatisfiableResolution right (maxVar - 1) >>= addVar varR S.empty
  return $ M.insert S.empty (S.singleton varL, S.singleton varR) $ M.union resL resR
  where
    addVar v root res
      | not $ root `M.member` res = return res
      | otherwise = do
        let (c,d) = res M.! root
        choice <- rollRange 0 2
        case (choice :: Int) of
          0 -> do
            res' <- addVar v c res
            return $ M.insert (S.insert v root) (S.insert v c, d) $ M.delete root res'
          1 -> do
            res' <- addVar v d res
            return $ M.insert (S.insert v root) (c, S.insert v d) $ M.delete root res'
          2 -> do
            res' <- addVar v c res >>= addVar v d
            return $ M.insert (S.insert v root) (S.insert v c, S.insert v d) $ M.delete root res'

randomResolutionPlanar depth vars width = randomResolution' depth vars 0 width
  where
    randomResolution' 0 _ center _ = return (M.empty, M.singleton S.empty (center,0))
    randomResolution' depth vars center width = do
      left <- rollRange 0 (min (depth - 1) $ length vars)
      let right = depth - left - 1
      let w' = width * ((fromIntegral $ left + 1) / (fromIntegral $ left + right + 2))
      varI <- rollRange 0 (length vars - 1)
      let varL = vars !! varI
          varR = Not varL
      (resL, embL) <- randomResolution' left (filter (/= varL) vars) (center - w') w' >>= addVar varL S.empty
      (resR, embR) <- randomResolution' right (filter (/= varL) vars) (center + w') w' >>= addVar varR S.empty
      return $ (M.insert S.empty (S.singleton varL, S.singleton varR) $ M.union resL resR, M.insert S.empty (center, fromIntegral depth) $ M.union embL embR)
      where
        addVar v root (res, emb)
          | not $ root `M.member` res = return
                ( res
                , M.insert (S.insert v root) (emb M.! root) $ M.delete root emb
                )
          | otherwise = do
            let (c,d) = res M.! root
                (x,y) = emb M.! root
            choice <- rollRange 0 2
            case (choice :: Int) of
              0 -> do
                (res', emb') <- addVar v c (res, emb)
                return $ ( M.insert (S.insert v root) (S.insert v c, d) $ M.delete root res'
                         , M.insert (S.insert v root) (x,y) $ M.delete root emb'
                         )
              1 -> do
                (res', emb') <- addVar v d (res, emb)
                return $ ( M.insert (S.insert v root) (c, S.insert v d) $ M.delete root res'
                         , M.insert (S.insert v root) (x,y) $ M.delete root emb'
                         )
              2 -> do
                (res', emb') <- addVar v c (res, emb) >>= addVar v d
                return $ ( M.insert (S.insert v root) (S.insert v c, S.insert v d) $ M.delete root res'
                         , M.insert (S.insert v root) (x,y) $ M.delete root emb'
                         ) 

randomResolutionFromDigraph dag root vars
  | not $ root `M.member` dag = return M.empty
  | otherwise = do
    let (l,r) = dag M.! root
    varI <- rollRange 0 (length vars - 1)
    let varL = vars !! varI
        varR = Not varL
        vars' = filter (/= varL) vars
    resL <- randomResolutionFromDigraph dag l vars'
    resR <- randomResolutionFromDigraph dag r vars'
    choice <- rollRange 0 1
    fmap (M.insert root S.empty) $ case (choice :: Int) of
      0 -> liftM2 M.union (addVar varL l resL) (addVar varR r resR) 
      1 -> liftM2 M.union (addVar varL r resR) (addVar varR l resL)
    where
      addVar v root res
        | not $ root `M.member` dag = return $ M.insertWith S.union root (S.singleton v) res
        | otherwise = do
            let (l,r) = dag M.! root
            choice <- rollRange 0 2
            fmap (M.insertWith (S.union) root (S.singleton v)) $ case (choice :: Int) of
              0 ->  addVar v l res
              1 ->  addVar v r res
              2 ->  addVar v l res >>= addVar v r

showClause phi = "$\\{" ++ (intercalate "," $ map show $ S.toList phi) ++ "\\}$"

p_formula :: CharParser st PLFormula
p_formula =
  (binaryOpOrFormula <$> (p_atom <|> p_brace_formula <|> p_not) <*> (optional $ liftM2 (,) p_bin p_formula))

p_brace_formula :: CharParser st PLFormula
p_brace_formula = between (char '(') (char ')') p_formula

p_bin :: CharParser st String
p_bin = choice $ (map string) ["→", "->", "↔", "<->", "∧", "∨", "|", "&"]

binaryOpOrFormula phi (Just (opStr, psi)) = 
  let op = case opStr of
              "->" -> Implication
              "→"  -> Implication
              "∧"  -> And
              "&"  -> And
              "∨"  -> Or
              "|"  -> Or
              "<->" -> Biimplication
              "↔"  -> Biimplication
  in op phi psi
binaryOpOrFormula phi Nothing = phi

p_not :: CharParser st PLFormula
p_not = (choice $ (map string) ["¬", "!"]) *> (Not <$> (p_brace_formula <|> p_atom))

p_var :: CharParser st PLFormula
p_var = Var <$> ((:) <$> letter <*> many varChar)
  where
    varChar = letter <|> digit <|> oneOf "_^,{}"

p_top_bottom :: CharParser st PLFormula
p_top_bottom = ((choice $ (map string) ["⊤", "1"]) *> return Top) <|> ((choice $ (map string) ["⊥", "0"]) *> return Bottom)

p_atom :: CharParser st PLFormula
p_atom = p_var <|> p_top_bottom

rollLiteral v = do
  gen <- get
  let (x,gen') = random gen
  put gen'
  return $ if x then v else Not v

rollRange mn mx = do
  gen <- get
  let (x, gen') = randomR (mn,mx) gen
  put gen'
  return x

choose n 0 = return []
choose n k = do
  i <- rollRange 0 (length n - 1)
  let x = n !! i
  liftM (x : ) (choose (delete x n) (k-1))

randomCNFClause vars minSize = do
  size <- rollRange minSize (length vars)
  vars' <- choose vars size
  phiL <- forM vars' (\v -> rollLiteral v)
  if null phiL then
    randomCNFClause vars minSize
  else
    return $ foldr1 Or phiL

randomCNF vars m = replicateM m (randomCNFClause vars 2) >>= (return . foldr1 And)

randomFormula vars 1 = do
  fmap head $ choose (Top : Bottom : map Var vars) 1
randomFormula vars height = do
  phi1 <- randomFormula vars (height - 1)
  phi2 <- randomFormula vars (height - 1)
  fmap head $ choose [Not phi1, phi1 `And` phi2, phi1 `Or` phi2, phi1 `Implication` phi2, phi1 `Biimplication` phi2 ] 1


cnfToClauseSet (And phi psi) = (cnfToClauseSet phi) ++ (cnfToClauseSet psi)
cnfToClauseSet (Or phi psi) = [(head $ cnfToClauseSet phi) `S.union` (head $ cnfToClauseSet psi)]
cnfToClauseSet phi@(Var x) = [S.fromList [ phi ]]
cnfToClauseSet phi@(Not _) = [S.fromList [ phi ]]

