module FO where

import Control.Applicative
import Control.Monad
import Control.Monad (liftM, liftM2, liftM3, mplus, replicateM, forM)
import Control.Monad.Trans.State
import Data.Char
import Data.Either
import Data.List
import Data.Maybe
import Data.Tree
import qualified Data.Map as M
import qualified Data.Set as S
import System.Random
import Text.Parsec.Char hiding (crlf)
import Text.ParserCombinators.Parsec hiding ((<|>), many, optional)

data FOFormula =
    Implication FOFormula FOFormula
  | Biimplication FOFormula FOFormula
  | And FOFormula FOFormula
  | Or FOFormula FOFormula
  | Not FOFormula
  | Exists String FOFormula
  | Forall String FOFormula
  | Predicate String [Term]
  deriving (Eq, Ord)

data Term = 
    Var String
  | Function String [Term]
  deriving (Eq, Ord)

instance Show FOFormula where
  show (Implication phi psi)   = "(" ++ (show phi) ++ " → " ++ (show psi) ++ ")"
  show (Biimplication phi psi) = "(" ++ (show phi) ++ " ↔ " ++ (show psi) ++ ")"
  show (And phi psi) = "(" ++ (show phi) ++ " ∧ " ++ (show psi) ++ ")"
  show (Or phi psi)  = "(" ++ (show phi) ++ " ∨ " ++ (show psi) ++ ")"
  show (Not phi)     = "¬" ++ show phi
  show (Exists x phi) = "∃ " ++ x ++ " " ++ show phi ++ ""
  show (Forall x phi) = "∀ " ++ x ++ " " ++ show phi ++ ""
  show (Predicate r ts)
    | null ts = r
    | r == "=" && (length $ take 3 ts) == 2 = 
      let [l,r] = ts in (show l) ++ " = " ++ (show r)
    | otherwise = r ++ "(" ++ (intercalate ", " $ map show ts) ++ ")"

instance Show Term where
  show (Var v) = v
  show (Function f ts) = f ++ "(" ++ (intercalate "," $ map show ts) ++ ")"

data SCRule = 
    NotLeft FOFormula
  | AndLeft FOFormula
  | OrLeft FOFormula
  | ImplicationLeft FOFormula
  | ExistsLeft FOFormula Term
  | ForallLeft FOFormula Term
  | NotRight FOFormula
  | SubstitutionLeft FOFormula FOFormula Term Term
  | SubstitutionRight FOFormula FOFormula Term Term
  | EqualityLeft FOFormula Term Term
  | AndRight FOFormula
  | OrRight FOFormula
  | ImplicationRight FOFormula
  | ExistsRight FOFormula Term
  | ForallRight FOFormula Term
  | Axiom
  deriving (Eq, Ord)

instance Show SCRule where
  show (NotLeft phi)                    = "(¬ => )"
  show (AndLeft phi)                    = "(∧ => )"
  show (OrLeft phi)                     = "(∨ => )"
  show (ImplicationLeft phi)            = "(→ => )"
  show (ExistsLeft phi x)               = "(∃ => )"
  show (ForallLeft phi x)               = "(∀ => )"
  show (NotRight phi)                   = "( => ¬)"
  show (SubstitutionLeft phi psi x x')  = "(S => )"
  show (SubstitutionRight phi psi x x') = "( => S)"
  show (EqualityLeft phi x x')          = "(= => )"
  show (AndRight phi)                   = "( => ∧)"
  show (OrRight phi)                    = "( => ∨)"
  show (ImplicationRight phi)           = "( => →)"
  show (ExistsRight phi x)              = "( => ∃)"
  show (ForallRight phi x)              = "( => ∀)"
  show (Axiom)                          = "(Axiom)"

applyRule (lset, rset) (NotLeft (Not phi))
  | (Not phi) `S.member` lset = return [(lset, S.insert phi rset)]
  | otherwise = Left $ show (Not phi) ++ " is not on left side of " ++ (show (lset, rset))
applyRule (lset, rset) (AndLeft p@(phi `And` psi))
  | p `S.member` lset = return [(S.insert phi $ S.insert psi lset, rset)]
  | otherwise = Left $ show p ++ " is not on left side of " ++ (show (lset, rset))
applyRule (lset, rset) (ImplicationLeft p@(phi `Implication` psi))
  | p `S.member` lset = return [(lset, S.insert phi rset), (S.insert psi lset, rset)]
  | otherwise = Left $ show p ++ " is not on left side of " ++ (show (lset, rset))
applyRule (lset, rset) (OrLeft p@(phi `Or` psi))
  | p `S.member` lset = return [(S.insert phi lset, rset), (S.insert psi lset, rset)]
  | otherwise = Left $ show p ++ " is not on left side of " ++ (show (lset, rset))
applyRule (lset, rset) (ExistsLeft p@(Exists x phi) x')
  | not (p `S.member` lset) = Left $ show p ++ " is not on left side of " ++ (show (lset, rset))
  | S.member x' $ terms $ S.union lset rset = Left $ show x' ++ " appears on " ++ (show (lset, rset))
  | otherwise = return [(S.insert (replace (Var x) x' phi) lset, rset)]
applyRule (lset, rset) (ForallLeft p@(Forall x phi) x')
  | not (p `S.member` lset) = Left $ show p ++ " is not on left side of " ++ (show (lset, rset))
  | otherwise = return [(S.insert (replace (Var x) x' phi) lset, rset)]
--
applyRule (lset, rset) (NotRight (Not phi))
  | (Not phi) `S.member` rset = return [(S.insert phi lset, rset)]
  | otherwise = Left $ show (Not phi) ++ " is not on right side of " ++ (show (lset, rset))
applyRule (lset, rset) (AndRight p@(phi `And` psi))
  | p `S.member` rset = return [(lset, S.insert phi rset), (lset, S.insert psi rset)]
  | otherwise = Left $ show p ++ " is not on right side of " ++ (show (lset, rset))
applyRule (lset, rset) (ImplicationRight p@(phi `Implication` psi))
  | p `S.member` rset = return [(S.insert phi lset, S.insert psi rset)]
  | otherwise = Left $ show p ++ " is not on right side of " ++ (show (lset, rset))
applyRule (lset, rset) (OrRight p@(phi `Or` psi))
  | p `S.member` rset = return [(lset, S.insert phi $ S.insert psi rset)]
  | otherwise = Left $ show p ++ " is not on right side of " ++ (show (lset, rset))
applyRule (lset, rset) (ForallRight p@(Forall x phi) x')
  | not (p `S.member` rset) = Left $ show p ++ " is not on right side of " ++ (show (lset, rset))
  | S.member x' $ terms $ S.union lset rset = Left $ show x' ++ " appears on " ++ (show (lset, rset))
  | otherwise = return [(lset, S.insert (replace (Var x) x' phi) rset)]
applyRule (lset, rset) (ExistsRight p@(Exists x phi) x')
  | not (p `S.member` rset) = Left $ show p ++ " is not on right side of " ++ (show (lset, rset))
  | otherwise = return [(lset, S.insert (replace (Var x) x' phi) rset)]
applyRule (lset, rset) Axiom
  | S.null (S.intersection lset rset) = Left $ "Sequence " ++ (show (lset, rset)) ++ " is not an axiom."
  | otherwise = return []
applyRule sequent rule = Left $ "Cannot apply " ++ show rule ++ " to " ++ show sequent

expandTree steps@(Node{rootLabel = (step, keep), subForest = subSteps}) (lset, rset) = do
  sequents <- applyRule (lset, rset) step
  when (length subSteps /= length sequents) $ do
    Left $ "Failed applying " ++ (show step) ++ " to " ++ show (lset, rset)
  children <- mapM (uncurry expandTree) $ zip subSteps $ map (\(l,r) -> (S.intersection keep l, S.intersection keep r)) sequents
  return Node{rootLabel = ((S.intersection keep lset, S.intersection keep rset), step), subForest = children}

generateSequent baseFormulas tree = 
  map (\t -> fromRight Node{rootLabel = ((S.empty, S.empty), Axiom), subForest = []} $ expandTree (proofTree t) (fst3 $ rootLabel t)) $ generateSequent' baseFormulas tree
  where
    fst3 (x,_,_) = x
    proofTree (Node{rootLabel = (_,s,used), subForest = children}) =
      Node{rootLabel = (s, used), subForest = map proofTree children}

generateSequent' baseFormulas Node{rootLabel = rule, subForest = children} = 
  let subTrees' = map (generateSequent' baseFormulas) children
  in case rule of
      Axiom -> [Node{rootLabel = ((S.singleton axiom, S.singleton axiom), Axiom, S.singleton axiom), subForest = []} | axiom <- baseFormulas]

      (NotLeft _) ->
        let subTrees = concat subTrees' in
          [ Node{ rootLabel = ((l', r'), NotLeft $ Not phi, S.insert (Not phi) used )
                , subForest = [seq']}
          | seq' <- subTrees
          , let ((l,r), _, used) = rootLabel seq'
          , phi <- S.toList r
          , let l' = S.insert (Not phi) l
          , let r' = S.delete phi r
          , S.null $ S.intersection l' r'
          ]
      (AndLeft _) ->
        let subTrees = concat subTrees'
        in
          [ Node{ rootLabel = ((l', r'), AndLeft $ And phi psi, S.insert (And phi psi) used)
                , subForest = [seq']}
          | seq' <- subTrees
          , let ((l,r), _, used) = rootLabel seq'
          , phi <- S.toList l
          , psi <- S.toList l
          , let l' = S.insert (And phi psi) $ S.delete phi $ S.delete psi l
          , let r' = r
          , phi < psi && (S.null $ S.intersection r' l')
          ]
      (OrLeft _) ->
        case subTrees' of
          [lt, rt] ->
              [ Node{ rootLabel = ((l', r'), OrLeft $ Or phi psi
                                           , S.insert (Or phi psi) $ S.union usedL usedR)
                    , subForest = [lseq, rseq]}
              | lseq <- lt
              , rseq <- rt
              , let ((ll,rl), _, usedL) = rootLabel lseq
              , let ((lr,rr), _, usedR) = rootLabel rseq
              , phi <- S.toList ll
              , psi <- S.toList lr
              , let l' = S.insert (Or phi psi) $ S.delete phi $ S.delete psi $ S.union ll lr
              , let r' = S.union rr rl
              , phi /= psi && {-rl == rr && (S.delete phi ll) == (S.delete psi lr) &&-} (S.null $ S.intersection l' r')
              ]
          _ -> []
      (ImplicationLeft _) ->
        case subTrees' of
          [lt, rt] ->
              [ Node{ rootLabel = ((l', r'), ImplicationLeft $ Implication phi psi
                                           , S.insert (Implication phi psi) $ S.union usedL usedR)
                    , subForest = [lseq, rseq]}
              | lseq <- lt
              , rseq <- rt
              , let ((ll,rl), _, usedL) = rootLabel lseq
              , let ((lr,rr), _, usedR) = rootLabel rseq
              , phi <- S.toList rl
              , psi <- S.toList lr
              , let l' = S.insert (Implication phi psi) $ S.delete psi $ S.union lr ll
              , let r' = S.delete phi $ S.union rl rr
              , {-(S.delete phi rl) == rr && (S.delete psi lr) == ll &&-} (S.null $ S.intersection l' r')
              ]
          _ -> []
      (ExistsLeft _ _) ->
        let subTrees = concat subTrees'
        in
        [ Node{ rootLabel = ((l',r'), ExistsLeft (Exists v $ replace t (Var v) psi) t
                                    , S.insert (Exists v $ replace t (Var v) psi) used )
              , subForest = [seq']}
        | seq' <- subTrees
        , let ((l,r), _, used) = rootLabel seq'
        , psi <- S.toList l
        , t <- S.toList $ formulaTerms psi `S.difference` (boundVars psi)
        , let ts = (terms $ S.union (S.delete psi l) r)
        , let v = head $ dropWhile (\s -> Var s `S.member` (formulaTerms psi)) $ ["x", "y", "z", "a", "b", "c", "d"] ++ ["x_" ++ show i | i <- [0..]]
        , let l' = S.insert (Exists v $ replace t (Var v) psi) $ S.delete psi $ l
        , let r' = r
        , (not $ t `S.member` ts) && (S.null $ S.intersection l' r')
        ]
      (ForallLeft _ _) ->
        let subTrees = concat subTrees'
        in
        [ Node{ rootLabel = ((l',r'), ForallLeft (Forall v $ replace t (Var v) psi) t
                                    , S.insert (Forall v $ replace t (Var v) psi) used)
              , subForest = [seq']}
        | seq' <- subTrees
        , let ((l,r), _, used) = rootLabel seq'
        , psi <- S.toList l
        , t <- S.toList $ formulaTerms psi `S.difference` (boundVars psi)
        , let ts = (terms $ S.union l r)
        , let v = head $ dropWhile (\s -> Var s `S.member` (formulaTerms psi)) $ ["x", "y", "z", "a", "b", "c", "d"] ++ ["x_" ++ show i | i <- [0..]]
        , let l' = S.insert (Forall v $ replace t (Var v) psi) $ S.delete psi $ l
        , let r' = r
        , (S.null $ S.intersection l' r')
        ]
      (NotRight _) ->
        let subTrees = concat subTrees'
        in
          [ Node{ rootLabel = ((l', r'), NotRight $ Not phi, S.insert (Not phi) used)
                , subForest = [seq']}
          | seq' <- subTrees
          , let ((l,r), _, used) = rootLabel seq'
          , phi <- S.toList l
          , let l' = S.delete phi l
          , let r' = S.insert (Not phi) r
          , (S.null $ S.intersection l' r')
          ]
       -- SubstitutionLeft FOFormula FOFormula Term Term
       -- SubstitutionRight FOFormula FOFormula Term Term
       -- EqualityLeft FOFormula Term Term
      (AndRight _) ->
          case subTrees' of
            [lt, rt] ->
                [ Node{ rootLabel = ((l', r'), AndRight $ And phi psi
                                             , S.insert (And phi psi) $ S.union usedL usedR )
                      , subForest = [lseq, rseq]}
                | lseq <- lt
                , rseq <- rt
                , let ((ll,rl), _, usedL) = rootLabel lseq
                , let ((lr,rr), _, usedR) = rootLabel rseq
                , phi <- S.toList ll
                , psi <- S.toList lr
                , let l' = S.union ll lr
                , let r' = S.insert (And phi psi) $ S.delete phi $ S.delete psi $ S.union rr rl
                , phi /= psi && {-ll == lr && (S.delete phi rl) == (S.delete psi rr) &&-} (S.null $ S.intersection l' r')
                ]
            _ -> []
      (OrRight _) ->
          let subTrees = concat subTrees'
          in
            [ Node{ rootLabel = ((l, r'), OrRight $ Or phi psi
                                        , S.insert (Or phi psi) used)
                  , subForest = [seq']}
            | seq' <- subTrees
            , let ((l,r), _, used) = rootLabel seq'
            , phi <- S.toList r
            , psi <- S.toList r
            , let l' = l
            , let r' = S.insert (Or phi psi) $ S.delete phi $ S.delete psi r
            , phi < psi && (S.null $ S.intersection l' r')
            ]
      (ImplicationRight _) ->
          let subTrees = concat subTrees'
          in
            [ Node{ rootLabel = ((l', r'), ImplicationRight $ Implication phi psi
                                         , S.insert (Implication phi psi) used)
                  , subForest = [seq']}
            | seq' <- subTrees
            , let ((l,r), _, used) = rootLabel seq'
            , phi <- S.toList l
            , psi <- S.toList r
            , let l' = S.delete phi l
            , let r' = S.insert (Implication phi psi) $ S.delete psi r
            , (S.null $ S.intersection l' r')
            ]
      (ExistsRight _ _) ->
          let subTrees = concat subTrees'
          in
          [ Node{ rootLabel = ((l', r'), ExistsRight (Exists v $ replace t (Var v) psi) t
                                       , S.insert (Exists v $ replace t (Var v) psi) used)
                , subForest = [seq']}
          | seq' <- subTrees
          , let ((l,r), _, used) = rootLabel seq'
          , psi <- S.toList r
          , t <- S.toList $ formulaTerms psi `S.difference` (boundVars psi)
          , let v = head $ dropWhile (\s -> Var s `S.member` (formulaTerms psi)) $ ["x", "y", "z", "a", "b", "c", "d"] ++ ["x_" ++ show i | i <- [0..]]
          , let l' = l
          , let r' = S.insert (Exists v $ replace t (Var v) psi) $ S.delete psi $ r
          , (S.null $ S.intersection l' r')
          ]
      (ForallRight _ _) ->
          let subTrees = concat subTrees'
          in
          [ Node{ rootLabel = ((l', r'), ForallRight (Forall v (replace t (Var v) psi)) t
                                       , S.insert (Forall v (replace t (Var v) psi)) used)
                , subForest = [seq']}
          | seq' <- subTrees
          , let ((l,r), _, used) = rootLabel seq'
          , psi <- S.toList r
          , t <- S.toList $ formulaTerms psi `S.difference` (boundVars psi)
          , let ts = (terms $ S.union l $ S.delete psi r) `S.difference` (boundVars psi)
          , let v = head $ dropWhile (\s -> Var s `S.member` (formulaTerms psi)) $ ["x", "y", "z", "a", "b", "c", "d"] ++ ["x_" ++ show i | i <- [0..]]
          , let l' = l
          , let r' = S.insert (Forall v $ replace t (Var v) psi) $ S.delete psi $ r
          , (not $ t `S.member` ts) && (S.null $ S.intersection l' r')
          ]

axioms baseFormulas = 
  [ (phi, delta)
  | phi <- subsets baseFormulas
  , delta <- subsets baseFormulas
  , not $ S.null $ phi `S.intersection` delta
  ]

prettySequentCalculus i n@Node{rootLabel = ((l,r), rule)} = 
  (take i $ repeat '.') ++ "       " ++ (intercalate "," $ map show $ S.toList l) ++ " => " ++ (intercalate "," $ map show $ S.toList r) ++ "\n" ++
  (take i $ repeat '.') ++ show rule ++ "----\n" ++ (concatMap (prettySequentCalculus (if length (subForest n) > 1 then i+2 else i)) $ subForest n)

ruleToLatex (NotLeft _)             = "\\left( \\neg \\Rightarrow \\right)" 
ruleToLatex (AndLeft _)             = "\\left( \\land \\Rightarrow \\right)" 
ruleToLatex (OrLeft _)              = "\\left( \\lor \\Rightarrow \\right)" 
ruleToLatex (ImplicationLeft _)     = "\\left( \\rightarrow \\Rightarrow \\right)" 
ruleToLatex (ExistsLeft _ _)          = "\\left( \\exists \\Rightarrow \\right)" 
ruleToLatex (ForallLeft _ _)          = "\\left( \\forall \\Rightarrow \\right)" 
ruleToLatex (SubstitutionLeft _ _ _ _)  = "\\left( S \\Rightarrow \\right)" 
ruleToLatex (EqualityLeft _ _ _)        = "\\left( = \\Rightarrow \\right)" 
ruleToLatex (NotRight _)            = "\\left( \\Rightarrow \\neg \\right)" 
ruleToLatex (AndRight _)            = "\\left( \\Rightarrow \\land  \\right)" 
ruleToLatex (OrRight _)             = "\\left( \\Rightarrow \\lor  \\right)" 
ruleToLatex (ImplicationRight _)    = "\\left( \\Rightarrow \\rightarrow  \\right)" 
ruleToLatex (ExistsRight _ _)         = "\\left( \\Rightarrow \\exists  \\right)" 
ruleToLatex (ForallRight _ _)         = "\\left( \\Rightarrow \\forall  \\right)" 
ruleToLatex (SubstitutionRight _ _ _ _) = "\\left( \\Rightarrow S  \\right)" 

numberOfPremises Axiom = 0
numberOfPremises (ImplicationLeft _) = 2
numberOfPremises (OrLeft _) = 2
numberOfPremises (AndRight _) = 2
numberOfPremises _ = 1

sequentToLatex (l,r) = (intercalate ", " $ map show $ S.toList l)  ++" \\Rightarrow " ++ (intercalate "," $ map show $ S.toList r)

sequentCalculusToLatex n = intercalate "\n" $ reverse $ sequentCalculusToLatex' n

sequentCalculusToLatex' n@(Node{rootLabel = ((l,r), rule)}) = 
  (case numberOfPremises rule of
    0 ->
      "\\hypo{\\overline{" ++ sequentToLatex (l,r)  ++ "\\overset{\\phantom{.}}{ }}}"
    1 -> 
      "\\infer[left label=$" ++ ruleToLatex rule ++ "$]1{" ++ 
        sequentToLatex (l,r) ++ "} "
    2 -> 
      "\\infer[left label=$" ++ ruleToLatex rule ++ "$]2{" ++ 
        sequentToLatex (l,r) ++ "} "
  ) : (concatMap sequentCalculusToLatex' $ subForest n)

boundVars (Exists v psi) = S.insert (Var v) $ boundVars psi
boundVars (Forall v psi) = S.insert (Var v) $ boundVars psi
boundVars (Not phi) = boundVars phi
boundVars (And phi psi) = S.union (boundVars phi) (boundVars psi)
boundVars (Or phi psi) = S.union (boundVars phi) (boundVars psi)
boundVars (Implication phi psi) = S.union (boundVars phi) (boundVars psi)
boundVars (Biimplication phi psi) = S.union (boundVars phi) (boundVars psi)
boundVars (Predicate r ts) = S.empty

terms foSet = foldr S.union S.empty $ map formulaTerms $ S.toList foSet

formulaTerms (Not phi)      = formulaTerms phi
formulaTerms (And phi psi)  = (formulaTerms phi) `S.union` (formulaTerms psi)
formulaTerms (Or phi psi)   = (formulaTerms phi) `S.union` (formulaTerms psi)
formulaTerms (Implication phi psi) = (formulaTerms phi) `S.union` (formulaTerms psi)
formulaTerms (Biimplication phi psi) = (formulaTerms phi) `S.union` (formulaTerms psi)
formulaTerms (Exists v phi) = formulaTerms phi
formulaTerms (Forall v phi) = formulaTerms phi
formulaTerms (Predicate r t) = foldr S.union S.empty $ map termTerms t

termTerms (Var s) = S.singleton $ Var s
termTerms (Function f ts) = foldr S.union S.empty $ map termTerms ts

replace x x' (Not phi) = Not (replace x x' phi)
replace x x' (And phi psi) = And (replace x x' phi) (replace x x' psi)
replace x x' (Or phi psi) = Or (replace x x' phi) (replace x x' psi)
replace x x' (Implication phi psi) = Implication (replace x x' phi) (replace x x' psi)
replace x x' (Biimplication phi psi) = Biimplication (replace x x' phi) (replace x x' psi)
replace x x' (Forall y phi) = Forall y (replace x x' phi)
replace x x' (Exists y phi) = Exists y (replace x x' phi)
replace x x' (Predicate p ts) = Predicate p (map (replaceTerm x x') ts)

replaceTerm x x' t@(Var v)
  | x == t = x'
  | otherwise = t
replaceTerm x x' t@(Function f ts)
  | x == t = x'
  | otherwise = Function f $ map (replaceTerm x x') ts

rollRange mn mx = do
  gen <- get
  let (x, gen') = randomR (mn,mx) gen
  put gen'
  return x

choose n 0 = return []
choose n k = do
  i <- rollRange 0 (length n - 1)
  let x = n !! i
  liftM (x : ) (choose (delete x n) (k-1))

subsets [] = [S.empty]
subsets (x:xs) = 
  let rs = subsets xs
  in rs ++ map (S.insert x) rs
