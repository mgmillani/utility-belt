" move cursor over wrapped lines
noremap <UP> gk
noremap <DOWN> gj
inoremap <UP> <C-o>gk
inoremap <DOWN> <C-o>gj
" Build commands
function SmartMake ()
	if filereadable(".vim.ninja")
		AsyncRun ninja -f ".vim.ninja"
	else
		AsyncRun make
	endif
endfunction
map <F9> :w<ENTER>:call SmartMake()<ENTER>
" Spell checking
map <F2> :set spell spelllang=en_us<enter>
map <F3> :set spell spelllang=de<enter>
map <F4> :set spell<Enter>:set spelllang=pt<Enter>
sunmap <F2>
sunmap <F3>
sunmap <F4>
