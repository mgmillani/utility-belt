let b:spreadsheet_script = bufname("%")".r"
let s:spreadsheet_result = tempname()
let b:spreadsheet_result = s:spreadsheet_result

function ProcessSheet(script, sheet, resultfile)
	exe "silent !Rscript " a:script a:sheet " | column -s '|' -o '|' -t --table-right \"\" > " a:resultfile
	checktime
endfunction

function AlignColumns()
	let s:previous_line = line('.')
	let s:previous_column = col('.')

endfunction

wincmd n
let b:spreadsheet_result = s:spreadsheet_result
exe "e " b:spreadsheet_result
set autoread
wincmd j
autocmd BufWritePost <buffer> call ProcessSheet(b:spreadsheet_script, bufname("%"), b:spreadsheet_result)
" autocmd InsertLeave <buffer>
