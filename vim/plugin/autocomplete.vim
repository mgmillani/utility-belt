"function! OpenCompletion()
"    if !pumvisible() && (v:char != ' ' && v:char != '\t')
"        call feedkeys("\<C-n>", \"i\")
"    endif
"endfunction

" autocmd InsertCharPre <buffer> call OpenCompletion()

function! TabCompletion()
	let Char = getline('.')[col('.')-2]
	if pumvisible()
		return "\<C-Y>"
	elseif Char != ' ' && Char != '\t' && Char != ""
		return "\<C-n>"
	else
		return "\<TAB>"
	endif
endfunction

inoremap <buffer> <Tab> <C-R>=TabCompletion()<CR>
