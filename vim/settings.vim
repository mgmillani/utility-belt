set shiftwidth=2
set tabstop=2
set textwidth=0
set ts=2 "tab size"
set clipboard=unnamedplus
set linebreak
set autochdir
set autoindent
set wrap
set wildmode=longest,list,full
set nolist
set backspace=indent,eol,start  " more powerful backspacing
set mouse=a
syntax on
filetype plugin on
