" =======
" Haskell
" =======
" Use spaces
autocmd BufRead,BufNewFile *.hs set expandtab

" ===========
" LaTeX StUfF
" ===========
autocmd BufRead,BufNewFile *.tex,*.sty let g:tex_flavor = "latex" 
autocmd BufRead,BufNewFile *.tex,*.sty set iskeyword+=@,\
autocmd BufRead,BufNewFile *.tex,*.sty runtime OPT autocomplete.vim
