#!/usr/bin/env fish

if test -e pdf-style.css
	set Path (realpath pdf-style.css)
	set PDFOpt --pdf-engine-opt='-s'$Path
else
	set PDFOpt
end

set Target $argv[1]
set Dir (dirname $Target)
set Destination pdf/$Dir
mkdir -p $Destination
set RealDestination (realpath $Destination)

if test -e $Dir
	cd $Dir
	set PDF (basename $Target .wiki).pdf
	pandoc -fvimwiki -tpdf --pdf-engine=weasyprint $PDFOpt (basename $Target .wiki).wiki -o $PDF
	if test $status -eq 0
		mv $PDF $RealDestination/$PDF
		echo "PDF written to $Destination/$PDF".
	else
		echo "PDF generation failed."
	end
else
	echo "$Dir" not found.
	exit 1
end

