#!/usr/bin/fish

set interval $argv[1]
set repos $argv[2..-1]
set dir (pwd)

while true
	for repo in $repos
		cd $dir
		cd $repo
		set old_commit (git log -n 1 | grep commit | cut -d' ' -f2 )
		set old_date (git log -n 1 | grep date | cut -d' ' -f1 --complement)
		git fetch
		set newer
	end	
end
